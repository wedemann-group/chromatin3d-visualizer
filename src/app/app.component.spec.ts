/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { WorkspaceService } from './core/services/workspace.service';
import { WorkspaceService as MockWorkspaceService } from './core/services/__mocks__/workspace.service';
import {
  APPLICATION_IMPORTS,
  MATERIAL_IMPORTS,
} from './core/services/__mocks__/imports';
import { DockableWorkspaceModule } from './modules/dockable-workspace/dockable-workspace.module';
import { CoreModule } from './core/core.module';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader';

describe('AppComponent', () => {
  beforeEach(async () => {
    spyOn(FontLoader.prototype, 'load');
    await TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [
        ...MATERIAL_IMPORTS,
        ...APPLICATION_IMPORTS,
        DockableWorkspaceModule,
        CoreModule,
        HttpClientModule,
        RouterTestingModule,
      ],
      providers: [
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
      ],
    }).compileComponents();
    localStorage.clear();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  /*
  it(`should have as title 'trj-viewer-angular'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('trj-viewer-angular');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain(
      'trj-viewer-angular app is running!'
    );
  });
   */
});
