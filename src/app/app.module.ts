/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { DockableWorkspaceModule } from './modules/dockable-workspace/dockable-workspace.module';
import { HttpClientModule } from '@angular/common/http';
import { FirstVisitModule } from './core/components/first-visit/first-visit.module';
import { parseBoolean } from './shared/utilities/parser.utility';
import { LOCAL_STORAGE_SETTINGS_ANIMATION } from './shared/local-storage-keys.constants';
import { SettingsService } from './core/services/settings/settings.service';
import { ETheme } from './shared/models/theme.enum';

/**
 * @source https://dev.to/this-is-angular/disabling-angular-animations-at-runtime-9a6
 */
function isAnimationDisabled(): boolean {
  const mediaQueryList = window.matchMedia('(prefers-reduced-motion)');
  const isAnimationEnabled = localStorage.getItem(
    LOCAL_STORAGE_SETTINGS_ANIMATION
  );

  return (
    mediaQueryList.matches ||
    (isAnimationEnabled !== null && !parseBoolean(isAnimationEnabled))
  );
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule.withConfig({
      disableAnimations: isAnimationDisabled(),
    }),
    CoreModule,
    DockableWorkspaceModule,
    SharedModule,
    HttpClientModule,
    FirstVisitModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
  public constructor(private readonly _settingsService: SettingsService) {
    this._settingsService.theme.value$.subscribe((v) => {
      let theme = v;
      if (theme === ETheme.SYSTEM) {
        theme = window.matchMedia('(prefers-color-scheme: dark)').matches
          ? ETheme.DARK
          : ETheme.LIGHT;
      }

      const oldThemes = Array.from(document.body.classList).filter((c) =>
        c.toLowerCase().includes('theme')
      );
      if (oldThemes.length > 0) document.body.classList.remove(...oldThemes);

      if (theme === ETheme.DARK) document.body.classList.add('dark-theme');
    });
  }
}
