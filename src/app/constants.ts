/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

export const NOTIFICATION_UPDATE_TITLE = 'Automatically Updated!';
export const NOTIFICATION_UPDATE_DESCRIPTION =
  'The software was updated <strong>{0}</strong>. See <a href="{1}" class="jet-brains-link" rel="noopener" target="_blank">commit</a>.';

export const NOTIFICATION_MAXIMUM_MONTHS = 5;

export const GITLAB_DOMAIN =
  'https://gitlab.com/wedemann-group/chromatin3d-visualizer';

export const GITLAB_DOMAIN_COMMITS = `${GITLAB_DOMAIN}/-/commit`;
export const GITLAB_URL_WIKI_HOWTO = `${GITLAB_DOMAIN}/-/wikis/howto/README`;

export const GITLAB_REPO_ID_C3D_VISUALIZER = '32582741';

export const DEFAULT_LOADING_DIALOG_WIDTH = '70%';

export const BEAD_USER_NAME_HISTONE_OCTAMER = 'Histone Octamer';
export const BEAD_USER_NAME_NUCLEOSOME_DNA = 'Nucleosome DNA';
export const BEAD_USER_NAME_COHESIN_PREFIX = 'Cohesin';
export const BEAD_USER_NAME_COHESIN_LINKER = 'Linker To Head';
export const BEAD_USER_NAME_COHESIN_HEAD = 'Head';
export const BEAD_USER_NAME_COHESIN_RING = 'Ring';
export const BEAD_USER_NAME_LINKER_DNA = 'Linker DNA';

export const EPS_IS_ZERO = 0.001;

/**
 * Specifies how many decimal numbers should be displayed for the user.
 */
export const USER_FLOATING_DECIMAL_PLACES = 4;

/**
 * Specifies in which unit the values of this application.
 */
export const UNIT = 'nm';

export const BREAK_POINT_SCREEN_SMALL = 650;
export const BREAK_POINT_TAB_SMALL = 250;
