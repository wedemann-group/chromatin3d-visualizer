/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuStripComponent } from './menu-strip.component';
import { WorkspaceService } from '../../services/workspace.service';
import { WorkspaceService as MockWorkspaceService } from '../../services/__mocks__/workspace.service';
import {
  APPLICATION_IMPORTS,
  MATERIAL_IMPORTS,
} from '../../services/__mocks__/imports';
import { MenuDebugComponent } from './menu-debug/menu-debug.component';
import { MenuFileComponent } from './menu-file/menu-file.component';
import { MenuHelpComponent } from './menu-help/menu-help.component';
import { MenuHelpAboutComponent } from './menu-help/menu-help-about/menu-help-about.component';
import { MenuViewComponent } from './menu-view/menu-view.component';
import { MenuWindowComponent } from './menu-window/menu-window.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';

describe('MenuStripComponent', () => {
  let component: MenuStripComponent;
  let fixture: ComponentFixture<MenuStripComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        MenuStripComponent,
        MenuDebugComponent,
        MenuFileComponent,
        MenuHelpComponent,
        MenuHelpAboutComponent,
        MenuViewComponent,
        MenuWindowComponent,
      ],
      imports: [
        HttpClientTestingModule,
        ...MATERIAL_IMPORTS,
        ...APPLICATION_IMPORTS,
      ],
      providers: [
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
        {
          provide: ActivatedRoute,
          useValue: {},
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuStripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
