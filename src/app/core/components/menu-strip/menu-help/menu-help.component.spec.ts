/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  APPLICATION_IMPORTS,
  MATERIAL_IMPORTS,
} from '../../../services/__mocks__/imports';
import { MenuHelpComponent } from './menu-help.component';

describe('MenuHelpComponent', () => {
  let component: MenuHelpComponent;
  let fixture: ComponentFixture<MenuHelpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MenuHelpComponent],
      imports: [...MATERIAL_IMPORTS, ...APPLICATION_IMPORTS],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
