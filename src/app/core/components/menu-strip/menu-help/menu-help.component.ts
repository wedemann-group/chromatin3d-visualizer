/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MenuHelpAboutComponent } from './menu-help-about/menu-help-about.component';
import {
  BREAK_POINT_SCREEN_SMALL,
  GITLAB_URL_WIKI_HOWTO,
} from '../../../../constants';

@Component({
  selector: 'trj-menu-help',
  templateUrl: './menu-help.component.html',
  styleUrls: ['./menu-help.component.scss'],
})
export class MenuHelpComponent implements OnInit {
  public readonly INTRODUCTION_WIKI_URL = GITLAB_URL_WIKI_HOWTO;

  private _innerWidth = 0;

  constructor(private readonly _matDialog: MatDialog) {}

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    this._innerWidth = window.innerWidth;
  }

  ngOnInit(): void {
    this._innerWidth = window.innerWidth;
  }

  openAbout(): void {
    console.log(this._innerWidth);

    this._matDialog.open(MenuHelpAboutComponent, {
      maxWidth: this._innerWidth > BREAK_POINT_SCREEN_SMALL ? '40vw' : '90vw',
    });
  }
}
