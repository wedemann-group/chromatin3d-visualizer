/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component } from '@angular/core';
import { GITLAB_DOMAIN, GITLAB_DOMAIN_COMMITS } from '../../../../../constants';

declare const PACKAGE_VERSION: string;
declare const BUILD_DATE: string;
declare const LICENSE: string;
declare const COMMIT: string;

@Component({
  selector: 'trj-menu-help-about',
  templateUrl: './menu-help-about.component.html',
  styleUrls: ['./menu-help-about.component.scss'],
})
export class MenuHelpAboutComponent {
  public readonly PACKAGE_VERSION = PACKAGE_VERSION;
  public readonly BUILD_DATE = BUILD_DATE;
  public readonly LICENSE = LICENSE;
  public readonly COMMIT = COMMIT;
  public readonly GITLAB_DOMAIN_COMMITS = GITLAB_DOMAIN_COMMITS;
  public readonly GITLAB_DOMAIN = GITLAB_DOMAIN;

  public readonly AUTHORS = [
    {
      name: 'Leif-Kristof Schultz',
      website: 'https://www.lui-studio.net/',
      websiteName: "Lui's Studio",
    },
    {
      name: 'Frederic Bauer',
    },
    {
      name: 'Janne Wernecken',
    },
  ];
}
