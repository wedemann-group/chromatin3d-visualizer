/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component } from '@angular/core';
import { MenuStripService } from '../../../services/menu-strip.service';
import { WorkspaceService } from '../../../services/workspace.service';

@Component({
  selector: 'trj-menu-view',
  templateUrl: './menu-view.component.html',
  styleUrls: ['./menu-view.component.scss'],
})
export class MenuViewComponent {
  constructor(
    private readonly _menuStripService: MenuStripService,
    public readonly workspaceService: WorkspaceService
  ) {}

  makeScreenshot(): void {
    this._menuStripService.makeScreen$.next(null);
  }

  resetLayout(): void {
    this._menuStripService.resetLayout$.next(null);
  }

  resetCamera(): void {
    this._menuStripService.resetCamera$.next(null);
  }
}
