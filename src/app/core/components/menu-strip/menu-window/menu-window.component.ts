/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { delay } from 'rxjs/operators';
import { ComponentItem, DragSource } from 'golden-layout';
import {
  COMPONENTS,
  DockingSuiteHelperService,
} from '../../../services/docking-suite-helper/docking-suite-helper.service';
import { MatMenu } from '@angular/material/menu';
import { RestoreTab } from '../../../services/docking-suite-helper/models/restore-tab.model';
import { DebugService } from '../../../services/debug.service';
import { SubscribeEvents } from '../../../classes/subscribe-events';

const DIVIDER_INDICES = [5, 8, 12];

type RestoreTabComponentWithOpenState = RestoreTab & {
  isOpen: boolean;
};

/**
 * Attention this is basically unclean, however the functionality is so cool that you can look over this "uncleanliness".
 * The two defined functions actually exist, but they are private. However, since JavaScript (the compiled TypeScript)
 * does not support OOP, you can work around the visibility.
 *
 * What are the two functions for?
 * GoldenLayout supports drag and drop of components. However, we want to use this feature only if there is no tab of the same type.
 * Creating a DragSource will in turn create the component, which will NOT be destroyed on "destroy" in the Angular lifecycle.
 * So the end result is hundreds of "viewer"s and thus hundreds of times rendering.
 * To avoid this problem, simply delete the listeners or add them again when the functionality is needed (or not).
 *
 * @source https://github.com/golden-layout/golden-layout/blob/v2.0.2/src/ts/layout-manager.ts#L997
 * @source https://github.com/golden-layout/golden-layout/blob/v2.0.2/src/ts/controls/drag-source.ts#L47
 */
type HackyDragSource = DragSource & {
  removeDragListener(): void;
  createDragListener(): void;
};

type HackyMenuItem = {
  _elementRef: ElementRef<HTMLButtonElement>;
};

@Component({
  selector: 'trj-menu-window',
  templateUrl: './menu-window.component.html',
  styleUrls: ['./menu-window.component.scss'],
})
export class MenuWindowComponent
  extends SubscribeEvents
  implements OnInit, AfterViewInit, OnDestroy
{
  public tabs: (RestoreTabComponentWithOpenState | undefined)[] = [];

  @ViewChild('menuWindow')
  private _matMenu?: MatMenu;

  private _dragSources: {
    dragSource: HackyDragSource;
    component: string;
  }[] = [];

  constructor(
    private readonly _dockingSuiteHelper: DockingSuiteHelperService,
    private readonly _debugService: DebugService
  ) {
    super();

    this.subscribes.push(
      // delay fixes NG0100: ExpressionChangedAfterItHasBeenCheckedError
      this._dockingSuiteHelper.tabs$.pipe(delay(0)).subscribe((tabs) => {
        this.setOpenStates(tabs);
      }),
      this._dockingSuiteHelper.layout$.pipe(delay(0)).subscribe(() => {
        this.setDragSources();
      })
    );
  }

  menuOpen(): void {
    this.tabs = [...COMPONENTS].map(
      (c) => ({ ...c, isOpen: false } as RestoreTabComponentWithOpenState)
    );

    const dividers = [...DIVIDER_INDICES].sort((d1, d2) => d2 - d1);
    dividers.forEach((dividerIndex) =>
      this.tabs.splice(dividerIndex, 0, undefined)
    );
    this.tabs = this.tabs.filter(
      (t) =>
        !t ||
        !t.onlyInDebugMode ||
        (t.onlyInDebugMode &&
          this._debugService.debugMode === t?.onlyInDebugMode)
    );

    if (this._debugService.debugMode) {
      const firstDebugElement = this.tabs.findIndex((t) => t?.onlyInDebugMode);
      if (firstDebugElement > 0)
        // ignore 0
        this.tabs.splice(firstDebugElement, 0, undefined);
    }

    this.disposeDragSources();
    this.setOpenStates(this._dockingSuiteHelper.tabs);
  }

  toggleTab(event: Event, tab: RestoreTab): void {
    event.stopPropagation();

    const tabComponent = this._dockingSuiteHelper.tabs.find(
      (t) => t.componentType === tab.jsonName
    );
    if (tabComponent) {
      tabComponent.close();
    } else {
      this._dockingSuiteHelper.layout?.addComponent(
        tab.jsonName,
        undefined,
        tab.displayName
      );
    }
  }

  ngOnInit(): void {
    this.setOpenStates(this._dockingSuiteHelper.tabs);
  }

  ngAfterViewInit(): void {
    if (!this._matMenu) return;

    this.subscribes.push(
      this._matMenu._allItems.changes.subscribe(() => {
        this.setDragSources();
      })
    );
    this.setDragSources();
  }

  setDragSources(): void {
    if (!this._matMenu) return;

    this.disposeDragSources();

    const possibleDragSources: HTMLButtonElement[] = [];
    for (const tab of this._matMenu._allItems as unknown as HackyMenuItem[]) {
      const domElement = tab._elementRef?.nativeElement;

      if (
        domElement &&
        this.tabs.find((t) => t && t.jsonName === domElement.name)
      )
        possibleDragSources.push(domElement);
    }

    for (const tab of this.tabs.filter((t) => !!t) as RestoreTab[]) {
      const domElement = possibleDragSources.find(
        (t) => t.name === tab.jsonName
      );
      if (!domElement) continue;

      this.setDragSourceFromMenuDOMElement(tab, domElement);
    }

    this.setOpenStates(this._dockingSuiteHelper.tabs);
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  private setDragSource(tab: RestoreTab, host: HTMLElement): void {
    if (!this._dockingSuiteHelper.layout) return;

    const source = this._dockingSuiteHelper.layout.newDragSource(
      host,
      tab.jsonName,
      undefined,
      tab.displayName
    ) as HackyDragSource;
    if (!source) return;

    this._dragSources.push({
      dragSource: source,
      component: tab.jsonName,
    });
  }

  private disposeDragSources(): void {
    if (!this._dockingSuiteHelper.layout) return;

    for (const dragSource of this._dragSources) {
      try {
        this._dockingSuiteHelper.layout.removeDragSource(dragSource.dragSource);
      } catch (e) {
        console.warn('Error to dispose tab drag source');
      }
    }

    this._dragSources = [];
  }

  private setOpenStates(tabs: ComponentItem[]): void {
    for (const tab of <RestoreTabComponentWithOpenState[]>(
      this.tabs.filter((t) => !!t)
    )) {
      tab.isOpen = !!tabs.find((t) => t.componentType === tab.jsonName);

      const source = this._dragSources.find(
        (ds) => ds.component === tab.jsonName
      );

      if (source && tab.isOpen) {
        source.dragSource.removeDragListener();
      } else if (source) {
        source.dragSource.createDragListener();
      }
    }
  }

  private setDragSourceFromMenuDOMElement(
    tab: RestoreTab,
    domElement: HTMLElement
  ): void {
    this.setDragSource(tab, domElement);

    // now: angular > 14 stuff:
    // angular splits into separate dom elements, so it necessary that the button holder (span element) and the text
    // element (the child of the button) are register as drag source, so that the user can drag from the complete
    // menu item

    // register holder
    if (domElement.parentElement)
      this.setDragSource(tab, domElement.parentElement);

    // register child
    const textElement = domElement.querySelector('* > span');
    if (textElement instanceof HTMLElement)
      this.setDragSource(tab, textElement);
  }
}
