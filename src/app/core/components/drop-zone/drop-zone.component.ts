/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgxDropzoneChangeEvent, NgxDropzoneComponent } from 'ngx-dropzone';
import { filter } from 'rxjs/operators';
import { WorkspaceService } from '../../services/workspace.service';
import { MenuStripService } from '../../services/menu-strip.service';
import { ZipExplorerComponent } from '../../../shared/components/zip-explorer/zip-explorer.component';
import {
  isArchive,
  isDraggableFile,
} from '../../../shared/utilities/file.utility';
import { ZipExplorerResult } from '../../../shared/components/zip-explorer/models/zip-explorer-result.model';
import { Session } from '../../services/session-mananger/models/session.model';
import { SubscribeEvents } from '../../classes/subscribe-events';
import { DEFAULT_LOADING_DIALOG_WIDTH } from '../../../constants';
import { DownloadService } from '../../services/download.service';
import { EDownloadParams } from '../../services/models/download-params.enum';

/**
 * SUPPORTED_MIME_TYPES doesn't work because "trj" files are an empty type and empty type is doesn't support.
 */

@Component({
  selector: 'trj-drop-zone',
  templateUrl: './drop-zone.component.html',
  styleUrls: ['./drop-zone.component.scss'],
})
export class DropZoneComponent
  extends SubscribeEvents
  implements AfterViewInit, OnDestroy
{
  @ViewChild('drop')
  public dropElement: NgxDropzoneComponent | undefined;

  private _dialogRef: MatDialogRef<any> | undefined;

  constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _msService: MenuStripService,
    private readonly _dialog: MatDialog,
    private readonly _downloadService: DownloadService,
    private readonly _router: Router
  ) {
    super();

    this.subscribes.push(
      this._downloadService.file$
        .pipe(filter((f) => !f.isTableOfContentFile))
        .subscribe((f) =>
          this.handleFile(f.file, undefined, f.triggeredFileNames, 1)
        )
    );
  }

  ngAfterViewInit(): void {
    this.registerMenuStripListener();
  }

  async onSelect(event: NgxDropzoneChangeEvent): Promise<void> {
    if (!event.addedFiles || event.addedFiles.length === 0) return;

    const possibleFiles = event.addedFiles.filter((f) =>
      isDraggableFile(f.name)
    );
    if (possibleFiles.length === 0) return;

    await this.clearQueryParameters();
    this.handleFile(possibleFiles[0]);
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  private handleFile(
    file: File,
    sessionString: string | undefined = undefined,
    triggeredFiles: string[] = [],
    skipTriggeredFileCounter = 0
  ): void {
    const isArchiveToRead = isArchive(file.name);

    if (isArchiveToRead) {
      this.createZipArchiveDialog(
        file,
        triggeredFiles,
        skipTriggeredFileCounter
      );
    } else {
      let session: Session | undefined;
      if (sessionString) {
        try {
          session = JSON.parse(sessionString);
        } catch (e) {
          console.warn(e);
        }
      }

      this._workspaceService.loadFile(file, session, triggeredFiles);
    }
  }

  private registerMenuStripListener(): void {
    this.subscribes.push(
      this._msService.openFileDialog$.subscribe(() => {
        this.dropElement?.showFileSelector();
      })
    );
  }

  private closeDialog(): void {
    if (this._dialogRef) {
      this._dialogRef.close();
      this._dialogRef = undefined;
    }
  }

  private createZipArchiveDialog(
    file: File,
    triggeredFiles: string[],
    skipTriggeredFileCounter: number
  ): void {
    this.closeDialog();

    if (!this._dialogRef) {
      this._dialogRef = this._dialog.open(ZipExplorerComponent, {
        width: DEFAULT_LOADING_DIALOG_WIDTH,
        disableClose: true,
        data: {
          file,
        },
      });

      this._dialogRef
        .afterClosed()
        .subscribe((data: ZipExplorerResult | undefined) => {
          this._dialogRef = undefined;

          if (!data || !(data.trjFile instanceof File)) return;

          if (skipTriggeredFileCounter-- <= 0) triggeredFiles.push(file.name);

          this.handleFile(
            data.trjFile,
            data.configContent,
            triggeredFiles,
            skipTriggeredFileCounter
          );
        });
    }
  }

  private async clearQueryParameters(): Promise<void> {
    const result: { [key: string]: null } = {};
    for (const key of Object.values(EDownloadParams)) {
      result[key] = null;
    }

    await this._router.navigate([], {
      queryParams: result,
      queryParamsHandling: 'merge',
    });
  }
}
