/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialogTitle,
} from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { MATERIAL_IMPORTS } from '../../services/__mocks__/imports';
import { TrajectorySelectorComponent } from './trajectory-selector.component';
import createSpy = jasmine.createSpy;
import Spy = jasmine.Spy;
import { FileListComponent } from './file-list/file-list.component';
import PaperHelper from '../../../shared/models/paper-helper.model';

describe('TrajectorySelectorComponent', () => {
  let component: TrajectorySelectorComponent;
  let fixture: ComponentFixture<TrajectorySelectorComponent>;
  let dialogRef: { close: Spy<() => void> };

  beforeEach(async () => {
    dialogRef = { close: createSpy('close') };
    await TestBed.configureTestingModule({
      declarations: [FileListComponent, TrajectorySelectorComponent],
      imports: [...MATERIAL_IMPORTS, RouterTestingModule],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            paper: { title: 'Luigi', authors: ['Lui'] },
            files: [{ path: 'test.test', name: 'test' }],
          } as PaperHelper,
        },
        {
          provide: MatDialogRef,
          useValue: dialogRef,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrajectorySelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select a file', () => {
    const selectableElement = fixture.debugElement.query(
      By.css('trj-file-list')
    );

    selectableElement.triggerEventHandler(
      'selectedFile',
      component.paper.files[0]
    );
    expect(component.selectedFile!.path).toEqual('test.test');
  });

  it('should close the dialog', fakeAsync(() => {
    const selectableElement = fixture.debugElement.query(
      By.directive(FileListComponent)
    );

    selectableElement.triggerEventHandler(
      'selectedFile',
      component.paper.files[0]
    );

    const closeButton = fixture.debugElement.query(
      By.css('mat-dialog-actions > button')
    );
    closeButton.triggerEventHandler('click', null);
    tick();
    expect(dialogRef.close).toHaveBeenCalled();
  }));

  it('should contain the paper name as title', () => {
    const title = fixture.debugElement.query(By.directive(MatDialogTitle));

    expect(title).toBeDefined();
    expect(title.nativeElement.innerText).toContain('Luigi');
  });

  it('should the authors of the paper', () => {
    const title = fixture.debugElement.query(By.css('.authors'));

    expect(title).toBeDefined();
    expect(title.nativeElement.innerText).toContain('Lui');
  });

  it('should cut of the author list', () => {
    component.paper.paper.authors = new Array(500).fill(0).map((_) => 'Name');
    fixture.detectChanges();

    const authors = fixture.debugElement.queryAll(By.css('.authors > *'));
    expect(authors.length).toBeLessThan(100);
  });

  it('should display "..." by too many authors', () => {
    component.paper.paper.authors = new Array(500).fill(0).map((_) => 'Name');
    fixture.detectChanges();

    const authors = fixture.debugElement.queryAll(By.css('.authors > *'));
    expect(authors[authors.length - 1].nativeElement.innerText).toContain(
      '...'
    );
  });

  it('should NOT display any information', () => {
    component.paper.paper.authors = undefined;
    component.paper.paper.doi = undefined;
    fixture.detectChanges();

    const paperInfo = fixture.debugElement.query(By.css('.paper-info'));
    expect(paperInfo).toBeNull();
  });
});
