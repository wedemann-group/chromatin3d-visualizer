/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PaperTrajectoryFile } from '../../../../shared/models/paper-trajectory-file.model';
import { getFileExtension } from '../../../../shared/utilities/file.utility';
import { ARCHIVE_EXTENSIONS } from '../../../../shared/supported-files.constants';

@Component({
  selector: 'trj-file-list[files]',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.scss'],
})
export class FileListComponent {
  @Input()
  public files: PaperTrajectoryFile[] = [];

  @Output()
  public readonly selectedFile = new EventEmitter<
    PaperTrajectoryFile | undefined
  >();

  @Output()
  public readonly submitFile = new EventEmitter<PaperTrajectoryFile>();

  isZip(path: string): boolean {
    return ARCHIVE_EXTENSIONS.includes(getFileExtension(path).toLowerCase());
  }
}
