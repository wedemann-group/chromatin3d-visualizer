/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MatListOption } from '@angular/material/list';
import { MATERIAL_IMPORTS } from '../../../services/__mocks__/imports';
import { FileListComponent } from './file-list.component';
import { PaperTrajectoryFile } from '../../../../shared/models/paper-trajectory-file.model';

describe('FileListComponent', () => {
  let component: FileListComponent;
  let fixture: ComponentFixture<FileListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FileListComponent],
      imports: [...MATERIAL_IMPORTS],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display one file', () => {
    component.files = [
      { name: 'that is a luihafte file', path: 'https://lui-studio.net/' },
    ];
    fixture.detectChanges();
    expect(
      fixture.debugElement.queryAll(By.directive(MatListOption)).length
    ).toEqual(1);
  });

  it('should display two files', () => {
    component.files = [
      { name: 'that is a luihafte file', path: 'https://lui-studio.net/' },
      {
        name: 'that is another luihafte file',
        path: 'https://lui-studio.net/',
      },
    ];
    fixture.detectChanges();
    expect(
      fixture.debugElement.queryAll(By.directive(MatListOption)).length
    ).toEqual(2);
  });

  it('should trigger selection omitter', () => {
    component.files = [{ name: 'test', path: 'test.zip' }];
    fixture.detectChanges();

    const listOption = fixture.debugElement.query(By.directive(MatListOption));
    expect(listOption).toBeTruthy();
    listOption.nativeElement.click();

    listOption.triggerEventHandler('selectedFile', {
      path: 'test.zip',
      name: 'test',
    } as PaperTrajectoryFile);
    expect(component.selectedFile).toBeTruthy();
  });
});
