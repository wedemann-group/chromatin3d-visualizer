/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { ExchangeError } from '../../shared/web-workers/models/exchange-error';
import { SessionManagerService } from './session-mananger/session-manager.service';
import { Session } from './session-mananger/models/session.model';
import { ExchangeFormat } from '../../shared/web-workers/models/exchange-format.model';
import { ReadProgress } from '../../shared/web-workers/models/read-progress.model';
import { getFileSizeString } from '../../shared/utilities/file.utility';
import {
  EXCHANGE_CONFIGURATION,
  EXCHANGE_ERROR,
  EXCHANGE_LOAD_TRAJECTORY,
  EXCHANGE_PARTIAL_TRAJECTORY,
  EXCHANGE_READING_PROGRESS,
} from '../../shared/web-workers/constants';
import { Configuration } from '../../shared/models/configuration.model';
import PartialTrajectory from '../../shared/parsers/models/partial-trajectory.model';
import { ParsedConfiguration } from '../../shared/parsers/models/parsed-configuration.model';
import { ExchangeConfiguration } from '../../shared/web-workers/models/exchange-configuration.model';
import { ProgressStateHandlerService } from './progress-state-handler.service';

/**
 * Specifies how many beads can be cached at runtime simultaneously without
 * deleting the configurations.
 * Otherwise, too many configurations or too many beads can lead to an
 * out-of-memory error. For this reason, the number of beads that can be cached
 * must be limited. A query of the memory capacity is not possible or not in all
 * browsers.
 */
const MAXIMAL_CACHED_BEADS = 75000;

@Injectable({
  providedIn: 'root',
})
export class WorkspaceService implements OnDestroy {
  private readonly _trajectory$$ =
    new BehaviorSubject<PartialTrajectory | null>(null);
  private readonly _config$$ = new BehaviorSubject<ParsedConfiguration | null>(
    null
  ); // null => NO CONFIGURATION
  private readonly _beforeSetTrajectory$$ = new Subject<Session | undefined>();
  private readonly _beforeSetConfig$$ =
    new Subject<ParsedConfiguration | null>();
  private readonly _selectionTexts$$ = new BehaviorSubject<string[]>([]);
  private readonly _triggeredFiles$$ = new BehaviorSubject<string[]>([]);

  public readonly trajectory$ = this._trajectory$$.asObservable();
  public readonly config$ = this._config$$.asObservable();
  public readonly beforeSetTrajectory$ =
    this._beforeSetTrajectory$$.asObservable();
  public readonly beforeSetConfig$ = this._beforeSetConfig$$.asObservable();
  public readonly selectionTexts$ = this._selectionTexts$$.asObservable();
  public readonly triggeredFiles$ = this._triggeredFiles$$.asObservable();

  private _currentFile: File | undefined;

  private _worker: Worker | undefined;
  private _session: Session | undefined;
  private _isViewerActive = false;
  private _cachedConfigs: { [key: number]: Configuration } = {};
  private _isLoading = false;

  public get currentFile(): File | undefined {
    return this._currentFile;
  }

  public set isViewerActive(val: boolean) {
    this._isViewerActive = val;
  }

  public get trajectory(): PartialTrajectory | null {
    return this._trajectory$$.getValue();
  }

  public get renderCount(): number {
    return this._trajectory$$.value?.maxBeadLength ?? -1;
  }

  public get config(): ParsedConfiguration | null {
    return this._config$$.value;
  }

  public get isLoading(): boolean {
    return this._isLoading;
  }

  public get selectionTexts(): string[] {
    return this._selectionTexts$$.value;
  }

  public set selectionTexts(val: string[]) {
    this._selectionTexts$$.next(val);
  }

  public get triggeredFiles(): string[] {
    return this._triggeredFiles$$.value;
  }

  public constructor(
    private readonly _sessionManager: SessionManagerService,
    private readonly _progressStateHandlerService: ProgressStateHandlerService
  ) {
    this.init();

    this._sessionManager.getCurrentSession.push(() => ({
      configIndex:
        this._config$$.value && this._config$$.value.index >= 0
          ? this._config$$.value.index
          : 0,
    }));
  }

  public isConfigurationLoaded(): boolean {
    return !!this._trajectory$$.value && !!this._config$$.value;
  }

  /**
   *
   * @param trj
   * @param showConfig The configuration to be displayed. By default 0. A negative number means that no configuration is displayed.
   */
  public setTrajectory(trj: PartialTrajectory | null, showConfig = 0): void {
    if (!trj) {
      this.unloadTrajectory();
    } else {
      this._cachedConfigs = {};
      this._trajectory$$.next(trj);
      this.setConfig(showConfig, true);
    }
  }

  public unloadTrajectory(): void {
    this._currentFile = undefined;
    this._session = undefined;
    this._cachedConfigs = {};
    this._beforeSetConfig$$.next(null);
    this._config$$.next(null);
    this._trajectory$$.next(null);
    this._triggeredFiles$$.next([]);
  }

  public ngOnDestroy(): void {
    if (this._worker) this._worker.terminate();
  }

  public loadFile(
    file: File,
    session: Session | undefined = undefined,
    triggeredFiles: string[] = []
  ): void {
    if (!this._worker) return;

    this._currentFile = file;
    this._isLoading = true;
    this._session = session;
    this._triggeredFiles$$.next(triggeredFiles);

    this._progressStateHandlerService.setProgressStatus({
      title: 'Reading...',
      textLeft: 'Load File from Disk...',
      textRight: '',
      value: -1,
    });

    this._worker.postMessage({
      message: EXCHANGE_LOAD_TRAJECTORY,
      data: file,
    } as ExchangeFormat<File>);
  }

  public setConfig(configIndex: number, showMessage: boolean): void {
    if (this._cachedConfigs[configIndex] !== undefined) {
      const config = {
        index: configIndex,
        config: this._cachedConfigs[configIndex],
      };

      this._beforeSetConfig$$.next(config);
      this._config$$.next(config);
      return;
    }

    this._isLoading = true;

    if (showMessage)
      this._progressStateHandlerService.setProgressStatus({
        title: 'Parsing...',
        textLeft: `Configuration ${configIndex}`,
        textRight: ``,
        value: -1,
      });

    this._worker?.postMessage({
      message: EXCHANGE_CONFIGURATION,
      data: configIndex,
    } as ExchangeFormat<number>);
  }

  private init(): void {
    if (typeof Worker !== 'undefined') {
      this._worker = new Worker(
        new URL('../../shared/web-workers/trj-parser.worker', import.meta.url),
        {
          type: 'module',
        }
      );
      this._worker.onmessage = this.handleParserWebWorkerResponse.bind(this);
    } else {
      console.error('No web workers available :(');
    }
  }

  private handleParserWebWorkerResponse({
    data,
  }: MessageEvent<
    ExchangeFormat<ReadProgress | ExchangeConfiguration | ExchangeError>
  >): void {
    this._isLoading = false;

    if (data.message === EXCHANGE_ERROR) {
      const errorMessage = (data.data as ExchangeError).errorMessage;
      console.error('Parsing failed!', errorMessage);
      this._progressStateHandlerService.setErrorMessage(
        `Error occurred while parsing. The file may be corrupted. Error message: ${errorMessage}`
      );
    } else if (data.message === EXCHANGE_READING_PROGRESS) {
      const readInfo = data.data as ReadProgress;
      this._progressStateHandlerService.setProgressStatus({
        title: 'Reading...',
        textLeft: this.currentFile?.name ?? '-',
        textRight: `${getFileSizeString(
          readInfo.current
        )} / ${getFileSizeString(readInfo.max)}`,
        value: (100 / readInfo.max) * readInfo.current,
      });
    } else if (data.message === EXCHANGE_PARTIAL_TRAJECTORY) {
      this._beforeSetTrajectory$$.next(this._session);
      this.setTrajectory(
        new PartialTrajectory(data.data as Partial<PartialTrajectory>),
        this._session &&
          this._session.configIndex &&
          this._session.configIndex >= 0
          ? this._session.configIndex
          : 0
      );
      if (this._session) this._sessionManager.loadSession(this._session);
      this._progressStateHandlerService.removeDialog();
    } else if (data.message === EXCHANGE_CONFIGURATION) {
      this._progressStateHandlerService.removeDialog();
      const configData = data.data as ExchangeConfiguration;
      const config = new Configuration(
        configData.config as Partial<Configuration>
      );

      this.clearCachedConfigurations(configData.index);
      this._cachedConfigs[configData.index] = config;
      const parsedConfig = { index: configData.index, config };
      this._beforeSetConfig$$.next(parsedConfig);
      this._config$$.next(parsedConfig);
    }
  }

  private getAmountOfCachedBeads(): number {
    let result = 0;
    for (const key of Object.keys(this._cachedConfigs).map((k) =>
      parseInt(k)
    )) {
      const config = this._cachedConfigs[key];
      result += config.beads.length;
    }

    return result;
  }

  private clearCachedConfigurations(currentIndex: number): void {
    const keys = Object.keys(this._cachedConfigs)
      .map((k) => parseInt(k))
      .map((k) => ({ key: k, dis: Math.abs(currentIndex - k) }))
      .sort((item1, item2) => item2.dis - item1.dis);

    while (
      keys.length > 0 &&
      this.getAmountOfCachedBeads() > MAXIMAL_CACHED_BEADS
    ) {
      const key = keys.splice(0, 1)[0];
      delete this._cachedConfigs[key.key];
    }
  }
}
