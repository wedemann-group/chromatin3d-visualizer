/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import {
  Camera,
  InstancedMesh,
  Intersection,
  Object3D,
  Raycaster,
  Vector2,
} from 'three';
import {
  MATERIAL_ATTRIBUTE_NAME_COLOR,
  MATERIAL_ATTRIBUTE_NAME_VIS,
} from '../../modules/dockable-workspace/viewer/beads/models.variables';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RaycastService {
  private readonly _clearSelections$$ = new Subject<null>();

  public readonly clearSelections$ = this._clearSelections$$.asObservable();
  public readonly clearDistanceMeasurementLines$$ = new Subject<null>();
  public readonly existsTempLines$$ = new BehaviorSubject<boolean>(false);

  public clearSelections(): void {
    this._clearSelections$$.next(null);
  }

  public getObjectFromLine(
    mousePosition: Vector2,
    clickableObjects: Object3D[],
    camera: Camera
  ): Intersection | null {
    const rayCaster = new Raycaster();

    rayCaster.setFromCamera(mousePosition, camera);
    const intersectedObjects = rayCaster.intersectObjects(
      clickableObjects,
      false
    );

    if (intersectedObjects.length <= 0) return null;

    for (const intersectedObject of intersectedObjects) {
      const mesh = intersectedObject.object;
      if (!mesh.visible) continue;

      // Check if instance mesh visible
      if (intersectedObject.instanceId && mesh instanceof InstancedMesh) {
        const meshGeometry = mesh.geometry;
        const visAttr = meshGeometry.getAttribute(MATERIAL_ATTRIBUTE_NAME_VIS);
        const colAttr = meshGeometry.getAttribute(
          MATERIAL_ATTRIBUTE_NAME_COLOR
        );

        const isVisible =
          colAttr.getW(intersectedObject.instanceId) > 0 ||
          visAttr.getX(intersectedObject.instanceId) != 0;

        if (!isVisible) continue;
      }

      return intersectedObject;
    }

    return null;
  }
}
