/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Animation } from './models/animation.model';
import { LOCAL_STORAGE_CAMERA_ANIMATIONS } from '../../../shared/local-storage-keys.constants';
import { Keyframe } from './models/keyframe.model';
import { getAnimationsFromDefinedCamera } from '../../../shared/utilities/animation.utility';
import { ViewService } from '../view/view.service';
import { SubscribeEvents } from '../../classes/subscribe-events';

@Injectable({
  providedIn: 'root',
})
export class AnimationService extends SubscribeEvents implements OnDestroy {
  private readonly _animations$$ = new BehaviorSubject<Animation[]>([]);
  private readonly _selectedAnimation$$ = new BehaviorSubject<Animation | null>(
    null
  );
  private readonly _invalidKeyframeRemoved$$ = new Subject<Animation>();

  public readonly animations$ = this._animations$$.asObservable();
  public readonly selectedAnimation$ = this._selectedAnimation$$.asObservable();
  public readonly invalidKeyframeRemoved$ =
    this._invalidKeyframeRemoved$$.asObservable();
  public readonly time$$ = new BehaviorSubject<number>(0);

  public get animations(): Animation[] {
    return this._animations$$.value;
  }

  public set animations(val: Animation[]) {
    this._animations$$.next(val);
  }

  public get selectedAnimation(): Animation | null {
    return this._selectedAnimation$$.value;
  }

  public set selectedAnimation(val: Animation | null) {
    this._selectedAnimation$$.next(val);
  }

  public constructor(private readonly _viewService: ViewService) {
    super();

    this.loadAnimations();

    this.subscribes.push(
      this._animations$$.subscribe(this.saveAnimations.bind(this)),
      this._viewService.definedCameras$.subscribe(
        this.clearUpAnimations.bind(this)
      )
    );
  }

  public saveAnimations(): void {
    localStorage.setItem(
      LOCAL_STORAGE_CAMERA_ANIMATIONS,
      JSON.stringify(
        this.animations.map((a) => ({
          ...a,
          keyframes: a.keyframes.value,
        }))
      )
    );
  }

  public removeKeyframesFromAnimationAndRemoveGeneratedDefinedCameras(
    animation: Animation
  ): void {
    animation.keyframes.next([]);

    // remove ALL generated defined cameras
    this._viewService.setDefinedCameras(
      this._viewService.definedCameras.filter(
        (c) =>
          !c.wasGenerated ||
          (c.wasGenerated &&
            getAnimationsFromDefinedCamera(c.id, this.animations).length > 0)
      )
    );
  }

  public ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  private loadAnimations(): void {
    const animationStr = localStorage.getItem(LOCAL_STORAGE_CAMERA_ANIMATIONS);
    if (!animationStr) return;

    try {
      const animations = JSON.parse(animationStr) as Animation[];
      animations.forEach(
        (a) =>
          (a.keyframes = new BehaviorSubject<Keyframe[]>(
            a.keyframes as unknown as Keyframe[]
          ))
      );
      this._animations$$.next(animations);
    } catch (e) {
      console.warn('Defined camera positions are invalid!', e);
    }
  }

  private clearUpAnimations(): void {
    for (const animation of this.animations) {
      const validKeyframes = animation.keyframes.value.filter((k) =>
        this._viewService.definedCameras.find((c) => c.id === k.cameraID)
      );
      console.log(animation, validKeyframes);
      if (validKeyframes.length === animation.keyframes.value.length) continue;

      animation.keyframes.next(validKeyframes);
      this._invalidKeyframeRemoved$$.next(animation);
    }
  }
}
