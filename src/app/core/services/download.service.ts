/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable, OnDestroy } from '@angular/core';
import { Params } from '@angular/router';
import {
  HttpClient,
  HttpEvent,
  HttpEventType,
  HttpHeaders,
} from '@angular/common/http';
import { Subject, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {
  getFileExtension,
  getFileNameFromPath,
  getFileNameWithoutExtension,
  getFileSizeString,
} from '../../shared/utilities/file.utility';
import { ProgressStateHandlerService } from './progress-state-handler.service';
import { EDownloadParams } from './models/download-params.enum';
import { SubscribeEvents } from '../classes/subscribe-events';

const FILE_EXTENSION_TABLE_OF_CONTENT = 'toc';
const MIME_TYPE_BINARY = 'application/octet-stream';

type MimeInfo = {
  mime: string;
  extension: string;
};

type DownloadInfo = {
  url: string;
  fileName?: string;
  triggeredFileNames: string[];
};

type DownloadedFile = {
  file: File;
  isTableOfContentFile: boolean;
  triggeredFileNames: string[];
};

const SUPPORTED_MIME_TYPES: MimeInfo[] = [
  {
    mime: 'application/xml',
    extension: 'xml',
  },
  {
    mime: 'application/zip',
    extension: 'zip',
  },
  {
    mime: 'text/plain',
    extension: 'trj',
  },
  {
    mime: MIME_TYPE_BINARY, // aka 'no extension'
    extension: 'trj',
  },
];

@Injectable({
  providedIn: 'root',
})
export class DownloadService extends SubscribeEvents implements OnDestroy {
  private readonly _file$$ = new Subject<DownloadedFile>();

  private readonly _providers = [
    { name: 'gitlab', downloader: DownloadService.getDownloadLinkFromGitLab },
  ];

  public readonly file$ = this._file$$.asObservable();

  public constructor(
    private readonly _httpClient: HttpClient,
    private readonly _loadingProgressHandler: ProgressStateHandlerService
  ) {
    super();
  }

  private static getDownloadLinkFromGitLab(params: Params): DownloadInfo {
    const repoID = parseInt(params[EDownloadParams.GITLAB_REPOSITORY]);
    if (repoID === undefined) throw new Error('Invalid repository ID!');

    const file = params[EDownloadParams.FILE];
    if (file === undefined) throw new Error('Repository file is missing!');

    const ref = params[EDownloadParams.GITLAB_REF] ?? 'master';

    const url = `https://gitlab.com/api/v4/projects/${repoID}/repository/files/${encodeURIComponent(
      file
    )}/raw?ref=${ref}`;

    return {
      url,
      fileName: getFileNameFromPath(decodeURIComponent(file)),
      triggeredFileNames: [decodeURIComponent(file)],
    };
  }

  public ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  public download(params: Params): void {
    const provider = this._providers.find(
      (h) => h.name === params[EDownloadParams.PROVIDER]
    );
    if (!provider) {
      console.error('Invalid provider. Provider is not supported!');
      return;
    }

    try {
      const info = provider.downloader(params);
      this.downloadFileAndEmitEvents(info);
    } catch (e) {
      console.error(e);
    }
  }

  private downloadFileAndEmitEvents({
    url,
    fileName,
    triggeredFileNames,
  }: DownloadInfo): void {
    this.subscribes.push(
      this._httpClient
        .get(url, {
          reportProgress: true,
          observe: 'events',
          responseType: 'blob',
        })
        .pipe<HttpEvent<Blob>, HttpEvent<Blob>>(
          map((res) => {
            if (res.type === HttpEventType.ResponseHeader) {
              if (!res.ok)
                throw new Error(`Invalid server response with ${res.status}.`);

              if (!this.getMimeInfoFromHeader(res.headers))
                throw new Error('Unsupported mime type.');
            }

            return res;
          }),
          catchError((err) => {
            this._loadingProgressHandler.setErrorMessage(err.message);
            return throwError(err);
          })
        )
        .subscribe(
          (result) => {
            if (!fileName) fileName = 'downloaded_file';

            if (result.type === HttpEventType.DownloadProgress) {
              this._loadingProgressHandler.setProgressStatus({
                title: 'Downloading...',
                textLeft: fileName,
                textRight: `${getFileSizeString(result.loaded)} / ${
                  result.total !== undefined
                    ? getFileSizeString(result.total)
                    : '-'
                }`,
                value:
                  result.total !== undefined
                    ? (result.loaded / result.total) * 100
                    : -1,
              });
            } else if (result.type === HttpEventType.Response) {
              this._loadingProgressHandler.removeDialog();

              if (result.body) {
                const mime = this.getMimeInfoFromHeader(result.headers)!;
                const originalExt = getFileExtension(fileName);
                // it is possible that the mime type of archive is also binary, like files without extension (alias for ".trj" files)
                const ext =
                  mime.mime === MIME_TYPE_BINARY && originalExt.length > 0
                    ? originalExt
                    : mime.extension;

                this._file$$.next({
                  file: new File(
                    [result.body],
                    `${getFileNameWithoutExtension(fileName)}.${ext}`
                  ),
                  // important: DON'T use mime.extension coz that is always ".trj" for unknown extensions like ".toc"
                  isTableOfContentFile:
                    getFileExtension(fileName).toLowerCase() ===
                    FILE_EXTENSION_TABLE_OF_CONTENT,
                  triggeredFileNames: triggeredFileNames,
                });
              }
            }
          },
          (err) => {
            this._loadingProgressHandler.setErrorMessage(err);
            console.error(err);
          }
        )
    );
  }

  private getMimeInfoFromHeader(headers: HttpHeaders): MimeInfo | undefined {
    const mime = headers.get('content-type');
    return SUPPORTED_MIME_TYPES.find((s) => mime?.includes(s.mime));
  }
}
