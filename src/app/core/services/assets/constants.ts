/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

export const BEAD_TEXTURE_REPEAT = 32;

export const THREE_JS_DEFAULT_FONT_NAME_BOLD = 'ThreejsDefaultFontBold';
export const THREE_JS_DEFAULT_FONT_PATH_BOLD =
  'fonts/helvetiker_bold.typeface.json';
export const THREE_JS_TEXTURE_NAME_BEAD = 'ThreejsTextureBeadTexture';
export const THREE_JS_TEXTURE_PATH_BEAD = 'assets/bead-texture.png';
/**
 * Provides the url to the configuration of the config change button.
 */
export const THREE_JS_TEXTURE_PATH_BTN_XR_CONF_CHANGE =
  '/assets/conf-change-btn-texture-xr.svg';
export const THREE_JS_TEXTURE_NAME_BTN_XR_CONF_CHANGE =
  'ThreejsTextureXRConfChangeBtn';
