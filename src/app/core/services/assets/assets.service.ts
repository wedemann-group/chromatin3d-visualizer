/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { LoadingManager, Texture, TextureLoader } from 'three';
import { Font, FontLoader } from 'three/examples/jsm/loaders/FontLoader';
import { TTFLoader } from 'three/examples/jsm/loaders/TTFLoader';
import { LoadingState } from './models/loading-state.model';
import { getFileExtension } from '../../../shared/utilities/file.utility';

@Injectable({
  providedIn: 'root',
})
export class AssetsService {
  private readonly _loadingState$$ = new BehaviorSubject<boolean>(true);
  private readonly _loadingStateInfo$$ = new Subject<LoadingState>();
  private readonly _textures: { [key: string]: Texture } = {};
  private readonly _fonts: { [key: string]: Font } = {};
  private readonly _lm = new LoadingManager();

  public readonly loadingState$ = this._loadingState$$.asObservable();
  public readonly loadingStateInfo$ = this._loadingStateInfo$$.asObservable();

  public get isLoading(): boolean {
    return this._loadingState$$.value;
  }

  public constructor() {
    this.setLoadManagerEvents();
  }

  public async getOrLoadTexture(
    path: string,
    name: string | null | undefined = null
  ): Promise<Texture> {
    if (!name) name = path;

    if (!this._textures[name])
      this._textures[name] = await this.loadTextureFromURL(path);

    return this._textures[name];
  }

  public getTexture(name: string): Texture {
    if (!this._textures[name])
      throw new Error(`"${name}" texture doesn't exist!`);

    return this._textures[name];
  }

  public async getOrLoadFont(
    path: string,
    name: string | null | undefined = null
  ): Promise<Font> {
    if (!name) name = path;

    if (!this._fonts[name]) {
      if (getFileExtension(path) === 'json')
        this._fonts[name] = await this.loadJSONFontFromURL(path);
      else this._fonts[name] = await this.loadTTFFontFromURL(path);
    }

    return this._fonts[name];
  }

  public getFont(name: string): Font {
    if (!this._fonts[name]) throw new Error(`"${name}" font doesn't exist!`);

    return this._fonts[name];
  }

  private setLoadManagerEvents(): void {
    this._lm.onStart = (url, itemsLoaded, itemsTotal) => {
      this._loadingState$$.next(true);
      console.debug(
        `Started loading file: ${url}; loaded ${itemsLoaded} / ${itemsTotal}`
      );
    };

    this._lm.onLoad = () => {
      this._loadingState$$.next(false);
      console.debug('Loading complete!');
    };

    this._lm.onProgress = (url, itemsLoaded, itemsTotal) => {
      this._loadingStateInfo$$.next({
        file: url,
        index: itemsLoaded,
        total: itemsTotal,
      });
      console.debug(
        'Loading file: ' +
          url +
          '.\nLoaded ' +
          itemsLoaded +
          ' of ' +
          itemsTotal +
          ' files.'
      );
    };

    this._lm.onError = (url) => {
      console.debug(`There was an error loading ${url}`);
    };
  }

  private async loadTextureFromURL(path: string): Promise<Texture> {
    return new Promise((resolve, reject) => {
      const loader = new TextureLoader(this._lm);
      loader.load(
        path,
        (texture) => resolve(texture),
        undefined,
        (e) => reject(e)
      );
    });
  }

  private async loadTTFFontFromURL(path: string): Promise<Font> {
    return new Promise((resolve, reject) => {
      const loader = new TTFLoader(this._lm);
      const fontLoader = new FontLoader(this._lm);
      loader.load(
        path,
        (fnt) => resolve(fontLoader.parse(fnt)),
        undefined,
        (e) => reject(e)
      );
    });
  }

  private async loadJSONFontFromURL(path: string): Promise<Font> {
    return new Promise((resolve, reject) => {
      const fontLoader = new FontLoader(this._lm);
      fontLoader.load(
        path,
        (fnt) => resolve(fnt),
        undefined,
        (e) => reject(e)
      );
    });
  }
}
