/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import QualityLevel from '../models/quality-level.model';
import { ELinkerDNABuilderType } from '../models/linker-dna-builder-type.enum';
import { QualityProfile } from '../models/quality-profile.model';

export const LOW_QUALITY: QualityProfile = {
  name: 'low',
  displayName: 'Low',
  level: new QualityLevel({
    linkerDNABuilder: ELinkerDNABuilderType.LOW,
    cohesinHeadWidthSegments: 10,
    cohesinHeadHeightSegments: 10,
    cohesinRingRadialSegments: 10,
    cohesinRingTubularSegments: 16,
    cohesinHingeRadialSegments: 10,
    cohesinHingeTubularSegments: 10,
    nucleosomeDNATubularSegments: 12,
    nucleosomeDNARadiusSegments: 6,
    linkerDNASmoothing: 6,
    histoneOctamerRadialSegments: 10,
    doubleHelixBasePairRadiusSegments: 8,
    doubleHelixBasePairTubularSegments: 8,
  }),
};
