/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { BufferGeometry, Vector3 } from 'three';
import { Curve } from 'three/src/extras/core/Curve';

export interface INucleosomeDNAData {
  get nucleosomeDNATubularSegments(): number;
  get nucleosomeDNARadiusSegments(): number;
  get normalNucleosomeDNACurve(): Curve<Vector3>;

  /**
   * Defines the points of the "outer" curve of the double strand for the nucleosome DNA.
   *
   * **Note:** These points are used to create the Geometry object. To calculate the DNA double helix, other values are used.
   */
  get doubleHelixNucleosomeDNA(): Curve<Vector3>;
  /**
   * Defines the points of the "inner" curve (rotated 180 degrees compared to {@link doubleHelixNucleosomeDNA}) of the double strand for the nucleosome DNA.
   *
   * **Note:** These points are used to create the Geometry object. To calculate the DNA double helix, other values are used.
   */
  get doubleHelixCounterNucleosomeDNA(): Curve<Vector3>;

  /**
   * Defines a Geometry that includes all points of a nucleosome DNA spiral (midpoints).
   *
   * The points are used to dock the linker DNA (in non-double-strand mode).
   */
  get normalNucleosomeDNA(): BufferGeometry;
  /**
   * Defines a Geometry that includes all points of a nucleosome dna spiral (midpoints) which is wrapped a tiny bit more than biologically
   * intended. The Geometry is used to display the wrapping in non-double strand mode. The increase in the turns provides a patchy
   * connection between the nucleosome DNA and linker DNA.
   */
  get normalNucleosomeDNAExpanded(): BufferGeometry;

  /**
   * Defines a Geometry that includes the outer points of the double-stranded nucleosome DNA.
   *
   * **Note:** These points are used to calculate the linker DNA double strand AND for the DNA base pair.
   */
  get doubleHelixNucleosomeDNAGeometry(): BufferGeometry;
  /**
   * Defines a Geometry that includes the inner (rotated 180 degrees compared to {@link doubleHelixNucleosomeDNA}) points of the double-stranded nucleosome DNA.
   *
   * **Note:** These points are used to calculate the linker DNA double strand AND for the DNA base pair.
   */
  get doubleHelixNucleosomeDNACounterGeometry(): BufferGeometry;
  get linkerDNASmoothing(): number;
}
