/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ICohesinRingData } from './cohesin-ring-data.interface';
import { ICohesinHeadData } from './cohesin-head-data.interface';
import { ICohesinHingeData } from './cohesin-hinge-data.interface';
import { INucleosomeDNAData } from './nucleosome-dna-data.interface';
import { IHistoneOctamerData } from './histone-octamer-data.interface';
import {
  BufferAttribute,
  BufferGeometry,
  CatmullRomCurve3,
  Vector3,
} from 'three';
import { getNucleosomeDNAPoints } from '../../../../modules/dockable-workspace/viewer/beads/nucleosome-dna.curve';
import { QualityLevelParameters } from './quality-level-parameters.model';
import { Curve } from 'three/src/extras/core/Curve';
import {
  NUCLEOSOME_DNA_HEIGHT,
  NUCLEOSOME_DNA_ROUNDS,
} from '../../../../modules/dockable-workspace/viewer/beads/models.variables';
import DoubleHelixCurve from '../../../../modules/dockable-workspace/viewer/beads/double-helix.curve';
import { IDoubleHelixBasePairData } from './double-helix-base-pair-data.interface';
import { ELinkerDNABuilderType } from './linker-dna-builder-type.enum';
import { convertVectorArrayToFloat32Array } from '../../../../shared/utilities/three.js.utility';

/**
 * ...
 *
 * **Note:** These values have an influence on the amount of polygons and can be changed by the user.
 */
export default class QualityLevel
  implements
    ICohesinHeadData,
    ICohesinRingData,
    ICohesinHingeData,
    INucleosomeDNAData,
    IHistoneOctamerData,
    IDoubleHelixBasePairData
{
  // GENERAL
  private readonly _linkerDNABuilder: ELinkerDNABuilderType;

  // COHESIN HEAD
  private readonly _cohesinHeadWidthSegments: number;
  private readonly _cohesinHeadHeightSegments: number;
  private readonly _cohesinRingRadialSegments: number;
  private readonly _cohesinRingTubularSegments: number;
  private readonly _cohesinHingeRadialSegments: number;
  private readonly _cohesinHingeTubularSegments: number;
  // nucleosome DNA
  /** Specifies the rounding of the strand of DNA that turns around a histone. */
  private readonly _nucleosomeDNATubularSegments: number;
  /** Specifies the rounding of the cross-section of a strand of DNA. */
  private readonly _nucleosomeDNARadiusSegments: number;
  private readonly _normalNucleosomeDNACurve: Curve<Vector3>;
  private readonly _doubleHelixNucleosomeDNA: Curve<Vector3>;
  private readonly _doubleHelixCounterNucleosomeDNA: Curve<Vector3>;
  private readonly _doubleHelixNucleosomeDNAGeometry: BufferGeometry;
  private readonly _doubleHelixNucleosomeDNACounterGeometry: BufferGeometry;
  private readonly _normalNucleosomeDNA: BufferGeometry;
  private readonly _normalNucleosomeDNAExpanded: BufferGeometry;
  // Linker DNA
  /** Describes how smooth the linker DNA strand is drawn (bigger = smoother) */
  private readonly _linkerDNASmoothing: number;
  // Histone Octamer
  /** Specifies the rounding of the nucleosome. */
  private readonly _histoneOctamerRadialSegments: number; // it is useful set this value on the same value as NUCLEOSOME_DNA_TUBULAR_SEGMENTS

  // DOUBLE STRAND BASE PAIR
  private readonly _doubleHelixBasePairTubularSegments: number;
  private readonly _doubleHelixBasePairRadiusSegments: number;

  public constructor(parameters: QualityLevelParameters) {
    this._linkerDNABuilder = parameters.linkerDNABuilder;

    this._cohesinHeadWidthSegments = parameters.cohesinHeadWidthSegments;
    this._cohesinHeadHeightSegments = parameters.cohesinHeadHeightSegments;
    this._cohesinRingRadialSegments = parameters.cohesinRingRadialSegments;
    this._cohesinRingTubularSegments = parameters.cohesinRingTubularSegments;
    this._cohesinHingeRadialSegments = parameters.cohesinHingeRadialSegments;
    this._cohesinHingeTubularSegments = parameters.cohesinHingeTubularSegments;
    this._nucleosomeDNATubularSegments =
      parameters.nucleosomeDNATubularSegments;
    this._nucleosomeDNARadiusSegments = parameters.nucleosomeDNARadiusSegments;
    this._linkerDNASmoothing = parameters.linkerDNASmoothing;
    this._histoneOctamerRadialSegments =
      parameters.histoneOctamerRadialSegments;
    this._doubleHelixBasePairTubularSegments =
      parameters.doubleHelixBasePairTubularSegments;
    this._doubleHelixBasePairRadiusSegments =
      parameters.doubleHelixBasePairRadiusSegments;

    const nucleosomeDNAPoints = getNucleosomeDNAPoints(
      this,
      NUCLEOSOME_DNA_HEIGHT,
      false,
      NUCLEOSOME_DNA_ROUNDS
    );
    const nucleosomeDNAPointsExpanded = getNucleosomeDNAPoints(
      this,
      NUCLEOSOME_DNA_HEIGHT,
      true,
      NUCLEOSOME_DNA_ROUNDS
    );
    this._normalNucleosomeDNACurve = new CatmullRomCurve3(nucleosomeDNAPoints);
    const doubleHelixCurveBuilder = new DoubleHelixCurve(
      nucleosomeDNAPoints,
      false
    );
    this._doubleHelixNucleosomeDNA = new CatmullRomCurve3(
      doubleHelixCurveBuilder.getCurvePoints()
    );
    this._doubleHelixCounterNucleosomeDNA = new CatmullRomCurve3(
      doubleHelixCurveBuilder.getCurvePoints(Math.PI)
    );
    this._normalNucleosomeDNA = new BufferGeometry();
    this._normalNucleosomeDNA.setAttribute(
      'position',
      new BufferAttribute(
        new Float32Array(convertVectorArrayToFloat32Array(nucleosomeDNAPoints)),
        3
      )
    );

    this._normalNucleosomeDNAExpanded = new BufferGeometry();
    this._normalNucleosomeDNAExpanded.setAttribute(
      'position',
      new BufferAttribute(
        new Float32Array(
          convertVectorArrayToFloat32Array(nucleosomeDNAPointsExpanded)
        ),
        3
      )
    );

    this._doubleHelixNucleosomeDNAGeometry = new BufferGeometry();
    this._doubleHelixNucleosomeDNAGeometry.setAttribute(
      'position',
      new BufferAttribute(
        new Float32Array(
          convertVectorArrayToFloat32Array(
            doubleHelixCurveBuilder.getCurvePoints(Math.PI)
          )
        ),
        3
      )
    );

    this._doubleHelixNucleosomeDNACounterGeometry = new BufferGeometry();
    this._doubleHelixNucleosomeDNACounterGeometry.setAttribute(
      'position',
      new BufferAttribute(
        new Float32Array(
          convertVectorArrayToFloat32Array(
            doubleHelixCurveBuilder.getCurvePoints()
          )
        ),
        3
      )
    );
  }

  public get linkerDNABuilder(): ELinkerDNABuilderType {
    return this._linkerDNABuilder;
  }

  public get cohesinHeadWidthSegments(): number {
    return this._cohesinHeadWidthSegments;
  }

  public get cohesinHeadHeightSegments(): number {
    return this._cohesinHeadHeightSegments;
  }

  public get cohesinRingRadialSegments(): number {
    return this._cohesinRingRadialSegments;
  }

  public get cohesinRingTubularSegments(): number {
    return this._cohesinRingTubularSegments;
  }

  public get cohesinHingeRadialSegments(): number {
    return this._cohesinHingeRadialSegments;
  }

  public get cohesinHingeTubularSegments(): number {
    return this._cohesinHingeTubularSegments;
  }

  public get nucleosomeDNATubularSegments(): number {
    return this._nucleosomeDNATubularSegments;
  }

  public get nucleosomeDNARadiusSegments(): number {
    return this._nucleosomeDNARadiusSegments;
  }

  public get normalNucleosomeDNACurve(): Curve<Vector3> {
    return this._normalNucleosomeDNACurve;
  }

  public get doubleHelixNucleosomeDNA(): Curve<Vector3> {
    return this._doubleHelixNucleosomeDNA;
  }

  public get doubleHelixCounterNucleosomeDNA(): Curve<Vector3> {
    return this._doubleHelixCounterNucleosomeDNA;
  }

  public get normalNucleosomeDNA(): BufferGeometry {
    return this._normalNucleosomeDNA;
  }

  public get normalNucleosomeDNAExpanded(): BufferGeometry {
    return this._normalNucleosomeDNAExpanded;
  }

  public get doubleHelixNucleosomeDNAGeometry(): BufferGeometry {
    return this._doubleHelixNucleosomeDNAGeometry;
  }

  public get doubleHelixNucleosomeDNACounterGeometry(): BufferGeometry {
    return this._doubleHelixNucleosomeDNACounterGeometry;
  }

  public get linkerDNASmoothing(): number {
    return this._linkerDNASmoothing;
  }

  public get histoneOctamerRadialSegments(): number {
    return this._histoneOctamerRadialSegments;
  }

  public get doubleHelixBasePairTubularSegments(): number {
    return this._doubleHelixBasePairTubularSegments;
  }

  public get doubleHelixBasePairRadiusSegments(): number {
    return this._doubleHelixBasePairRadiusSegments;
  }
}
