/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { SerializedCamera } from '../../../../modules/dockable-workspace/viewer/models/serialized-camera.model';
import { DefaultColors } from '../../colors/models/default-colors.model';
import { BeadColors } from '../../colors/models/bead-colors.model';
import { SerializedPBC } from './serialized-pbc.model';
import { EMaterialType } from '../../../../shared/models/material-type.enum';

export type Session = {
  defaultColors: Partial<DefaultColors>;
  beadColors: Partial<BeadColors>;
  camera: SerializedCamera;
  configIndex: number;
  pbc: Partial<SerializedPBC>;
  material: EMaterialType;
  glossy: boolean;
};
