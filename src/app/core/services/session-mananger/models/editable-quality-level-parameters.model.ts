/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ELinkerDNABuilderType } from './linker-dna-builder-type.enum';
import { ChangeableOption } from '../../../../shared/decorators/changeable-option.decorator';
import { NORMAL_QUALITY } from '../qualities/normal-quality.model';
import { ChangeableNumber } from '../../../../shared/decorators/changeable-number.decorator';
import { ChangeableCategory } from '../../../../shared/decorators/changeable-category.decorator';
import { ChangeableCondition } from '../../../../shared/decorators/changeable-condition.decorator';
import * as Names from '../constants';
import { DisplayName } from '../../../../shared/decorators/display-name.decorator';

/**
 * ...
 *
 * **Note:** These values have an influence on the amount of polygons and can be changed by the user.
 */
export default class EditableQualityLevelParameters {
  [key: string]: ELinkerDNABuilderType | number | undefined;

  // GENERAL
  private _linkerDNABuilder: ELinkerDNABuilderType;

  // COHESIN HEAD
  private _cohesinHeadWidthSegments: number;
  private _cohesinHeadHeightSegments: number;
  private _cohesinRingRadialSegments: number;
  private _cohesinRingTubularSegments: number;
  private _cohesinHingeRadialSegments: number;
  private _cohesinHingeTubularSegments: number;
  // nucleosome DNA
  /** Specifies the rounding of the strand of DNA that turns around a nucleosome. */
  private _nucleosomeDNATubularSegments: number;
  /** Specifies the rounding of the cross-section of a strand of DNA. */
  private _nucleosomeDNARadiusSegments: number;
  // Linker DNA
  /** Describes how smooth the linker DNA strand is drawn (bigger = smoother) */
  private _linkerDNASmoothing: number;
  // Histone Octamer
  /** Specifies the rounding of the nucleosome. */
  private _histoneOctamerRadialSegments: number; // it is useful set this value on the same value as NUCLEOSOME_DNA_TUBULAR_SEGMENTS

  // DOUBLE STRAND BASE PAIR
  private _doubleHelixBasePairTubularSegments: number;
  private _doubleHelixBasePairRadiusSegments: number;

  public constructor(level = NORMAL_QUALITY.level) {
    this._linkerDNABuilder = level.linkerDNABuilder;

    this._cohesinHeadWidthSegments = level.cohesinHeadWidthSegments;
    this._cohesinHeadHeightSegments = level.cohesinHeadHeightSegments;
    this._cohesinRingRadialSegments = level.cohesinRingRadialSegments;
    this._cohesinRingTubularSegments = level.cohesinRingTubularSegments;
    this._cohesinHingeRadialSegments = level.cohesinHingeRadialSegments;
    this._cohesinHingeTubularSegments = level.cohesinHingeTubularSegments;
    this._nucleosomeDNATubularSegments = level.nucleosomeDNATubularSegments;
    this._nucleosomeDNARadiusSegments = level.nucleosomeDNARadiusSegments;
    this._linkerDNASmoothing = level.linkerDNASmoothing;
    this._histoneOctamerRadialSegments = level.histoneOctamerRadialSegments;
    this._doubleHelixBasePairTubularSegments =
      level.doubleHelixBasePairTubularSegments;
    this._doubleHelixBasePairRadiusSegments =
      level.doubleHelixBasePairRadiusSegments;
  }

  public get linkerDNABuilder(): ELinkerDNABuilderType {
    return this._linkerDNABuilder;
  }

  @ChangeableCategory(Names.CATEGORY_NAME_LINKER_DNA)
  @DisplayName('Linker DNA builder')
  @ChangeableOption({
    '2D': ELinkerDNABuilderType.LOW,
    Approximate: ELinkerDNABuilderType.INSTANCED_MESHED,
    Normal: ELinkerDNABuilderType.NORMAL,
    'Double Helix': ELinkerDNABuilderType.DOUBLE_HELIX,
  })
  public set linkerDNABuilder(value: ELinkerDNABuilderType) {
    this._linkerDNABuilder = value;
  }

  public get linkerDNASmoothing(): number {
    return this._linkerDNASmoothing;
  }

  @ChangeableCategory(Names.CATEGORY_NAME_LINKER_DNA)
  @DisplayName('Linker DNA smoothing')
  @ChangeableNumber(1, 50)
  public set linkerDNASmoothing(value: number) {
    this._linkerDNASmoothing = value;
  }

  public get cohesinHeadWidthSegments(): number {
    return this._cohesinHeadWidthSegments;
  }

  @ChangeableCategory(Names.CATEGORY_NAME_COHESIN)
  @DisplayName('Head width segments')
  @ChangeableNumber(4, 50)
  public set cohesinHeadWidthSegments(value: number) {
    this._cohesinHeadWidthSegments = value;
  }

  public get cohesinHeadHeightSegments(): number {
    return this._cohesinHeadHeightSegments;
  }

  @ChangeableCategory(Names.CATEGORY_NAME_COHESIN)
  @DisplayName('Head height segments')
  @ChangeableNumber(4, 50)
  public set cohesinHeadHeightSegments(value: number) {
    this._cohesinHeadHeightSegments = value;
  }

  public get cohesinRingRadialSegments(): number {
    return this._cohesinRingRadialSegments;
  }

  @ChangeableCategory(Names.CATEGORY_NAME_COHESIN)
  @DisplayName('Ring radial segments')
  @ChangeableNumber(3, 100)
  public set cohesinRingRadialSegments(value: number) {
    this._cohesinRingRadialSegments = value;
  }

  public get cohesinRingTubularSegments(): number {
    return this._cohesinRingTubularSegments;
  }

  @ChangeableCategory(Names.CATEGORY_NAME_COHESIN)
  @DisplayName('Ring tubular segments')
  @ChangeableNumber(4, 100)
  public set cohesinRingTubularSegments(value: number) {
    this._cohesinRingTubularSegments = value;
  }

  public get cohesinHingeRadialSegments(): number {
    return this._cohesinHingeRadialSegments;
  }

  @ChangeableCategory(Names.CATEGORY_NAME_COHESIN)
  @DisplayName('Hinge tubular segments')
  @ChangeableNumber(4, 50)
  public set cohesinHingeTubularSegments(value: number) {
    this._cohesinHingeTubularSegments = value;
  }

  @ChangeableCategory(Names.CATEGORY_NAME_COHESIN)
  @DisplayName('Hinge radial segments')
  @ChangeableNumber(3, 50)
  public set cohesinHingeRadialSegments(value: number) {
    this._cohesinHingeRadialSegments = value;
  }

  public get cohesinHingeTubularSegments(): number {
    return this._cohesinHingeTubularSegments;
  }

  public get nucleosomeDNATubularSegments(): number {
    return this._nucleosomeDNATubularSegments;
  }

  @ChangeableCategory(Names.CATEGORY_NAME_LINKER_DNA)
  @DisplayName('Tubular segments (nucleosome dna )')
  @ChangeableNumber(6, 100)
  public set nucleosomeDNATubularSegments(value: number) {
    this._nucleosomeDNATubularSegments = value;
  }

  public get nucleosomeDNARadiusSegments(): number {
    return this._nucleosomeDNARadiusSegments;
  }

  @ChangeableCategory(Names.CATEGORY_NAME_LINKER_DNA)
  @DisplayName('Radius segments (nucleosome dna)')
  @ChangeableNumber(3, 100)
  public set nucleosomeDNARadiusSegments(value: number) {
    this._nucleosomeDNARadiusSegments = value;
  }

  public get histoneOctamerRadialSegments(): number {
    return this._histoneOctamerRadialSegments;
  }

  @ChangeableCategory(Names.CATEGORY_NAME_HISTONE_OCTAMER)
  @DisplayName('Radial segments')
  @ChangeableNumber(3, 100)
  public set histoneOctamerRadialSegments(value: number) {
    this._histoneOctamerRadialSegments = value;
  }

  public get doubleHelixBasePairTubularSegments(): number {
    return this._doubleHelixBasePairTubularSegments;
  }

  @ChangeableCategory(Names.CATEGORY_NAME_LINKER_DNA)
  @DisplayName('Tubular segments (double helix base pair)')
  @ChangeableCondition<ELinkerDNABuilderType>(
    ELinkerDNABuilderType.DOUBLE_HELIX,
    'linkerDNABuilder'
  )
  @ChangeableNumber(3, 30)
  public set doubleHelixBasePairTubularSegments(value: number) {
    this._doubleHelixBasePairTubularSegments = value;
  }

  public get doubleHelixBasePairRadiusSegments(): number {
    return this._doubleHelixBasePairRadiusSegments;
  }

  @ChangeableCategory(Names.CATEGORY_NAME_LINKER_DNA)
  @DisplayName('Radius segments (double helix base pair)')
  @ChangeableCondition<ELinkerDNABuilderType>(
    ELinkerDNABuilderType.DOUBLE_HELIX,
    'linkerDNABuilder'
  )
  @ChangeableNumber(3, 30)
  public set doubleHelixBasePairRadiusSegments(value: number) {
    this._doubleHelixBasePairRadiusSegments = value;
  }
}
