/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ELinkerDNABuilderType } from './linker-dna-builder-type.enum';

export type QualityLevelParameters = {
  // general
  linkerDNABuilder: ELinkerDNABuilderType;
  // cohesin
  cohesinHeadWidthSegments: number;
  cohesinHeadHeightSegments: number;
  cohesinRingRadialSegments: number;
  cohesinRingTubularSegments: number;
  cohesinHingeRadialSegments: number;
  cohesinHingeTubularSegments: number;
  // nucleosome dna
  nucleosomeDNATubularSegments: number;
  nucleosomeDNARadiusSegments: number;
  // linker dna
  linkerDNASmoothing: number;
  // histone octamer
  histoneOctamerRadialSegments: number;
  // double helix base pair
  doubleHelixBasePairTubularSegments: number;
  doubleHelixBasePairRadiusSegments: number;
};
