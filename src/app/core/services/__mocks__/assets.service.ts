/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LoadingManager, Texture } from 'three';
import { Font } from 'three/examples/jsm/loaders/FontLoader';

@Injectable({
  providedIn: 'root',
})
export class AssetsService {
  private _loadingState$$ = new BehaviorSubject<boolean>(true);
  private _textures: { [key: string]: Texture } = {};
  private _fonts: { [key: string]: Font } = {};
  private _lm = new LoadingManager();

  public loadingState$ = this._loadingState$$.asObservable();

  public get isLoading(): boolean {
    return this._loadingState$$.value;
  }

  public constructor() {}

  public async getOrLoadTexture(
    path: string,
    name: string | null | undefined = null
  ): Promise<Texture> {
    return new Promise((resolve) => resolve(new Texture()));
  }

  public getTexture(name: string): Texture {
    return new Texture();
  }

  public async getOrLoadFont(
    path: string,
    name: string | null | undefined = null
  ): Promise<Font> {
    return new Promise((resolve) => resolve(new Font({})));
  }

  public getFont(name: string): Font {
    return new Font({});
  }
}
