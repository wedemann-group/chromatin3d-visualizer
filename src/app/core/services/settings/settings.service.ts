/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import {
  LOCAL_STORAGE_LAST_VISIT,
  LOCAL_STORAGE_SETTINGS,
} from '../../../shared/local-storage-keys.constants';
import DateDiff from 'date-diff';
import { LastVisit } from '../../../shared/models/last-visit.model';
import ChangeableOption from '../../../shared/changeable-option';
import QualityLevel from '../session-mananger/models/quality-level.model';
import { QualityLevelParameters } from '../session-mananger/models/quality-level-parameters.model';
import { SessionManagerService } from '../session-mananger/session-manager.service';
import { Settings } from './models/settings.model';
import { ETheme } from '../../../shared/models/theme.enum';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  public readonly theme = new ChangeableOption<ETheme>(ETheme.SYSTEM);
  public readonly animationEnabled = new ChangeableOption<boolean>(true);
  public readonly autoPBC = new ChangeableOption<boolean>(true);
  public readonly autoFindFiber = new ChangeableOption<boolean>(true);
  /** Whether to export invisible objects from the scene */
  public readonly exportInvisibleObjectsGLTF = new ChangeableOption<boolean>(
    true
  );

  public constructor(
    private readonly _sessionManagerService: SessionManagerService
  ) {
    this.load();

    this._sessionManagerService.customQuality$.subscribe(this.save.bind(this));
    this._sessionManagerService.qualityProfile$.subscribe(this.save.bind(this));
  }

  public static getLastVisitDateFromLocalStorage(): LastVisit {
    const currentDate = new Date();
    const lastVisitTime = localStorage.getItem(LOCAL_STORAGE_LAST_VISIT);
    if (!lastVisitTime) return { date: null, isInFuture: false };

    try {
      const parsedDate = Date.parse(lastVisitTime);
      // "lastVisit" is set in the future to hide the update notification forever
      return {
        date: parsedDate,
        isInFuture: new DateDiff(new Date(parsedDate), currentDate).years() > 1,
      };
    } catch (e) {
      console.warn("'Last Visit' failed to parse");
    }

    return { date: null, isInFuture: false };
  }

  public static writeLastVisitDateToLocalStorage(date: Date): void {
    localStorage.setItem(LOCAL_STORAGE_LAST_VISIT, date.toISOString());
  }

  private static getQualityLevel(level: QualityLevel): QualityLevelParameters {
    return {
      linkerDNABuilder: level.linkerDNABuilder,
      cohesinHeadWidthSegments: level.cohesinHeadWidthSegments,
      cohesinHeadHeightSegments: level.cohesinHeadHeightSegments,
      cohesinRingRadialSegments: level.cohesinRingRadialSegments,
      cohesinRingTubularSegments: level.cohesinRingTubularSegments,
      cohesinHingeRadialSegments: level.cohesinHingeRadialSegments,
      cohesinHingeTubularSegments: level.cohesinHingeTubularSegments,
      nucleosomeDNATubularSegments: level.nucleosomeDNATubularSegments,
      nucleosomeDNARadiusSegments: level.nucleosomeDNARadiusSegments,
      linkerDNASmoothing: level.linkerDNASmoothing,
      histoneOctamerRadialSegments: level.histoneOctamerRadialSegments,
      doubleHelixBasePairTubularSegments:
        level.doubleHelixBasePairTubularSegments,
      doubleHelixBasePairRadiusSegments:
        level.doubleHelixBasePairRadiusSegments,
    } as QualityLevelParameters;
  }

  public save(): void {
    localStorage.setItem(
      LOCAL_STORAGE_SETTINGS,
      JSON.stringify(this.getSettings())
    );
  }

  public getSettings(): Settings {
    return {
      appearance: {
        animationEnabled: this.animationEnabled.value,
        theme: this.theme.value,
      },
      performance: {
        customQualityLevel: SettingsService.getQualityLevel(
          this._sessionManagerService.getCustomQualityLevel()
        ),
        selectedQualityLevel: this._sessionManagerService.qualityProfileName,
      },
      behaviour: {
        autoPBC: this.autoPBC.value,
        autoFindFiber: this.autoFindFiber.value,
      },
      export: {
        exportInvisibleObjectsGLTF: this.exportInvisibleObjectsGLTF.value,
      },
    };
  }

  public apply(settings: Partial<Settings>): void {
    if (settings.behaviour) {
      if (settings.behaviour.autoPBC !== undefined)
        this.autoPBC.value = settings.behaviour.autoPBC;

      if (settings.behaviour.autoFindFiber !== undefined)
        this.autoFindFiber.value = settings.behaviour.autoFindFiber;
    }

    if (settings.appearance) {
      if (settings.appearance.animationEnabled !== undefined)
        this.animationEnabled.value = settings.appearance.animationEnabled;

      if (settings.appearance.theme !== undefined)
        this.theme.value = settings.appearance.theme;
    }

    if (settings.performance) {
      this._sessionManagerService.setCustomLevel(
        new QualityLevel(settings.performance.customQualityLevel)
      );
      this._sessionManagerService.setQualityLevel(
        settings.performance.selectedQualityLevel
      );
    }

    if (settings.export) {
      if (settings.export.exportInvisibleObjectsGLTF !== undefined) {
        this.exportInvisibleObjectsGLTF.value =
          settings.export.exportInvisibleObjectsGLTF;
      }
    }
  }

  private load(): void {
    const str = localStorage.getItem(LOCAL_STORAGE_SETTINGS);
    if (!str) return;

    try {
      this.apply(JSON.parse(str) as Partial<Settings>);
    } catch (e) {
      console.warn('Invalid settings in localStorage!');
    }
  }
}
