/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { SettingsBehaviour } from './settings-behaviour.model';
import { SettingsPerformance } from './settings-performance.model';
import { SettingsAppearance } from './settings-appearance.model';
import { SettingsExport } from './settings-export.model';

export type Settings = {
  behaviour: SettingsBehaviour;
  appearance: SettingsAppearance;
  performance: SettingsPerformance;
  export: SettingsExport;
};
