/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  ComponentFactoryResolver,
  ComponentRef,
  Injectable,
  Injector,
  StaticProvider,
  Type,
} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {
  ComponentContainer,
  ComponentItem,
  GoldenLayout,
  JsonValue,
} from 'golden-layout';
import { BaseTabComponentDirective } from '../../../shared/directives/base-tab-component.directive';
import * as TabNames from '../../../modules/dockable-workspace/tab-names.constants';
import { ViewerComponent } from '../../../modules/dockable-workspace/viewer/viewer.component';
import { PbcComponent } from '../../../modules/dockable-workspace/pbc/pbc.component';
import { ViewSettingsComponent } from '../../../modules/dockable-workspace/view-settings/view-settings.component';
import { ConfigurationPlayerComponent } from '../../../modules/dockable-workspace/configuration-player/configuration-player.component';
import { CameraSettingsComponent } from '../../../modules/dockable-workspace/camera-settings/camera-settings.component';
import { BeadsExplorerHistoneOctamerComponent } from '../../../modules/dockable-workspace/beads-explorer/beads-explorer-histone-octamer/beads-explorer-histone-octamer.component';
import { BeadsExplorerNucleosomeDNAComponent } from '../../../modules/dockable-workspace/beads-explorer/beads-explorer-nucleosome-dna/beads-explorer-nucleosome-dna.component';
import { RestoreTab } from './models/restore-tab.model';
import { CohesinCtcfComponent } from '../../../modules/dockable-workspace/cohesin-ctcf/cohesin-ctcf.component';
import { DebugSettingsComponent } from '../../../modules/dockable-workspace/debug-settings/debug-settings.component';
import { WorkspaceService } from '../workspace.service';
import { BeadsExplorerLinkerDnaComponent } from '../../../modules/dockable-workspace/beads-explorer/beads-explorer-linker-dna/beads-explorer-linker-dna.component';
import { BeadsExplorerCohesinComponent } from '../../../modules/dockable-workspace/beads-explorer/beads-explorer-cohesin/beads-explorer-cohesin.component';
import { FiberInformationComponent } from '../../../modules/dockable-workspace/fiber-information/fiber-information.component';
import { ToolColorGradientComponent } from '../../../modules/dockable-workspace/tools/tool-color-gradient/tool-color-gradient.component';
import { SelectionInformationComponent } from '../../../modules/dockable-workspace/selection-information/selection-information.component';
import { ToolDistanceMeasurementComponent } from '../../../modules/dockable-workspace/tools/tool-distance-measurement/tool-distance-measurement.component';
import { ToolDefinedCameraPositionsComponent } from '../../../modules/dockable-workspace/tools/tool-defined-camera-positions/tool-defined-camera-positions.component';
import { ToolCameraAnimatorComponent } from '../../../modules/dockable-workspace/tools/tool-camera-animator/tool-camera-animator.component';

export const COMPONENTS: RestoreTab[] = [
  {
    jsonName: TabNames.TAB_JSON_NAME_TRAJECTORY_VIEWER,
    component: ViewerComponent,
    displayName: TabNames.TAB_NAME_TRAJECTORY_VIEWER,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_CONFIGURATION_PLAYER,
    component: ConfigurationPlayerComponent,
    displayName: TabNames.TAB_NAME_CONFIGURATION_PLAYER,
    fullNameInMenu: TabNames.TAB_FULL_NAME_CONFIGURATION_PLAYER,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_PBC,
    component: PbcComponent,
    displayName: TabNames.TAB_NAME_PBC,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_VIEW_SETTINGS,
    component: ViewSettingsComponent,
    displayName: TabNames.TAB_NAME_VIEW_SETTINGS,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_CAMERA_SETTINGS,
    component: CameraSettingsComponent,
    displayName: TabNames.TAB_NAME_CAMERA_SETTINGS,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_FIBER_INFORMATION,
    component: FiberInformationComponent,
    displayName: TabNames.TAB_NAME_NAME_FIBER_INFORMATION,
    fullNameInMenu: TabNames.TAB_FULL_NAME_FIBER_INFORMATION,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_COHESIN_CTCF,
    component: CohesinCtcfComponent,
    displayName: TabNames.TAB_NAME_COHESIN_CTCF_INFORMATION,
    fullNameInMenu: TabNames.TAB_FULL_NAME_COHESIN_CTCF_INFORMATION,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_SELECTION_INFORMATION,
    component: SelectionInformationComponent,
    displayName: TabNames.TAB_NAME_NAME_SELECTION_INFORMATION,
    fullNameInMenu: TabNames.TAB_FULL_NAME_SELECTION_INFORMATION,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_EXPLORER_NUCLEOSOMES,
    component: BeadsExplorerHistoneOctamerComponent,
    displayName: TabNames.TAB_NAME_EXPLORER_HISTONE_OCTAMER,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_EXPLORER_DNA_HELIX,
    component: BeadsExplorerNucleosomeDNAComponent,
    displayName: TabNames.TAB_NAME_EXPLORER_NUCLEOSOME_DNA_HELIX,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_EXPLORER_LINKER_DNA,
    component: BeadsExplorerLinkerDnaComponent,
    displayName: TabNames.TAB_NAME_EXPLORER_LINKER_DNA,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_EXPLORER_COHESIN,
    component: BeadsExplorerCohesinComponent,
    displayName: TabNames.TAB_NAME_EXPLORER_COHESIN,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_TOOL_COLOR_GRADIENT,
    component: ToolColorGradientComponent,
    displayName: TabNames.TAB_NAME_TOOL_COLOR_GRADIENT,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_TOOL_DISTANCE_MEASUREMENT,
    component: ToolDistanceMeasurementComponent,
    displayName: TabNames.TAB_NAME_TOOL_DISTANCE_MEASUREMENT,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_TOOL_DEFINED_CAMERA_POSITIONS,
    component: ToolDefinedCameraPositionsComponent,
    displayName: TabNames.TAB_NAME_TOOL_DEFINED_CAMERA_POSITIONS,
    fullNameInMenu: TabNames.TAB_FULL_NAME_TOOL_DEFINED_CAMERA_POSITIONS,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_TOOL_CAMERA_ANIMATOR,
    component: ToolCameraAnimatorComponent,
    displayName: TabNames.TAB_NAME_NAME_TOOL_CAMERA_ANIMATOR,
  },
  {
    jsonName: TabNames.TAB_JSON_NAME_DEBUG_SETTINGS,
    component: DebugSettingsComponent,
    displayName: TabNames.TAB_NAME_DEBUG_SETTINGS,
    onlyInDebugMode: true,
  },
];

@Injectable({
  providedIn: 'root',
})
export class DockingSuiteHelperService {
  private _componentTypeMap = new Map<
    string,
    Type<BaseTabComponentDirective>
  >();
  private readonly _tabs$$ = new BehaviorSubject<ComponentItem[]>([]);
  private readonly _layout$$ = new BehaviorSubject<GoldenLayout | undefined>(
    undefined
  );

  public readonly layout$ = this._layout$$.asObservable();
  public readonly tabs$ = this._tabs$$.asObservable();

  public get tabs(): ComponentItem[] {
    return this._tabs$$.value;
  }

  public set tabs(tabs: ComponentItem[]) {
    this._tabs$$.next(tabs);
  }

  public get layout(): GoldenLayout | undefined {
    return this._layout$$.value;
  }

  public set layout(val: GoldenLayout | undefined) {
    this._layout$$.next(val);
  }

  public constructor(
    private readonly _componentFactoryResolver: ComponentFactoryResolver,
    private readonly _workspaceService: WorkspaceService
  ) {
    COMPONENTS.forEach((c) =>
      this.registerComponentType(c.jsonName, c.component)
    );

    // dependency injection workaround
    this._tabs$$.subscribe((tabs) => {
      this._workspaceService.isViewerActive = !!tabs.find(
        (t) => t.componentType === TabNames.TAB_JSON_NAME_TRAJECTORY_VIEWER
      );
    });
  }

  public registerComponentType(
    name: string,
    componentType: Type<BaseTabComponentDirective>
  ): void {
    this._componentTypeMap.set(name, componentType);
  }

  public createComponent(
    componentTypeJsonValue: JsonValue,
    container: ComponentContainer
  ): ComponentRef<BaseTabComponentDirective> {
    const componentType = this._componentTypeMap.get(
      componentTypeJsonValue as string
    );
    if (!componentType) throw new Error('Unknown component type');

    const provider: StaticProvider = {
      provide:
        BaseTabComponentDirective.GOLDEN_LAYOUT_CONTAINER_INJECTION_TOKEN,
      useValue: container,
    };
    const injector = Injector.create({
      providers: [provider],
    });
    const componentFactoryRef =
      this._componentFactoryResolver.resolveComponentFactory<BaseTabComponentDirective>(
        componentType
      );
    return componentFactoryRef.create(injector);
  }
}
