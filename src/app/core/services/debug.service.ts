/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import {
  LOCAL_STORAGE_DEBUG,
  LOCAL_STORAGE_DEBUG_CACHED_FILE_TRJ,
  LOCAL_STORAGE_DEBUG_CACHED_FILE_XML,
  LOCAL_STORAGE_DEBUG_CACHED_SESSION,
} from '../../shared/local-storage-keys.constants';
import { Session } from './session-mananger/models/session.model';
import { WorkspaceService } from './workspace.service';
import { parseBoolean } from '../../shared/utilities/parser.utility';

@Injectable({
  providedIn: 'root',
})
export class DebugService {
  private readonly _debugMode$$ = new BehaviorSubject<boolean>(false);

  public readonly showStats$$ = new BehaviorSubject<boolean>(true);
  public readonly showFiberBox$$ = new BehaviorSubject<boolean>(false);
  public readonly showCenterOfMass$$ = new BehaviorSubject<boolean>(false);
  public readonly showAnimationKeyframes$$ = new BehaviorSubject<boolean>(
    false
  );
  public readonly saveLayout$ = new Subject<null>();

  public readonly debugMode$ = this._debugMode$$.asObservable();

  public get debugMode(): boolean {
    return this._debugMode$$.value;
  }

  public constructor(private readonly _workspaceService: WorkspaceService) {
    this.init();
  }

  public afterViewInit(): void {
    const debugSessionStr: string | null = localStorage.getItem(
      LOCAL_STORAGE_DEBUG_CACHED_SESSION
    );
    let debugSession: Session | undefined;
    try {
      if (debugSessionStr)
        debugSession = JSON.parse(debugSessionStr) as Session;
    } catch (e) {
      console.warn('Cached debug session is invalid!');
    }

    const debugConfigXML = localStorage.getItem(
      LOCAL_STORAGE_DEBUG_CACHED_FILE_XML
    );
    if (debugConfigXML) {
      this._workspaceService.loadFile(
        new File([debugConfigXML], 'cached.xml'),
        debugSession
      );
      return;
    }

    const debugConfigTRJ = localStorage.getItem(
      LOCAL_STORAGE_DEBUG_CACHED_FILE_TRJ
    );
    if (debugConfigTRJ) {
      this._workspaceService.loadFile(
        new File([debugConfigTRJ], 'cached.trj'),
        debugSession
      );
    }
  }

  private init(): void {
    const isDebugMode = localStorage.getItem(LOCAL_STORAGE_DEBUG);
    if (isDebugMode) {
      this._debugMode$$.next(parseBoolean(isDebugMode));
    }

    addEventListener('storage', (event) => {
      if (event.key !== LOCAL_STORAGE_DEBUG) return;

      this._debugMode$$.next(
        event.newValue ? parseBoolean(event.newValue) : false
      );
    });
  }
}
