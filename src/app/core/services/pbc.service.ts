/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { WorkspaceService } from './workspace.service';
import {
  DEFAULT_COLOR_MICROSCOPE_SLIDE_BOX,
  DEFAULT_COLOR_PBC_CONTAINER,
} from '../../shared/color.constants';
import { SessionManagerService } from './session-mananger/session-manager.service';
import { Configuration } from '../../shared/models/configuration.model';
import { SettingsService } from './settings/settings.service';

@Injectable({
  providedIn: 'root',
})
export class PbcService {
  private readonly _pbcMode$$ = new BehaviorSubject<boolean>(false);
  private readonly _pbcContainerVisibility$$ = new BehaviorSubject<boolean>(
    false
  );
  private readonly _pbcContainerColor$$ = new BehaviorSubject<string>(
    DEFAULT_COLOR_PBC_CONTAINER
  );
  private readonly _microscopeSlideEnableState$$ = new BehaviorSubject<boolean>(
    false
  );
  private readonly _microscopeSlideCutSize$$ = new BehaviorSubject<number>(1);
  private readonly _microscopeSlideIndex$$ = new BehaviorSubject<number>(0);
  private readonly _microscopeSlideBoxVisibility$$ =
    new BehaviorSubject<boolean>(false);
  private readonly _microscopeSlideBoxColor$$ = new BehaviorSubject<string>(
    DEFAULT_COLOR_MICROSCOPE_SLIDE_BOX
  );
  private _isFirstConfig = true;

  public readonly pbcMode$ = this._pbcMode$$.asObservable();
  public readonly pbcContainerVisibility$ =
    this._pbcContainerVisibility$$.asObservable();
  public readonly pbcContainerColor$ = this._pbcContainerColor$$.asObservable();
  public readonly microscopeSlideEnableState$ =
    this._microscopeSlideEnableState$$.asObservable();
  public readonly microscopeSlideCutSize$ =
    this._microscopeSlideCutSize$$.asObservable();
  public readonly microscopeSlideIndex$ =
    this._microscopeSlideIndex$$.asObservable();

  public readonly microscopeSlideBoxVisibility$ =
    this._microscopeSlideBoxVisibility$$.asObservable();
  public readonly microscopeSlideBoxColor$ =
    this._microscopeSlideBoxColor$$.asObservable();

  public get pbcMode(): boolean {
    return this._pbcMode$$.value;
  }

  public set pbcMode(val: boolean) {
    if (val === this.pbcMode) return;

    this._pbcMode$$.next(val);
  }

  public get pbcContainerVisibleState(): boolean {
    return this._pbcContainerVisibility$$.value;
  }

  public set pbcContainerVisibleState(val: boolean) {
    if (val === this._pbcContainerVisibility$$.value) return;

    this._pbcContainerVisibility$$.next(val);
  }

  public get pbcContainerColor(): string {
    return this._pbcContainerColor$$.value;
  }

  public set pbcContainerColor(val: string) {
    if (val === this._pbcContainerColor$$.value) return;

    this._pbcContainerColor$$.next(val);
  }

  public get microscopeSlideEnableState(): boolean {
    return this._microscopeSlideEnableState$$.value;
  }

  public set microscopeSlideEnableState(val: boolean) {
    if (val === this._microscopeSlideEnableState$$.value) return;

    this._microscopeSlideEnableState$$.next(val);
  }

  public get microscopeSlideCutSize(): number {
    return this._microscopeSlideCutSize$$.value;
  }

  public set microscopeSlideCutSize(val: number) {
    const size = this.getContainerSize();
    if (val > size) val = size;
    else if (val < 1) val = 1;

    if (val === this._microscopeSlideCutSize$$.value) return;

    this._microscopeSlideCutSize$$.next(val);
  }

  public get microscopeSlideIndex(): number {
    const maxIndex = this.getMicroscopeSlideMaxIndex();
    if (this._microscopeSlideIndex$$.value > maxIndex) return maxIndex;

    return this._microscopeSlideIndex$$.value;
  }

  public set microscopeSlideIndex(val: number) {
    const maxIndex = this.getMicroscopeSlideMaxIndex();
    if (val > maxIndex) val = maxIndex;
    else if (val < 0) val = 0;

    if (val === this._microscopeSlideIndex$$.value) return;

    this._microscopeSlideIndex$$.next(val);
  }

  public get microscopeSlideBoxVisibleState(): boolean {
    return this._microscopeSlideBoxVisibility$$.value;
  }

  public set microscopeSlideBoxVisibleState(val: boolean) {
    if (val === this._microscopeSlideBoxVisibility$$.value) return;

    this._microscopeSlideBoxVisibility$$.next(val);
  }

  public get microscopeSlideBoxColor(): string {
    return this._microscopeSlideBoxColor$$.value;
  }

  public set microscopeSlideBoxColor(val: string) {
    if (val === this._microscopeSlideBoxColor$$.value) return;

    this._microscopeSlideBoxColor$$.next(val);
  }

  public constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _settingsService: SettingsService,
    private readonly _sessionManagerService: SessionManagerService
  ) {
    this._sessionManagerService.getCurrentSession.push(() => ({
      pbc: {
        enabled: this._pbcMode$$.value,
        containerVis: this._pbcContainerVisibility$$.value,
        containerColor: this._pbcContainerColor$$.value,
        mSlide: this._microscopeSlideEnableState$$.value,
        mSlideIndex: this._microscopeSlideIndex$$.value,
        mSlideSize: this._microscopeSlideCutSize$$.value,
        mSlideBox: this._microscopeSlideBoxVisibility$$.value,
        mSlideBoxColor: this._microscopeSlideBoxColor$$.value,
      },
    }));

    this._workspaceService.beforeSetTrajectory$.subscribe(() => {
      this._isFirstConfig = true;
    });

    this._workspaceService.beforeSetConfig$.subscribe((config) => {
      if (!config) return;

      const isFirstConfig = this._isFirstConfig;
      this._isFirstConfig = false;

      if (!config.config.parameters) return;

      const pbcContDim = config.config.parameters.pbcContainerStartDimension;

      if (!pbcContDim || !isFirstConfig || !this._settingsService.autoPBC.value)
        return;

      this._pbcMode$$.next(true);
    });

    this._sessionManagerService.loadSession$.subscribe((session) => {
      if (!session.pbc) return;

      if (session.pbc.containerColor)
        this._pbcContainerColor$$.next(session.pbc.containerColor);
      if (session.pbc.containerVis !== undefined)
        this._pbcContainerVisibility$$.next(session.pbc.containerVis);
      if (session.pbc.mSlide !== undefined)
        this._microscopeSlideEnableState$$.next(session.pbc.mSlide);
      if (session.pbc.mSlideIndex !== undefined)
        this._microscopeSlideIndex$$.next(session.pbc.mSlideIndex);
      if (session.pbc.mSlideSize !== undefined)
        this._microscopeSlideCutSize$$.next(session.pbc.mSlideSize);
      if (session.pbc.mSlideBox !== undefined)
        this._microscopeSlideBoxVisibility$$.next(session.pbc.mSlideBox);
      if (session.pbc.mSlideBoxColor !== undefined)
        this._microscopeSlideBoxColor$$.next(session.pbc.mSlideBoxColor);
    });
  }

  public getContainerSize(): number {
    return this.getContainerSizeFromConfig(
      this._workspaceService.config?.config
    );
  }

  public getContainerSizeFromConfig(config: Configuration | undefined): number {
    let containerSize =
      this._workspaceService.trajectory?.globalParameters
        ?.pbcContainerStartDimension ?? -1;
    const configContainerSize =
      config?.parameters?.pbcContainerStartDimension ?? 0;
    if (configContainerSize > 0) containerSize = configContainerSize;

    return containerSize;
  }

  public getCubeSizeLJ(): number {
    return this.getCubeSizeLJFromConfig(this._workspaceService.config?.config);
  }

  public getCubeSizeLJFromConfig(config: Configuration | undefined): number {
    let cubeSizeLJ =
      this._workspaceService.trajectory?.globalParameters?.cubeSizeLJ ?? -1;
    const configCubeSizeLJ = config?.parameters?.cubeSizeLJ ?? 0;
    if (configCubeSizeLJ > 0) cubeSizeLJ = configCubeSizeLJ;

    return cubeSizeLJ;
  }

  public getCubeSizeDNANucItr(): number {
    return this.getCubeSizeDNANucItrFromConfig(
      this._workspaceService.config?.config
    );
  }

  public getCubeSizeDNANucItrFromConfig(
    config: Configuration | undefined
  ): number {
    let cubeSizeDNANucItr =
      this._workspaceService.trajectory?.globalParameters
        ?.cubeSizeDNANucIntersection ?? -1;
    const configCubeSizeDNANucItr =
      config?.parameters?.cubeSizeDNANucIntersection ?? 0;
    if (configCubeSizeDNANucItr > 0)
      cubeSizeDNANucItr = configCubeSizeDNANucItr;

    return cubeSizeDNANucItr;
  }

  public getCubeSizeEstat(): number {
    return this.getCubeSizeEstatFromConfig(
      this._workspaceService.config?.config
    );
  }

  public getCubeSizeEstatFromConfig(config: Configuration | undefined): number {
    let cubeSizeEstat =
      this._workspaceService.trajectory?.globalParameters?.cubeSizeEstat ?? -1;
    const configCubeSizeEstat = config?.parameters?.cubeSizeEstat ?? 0;
    if (configCubeSizeEstat > 0) cubeSizeEstat = configCubeSizeEstat;

    return cubeSizeEstat;
  }

  public getMicroscopeSlideMaxIndex(): number {
    const size = this.getContainerSize();
    return Math.max(-1, Math.ceil(size / this.microscopeSlideCutSize) - 1);
  }
}
