/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import { pairwise } from 'rxjs/operators';
import { PbcService } from '../pbc.service';
import { LOCAL_STORAGE_SETTINGS_PBC } from '../../../shared/local-storage-keys.constants';

type DeserializedPBCSettings = {
  colors: {
    pbcContainer: string;
    microscopeSlideBox: string;
  };
};

@Injectable({
  providedIn: 'root',
})
export class PbcSettingsSerializerDeserializerService {
  public constructor(private readonly _pbcService: PbcService) {
    this.restoreColors();

    this._pbcService.pbcContainerColor$
      .pipe(pairwise())
      .subscribe(this.saveColors.bind(this));
    this._pbcService.microscopeSlideBoxColor$
      .pipe(pairwise())
      .subscribe(this.saveColors.bind(this));
  }

  private restoreColors(): void {
    const colorsStr = localStorage.getItem(LOCAL_STORAGE_SETTINGS_PBC);
    if (!colorsStr) {
      return;
    }

    try {
      this.apply(JSON.parse(colorsStr) as Partial<DeserializedPBCSettings>);
    } catch (e) {
      console.warn('Default settings could not restore.');
    }
  }

  private saveColors(): void {
    localStorage.setItem(
      LOCAL_STORAGE_SETTINGS_PBC,
      JSON.stringify({
        colors: {
          pbcContainer: this._pbcService.pbcContainerColor,
          microscopeSlideBox: this._pbcService.microscopeSlideBoxColor,
        },
      } as DeserializedPBCSettings)
    );
  }

  private apply(settings: Partial<DeserializedPBCSettings>): void {
    if (!settings.colors) return;

    if (settings.colors.pbcContainer)
      this._pbcService.pbcContainerColor = settings.colors.pbcContainer;

    if (settings.colors.microscopeSlideBox)
      this._pbcService.microscopeSlideBoxColor =
        settings.colors.microscopeSlideBox;
  }
}
