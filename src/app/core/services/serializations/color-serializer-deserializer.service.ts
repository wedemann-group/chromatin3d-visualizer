/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import { pairwise } from 'rxjs/operators';
import { ColorsService } from '../colors/colors.service';
import { LOCAL_STORAGE_DEFAULT_COLORS } from '../../../shared/local-storage-keys.constants';
import { DefaultColors } from '../colors/models/default-colors.model';

@Injectable({
  providedIn: 'root',
})
export class ColorSerializerDeserializerService {
  public constructor(private readonly _colorService: ColorsService) {
    this.restoreColors();

    this._colorService
      .getAllChangeableColorsWithConfigColor()
      .forEach((c) =>
        c.color$.pipe(pairwise()).subscribe(this.saveColors.bind(this))
      );

    this._colorService
      .getAllChangeableColorsWithoutConfigColor()
      .forEach((c) =>
        c.color$.pipe(pairwise()).subscribe(this.saveColors.bind(this))
      );
  }

  private restoreColors(): void {
    const colorsStr = localStorage.getItem(LOCAL_STORAGE_DEFAULT_COLORS);
    if (!colorsStr) {
      return;
    }

    try {
      this.setColorsFromParsedColors(JSON.parse(colorsStr) as DefaultColors);
    } catch (e) {
      console.warn('Default colors could not restore.');
    }
  }

  private setColorsFromParsedColors(colors: DefaultColors): void {
    const toCheck = [
      {
        color: colors.back,
        changeableColor: this._colorService.backColor,
      },
      {
        color: colors.nucleosome,
        changeableColor: this._colorService.histoneOctamerColors,
      },
      {
        color: colors.dna,
        changeableColor: this._colorService.dnaColors,
      },
      {
        color: colors.linkerDNA,
        changeableColor: this._colorService.linkerDNAColors,
      },
      {
        color: colors.basePair,
        changeableColor: this._colorService.basePairColors,
      },
      {
        color: colors.cohesinHead,
        changeableColor: this._colorService.cohesinHeadColors,
      },
      {
        color: colors.cohesinRingNHinge,
        changeableColor: this._colorService.cohesinRingAndHingeColors,
      },
      {
        color: colors.cohesinLinker,
        changeableColor: this._colorService.cohesinLinkerColors,
      },
      {
        color: colors.selectionColor,
        changeableColor: this._colorService.selectionColor,
      },
    ];

    for (const obj of toCheck.filter((o) => o.color))
      obj.changeableColor.color = obj.color!;
  }

  private saveColors(): void {
    localStorage.setItem(
      LOCAL_STORAGE_DEFAULT_COLORS,
      JSON.stringify({
        back: this._colorService.backColor.color,
        dna: this._colorService.dnaColors.color,
        linkerDNA: this._colorService.linkerDNAColors.color,
        nucleosome: this._colorService.histoneOctamerColors.color,
        cohesinHead: this._colorService.cohesinHeadColors.color,
        cohesinRingNHinge: this._colorService.cohesinRingAndHingeColors.color,
        cohesinLinker: this._colorService.cohesinLinkerColors.color,
        basePair: this._colorService.basePairColors.color,
        selectionColor: this._colorService.selectionColor.color,
      } as DefaultColors)
    );
  }
}
