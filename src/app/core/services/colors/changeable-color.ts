/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { BehaviorSubject, Observable } from 'rxjs';

export default class ChangeableColor {
  protected readonly color$$: BehaviorSubject<string>;

  public readonly color$: Observable<string>;

  public get color(): string {
    return this.color$$.value;
  }

  public set color(col: string) {
    this.color$$.next(col);
  }

  public constructor(defaultColor: string) {
    this.color$$ = new BehaviorSubject(defaultColor);
    this.color$ = this.color$$.asObservable();
  }
}
