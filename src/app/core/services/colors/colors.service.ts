/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import {
  DEFAULT_BACK_COLOR,
  DEFAULT_COLOR_COHESIN_HEAD,
  DEFAULT_COLOR_COHESIN_LINKER,
  DEFAULT_COLOR_COHESIN_RING,
  DEFAULT_COLOR_HISTONE_OCTAMER,
  DEFAULT_COLOR_LINKER_DNA,
  DEFAULT_COLOR_NUCLEOSOME_DNA,
  DEFAULT_COLOR_SELECTION,
} from '../../../shared/color.constants';
import { SessionManagerService } from '../session-mananger/session-manager.service';
import { WorkspaceService } from '../workspace.service';
import ChangeableColor from './changeable-color';
import ChangeableColorWithConfigColor from './changeable-color-with-config-color';
import { Session } from '../session-mananger/models/session.model';
import { DefaultColors } from './models/default-colors.model';
import { BeadColors } from './models/bead-colors.model';

@Injectable({
  providedIn: 'root',
})
export class ColorsService {
  public readonly backColor = new ChangeableColor(DEFAULT_BACK_COLOR);
  public readonly histoneOctamerColors = new ChangeableColorWithConfigColor(
    DEFAULT_COLOR_HISTONE_OCTAMER
  );
  public readonly dnaColors = new ChangeableColorWithConfigColor(
    DEFAULT_COLOR_NUCLEOSOME_DNA
  );
  public readonly linkerDNAColors = new ChangeableColorWithConfigColor(
    DEFAULT_COLOR_LINKER_DNA
  );
  public readonly basePairColors = new ChangeableColorWithConfigColor(
    DEFAULT_COLOR_LINKER_DNA
  );
  public readonly cohesinHeadColors = new ChangeableColorWithConfigColor(
    DEFAULT_COLOR_COHESIN_HEAD
  );
  public readonly cohesinRingAndHingeColors =
    new ChangeableColorWithConfigColor(DEFAULT_COLOR_COHESIN_RING);
  public readonly cohesinLinkerColors = new ChangeableColorWithConfigColor(
    DEFAULT_COLOR_COHESIN_LINKER
  );
  public readonly selectionColor = new ChangeableColor(DEFAULT_COLOR_SELECTION);

  private readonly _changeableColorsWithConfigColor = [
    this.histoneOctamerColors,
    this.dnaColors,
    this.linkerDNAColors,
    this.basePairColors,
    this.cohesinHeadColors,
    this.cohesinRingAndHingeColors,
    this.cohesinLinkerColors,
  ];

  private readonly _changeableColorsWithoutConfigColor = [
    this.backColor,
    this.selectionColor,
  ];

  public trajectoryHasCohesin = false;

  public constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _sessionManager: SessionManagerService
  ) {
    this._workspaceService.beforeSetTrajectory$.subscribe(
      this.restoreConfigColors.bind(this)
    );
    this._sessionManager.getCurrentSession.push(
      this.getSessionColors.bind(this)
    );
    this._sessionManager.loadSession$.subscribe(
      this.restoreDefaultColors.bind(this)
    );
  }

  public getSessionColors(): Partial<Session> {
    let defaultColors: DefaultColors = {
      dna: this.dnaColors.color,
      basePair: this.basePairColors.color,
      back: this.backColor.color,
      nucleosome: this.histoneOctamerColors.color,
      linkerDNA: this.linkerDNAColors.color,
      selection: this.selectionColor.color,
    };
    let beadColors: BeadColors = {
      nucleosomes: [this.histoneOctamerColors.configSpecificColors],
      linkerDNA: [this.linkerDNAColors.configSpecificColors],
      dna: [this.dnaColors.configSpecificColors],
      basePair: [this.basePairColors.configSpecificColors],
    };

    if (this.trajectoryHasCohesin) {
      defaultColors = {
        ...defaultColors,
        cohesinHead: this.cohesinHeadColors.color,
        cohesinRingNHinge: this.cohesinRingAndHingeColors.color,
        cohesinLinker: this.cohesinLinkerColors.color,
      };

      beadColors = {
        ...beadColors,
        cohesinHead: [this.cohesinHeadColors.configSpecificColors],
        cohesinRingNHinge: [
          this.cohesinRingAndHingeColors.configSpecificColors,
        ],
        cohesinLinker: [this.cohesinLinkerColors.configSpecificColors],
      };
    }

    return {
      defaultColors,
      beadColors,
    };
  }

  public getAllChangeableColorsWithConfigColor(): ChangeableColorWithConfigColor[] {
    return this._changeableColorsWithConfigColor;
  }

  public getAllChangeableColorsWithoutConfigColor(): ChangeableColor[] {
    return this._changeableColorsWithoutConfigColor;
  }

  private restoreDefaultColors(session: Partial<Session>): void {
    if (!session.defaultColors) return;

    const toCheck = [
      {
        value: session.defaultColors.back,
        colorClass: this.backColor,
      },
      {
        value: session.defaultColors.nucleosome,
        colorClass: this.histoneOctamerColors,
      },
      {
        value: session.defaultColors.linkerDNA,
        colorClass: this.linkerDNAColors,
      },
      {
        value: session.defaultColors.basePair,
        colorClass: this.basePairColors,
      },
      {
        value: session.defaultColors.dna,
        colorClass: this.dnaColors,
      },
      {
        value: session.defaultColors.cohesinHead,
        colorClass: this.cohesinHeadColors,
      },
      {
        value: session.defaultColors.cohesinRingNHinge,
        colorClass: this.cohesinRingAndHingeColors,
      },
      {
        value: session.defaultColors.cohesinLinker,
        colorClass: this.cohesinLinkerColors,
      },
      {
        value: session.defaultColors.selection,
        colorClass: this.selectionColor,
      },
    ];

    for (const restoreOption of toCheck.filter((o) => o.value))
      restoreOption.colorClass.color = restoreOption.value!;
  }

  private restoreConfigColors(session: Session | undefined): void {
    this.resetColors();

    if (!session || !session.beadColors) return;

    const toCheck = [
      {
        colorBeads: session.beadColors.nucleosomes,
        colorClass: this.histoneOctamerColors,
      },
      {
        colorBeads: session.beadColors.linkerDNA,
        colorClass: this.linkerDNAColors,
      },
      {
        colorBeads: session.beadColors.dna,
        colorClass: this.dnaColors,
      },
      {
        colorBeads: session.beadColors.cohesinHead,
        colorClass: this.cohesinHeadColors,
      },
      {
        colorBeads: session.beadColors.cohesinRingNHinge,
        colorClass: this.cohesinRingAndHingeColors,
      },
      {
        colorBeads: session.beadColors.cohesinLinker,
        colorClass: this.cohesinLinkerColors,
      },
    ];

    for (const restoreOption of toCheck) {
      if (restoreOption.colorBeads && restoreOption.colorBeads.length > 0)
        restoreOption.colorClass.overrideColors(restoreOption.colorBeads[0]);
    }
  }

  private resetColors(): void {
    this._changeableColorsWithConfigColor.forEach((cc) => cc.clear());
  }
}
