/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Subject } from 'rxjs';
import { MathUtils } from 'three';
import ChangeableColor from './changeable-color';
import { ColorBead } from './models/color-bead.model';

export default class ChangeableColorWithConfigColor extends ChangeableColor {
  private readonly _sessionID: string;

  private _configSpecificColors: ColorBead[] = [];

  public readonly configSpecificColors$ = new Subject<ColorBead[]>();

  public get configSpecificColors(): ColorBead[] {
    return [...this._configSpecificColors];
  }

  public get sessionID(): string {
    return this._sessionID;
  }

  public constructor(defaultColor: string) {
    super(defaultColor);

    this._sessionID = MathUtils.generateUUID();
  }

  public setConfigSpecificColors(color: string, indicies: number[]): void {
    const colorEqualsDefaultColor = color === this.color$$.value;

    if (colorEqualsDefaultColor) {
      this._configSpecificColors = this._configSpecificColors.filter(
        (cb) => !indicies.includes(cb.index)
      );
    } else {
      for (const id of indicies) {
        const ele = this._configSpecificColors.findIndex(
          (cb) => cb.index === id
        );
        if (ele < 0) this._configSpecificColors.push({ index: id, color });
        else this._configSpecificColors[ele].color = color;
      }
    }

    this.configSpecificColors$.next(
      indicies
        .map((index) => ({ index, color }))
        .sort((a, b) => a.index - b.index)
    );
  }

  public setConfigSpecificColorsFromColorBeads(colorBeads: ColorBead[]): void {
    for (const colorBead of colorBeads) {
      const colorEqualsDefaultColor = colorBead.color === this.color$$.value;

      if (colorEqualsDefaultColor) {
        this._configSpecificColors = this._configSpecificColors.filter(
          (cb) => cb.index != colorBead.index
        );
      } else {
        const ele = this._configSpecificColors.findIndex(
          (cb) => cb.index === colorBead.index
        );
        // flat copy
        if (ele < 0) this._configSpecificColors.push({ ...colorBead });
        else this._configSpecificColors[ele].color = colorBead.color;
      }
    }

    colorBeads.sort((a, b) => a.index - b.index);

    this.configSpecificColors$.next(colorBeads);
  }

  public overrideColors(colors: ColorBead[]): void {
    this._configSpecificColors = colors;
  }

  public clear(): void {
    this._configSpecificColors = [];
  }
}
