/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Intersection, Vector2 } from 'three';
import {
  DEFAULT_COLOR_DISTANCE_LINE,
  DEFAULT_COLOR_DISTANCE_TEXT,
} from '../../shared/color.constants';

const DEFAULT_LINE_WIDTH = 5;
const DEFAULT_TEXT_SIZE = 2;

type TempPoint = Intersection & {
  point2D: Vector2;
};

@Injectable({
  providedIn: 'root',
})
export class DistanceMeasurementService {
  private readonly _lineColor$$ = new BehaviorSubject<string>(
    DEFAULT_COLOR_DISTANCE_LINE
  );
  private readonly _lineWidth$$ = new BehaviorSubject<number>(
    DEFAULT_LINE_WIDTH
  );
  private readonly _textColor$$ = new BehaviorSubject<string>(
    DEFAULT_COLOR_DISTANCE_TEXT
  );
  private readonly _textSize$$ = new BehaviorSubject<number>(DEFAULT_TEXT_SIZE);
  private readonly _isDistanceMeasurement$$ = new BehaviorSubject<boolean>(
    false
  );
  private readonly _firstPoint$$ = new BehaviorSubject<TempPoint | null>(null);
  private readonly _secondPoint$$ = new BehaviorSubject<TempPoint | null>(null);

  public readonly lineColor$ = this._lineColor$$.asObservable();
  public readonly lineWidth$ = this._lineWidth$$.asObservable();
  public readonly textColor$ = this._textColor$$.asObservable();
  public readonly textSize$ = this._textSize$$.asObservable();
  public readonly isDistanceMeasurement$ =
    this._isDistanceMeasurement$$.asObservable();
  public readonly firstPoint$ = this._firstPoint$$.asObservable();
  public readonly secondPoint$ = this._secondPoint$$.asObservable();

  public get lineColor(): string {
    return this._lineColor$$.value;
  }

  public set lineColor(val: string) {
    if (val === undefined || val === null || this._lineColor$$.value === val)
      return;

    this._lineColor$$.next(val);
  }

  public get lineWidth(): number {
    return this._lineWidth$$.value;
  }

  public set lineWidth(val: number) {
    if (val === undefined || val === null || this._lineWidth$$.value === val)
      return;

    this._lineWidth$$.next(val);
  }

  public get textColor(): string {
    return this._textColor$$.value;
  }

  public set textColor(val: string) {
    if (val === undefined || val === null || this._textColor$$.value === val)
      return;

    this._textColor$$.next(val);
  }

  public get textSize(): number {
    return this._textSize$$.value;
  }

  public set textSize(val: number) {
    if (val === undefined || val === null || this._textSize$$.value === val)
      return;

    this._textSize$$.next(val);
  }

  public get isDistanceMeasurement(): boolean {
    return this._isDistanceMeasurement$$.value;
  }

  public set isDistanceMeasurement(val: boolean) {
    if (this._isDistanceMeasurement$$.value === val) return;

    this._isDistanceMeasurement$$.next(val);
  }

  public get firstPoint(): TempPoint | null {
    return this._firstPoint$$.value;
  }

  public set firstPoint(val: TempPoint | null) {
    if (!val) {
      this.secondPoint = val;
    }

    if (this._firstPoint$$.value === val) return;

    this._firstPoint$$.next(val);
  }

  public get secondPoint(): TempPoint | null {
    return this._secondPoint$$.value;
  }

  public set secondPoint(val: TempPoint | null) {
    if (this._secondPoint$$.value === val) return;

    this._secondPoint$$.next(val);

    if (val) this.isDistanceMeasurement = false;
  }
}
