/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { TestBed } from '@angular/core/testing';

import { ViewService } from './view.service';
import { WorkspaceService } from '../workspace.service';
import { WorkspaceService as MockWorkspaceService } from '../__mocks__/workspace.service';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader';

describe('ViewService', () => {
  let service: ViewService;

  beforeEach(() => {
    spyOn(FontLoader.prototype, 'load');
    TestBed.configureTestingModule({
      providers: [
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
      ],
    });
    service = TestBed.inject(ViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
