/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Euler, Vector3 } from 'three';
import { generateUUID } from 'three/src/math/MathUtils';
import { WorkspaceService } from '../workspace.service';
import { PbcService } from '../pbc.service';
import {
  computeCohesinHingePosition,
  getBeadPosition,
  getRotationFromNucleosome,
} from '../../../shared/utilities/bead.utility';
import SoftwareNucleosomeBead from '../../../shared/models/sw-nucleosome-bead.model';
import NucleosomeBead from '../../../shared/models/nucleosome-bead.model';
import { EControlType } from '../../../modules/dockable-workspace/viewer/models/control-type.enum';
import Position from '../../../shared/models/position.model';
import Bead from '../../../shared/models/bead.model';
import { ECameraSetter } from '../../../modules/dockable-workspace/viewer/models/camera-setter.enum';
import { ContainerDimension } from './models/container-dimension.model';
import CohesinHeadBead from '../../../shared/models/cohesin-head-bead.model';
import { Configuration } from '../../../shared/models/configuration.model';
import { ContainerDimensionExtended } from './models/container-dimension-extended.model';
import {
  getBoxDimension,
  getBoxDimensionCenterAndSizesFromDim,
} from '../../../shared/utilities/three.js.utility';
import { EMaterialType } from '../../../shared/models/material-type.enum';
import { SessionManagerService } from '../session-mananger/session-manager.service';
import { Session } from '../session-mananger/models/session.model';
import { ParsedConfiguration } from '../../../shared/parsers/models/parsed-configuration.model';
import { SUPPORTED_FILE_EXTENSION_CSV } from '../../../shared/supported-files.constants';
import { SerializableCamera } from '../../../modules/dockable-workspace/viewer/models/serializable-camera.model';
import { SettingsService } from '../settings/settings.service';
import { DefinedCamera } from '../../../modules/dockable-workspace/tools/tool-defined-camera-positions/models/defined-camera.model';
import { LOCAL_STORAGE_DEFINED_CAMERA_POSITIONS } from '../../../shared/local-storage-keys.constants';
import { SerializedDefinedCamera } from '../../../modules/dockable-workspace/tools/tool-defined-camera-positions/models/serialized-defined-camera.model';
import {
  deserializeCamera,
  serializeCamera,
} from '../../../modules/dockable-workspace/viewer/camera/camera.utility';

const EPS_POSITION_DIFFERENCE = 0.01;

@Injectable({
  providedIn: 'root',
})
export class ViewService {
  private readonly _rerender$$ = new BehaviorSubject<null>(null);
  private readonly _revisibility$$ = new BehaviorSubject<null>(null);
  private readonly _dispose$$ = new BehaviorSubject<null>(null);
  private readonly _controlsType$$ = new BehaviorSubject<EControlType>(
    EControlType.firstPersonPerspective
  );
  private readonly _cameraSetType$$ = new BehaviorSubject<ECameraSetter>(
    ECameraSetter.none
  );
  private readonly _materialType$$ = new BehaviorSubject<EMaterialType>(
    EMaterialType.NORMAL
  );
  private readonly _definedCameras$$ = new BehaviorSubject<DefinedCamera[]>([]);

  public readonly glossyMaterial$$ = new BehaviorSubject<boolean>(true);
  public readonly rerender$ = this._rerender$$.asObservable();
  public readonly revisibility$ = this._revisibility$$.asObservable();
  public readonly dispose$ = this._dispose$$.asObservable();
  public readonly controls$ = this._controlsType$$.asObservable();
  public readonly cameraSetType$ = this._cameraSetType$$.asObservable();
  public readonly materialType$ = this._materialType$$.asObservable();
  public readonly definedCameras$ = this._definedCameras$$.asObservable();

  public readonly showCoordinateCross$$ = new BehaviorSubject<boolean>(true);
  public readonly setCameraToCenterOfMass$ = new Subject<null>();

  public readonly cameraRotation$$ = new BehaviorSubject<Euler>(new Euler());
  public readonly cameraUpVector$$ = new BehaviorSubject<Vector3>(
    new Vector3()
  );

  public readonly cameraGetter$$ = new Subject<
    (camera: SerializableCamera) => void
  >();
  public readonly cameraSetter$$ = new Subject<SerializableCamera>();
  public readonly cameraGetter$ = this.cameraGetter$$.asObservable();
  public readonly cameraSetter$ = this.cameraSetter$$.asObservable();

  private _linkerDNASplitter: number[] = [];
  private _invisibleIDs: number[] = [];
  private _isFirstConfig = true;

  public get linkerDNASplitter(): number[] {
    if (!this._pbcService.pbcMode) return [];

    return this._linkerDNASplitter;
  }

  public get invisibleIDs(): number[] {
    if (
      !this._workspaceService.config || // -1 if trj is unloaded
      this._pbcService.getContainerSizeFromConfig(
        this._workspaceService.config.config
      ) <= 0 || // has pbc
      !this._pbcService.pbcMode || // pbc is enabled
      !this._pbcService.microscopeSlideEnableState // microscope slide is enabled
    )
      return [];

    return this._invisibleIDs;
  }

  public get materialType(): EMaterialType {
    return this._materialType$$.value;
  }

  public set materialType(val: EMaterialType) {
    this._materialType$$.next(val);
  }

  public get definedCameras(): DefinedCamera[] {
    return this._definedCameras$$.value;
  }

  public constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _pbcService: PbcService,
    private readonly _sessionManagerService: SessionManagerService,
    private readonly _settingsService: SettingsService
  ) {
    this.restoreDefinedCameraPositions();
    this._sessionManagerService.getCurrentSession.push(
      () =>
        ({
          material: this.materialType,
          glossy: this.glossyMaterial$$.value,
        } as Partial<Session>)
    );
    this._sessionManagerService.loadSession$.subscribe((session) => {
      if (session.material !== undefined) this.materialType = session.material;
      if (session.glossy !== undefined)
        this.glossyMaterial$$.next(session.glossy);
    });
    this._workspaceService.trajectory$.subscribe(() => {
      this._isFirstConfig = true;
    });
    this._workspaceService.config$
      .pipe(
        filter((c) => !!c),
        map((c) => c as ParsedConfiguration)
      )
      .subscribe(() => {
        if (!this._isFirstConfig || !this._settingsService.autoFindFiber.value)
          return;

        this._isFirstConfig = false;
        this.setCameraToCenterOfMass$.next(null);
      });
    // disable "auto find fiber" while loading a zip with session options with valid camera settings
    this._sessionManagerService.loadSession$
      .pipe(filter((s) => !!s.camera))
      .subscribe(() => {
        this._isFirstConfig = false;
      });
    this._workspaceService.config$.subscribe(this.configChanged.bind(this));
    this._pbcService.pbcMode$.subscribe(this.pbcModeChanged.bind(this));
    this._pbcService.microscopeSlideEnableState$.subscribe(
      this.microscopeSlideChanged.bind(this)
    );
    this._pbcService.microscopeSlideIndex$.subscribe(
      this.microscopeSlideChanged.bind(this)
    );
    this._pbcService.microscopeSlideCutSize$.subscribe(
      this.microscopeSlideChanged.bind(this)
    );
    this.definedCameras$.subscribe(this.saveDefinedCameraPositions.bind(this));
  }

  private static getPosFromBead(bead: Bead): Position {
    if (bead instanceof NucleosomeBead) return bead.center;

    return bead.position;
  }

  public setControlType(controls: EControlType): void {
    this._controlsType$$.next(controls);
  }

  public setCameraSetterType(setter: ECameraSetter): void {
    this._cameraSetType$$.next(setter);
  }

  public setDefinedCameras(cameras: DefinedCamera[]): void {
    this._definedCameras$$.next(cameras);
  }

  public getContainerSizeOfPBCOrFiber(): ContainerDimension {
    const pbcContainerSize = this._pbcService.getContainerSize();
    if (this._pbcService.pbcMode && pbcContainerSize > 0) {
      return {
        xStart: 0,
        yStart: 0,
        zStart: 0,
        xEnd: pbcContainerSize,
        yEnd: pbcContainerSize,
        zEnd: pbcContainerSize,
      };
    }

    return getBoxDimension(
      this._workspaceService.config?.config?.beads?.map((b) => b.position) ?? []
    );
  }

  public getContainerSizeOfPBCOrFiberWithCenterPoint(): ContainerDimensionExtended {
    return getBoxDimensionCenterAndSizesFromDim(
      this.getContainerSizeOfPBCOrFiber()
    );
  }

  public setBeadsFromConfiguration(
    configuration: Configuration,
    containerSize: number
  ): void {
    const beads = configuration.beads ?? [];

    for (const bead of beads) {
      bead.drawPosition = getBeadPosition(bead, containerSize);

      if (bead instanceof SoftwareNucleosomeBead)
        bead.rotation = getRotationFromNucleosome(
          bead as NucleosomeBead,
          containerSize
        );
    }

    const heads = beads.filter((b) => b instanceof CohesinHeadBead);
    for (const ring of configuration.cohesinRings) {
      ring.hingePosition = computeCohesinHingePosition(heads, ring); // configuration.parameters?.cohDistance
    }
  }

  private restoreDefinedCameraPositions(): void {
    const camerasStr = localStorage.getItem(
      LOCAL_STORAGE_DEFINED_CAMERA_POSITIONS
    );
    if (!camerasStr) return;

    try {
      const cameraList: DefinedCamera[] = [];
      const cameras = JSON.parse(camerasStr) as SerializedDefinedCamera[];
      for (const camera of cameras) {
        cameraList.push({
          camera: deserializeCamera(camera.camera),
          name: camera.name,
          id: camera.id ?? generateUUID(), // possible null coz old version...
          wasGenerated:
            camera.wasGenerated === undefined ? false : camera.wasGenerated,
        });
      }

      this._definedCameras$$.next(cameraList);
    } catch (e) {
      console.warn('Defined camera positions are invalid!', e);
    }
  }

  private configChanged(config: ParsedConfiguration | null): void {
    this._invisibleIDs = [];
    if (!this._workspaceService.trajectory || !config) {
      this._dispose$$.next(null);
      return;
    }

    const containerSize = !this._pbcService.pbcMode
      ? -1
      : this._pbcService.getContainerSizeFromConfig(config.config);
    if (
      !this._workspaceService.currentFile?.name
        .toLowerCase()
        .endsWith(`.${SUPPORTED_FILE_EXTENSION_CSV}`)
    )
      this.computeBeadPositionAndRotation(config.config, containerSize);
    this.setLinkerDNASplitIndices(config.config, containerSize);

    this._rerender$$.next(null);

    if (this._pbcService.microscopeSlideEnableState)
      this.microscopeSlideChanged();
  }

  private pbcModeChanged(): void {
    this.configChanged(this._workspaceService.config);

    if (this._pbcService.microscopeSlideEnableState)
      this._revisibility$$.next(null);
  }

  private microscopeSlideChanged(): void {
    this.setInvisibleBeadIDs();
    this._revisibility$$.next(null);
  }

  private setInvisibleBeadIDs(): void {
    this._invisibleIDs = [];

    if (
      !this._pbcService.pbcMode ||
      !this._pbcService.microscopeSlideEnableState
    )
      return;

    if (!this._workspaceService.trajectory || !this._workspaceService.config) {
      return;
    }

    const beads = this._workspaceService.config.config.beads;

    for (const bead of beads) {
      if (this.isBeadOutOfMicroscopeSlideView(bead))
        this._invisibleIDs.push(bead.id);
    }

    this._invisibleIDs.sort((a, b) => a - b);
  }

  private computeBeadPositionAndRotation(
    config: Configuration | undefined,
    containerSize: number
  ): void {
    if (!this._workspaceService.trajectory || !config) return;

    const beads = config.beads ?? [];

    for (const bead of beads) {
      bead.drawPosition = getBeadPosition(bead, containerSize);

      if (bead instanceof SoftwareNucleosomeBead)
        bead.rotation = getRotationFromNucleosome(
          bead as NucleosomeBead,
          containerSize
        );
    }

    const heads = beads.filter((b) => b instanceof CohesinHeadBead);
    for (const ring of config.cohesinRings) {
      ring.hingePosition = computeCohesinHingePosition(heads, ring); // configuration.parameters?.cohDistance
    }
  }

  private setLinkerDNASplitIndices(
    config: Configuration,
    containerSize: number
  ): void {
    if (containerSize <= 0) {
      this._linkerDNASplitter = [];
      return;
    }

    const linkerSplicer: number[] = [];

    const beads = config?.beads ?? [];
    for (let i = 0; i < beads.length; ++i) {
      let splitLinkerDNAThisIndex = false; // beads[i] instanceof NucleosomeBead

      if (i > 0) {
        const drawPos = beads[i].drawPosition
          .clone()
          .sub(beads[i - 1].drawPosition);
        const pos = ViewService.getPosFromBead(beads[i])
          .clone()
          .sub(ViewService.getPosFromBead(beads[i - 1]));
        const diff = drawPos.sub(pos);
        splitLinkerDNAThisIndex =
          Math.abs(diff.x) > EPS_POSITION_DIFFERENCE ||
          Math.abs(diff.y) > EPS_POSITION_DIFFERENCE ||
          Math.abs(diff.z) > EPS_POSITION_DIFFERENCE;
      }

      if (splitLinkerDNAThisIndex) linkerSplicer.push(i);
    }

    this._linkerDNASplitter = linkerSplicer;
  }

  private isBeadOutOfMicroscopeSlideView(bead: Bead): boolean {
    return (
      bead.drawPosition.x <
        this._pbcService.microscopeSlideIndex *
          this._pbcService.microscopeSlideCutSize ||
      bead.drawPosition.x >
        (this._pbcService.microscopeSlideIndex + 1) *
          this._pbcService.microscopeSlideCutSize
    );
  }

  private saveDefinedCameraPositions(): void {
    localStorage.setItem(
      LOCAL_STORAGE_DEFINED_CAMERA_POSITIONS,
      JSON.stringify(
        this._definedCameras$$.value.map(
          (c) =>
            ({
              name: c.name,
              camera: serializeCamera(c.camera),
              id: c.id,
              wasGenerated: c.wasGenerated,
            } as SerializedDefinedCamera)
        )
      )
    );
  }
}
