/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Notification } from '../../shared/components/notification-center/models/notification.model';

@Injectable({
  providedIn: 'root',
})
export class PopUpService {
  private readonly _popUpAdded$$ = new Subject<Notification>();
  public readonly popUpAdded$ = this._popUpAdded$$.asObservable();

  public addNotification(notification: Notification): void {
    this._popUpAdded$$.next(notification);
  }
}
