/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { AngularResizeEventModule } from 'angular-resize-event';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { throwIfAlreadyLoaded } from './guards/module-import.guard';
import { MenuStripModule } from './components/menu-strip/menu-strip.module';
import { MenuStripComponent } from './components/menu-strip/menu-strip.component';
import { FooterComponent } from './components/footer/footer.component';
import { WorkspaceComponent } from './components/workspace/workspace.component';
import { DropZoneComponent } from './components/drop-zone/drop-zone.component';
import { SharedModule } from '../shared/shared.module';
import { TrajectorySelectorModule } from './components/trajectory-selector/trajectory-selector.module';

@NgModule({
  declarations: [DropZoneComponent, FooterComponent, WorkspaceComponent],
  imports: [
    AngularResizeEventModule,
    CommonModule,
    MenuStripModule,
    MatIconModule,
    MatButtonModule,
    NgxDropzoneModule,
    SharedModule,
    TrajectorySelectorModule,
  ],
  exports: [FooterComponent, MenuStripComponent, WorkspaceComponent],
})
export class CoreModule {
  public constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
