/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { BehaviorSubject, Observable } from 'rxjs';

export default class ChangeableOption<TVal> {
  private readonly _value$$: BehaviorSubject<TVal>;
  public readonly value$: Observable<TVal>;

  public get value(): TVal {
    return this._value$$.value;
  }

  public set value(val: TVal) {
    if (this.isValueEquals(val)) return;

    this._value$$.next(val);
  }

  public constructor(initVal: TVal) {
    this._value$$ = new BehaviorSubject<TVal>(initVal);
    this.value$ = this._value$$.asObservable();
  }

  protected isValueEquals(val: TVal): boolean {
    return this._value$$.value === val;
  }
}
