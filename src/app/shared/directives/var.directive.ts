/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

/**
 * **Note:** The directive must start with "ng" (I don't know why) and is copied from StackOverflow.
 * @source https://stackoverflow.com/a/43172992
 */
@Directive({
  selector: '[ngVar]',
})
export class VarDirective {
  @Input()
  public set ngVar(context: unknown) {
    this._context.$implicit = this._context.ngVar = context;

    if (!this._hasView) {
      this._vcRef.createEmbeddedView(this._templateRef, this._context);
      this._hasView = true;
    }
  }

  private _context: {
    $implicit: unknown;
    ngVar: unknown;
  } = {
    $implicit: null,
    ngVar: null,
  };

  private _hasView: boolean = false;

  public constructor(
    private readonly _templateRef: TemplateRef<any>,
    private readonly _vcRef: ViewContainerRef
  ) {}
}
