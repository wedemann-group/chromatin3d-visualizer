/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Directive, InjectionToken } from '@angular/core';
import { ComponentContainer } from 'golden-layout';
import { SubscribeEvents } from '../../core/classes/subscribe-events';
import { BREAK_POINT_TAB_SMALL } from '../../constants';

const GOLDEN_LAYOUT_CONTAINER_TOKEN_NAME = 'GoldenLayoutContainer';

@Directive({
  selector: '[appBaseTabComponent]',
})
export class BaseTabComponentDirective extends SubscribeEvents {
  public isSmall = false; // must public for access inside the html template

  // eslint-disable-next-line @typescript-eslint/member-ordering
  public static GOLDEN_LAYOUT_CONTAINER_INJECTION_TOKEN =
    new InjectionToken<ComponentContainer>(GOLDEN_LAYOUT_CONTAINER_TOKEN_NAME);

  protected subscribeTabResizeForBreakpoints(
    container: ComponentContainer
  ): void {
    container.addEventListener('resize', () => {
      this.isSmall = container.width < BREAK_POINT_TAB_SMALL;
    });
  }
}
