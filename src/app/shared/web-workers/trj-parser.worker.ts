/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

/// <reference lib="webworker" />

import { BlockTrajectoryParser } from '../parsers/block-trajectory/block-trajectory.parser';
import ConfigurationParser from '../parsers/configuration-parser.abstract';
import BlockParser from '../parsers/block-parser.abstract';
import { ExchangeError } from './models/exchange-error';
import { CachedTrajectory } from '../parsers/models/cached-trajectory.model';
import { ExchangeFormat } from './models/exchange-format.model';
import { ReadProgress } from './models/read-progress.model';
import {
  EXCHANGE_CONFIGURATION,
  EXCHANGE_ERROR,
  EXCHANGE_LOAD_TRAJECTORY,
  EXCHANGE_PARTIAL_TRAJECTORY,
  EXCHANGE_READING_PROGRESS,
} from './constants';
import { ExchangeConfiguration } from './models/exchange-configuration.model';
import PartialTrajectory from '../parsers/models/partial-trajectory.model';
import FastXmlParser from '../parsers/fast-xml/fast-xml.parser';
import BlockTrajectoryConfigurationParser from '../parsers/block-trajectory/block-trajectory-configuration.parser';
import XmlConfigurationParser from '../parsers/fast-xml/xml-configuration.parser';
import AiPredictionParser from '../parsers/ai-prediction/ai-prediction.parser';
import AiPredictionConfigurationReaderParser from '../parsers/ai-prediction/ai-prediction-configuration-reader.parser';

const PARSERS: (typeof BlockParser & { new (): BlockParser })[] = [
  BlockTrajectoryParser,
  FastXmlParser,
  AiPredictionParser,
];

const CONFIG_PARSERS: (typeof ConfigurationParser & {
  new (): ConfigurationParser;
})[] = [
  BlockTrajectoryConfigurationParser,
  XmlConfigurationParser,
  AiPredictionConfigurationReaderParser,
];

let currentTrajectory: CachedTrajectory | undefined;

addEventListener(
  'message',
  async ({
    data: input,
  }: MessageEvent<ExchangeFormat<File | number>>): Promise<void> => {
    if (
      input.message === EXCHANGE_LOAD_TRAJECTORY &&
      input.data instanceof File
    ) {
      await parseFile(input.data);
    } else if (input.message === EXCHANGE_CONFIGURATION) {
      await getConfiguration(input.data as number);
    }
  }
);

async function parseFile(file: File): Promise<void> {
  currentTrajectory = undefined;

  try {
    const parser = PARSERS.find((p) => p.useThisParser(file.name));
    if (!parser) throw new Error('No parser found!');

    // actually it is not an abstract class, it will be class that inherits from the abstract class
    // so just ignore the wrong ts issue
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const instanceOfParser = new parser();
    instanceOfParser.readBytesChanged.subscribe((readBytes: number) =>
      postMessage({
        message: EXCHANGE_READING_PROGRESS,
        data: { current: readBytes, max: file.size } as ReadProgress,
      } as ExchangeFormat<ReadProgress>)
    );
    currentTrajectory = await instanceOfParser.parse(file);
    console.log(currentTrajectory);
    postMessage({
      message: EXCHANGE_READING_PROGRESS,
      data: { current: file.size, max: file.size } as ReadProgress,
    } as ExchangeFormat<ReadProgress>);
    postMessage({
      message: EXCHANGE_PARTIAL_TRAJECTORY,
      data: currentTrajectory!.trajectory,
    } as ExchangeFormat<PartialTrajectory>);
  } catch (e: any) {
    console.error(e);
    postMessage({
      message: EXCHANGE_ERROR,
      data: { errorCode: -1, errorMessage: e.message },
    } as ExchangeFormat<ExchangeError>);
  }
}

async function getConfiguration(configIndex: number): Promise<void> {
  try {
    if (!currentTrajectory) throw new Error('Trajectory is not loaded!');

    if (configIndex >= currentTrajectory.configurations.length)
      throw new Error('Invalid configuration index!');

    const config = currentTrajectory.configurations[configIndex];

    const parser = CONFIG_PARSERS.find((p) =>
      p.useThisParser(currentTrajectory!.file.name)
    );
    if (!parser) throw new Error('No parser found!');

    postMessage({
      message: EXCHANGE_CONFIGURATION,
      data: {
        index: configIndex,
        // actually it is not an abstract class, it will be class that inherits from the abstract class
        // so just ignore the wrong ts issue
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        config: await new parser().parse(currentTrajectory.file, config),
      } as ExchangeConfiguration,
    });
  } catch (e: any) {
    console.error(e);
    postMessage({
      message: EXCHANGE_ERROR,
      data: { errorCode: -1, errorMessage: e.message },
    } as ExchangeFormat<ExchangeError>);
  }
}
