/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component } from '@angular/core';
import { Notification } from './models/notification.model';
import { animate, style, transition, trigger } from '@angular/animations';
import { ANIMATION_SHOW_HIDE_DURATION } from './constants';
import { PopUpService } from '../../../core/services/pop-up.service';

@Component({
  selector: 'trj-notification-center',
  templateUrl: './notification-center.component.html',
  styleUrls: ['./notification-center.component.scss'],
  animations: [
    trigger('openClose', [
      transition('* => void', [
        animate(
          ANIMATION_SHOW_HIDE_DURATION,
          style({ opacity: 0, height: 0, marginTop: 0 })
        ),
      ]),
      transition('void => *', [
        animate(ANIMATION_SHOW_HIDE_DURATION, style({ opacity: 1 })),
      ]),
    ]),
  ],
})
export class NotificationCenterComponent {
  public readonly notifications: Notification[] = [];

  constructor(private readonly _popUpService: PopUpService) {
    this._popUpService.popUpAdded$.subscribe((notification) =>
      this.notifications.push(notification)
    );
  }

  closePopUp(popup: number): void {
    if (popup >= 0 && popup < this.notifications.length)
      this.notifications.splice(popup, 1);
  }
}
