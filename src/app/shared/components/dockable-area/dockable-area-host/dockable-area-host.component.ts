/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  AfterViewInit,
  ApplicationRef,
  Component,
  ComponentRef,
  ElementRef,
  EmbeddedViewRef,
  Input,
  OnDestroy,
} from '@angular/core';
import {
  ComponentContainer,
  ComponentItem,
  EventEmitter,
  GoldenLayout,
  LayoutConfig,
  ResolvedComponentItemConfig,
  ResolvedLayoutConfig,
} from 'golden-layout';
import { BaseTabComponentDirective } from '../../../directives/base-tab-component.directive';
import { DockingSuiteHelperService } from '../../../../core/services/docking-suite-helper/docking-suite-helper.service';

@Component({
  selector: 'trj-dockable-area-host[fallbackLayout]',
  template: '',
  styleUrls: [],
})
export class DockableAreaHostComponent implements AfterViewInit, OnDestroy {
  @Input()
  public layout: LayoutConfig | undefined;

  @Input()
  public fallbackLayout!: LayoutConfig;

  private readonly _goldenLayout: GoldenLayout;
  private _containerMap = new Map<
    ComponentContainer,
    ComponentRef<BaseTabComponentDirective>
  >();

  constructor(
    private readonly _elRef: ElementRef<HTMLElement>,
    private readonly _appRef: ApplicationRef,
    private readonly _dockingSuiteHelperService: DockingSuiteHelperService
  ) {
    this._goldenLayout = new GoldenLayout(this._elRef.nativeElement);
    this._goldenLayout.bindComponentEvent = (container, itemConfig) =>
      this.handleGetComponentEvent(container, itemConfig);
    this._goldenLayout.unbindComponentEvent = (
      container // optional: , component
    ) => this.handleReleaseComponentEvent(container);

    this._goldenLayout.addEventListener(
      'itemCreated',
      this.handleItemCreated.bind(this)
    );
    this._goldenLayout.addEventListener(
      'itemDestroyed',
      this.handleItemDestroyed.bind(this)
    );
    this._dockingSuiteHelperService.layout = this._goldenLayout;
  }

  ngAfterViewInit(): void {
    let loadedCachedLayout = false;
    if (this.layout) {
      try {
        this._goldenLayout?.loadLayout(this.layout);
        loadedCachedLayout = true;
      } catch (e) {
        console.warn('Cached layout is invalid!', e);
      }
    }

    if (!loadedCachedLayout) {
      this._goldenLayout?.loadLayout(this.fallbackLayout);
    }
  }

  ngOnDestroy(): void {
    this._goldenLayout.destroy();
  }

  setSize(width: number, height: number): void {
    this._goldenLayout.setSize(width, height);
  }

  setLayout(layout: LayoutConfig): void {
    this._goldenLayout.loadLayout(layout);
  }

  getConfig(): ResolvedLayoutConfig {
    return this._goldenLayout.saveLayout();
  }

  private handleGetComponentEvent(
    container: ComponentContainer,
    itemConfig: ResolvedComponentItemConfig
  ): ComponentContainer.BindableComponent {
    const componentType = itemConfig.componentType;
    const componentRef = this._dockingSuiteHelperService.createComponent(
      componentType,
      container
    );

    this._appRef.attachView(componentRef.hostView);

    const domElem = (componentRef.hostView as EmbeddedViewRef<unknown>)
      .rootNodes[0] as HTMLElement;
    container.element.appendChild(domElem);

    this._containerMap.set(container, componentRef);

    return { virtual: false, component: componentRef.instance };
  }

  private handleReleaseComponentEvent(
    container: ComponentContainer
    // component: ComponentItem.Component
  ): void {
    console.log('RELEASE');
    const componentRef = this._containerMap.get(container);
    if (!componentRef)
      throw new Error('Could not release component. Container not found');

    this._appRef.detachView(componentRef.hostView);
    try {
      componentRef.destroy();
    } catch (e) {
      console.warn(e);
    }
    this._containerMap.delete(container);
  }

  private handleItemCreated(e: EventEmitter.BubblingEvent): void {
    const comItem = e.target as ComponentItem;
    if (!comItem.isComponent) return;

    const tabs = [...this._dockingSuiteHelperService.tabs];
    tabs.push(comItem);
    this._dockingSuiteHelperService.tabs = tabs;
  }

  private handleItemDestroyed(e: EventEmitter.BubblingEvent): void {
    const comItem = e.target as ComponentItem;
    if (!comItem.isComponent) return;

    this._dockingSuiteHelperService.tabs =
      this._dockingSuiteHelperService.tabs.filter((c) => c !== comItem);
  }
}
