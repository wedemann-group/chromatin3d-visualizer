/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { BlobWriter, Entry, TextWriter } from '@zip.js/zip.js';

export async function getContent(
  entry: Entry,
  password: string | undefined = undefined
): Promise<Blob> {
  if (!entry || !entry.getData) throw new Error('Invalid input!');

  return entry.getData(new BlobWriter(), {
    password: password,
  });
}

export async function getContentString(
  entry: Entry,
  password: string | undefined = undefined
): Promise<string> {
  if (!entry || !entry.getData) throw new Error('Invalid input!');

  return entry.getData(new TextWriter(), {
    password: password,
  });
}
