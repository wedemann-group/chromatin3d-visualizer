/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { MATERIAL_IMPORTS } from '../../../../core/services/__mocks__/imports';
import { PasswordRequestDialogComponent } from './password-request-dialog.component';
import Spy = jasmine.Spy;
import createSpy = jasmine.createSpy;
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {
  BlobReader,
  BlobWriter,
  configure,
  ZipReader,
  ZipWriter,
} from '@zip.js/zip.js';
import { By } from '@angular/platform-browser';
import { MatInput } from '@angular/material/input';
import { MatButton } from '@angular/material/button';
import { MatError } from '@angular/material/form-field';

async function getAsset(file: string): Promise<Blob> {
  const response = await fetch(file, { method: 'GET' });
  return response.blob();
}

async function generateZipFile(
  zipFileContent: Blob,
  fileCount: number,
  password: string | undefined = undefined
): Promise<Blob> {
  configure({ useWebWorkers: false });
  const blobWriter = new BlobWriter('application/zip');
  const writer = new ZipWriter(blobWriter, { password });

  for (let i = 0; i < fileCount; ++i) {
    await writer.add(`my-file-one_${i}.trj`, new BlobReader(zipFileContent));
  }

  return writer.close(undefined);
}

describe('PasswordRequestDialogComponent', () => {
  let sampleTrj!: Blob;
  let component: PasswordRequestDialogComponent;
  let fixture: ComponentFixture<PasswordRequestDialogComponent>;
  let dialogRef: { close: Spy<(file: File) => void> };

  beforeAll(async () => {
    sampleTrj = await getAsset('/mock_data/shortTestTrajectory.trj');
  });

  beforeEach(async () => {
    dialogRef = { close: createSpy('close') };

    const blob = new ZipReader(
      new BlobReader(await generateZipFile(sampleTrj, 2, 'password')),
      { useWebWorkers: false }
    );

    await TestBed.configureTestingModule({
      declarations: [PasswordRequestDialogComponent],
      imports: [...MATERIAL_IMPORTS],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: { entry: (await blob.getEntries())[0], name: 'My File' },
        },
        {
          provide: MatDialogRef,
          useValue: dialogRef,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordRequestDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not display invalid password info', () => {
    expect(fixture.debugElement.query(By.directive(MatError))).toBeNull();
  });

  it('should fail on invalid password', fakeAsync(() => {
    const input = fixture.debugElement.query(By.directive(MatInput));
    expect(input).toBeDefined();

    input.nativeElement.value = '123';
    input.nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    const buttons = fixture.debugElement.queryAll(By.directive(MatButton));
    const btn = buttons[buttons.length - 1];

    expect(btn.nativeElement.disabled).toBeFalsy();
    btn.triggerEventHandler('click', undefined);
    tick();
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.directive(MatError))).toBeDefined();
    expect(dialogRef.close.calls.count()).toEqual(0);
  }));
});
