/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Bead from './bead.model';
import Parameters from './parameters.model';
import IMetaInfo from './meta-info.interface';
import ICohesinRing from './cohesin-ring.interface';

/**
 * Represents a partial of configuration that members are not "read only" and optional.
 */
export default interface IConfiguration {
  mcSteps: number;
  metaInfo: IMetaInfo | undefined;
  parameters: Partial<Parameters> | undefined;
  beads: Bead[];

  cohesinRings: ICohesinRing[] | undefined;
}
