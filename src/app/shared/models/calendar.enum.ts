/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

export enum ECalendar {
  YEAR = 'a year',
  YEAR_MULTIPLE = '{0} years',
  MONTH = 'a month',
  MONTH_MULTIPLE = '{0} months',
  DAY = 'a day',
  DAY_MULTIPLE = '{0} days',
  HOUR = 'a hour',
  HOUR_MULTIPLE = '{0} hours',
  MINUTE = 'a minute',
  MINUTE_MULTIPLE = '{0} minutes',
  SECOND = 'a second',
  SECOND_MULTIPLE = '{0} seconds',
}
