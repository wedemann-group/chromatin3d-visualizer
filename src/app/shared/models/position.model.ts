/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

/**
 * Represents a 3-dimension position of a (bead) object.
 */
export default class Position {
  /** Specifies the x coordinate of the point. */
  protected _x: number;
  /** Specifies the y coordinate of the point. */
  protected _y: number;
  /** Specifies the z coordinate of the point. */
  protected _z: number;

  // region Properties
  /** Specifies the x coordinate of the point. */
  public get x(): number {
    return this._x;
  }

  /** Specifies the y coordinate of the point. */
  public get y(): number {
    return this._y;
  }

  /** Specifies the z coordinate of the point. */
  public get z(): number {
    return this._z;
  }
  // endregion

  public constructor(x: number, y: number, z: number) {
    this._x = x;
    this._y = y;
    this._z = z;
  }

  public static difference(pos1: Position, pos2: Position): Position {
    return new Position(pos2.x - pos1.x, pos2.y - pos1.y, pos2.z - pos1.z);
  }

  public clone(): Position {
    return new Position(this._x, this._y, this._z);
  }

  public add(adder: Position | number): Position {
    if (adder instanceof Position) {
      this._x += adder.x;
      this._y += adder.y;
      this._z += adder.z;
    } else {
      this._x += adder;
      this._y += adder;
      this._z += adder;
    }

    return this;
  }

  public sub(suber: Position | number): Position {
    if (suber instanceof Position) {
      this._x -= suber.x;
      this._y -= suber.y;
      this._z -= suber.z;
    } else {
      this._x -= suber;
      this._y -= suber;
      this._z -= suber;
    }

    return this;
  }

  // eslint-disable-next-line
  public equals(obj: any): boolean {
    if (obj.x !== undefined && obj.y !== undefined && obj.z !== undefined) {
      const castedObj = obj as Position;
      return (
        castedObj.x === this._x &&
        castedObj.y === this._y &&
        castedObj.z === this._z
      );
    }

    return false;
  }
}
