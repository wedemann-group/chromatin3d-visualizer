/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import MetaInfo from './meta-info.model';

describe('Meta-Info Model', () => {
  it('should create a new meta-info', () => {
    const info = {
      date: 1012,
      program: 'LF2 Modding Environment',
      schemaVersion: undefined,
      version: 'v0.2',
    } as MetaInfo;

    expect(new MetaInfo(info)).toEqual(jasmine.objectContaining(info));
  });

  it('should create a new meta-info with default props', () => {
    const info = new MetaInfo({});

    expect(info).toEqual(
      jasmine.objectContaining({ ...info, program: 'webViewer' })
    );
    expect(info.schemaVersion).toBeUndefined();
    expect(info.version).toBeDefined();
    expect(info.version.split('.').length).toBeGreaterThan(1);
  });
});
