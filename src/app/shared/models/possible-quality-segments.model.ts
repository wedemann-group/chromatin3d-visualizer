/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ICohesinRingData } from '../../core/services/session-mananger/models/cohesin-ring-data.interface';
import { ICohesinHeadData } from '../../core/services/session-mananger/models/cohesin-head-data.interface';
import { ICohesinHingeData } from '../../core/services/session-mananger/models/cohesin-hinge-data.interface';
import { IHistoneOctamerData } from '../../core/services/session-mananger/models/histone-octamer-data.interface';
import { INucleosomeDNAData } from '../../core/services/session-mananger/models/nucleosome-dna-data.interface';
import { IDoubleHelixBasePairData } from '../../core/services/session-mananger/models/double-helix-base-pair-data.interface';

export type PossibleQualitySegments =
  | ICohesinHeadData
  | ICohesinRingData
  | ICohesinHingeData
  | IHistoneOctamerData
  | INucleosomeDNAData
  | IDoubleHelixBasePairData;
