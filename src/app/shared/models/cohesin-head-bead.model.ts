/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Bead from './bead.model';
import IBead from './bead.interface';
import { BEAD_MARKER_COHESIN_HEAD } from '../parsers/block-trajectory/constants.static';

/**
 * Represents a cohesin head bead.
 */
export default class CohesinHeadBead extends Bead {
  public constructor(cohesinHead: Partial<IBead>) {
    super({ ...cohesinHead, marker: BEAD_MARKER_COHESIN_HEAD });
  }
}
