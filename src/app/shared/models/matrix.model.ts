/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Vector from './vector.model';

export default abstract class Matrix {
  protected data: number[][];

  protected constructor(n: number, m: number) {
    this.data = new Array(n).fill(new Array(m).fill(0));
  }

  public multiply(b: Matrix): this {
    const emptyArray = new Array(this.data.length)
      .fill(0)
      .map(() => new Array(b.data[0].length).fill(0));

    this.data = emptyArray.map((row, i) => {
      return row.map((_, j) => {
        return this.data[i].reduce(
          (sum, elm, k) => sum + elm * b.data[k][j],
          0
        );
      });
    });

    return this;
  }

  public multiplyVector(v: Vector): Vector {
    return new Vector(
      this.data[0][0] * v.x + this.data[0][1] * v.y + this.data[0][2] * v.z,
      this.data[1][0] * v.x + this.data[1][1] * v.y + this.data[1][2] * v.z,
      this.data[2][0] * v.x + this.data[2][1] * v.y + this.data[2][2] * v.z
    );
  }

  public get(x: number, y: number): number {
    this.checkRange(x, y);

    return this.data[x][y];
  }

  public set(x: number, y: number, val: number): void {
    this.checkRange(x, y);

    this.data[x][y] = val;
  }

  // eslint-disable-next-line
  public equals(matrix: any): boolean {
    if (!(matrix instanceof Matrix)) return false;

    if (matrix.data.length !== this.data.length) return false;

    if (matrix.data[0].length !== this.data[0].length) return false;

    for (let y = 0; y < this.data.length; ++y) {
      for (let x = 0; x < this.data[y].length; ++x) {
        if (this.data[y][x] !== matrix.data[y][x]) return false;
      }
    }

    return true;
  }

  private checkRange(x: number, y: number): void {
    if (x >= this.data.length || x < 0) throw new RangeError();

    if (y >= this.data[x].length || y < 0) throw new RangeError();
  }
}
