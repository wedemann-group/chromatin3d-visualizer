/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Bead from './bead.model';
import Position from './position.model';
import Vector from './vector.model';

describe('Bead Model', () => {
  class BeadClass extends Bead {
    public constructor(bead: Partial<Bead>) {
      super(bead);
    }
  }

  it('should create a new model', () => {
    const model = {
      id: 51,
      equilibriumSegmentLength: 666,
      intrinsicTwist: 333,
      position: new Position(12, 0, 5),
      fVector: new Vector(52, 1, -5),
      bendVector: new Vector(-12, -69, -2),
      segmentVector: new Vector(6, 6, 6),
    } as Bead;
    const modelClass = new BeadClass(model);
    expect(modelClass).toEqual(jasmine.objectContaining(model));
  });

  it('should create a new model with default props', () => {
    let model = {
      id: 51,
      position: new Position(12, 0, 5),
    } as Bead;
    const modelClass = new BeadClass(model);
    model = {
      ...model,
      equilibriumSegmentLength: 0,
      intrinsicTwist: 0,
      fVector: new Vector(0, 0, 0),
      bendVector: new Vector(0, 0, 0),
      segmentVector: new Vector(0, 0, 0),
    };
    expect(modelClass).toEqual(jasmine.objectContaining(model));
  });

  it('should NOT create a new one without id', () => {
    const model = {
      position: new Position(12, 0, 5),
    } as Bead;
    expect(() => new BeadClass(model)).toThrowError(Error);
  });

  it('should NOT create a new one without position', () => {
    const model = {
      id: 51,
    } as Bead;
    expect(() => new BeadClass(model)).toThrowError(Error);
  });
});
