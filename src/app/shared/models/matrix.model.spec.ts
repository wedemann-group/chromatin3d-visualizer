/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Matrix from './matrix.model';
import Vector from './vector.model';

describe('Matrix Model', () => {
  class BasicMatrix extends Matrix {
    public constructor(n: number, m: number) {
      super(n, m);
    }

    public get values(): number[][] {
      return this.data;
    }
  }

  class BasicMatrixWithInitArray extends Matrix {
    public constructor(n: number[][]) {
      super(n.length, n[0].length);

      this.data = n;
    }
  }

  it('should create a new n x m matrix', () => {
    expect(new BasicMatrix(3, 2).values).toEqual([
      [0, 0],
      [0, 0],
      [0, 0],
    ]);
  });

  it('should multiply two 3x3 matrices', () => {
    const matrix1 = new BasicMatrixWithInitArray([
      [8, 3, 2],
      [5, 7, 12],
      [6, 0, 0],
    ]);
    const matrix2 = new BasicMatrixWithInitArray([
      [-8, 2, 0],
      [0, 0, -3],
      [1, 11, 0],
    ]);

    matrix1.multiply(matrix2);

    const solutionMatrix = new BasicMatrixWithInitArray([
      [-62, 38, -9],
      [-28, 142, -21],
      [-48, 12, 0],
    ]);

    expect(matrix1.equals(solutionMatrix)).toBeTruthy();
  });

  it('should get the value of valid indices', () => {
    const matrix1 = new BasicMatrixWithInitArray([
      [8, 3, 2],
      [5, 7, 12],
      [6, 0, 0],
    ]);

    expect(matrix1.get(1, 1)).toEqual(7);
  });

  it('should NOT get the value of invalid x-index', () => {
    const matrix1 = new BasicMatrixWithInitArray([
      [8, 3, 2],
      [5, 7, 12],
      [6, 0, 0],
    ]);

    expect(() => matrix1.get(5, 1)).toThrowError(RangeError);
  });

  it('should NOT get the value of invalid y-index', () => {
    const matrix1 = new BasicMatrixWithInitArray([
      [8, 3, 2],
      [5, 7, 12],
      [6, 0, 0],
    ]);

    expect(() => matrix1.get(1, 5)).toThrowError(RangeError);
  });

  it('should NOT get the value of invalid x-index (negative)', () => {
    const matrix1 = new BasicMatrixWithInitArray([
      [8, 3, 2],
      [5, 7, 12],
      [6, 0, 0],
    ]);

    expect(() => matrix1.get(-1, 1)).toThrowError(RangeError);
  });

  it('should change the value at a specific index', () => {
    const matrix1 = new BasicMatrixWithInitArray([
      [8, 3, 2],
      [5, 7, 12],
      [6, 0, 0],
    ]);

    matrix1.set(1, 1, -2);
    expect(matrix1.get(1, 1)).toEqual(-2);
  });

  it('should NOT be equal to a random object', () => {
    const matrix1 = new BasicMatrixWithInitArray([
      [8, 3, 2],
      [5, 7, 12],
      [6, 0, 0],
    ]);

    expect(matrix1.equals(new Vector(0, 0, 0))).toBeFalsy();
  });

  it('should NOT be equal to another dimension (cols)', () => {
    const matrix1 = new BasicMatrixWithInitArray([
      [8, 3, 2],
      [5, 7, 12],
      [6, 0, 0],
    ]);

    const matrix2 = new BasicMatrixWithInitArray([
      [8, 3, 2, 2],
      [5, 7, 12, 2],
      [6, 0, 0, -1],
    ]);

    expect(matrix1.equals(matrix2)).toBeFalsy();
  });

  it('should NOT be equal to another dimension (rows)', () => {
    const matrix1 = new BasicMatrixWithInitArray([
      [8, 3, 2],
      [5, 7, 12],
      [6, 0, 0],
    ]);

    const matrix2 = new BasicMatrixWithInitArray([
      [8, 3, 2],
      [5, 7, 12],
    ]);

    expect(matrix1.equals(matrix2)).toBeFalsy();
  });

  it('should NOT be equal to another matrix with different values', () => {
    const matrix1 = new BasicMatrixWithInitArray([
      [8, 3, 2],
      [5, 7, 12],
      [6, 0, -1],
    ]);

    const matrix2 = new BasicMatrixWithInitArray([
      [8, 3, 2],
      [5, 1, 12],
      [6, 0, -1],
    ]);

    expect(matrix1.equals(matrix2)).toBeFalsy();
  });
});
