/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Parameters from './parameters.model';
import Bead from './bead.model';
import IConfiguration from './configuration.interface';
import MetaInfo from './meta-info.model';
import DNABead from './dna-bead.model';
import SoftwareNucleosomeBead from './sw-nucleosome-bead.model';
import { CohesinRing } from './cohesin-ring.model';
import IBead from './bead.interface';
import {
  BEAD_MARKER_COHESIN_HEAD,
  BEAD_MARKER_DNA,
  BEAD_MARKER_NUCLEOSOME,
} from '../parsers/block-trajectory/constants.static';
import CohesinHeadBead from './cohesin-head-bead.model';

/**
 * Represents a configuration of a trajectory file.
 */
export class Configuration implements IConfiguration {
  public readonly mcSteps: number;
  public readonly parameters: Partial<Parameters> | undefined;
  public readonly metaInfo: MetaInfo | undefined;

  // rename to beadChain (see 2011_Masterarbeit_Moerl_Manie)
  public readonly beads: Bead[];

  public readonly cohesinRings: CohesinRing[];

  public constructor(configuration: Partial<IConfiguration>) {
    this.mcSteps = configuration.mcSteps ?? 0;
    this.parameters = configuration.parameters ?? undefined;

    this.beads = this.parseBeads(configuration.beads);

    if (configuration.metaInfo)
      this.metaInfo = new MetaInfo(configuration.metaInfo);

    if (configuration.cohesinRings)
      this.cohesinRings = configuration.cohesinRings.map(
        (cr) => new CohesinRing(cr)
      );
    else this.cohesinRings = [];
  }

  private static getBeadFromMarker(bead: Partial<IBead>): Bead {
    const marker = bead.marker;
    if (marker === BEAD_MARKER_NUCLEOSOME)
      return new SoftwareNucleosomeBead(bead);
    else if (marker === BEAD_MARKER_COHESIN_HEAD)
      return new CohesinHeadBead(bead);
    else if (marker === BEAD_MARKER_DNA) return new DNABead(bead);
    else throw new Error(`Invalid bead marker! ${marker}`);
  }

  protected parseBeads(beads: Partial<IBead>[] | null | undefined): Bead[] {
    const result: Bead[] = [];
    if (beads && Array.isArray(beads)) {
      for (const bead of beads) {
        if (bead instanceof Bead) {
          result.push(bead);
        } else if (bead.marker) {
          result.push(Configuration.getBeadFromMarker(bead));
        } else {
          throw new Error('Invalid bead!');
        }
      }
    }

    return result;
  }
}
