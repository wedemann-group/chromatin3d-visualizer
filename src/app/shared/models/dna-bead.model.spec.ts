/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Bead from './bead.model';
import DNABead from './dna-bead.model';
import Position from './position.model';

describe('DNA Bead Model', () => {
  it('should create a DNA bead', () => {
    const bead = {
      position: new Position(2, 0, -5),
      id: 12,
      charged: true,
    } as DNABead;

    const dnaBead = new DNABead(bead);
    expect(dnaBead).toBeDefined();
    expect(dnaBead.charged).toBeTruthy();
  });

  it('should create a DNA bead with default values', () => {
    const bead = { position: new Position(2, 0, -5), id: 12 } as Bead;

    const dnaBead = new DNABead(bead);
    expect(dnaBead).toBeDefined();
    expect(dnaBead.charged).toBeFalsy();
  });
});
