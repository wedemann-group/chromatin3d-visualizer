/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import ICohesinRing from './cohesin-ring.interface';
import Position from './position.model';
import Vector from './vector.model';
import {
  parsePositionOrReturnDefaultPosition,
  parseVectorOrReturnDefaultVector,
} from '../utilities/parser.utility';

/**
 * Represents a ring of a cohesin protein.
 */
export class CohesinRing implements ICohesinRing {
  public readonly index: number;
  public readonly centerPos: Position;
  public readonly orientationVector: Vector;

  public hingePosition: Position;

  public constructor(ring: Partial<CohesinRing>) {
    if (ring.index === undefined || ring.index === null || ring.index < 0)
      throw new Error('Index is missing!');
    if (!ring.centerPos) throw new Error('Center Position is missing!');
    if (!ring.orientationVector)
      throw new Error('Orientation Vector is missing!');

    this.index = ring.index;
    this.centerPos = parsePositionOrReturnDefaultPosition(ring.centerPos);

    this.orientationVector = parseVectorOrReturnDefaultVector(
      ring.orientationVector
    );

    this.hingePosition = parsePositionOrReturnDefaultPosition(
      ring.hingePosition
    );
  }
}
