/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import MultipleMeshBeadBuilder from '../../modules/dockable-workspace/viewer/beads/multiple-mesh-bead.builder';
import InstancedObjectBuilder from '../../modules/dockable-workspace/viewer/beads/instanced-object.builder';

export type OneTimeBuilder =
  | MultipleMeshBeadBuilder<any, any, any, any>
  | InstancedObjectBuilder<any, any, any, any>;
