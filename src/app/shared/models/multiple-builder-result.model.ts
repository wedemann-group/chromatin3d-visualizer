/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  MultipleBuilder,
  MultipleLinkerBuilder,
} from './multiple-builder.model';
import ChangeableColorWithConfigColor from '../../core/services/colors/changeable-color-with-config-color';
import Bead from './bead.model';

export type MultipleBuilderResult = {
  builder: MultipleBuilder | MultipleLinkerBuilder;
  colorClass: ChangeableColorWithConfigColor;
  param: (Bead | { id: number })[];
  renderSize: number;
  isActive?: boolean;
};
