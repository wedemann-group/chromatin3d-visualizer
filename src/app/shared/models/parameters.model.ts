/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import IParameters from './parameters.interface';

/**
 * Represents a list of parameters of a trajectory file.
 */
export default class Parameters implements IParameters {
  [key: string]: number | boolean;

  public readonly numberOfBeads: number;
  public readonly radiusDNA: number;
  public readonly radiusNucleosome: number;
  public readonly heightNucleosome: number;
  public readonly chainIsClosed: boolean;
  public readonly pbcContainerStartDimension: number;
  public readonly cubeSizeLJ: number;
  public readonly cubeSizeDNANucIntersection: number;
  public readonly cubeSizeEstat: number;

  public readonly cohesinHeadRadius: number;
  public readonly ringThickness: number;
  public readonly ringRadius: number;
  public readonly cohDistance: number;

  public readonly temperature: number;
  public readonly eStatTemperature: number;

  public constructor(parameters: Partial<IParameters>) {
    this.numberOfBeads = parameters.numberOfBeads ?? 0;
    this.radiusDNA = parameters.radiusDNA ?? 2.0;
    this.radiusNucleosome = parameters.radiusNucleosome ?? 2.0;
    this.heightNucleosome = parameters.heightNucleosome ?? 0.0;
    this.chainIsClosed = parameters.chainIsClosed ?? false;
    this.pbcContainerStartDimension =
      parameters.pbcContainerStartDimension ?? -1;

    this.cubeSizeLJ = parameters.cubeSizeLJ ?? -1;
    this.cubeSizeDNANucIntersection =
      parameters.cubeSizeDNANucIntersection ?? -1;
    this.cubeSizeEstat = parameters.cubeSizeEstat ?? -1;

    this.cohesinHeadRadius = parameters.cohesinHeadRadius ?? -1;
    this.ringThickness = parameters.ringThickness ?? -1;
    this.ringRadius = parameters.ringRadius ?? -1;
    this.cohDistance = parameters.cohDistance ?? -1;

    this.temperature = parameters.temperature ?? -1;
    this.eStatTemperature = parameters.eStatTemperature ?? -1;
  }
}
