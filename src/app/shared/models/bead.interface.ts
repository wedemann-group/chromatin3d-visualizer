/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Position from './position.model';
import Vector from './vector.model';

export default interface IBead {
  /** Specifies the id of the bead. */
  id: number;
  equilibriumSegmentLength: number;
  intrinsicTwist: number;
  /** Specifies the position of the bead. */
  position: Position;

  drawPosition: Position;
  fVector: Vector;
  bendVector: Vector;
  segmentVector: Vector;

  marker?: string;
}
