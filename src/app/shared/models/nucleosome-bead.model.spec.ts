/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Bead from './bead.model';
import NucleosomeBead from './nucleosome-bead.model';
import Position from './position.model';
import Vector from './vector.model';

describe('Nucleosome Model', () => {
  it('should create a new nucleosome', () => {
    const nucleosomeBead = {
      position: new Position(1, -1, 0),
      id: 666,
      centerOrientationVector: new Vector(5, -2, 7),
      center: new Position(-12, 401, 666),
    } as NucleosomeBead;
    expect(new NucleosomeBead(nucleosomeBead)).toEqual(
      jasmine.objectContaining(nucleosomeBead)
    );
  });

  it('should create a new nucleosome with default props', () => {
    const bead = {
      position: new Position(1, -1, 0),
      id: 666,
    } as Bead;
    expect(new NucleosomeBead(bead)).toEqual(
      jasmine.objectContaining({
        ...bead,
        center: new Position(0, 0, 0),
        centerOrientationVector: new Vector(0, 0, 0),
      } as NucleosomeBead)
    );
  });
});
