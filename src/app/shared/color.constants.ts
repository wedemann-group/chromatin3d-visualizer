/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

export const DEFAULT_BACK_COLOR = '#fff';

export const DEFAULT_COLOR_HISTONE_OCTAMER = '#ff0000';
export const DEFAULT_COLOR_NUCLEOSOME_DNA = '#0000ff';
export const DEFAULT_COLOR_LINKER_DNA = DEFAULT_COLOR_NUCLEOSOME_DNA;
export const DEFAULT_COLOR_COHESIN_HEAD = '#ffaa00ff';
export const DEFAULT_COLOR_COHESIN_RING = '#ffaa00';
export const DEFAULT_COLOR_COHESIN_LINKER = 'rgba(255,70,0,0.4)';
export const DEFAULT_COLOR_COHESIN_COLORING_DNA = '#ff5900';
export const DEFAULT_COLOR_SELECTION = '#6fff6f';
export const DEFAULT_COLOR_DISTANCE_LINE = '#ff0000';
export const DEFAULT_COLOR_DISTANCE_TEXT = '#000';

export const DEFAULT_COLOR_PBC_CONTAINER = 'rgba(180,180,180,0.6)';
export const DEFAULT_COLOR_MICROSCOPE_SLIDE_BOX = 'rgba(145,250,171,0.6)';
