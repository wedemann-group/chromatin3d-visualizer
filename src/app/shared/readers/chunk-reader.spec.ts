/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ChunkReader } from './chunk-reader';

describe('Chunk Reader', () => {
  it('should read a chunk as string', async () => {
    const blob = new Blob(['line1\nline2,line2b\nline3']);
    expect(
      await new ChunkReader(new File([blob], 'test.txt')).readChunkAsString(0)
    ).toEqual('line1\nline2,line2b\nline3');
  });

  it('should read a chunk as string with offset', async () => {
    const blob = new Blob(['line1\nline2,line2b\nline3']);
    expect(
      await new ChunkReader(new File([blob], 'test.txt')).readChunkAsString(6)
    ).toEqual('line2,line2b\nline3');
  });

  it('should NOT accept negative seeks', async () => {
    const blob = new Blob(['line1\nline2,line2b\nline3']);
    expect(
      () =>
        new ChunkReader(new File([blob], 'test.txt'), {
          seek: -1,
        })
    ).toThrowMatching(
      (t) =>
        t.toString().toLowerCase().includes('offset') ||
        t.toString().toLowerCase().includes('seek')
    );
  });

  it('should NOT negative read length', async () => {
    const blob = new Blob(['line1\nline2,line2b\nline3']);
    expect(
      () =>
        new ChunkReader(new File([blob], 'test.txt'), {
          readLength: -1,
        })
    ).toThrowMatching((t) => t.toString().toLowerCase().includes('length'));
  });

  it('should NOT too high read length', async () => {
    const blob = new Blob(['line1\nline2,line2b\nline3']);
    expect(
      () =>
        new ChunkReader(new File([blob], 'test.txt'), {
          readLength: 200000,
        })
    ).toThrowMatching((t) => t.toString().toLowerCase().includes('length'));
  });

  it('should NOT too high seek', async () => {
    const blob = new Blob(['line1\nline2,line2b\nline3']);
    expect(
      () =>
        new ChunkReader(new File([blob], 'test.txt'), {
          seek: 200000,
        })
    ).toThrowMatching(
      (t) =>
        t.toString().toLowerCase().includes('seek') ||
        t.toString().toLowerCase().includes('offset')
    );
  });
});
