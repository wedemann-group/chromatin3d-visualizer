/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ReadLineOptions } from './models/read-line-options.model';

const DEFAULT_CHUNK_SIZE = 1024 * 512; // in bytes

export class ChunkReader {
  protected readonly _file: File;
  protected readonly _seek: number;
  protected readonly _readLength: number;
  protected readonly _decoder = new TextDecoder();
  protected readonly _reader = new FileReader();

  protected readonly _chunkSize: number;
  protected readonly _lastBytes: number[] = [];

  public constructor(file: File, options?: Partial<ReadLineOptions>) {
    this._file = file;
    this._chunkSize = options?.chunkSize ?? DEFAULT_CHUNK_SIZE;
    this._seek = options?.seek ?? 0;
    this._readLength = options?.readLength ?? file.size;

    if (this._seek < 0)
      throw new Error('Invalid read offset! Offset must be at least 0.');
    if (this._readLength < 0 || this._readLength > file.size)
      throw new Error('Invalid read length!');
    if (this._seek >= this._readLength)
      throw new Error(
        'Invalid read offset! Offset is greater than the read length!'
      );
  }

  public async readChunkAsString(offset: number): Promise<string> {
    const chunk = await this.readChunk(offset);
    if (!chunk) throw new Error('Invalid chunk! Chunk was empty.');

    return this._decoder.decode(new Uint8Array(chunk));
  }

  protected readChunk(offset: number): Promise<ArrayBuffer | null> {
    return new Promise((resolve, reject) => {
      this._reader.onloadend = () => {
        resolve(this._reader.result as ArrayBuffer | null);
      };
      this._reader.onerror = (e) => {
        reject(e);
      };

      const chunk = this._file.slice(
        offset,
        Math.min(offset + this._chunkSize, this._readLength)
      );
      this._reader.readAsArrayBuffer(chunk);
    });
  }

  protected getMerged(
    bytes: Uint8Array,
    point: number,
    addSplitChar: boolean
  ): Uint8Array {
    const tmpLastPoints: number[] = [];
    tmpLastPoints.push(...bytes.slice(point + 1));

    if (addSplitChar) ++point;

    const splicedBytes = bytes.slice(0, point); // point NOT +1 coz skip last \n char
    bytes = new Uint8Array(this._lastBytes.length + splicedBytes.length);
    bytes.set(this._lastBytes, 0);
    bytes.set(splicedBytes, this._lastBytes.length);
    this._lastBytes.splice(0);

    this._lastBytes.push(...tmpLastPoints);

    return bytes;
  }
}
