/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ReadLine } from './read-line';

describe('Read Line', () => {
  it('should read a single line', async () => {
    const blob = new Blob(['test']);
    const file = new ReadLine(new File([blob], 'test.txt'));
    const lines: string[] = [];

    for await (const line of file.lines()) lines.push(line);

    expect(lines).toEqual(['test']);
  });

  it('should read two lines', async () => {
    const blob = new Blob(['test\ntest5']);
    const file = new ReadLine(new File([blob], 'test.txt'));
    const lines: string[] = [];

    for await (const line of file.lines()) lines.push(line);

    expect(lines).toEqual(['test', 'test5']);
  });

  it('should read UTF-8 chars', async () => {
    const blob = new Blob(['test ℕ ⊆ ℕ₀ test']);
    const file = new ReadLine(new File([blob], 'test.txt'));
    const lines: string[] = [];

    for await (const line of file.lines()) lines.push(line);

    expect(lines).toEqual(['test ℕ ⊆ ℕ₀ test']);
  });

  it('should read a long line in two chunks', async () => {
    const blob = new Blob([
      'that is a very long line that will read in two chunks',
    ]);
    const file = new ReadLine(new File([blob], 'test.txt'), {
      chunkSize: 40,
    });
    const lines: string[] = [];

    for await (const line of file.lines()) lines.push(line);

    expect(lines).toEqual([
      'that is a very long line that will read in two chunks',
    ]);
  });

  it('should return completely lines', async () => {
    const blob = new Blob([
      'that is a\nvery long line\nthat is longer than\na read chunk',
    ]);
    const file = new ReadLine(new File([blob], 'test.txt'), {
      chunkSize: 5,
    });
    const lines: string[] = [];

    for await (const line of file.lines()) lines.push(line);

    expect(lines).toEqual([
      'that is a',
      'very long line',
      'that is longer than',
      'a read chunk',
    ]);
  });

  it('should handle empty lines', async () => {
    const blob = new Blob(['line1\n\nline2\n']);
    const file = new ReadLine(new File([blob], 'test.txt'), {
      chunkSize: 5,
    });
    const lines: string[] = [];

    for await (const line of file.lines()) lines.push(line);

    expect(lines).toEqual(['line1', '', 'line2', '']);
  });

  it('should skip the first line', async () => {
    const blob = new Blob(['line1\nline2,line2b\nline3']);
    const file = new ReadLine(new File([blob], 'test.txt'), {
      seek: 6, // skip the first 6 chars
    });
    const lines: string[] = [];

    for await (const line of file.lines()) lines.push(line);

    expect(lines).toEqual(['line2,line2b', 'line3']);
  });

  it("should a block that doesn't contain a line", async () => {
    const blob = new Blob(['line1\n\n']);
    const file = new ReadLine(new File([blob], 'test.txt'), {
      chunkSize: 1024,
    });
    const lines: string[] = [];

    for await (const line of file.lines()) lines.push(line);

    expect(lines).toEqual(['line1', '', '']);
  });
});
