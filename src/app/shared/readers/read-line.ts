/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ReadLineOptions } from './models/read-line-options.model';
import { ChunkReader } from './chunk-reader';

const NEW_LINE_CHAR = 10;

export class ReadLine extends ChunkReader {
  public constructor(file: File, options?: Partial<ReadLineOptions>) {
    super(file, options);
  }

  public async *lines(): AsyncGenerator<string> {
    for (
      let offset = this._seek;
      offset < this._readLength;
      offset += this._chunkSize
    ) {
      const chunk = await this.readChunk(offset);
      if (!chunk) continue;

      let bytes = new Uint8Array(chunk);
      let point = bytes.length - 1;

      while (point >= 0 && bytes[point] !== NEW_LINE_CHAR) {
        --point;
      }

      if (point >= 0) {
        bytes = this.getMerged(
          bytes,
          point,
          point === bytes.length - 1 &&
            offset + this._chunkSize >= this._readLength
        );

        // @ts-ignore
        yield* this.getLines(bytes);
      } else {
        this._lastBytes.push(...bytes);
      }
    }

    if (this._lastBytes.length === 0) return;

    // @ts-ignore
    yield* this.getLines(new Uint8Array(this._lastBytes));
  }

  private getLines(bytes: Uint8Array): string[] {
    return this._decoder.decode(bytes).split('\n');
  }
}
