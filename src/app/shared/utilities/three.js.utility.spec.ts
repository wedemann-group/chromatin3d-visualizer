/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  convertBufferAttrToVector3,
  convertBufferAttrToVector3List,
  convertRadiansToDegree,
  convertVectorArrayToFloat32Array,
  deserializeVector3,
  getBoxDimensionCenterAndSizesFromDim,
  getBoxSizeDimension,
  isShaderWithUniforms,
  parseQuaternionOrReturnDefaultQuaternion,
  serializeVector3,
} from './three.js.utility';
import {
  BufferAttribute,
  Quaternion,
  ShaderMaterial,
  Vector2,
  Vector3,
} from 'three';

describe('Three.js Utility', () => {
  it('should convert 3d vector array to float32 array', () => {
    expect(
      convertVectorArrayToFloat32Array([
        new Vector3(3, 2, 1),
        new Vector3(1, 1, 1),
      ])
    ).toEqual([3, 2, 1, 1, 1, 1]);
  });

  it('should convert 2d vector array to float32 array', () => {
    expect(
      convertVectorArrayToFloat32Array([new Vector2(3, 2), new Vector2(1, 5)])
    ).toEqual([3, 2, 1, 5]);
  });

  it('should convert empty vector array to empty float32 array', () => {
    expect(convertVectorArrayToFloat32Array([])).toEqual([]);
  });

  it('should convert buffer attribute to 3d vector list', () => {
    const bufferAttr = new BufferAttribute(
      new Int32Array([5, 2, 1, 1, 2, 10]),
      3
    );
    const result = convertBufferAttrToVector3List(bufferAttr);
    expect(result.length).toEqual(2);
    expect(result[0].equals(new Vector3(5, 2, 1))).toBeTruthy();
    expect(result[1].equals(new Vector3(1, 2, 10))).toBeTruthy();
  });

  it('should convert buffer attribute to a single 3d vector', () => {
    const bufferAttr = new BufferAttribute(
      new Int32Array([5, 2, 1, 1, 2, 10]),
      3
    );
    expect(
      convertBufferAttrToVector3(bufferAttr, 1).equals(new Vector3(1, 2, 10))
    ).toBeTruthy();
    expect(
      convertBufferAttrToVector3(bufferAttr, 0).equals(new Vector3(5, 2, 1))
    ).toBeTruthy();
  });

  it('should serialize a vector from string', () => {
    const vec = serializeVector3(new Vector3(12.5, 5, -0.2));
    expect(vec).toContain('"x":"12.5');
    expect(vec).toContain('"y":"5');
    expect(vec).toContain('"z":"-0.2');
  });

  it('should deserialize a vector from string', () => {
    const vec = deserializeVector3(JSON.stringify({ x: 12.5, y: 5, z: -0.2 }));
    expect(vec.x).toBeCloseTo(12.5, 0.001);
    expect(vec.y).toBeCloseTo(5, 0.001);
    expect(vec.z).toBeCloseTo(-0.2, 0.001);
  });

  it('should convert radians to degree', () => {
    expect(convertRadiansToDegree(Math.PI)).toBeCloseTo(180, 0.5);
  });

  it('should convert radians to degree', () => {
    expect(convertRadiansToDegree(0)).toBeCloseTo(0, 0.00001);
  });

  it('should get box dimension', () => {
    const box = getBoxSizeDimension([
      new Vector3(0, 0, 0),
      new Vector3(12, 0, -10),
      new Vector3(-50, 0, 5),
      new Vector3(0, 0, 50),
    ]);

    expect(box.width).toEqual(62);
    expect(box.height).toEqual(0);
    expect(box.depth).toEqual(60);
  });

  it('should get box dimension info', () => {
    const box = getBoxDimensionCenterAndSizesFromDim({
      xStart: 0,
      xEnd: 10,
      yStart: 12,
      yEnd: 0,
      zStart: -5,
      zEnd: 5,
    });
    expect(box.width).toEqual(10);
    expect(box.height).toEqual(-12);
    expect(box.depth).toEqual(10);
    expect(box.centerX).toEqual(5);
    expect(box.centerY).toEqual(6);
    expect(box.centerZ).toEqual(0);
  });

  it('should parse quaternion', () => {
    const quat = new Quaternion(0, 2, 5, 1);
    expect(
      parseQuaternionOrReturnDefaultQuaternion(quat) === quat
    ).toBeTruthy();
  });

  it('should parse serialized quaternion', () => {
    const quat = new Quaternion(0, 2, 5, 1);
    const newQuat = parseQuaternionOrReturnDefaultQuaternion(
      JSON.parse(JSON.stringify(quat))
    );
    expect(newQuat.x).toEqual(quat.x);
    expect(newQuat.y).toEqual(quat.y);
    expect(newQuat.z).toEqual(quat.z);
    expect(newQuat.w).toEqual(quat.w);
  });

  it('should get a default quaternion', () => {
    const newQuat = parseQuaternionOrReturnDefaultQuaternion(undefined);
    expect(newQuat.x).toEqual(0);
    expect(newQuat.y).toEqual(0);
    expect(newQuat.z).toEqual(0);
    expect(newQuat.w).toEqual(1);

    const newQuat2 = parseQuaternionOrReturnDefaultQuaternion(null);
    expect(newQuat2.x).toEqual(0);
    expect(newQuat2.y).toEqual(0);
    expect(newQuat2.z).toEqual(0);
    expect(newQuat2.w).toEqual(1);
  });

  it('should detect truthy a shader with uniforms', () => {
    expect(
      isShaderWithUniforms(
        new ShaderMaterial({ uniforms: { data: { value: 1.0 } } })
      )
    ).toBeTruthy();
  });

  it('should detect falsy a shader with uniforms', () => {
    expect(isShaderWithUniforms({})).toBeFalsy();
  });
});
