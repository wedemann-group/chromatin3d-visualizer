/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import createSpyObj = jasmine.createSpyObj;
import { downloadBlob } from './window.utility';
import createSpy = jasmine.createSpy;

describe('Window Utility', () => {
  beforeEach(() => {
    // @ts-ignore
    window.navigator.msSaveOrOpenBlob = undefined;
  });

  it('should download a file', () => {
    const spyObj = createSpyObj('a', ['click']);
    spyOn(document, 'createElement').and.returnValue(spyObj);
    spyOn(document.body, 'appendChild');
    spyOn(document.body, 'removeChild');

    downloadBlob(new Blob(['that is my file content']), 'my-file.ext');

    expect(document.createElement).toHaveBeenCalledTimes(1);
    expect(document.createElement).toHaveBeenCalledWith('a');

    expect(spyObj.download).toBe('my-file.ext');
    expect(spyObj.click).toHaveBeenCalledTimes(1);
    expect(spyObj.click).toHaveBeenCalledWith();
  });

  it('should download a file with microsoft special functionality', () => {
    window.navigator.msSaveOrOpenBlob = createSpy();

    const file = new Blob(['that is my file content']);
    downloadBlob(file, 'my-file.ext');

    expect(window.navigator.msSaveOrOpenBlob).toHaveBeenCalledTimes(1);
    expect(window.navigator.msSaveOrOpenBlob).toHaveBeenCalledWith(
      file,
      'my-file.ext'
    );
  });
});
