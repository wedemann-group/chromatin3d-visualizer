/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  BufferAttribute,
  InterleavedBufferAttribute,
  Quaternion,
  ShaderMaterial,
  Vector2,
  Vector3,
} from 'three';
import Position from '../models/position.model';
import { ContainerDimension } from '../../core/services/view/models/container-dimension.model';
import { ContainerDimensionExtended } from '../../core/services/view/models/container-dimension-extended.model';

type PrivateQuaternion = {
  _x: number;
  _y: number;
  _z: number;
  _w: number;
};

function isVector3(com: any): com is Vector3 {
  return (com as Vector3).z !== undefined;
}

export const FRACTION_DIGITS_CAMERA = 8;

export const DEGREE_MAX_VALUE = 360;
export const DEGREE_ONE_QUARTER = DEGREE_MAX_VALUE / 4;

export function convertVectorArrayToFloat32Array<TI extends Vector2 | Vector3>(
  input: TI[]
): number[] {
  const result: number[] = [];
  for (const point of input) {
    result.push(point.x);
    result.push(point.y);
    if (isVector3(point)) result.push(point.z);
  }

  return result;
}

export function convertBufferAttrToVector3List(
  points: BufferAttribute | InterleavedBufferAttribute
): Vector3[] {
  const result: Vector3[] = [];
  for (let i = 0; i < points.count; ++i) {
    result.push(new Vector3(points.getX(i), points.getY(i), points.getZ(i)));
  }

  return result;
}

export function convertBufferAttrToVector3(
  points: BufferAttribute | InterleavedBufferAttribute,
  index: number
): Vector3 {
  return new Vector3(
    points.getX(index),
    points.getY(index),
    points.getZ(index)
  );
}

export function serializeVector3(vec: Vector3): string {
  return JSON.stringify({
    x: vec.x.toFixed(FRACTION_DIGITS_CAMERA),
    y: vec.y.toFixed(FRACTION_DIGITS_CAMERA),
    z: vec.z.toFixed(FRACTION_DIGITS_CAMERA),
  });
}

export function deserializeVector3(vecString: string): Vector3 {
  const parsedVector: { x: string; y: string; z: string } =
    JSON.parse(vecString);
  return new Vector3(
    parseFloat(parsedVector.x),
    parseFloat(parsedVector.y),
    parseFloat(parsedVector.z)
  );
}

export function convertDegreeToRadians(degree: number): number {
  return ((2 * Math.PI) / 360) * degree;
}

export function convertRadiansToDegree(radians: number): number {
  return (360 / (2 * Math.PI)) * radians;
}

/**
 * Returns the hFov of fov in degrees.
 * @param fov
 * @param aspect
 * @source https://github.com/mrdoob/three.js/issues/15968#issuecomment-472680184
 */
export function convertFOVTohFov(fov: number, aspect: number): number {
  return (
    (2 * Math.atan(Math.tan((fov * Math.PI) / 180 / 2) * aspect) * 180) /
    Math.PI
  );
}

export function getBoxDimension(
  positions: (Position | Vector3)[]
): ContainerDimension {
  let minX = Number.MAX_SAFE_INTEGER;
  let maxX = -Number.MAX_SAFE_INTEGER;
  let minY = Number.MAX_SAFE_INTEGER;
  let maxY = -Number.MAX_SAFE_INTEGER;
  let minZ = Number.MAX_SAFE_INTEGER;
  let maxZ = -Number.MAX_SAFE_INTEGER;
  for (const bead of positions) {
    minX = Math.min(bead.x, minX);
    maxX = Math.max(bead.x, maxX);

    minY = Math.min(bead.y, minY);
    maxY = Math.max(bead.y, maxY);

    minZ = Math.min(bead.z, minZ);
    maxZ = Math.max(bead.z, maxZ);
  }

  return {
    xStart: minX,
    xEnd: maxX,
    yStart: minY,
    yEnd: maxY,
    zStart: minZ,
    zEnd: maxZ,
  } as ContainerDimension;
}

export function getBoxSizeDimension(positions: (Position | Vector3)[]): {
  width: number;
  height: number;
  depth: number;
} {
  return getBoxSizeDimensionFromDim(getBoxDimension(positions));
}

export function getBoxSizeDimensionFromDim(info: ContainerDimension): {
  width: number;
  height: number;
  depth: number;
} {
  return {
    width: info.xEnd - info.xStart,
    height: info.yEnd - info.yStart,
    depth: info.zEnd - info.zStart,
  };
}

export function getCenterPosition(positions: (Position | Vector3)[]): {
  x: number;
  y: number;
  z: number;
} {
  return getCenterPositionFromDim(getBoxDimension(positions));
}

export function getCenterPositionFromDim(info: ContainerDimension): Position {
  return new Position(
    info.xStart + (info.xEnd - info.xStart) / 2,
    info.yStart + (info.yEnd - info.yStart) / 2,
    info.zStart + (info.zEnd - info.zStart) / 2
  );
}

export function getBoxDimensionCenterAndSizes(
  positions: (Position | Vector3)[]
): ContainerDimensionExtended {
  return getBoxDimensionCenterAndSizesFromDim(getBoxDimension(positions));
}

export function getBoxDimensionCenterAndSizesFromDim(
  info: ContainerDimension
): ContainerDimensionExtended {
  const center = getCenterPositionFromDim(info);
  const sizes = getBoxSizeDimensionFromDim(info);
  return {
    ...info,
    width: sizes.width,
    height: sizes.height,
    depth: sizes.depth,
    centerX: center.x,
    centerY: center.y,
    centerZ: center.z,
  } as ContainerDimensionExtended;
}

export function parseQuaternionOrReturnDefaultQuaternion(
  qu: Quaternion | PrivateQuaternion | null | undefined
): Quaternion {
  if (qu instanceof Quaternion) {
    return qu;
  } else if (qu) {
    const privateQuaternion = qu as PrivateQuaternion;
    return new Quaternion(
      privateQuaternion._x,
      privateQuaternion._y,
      privateQuaternion._z,
      privateQuaternion._w
    );
  }

  return new Quaternion();
}

export function isShaderWithUniforms(shader: any): shader is ShaderMaterial {
  return shader.uniforms != undefined;
}
