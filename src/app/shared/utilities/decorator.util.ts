/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Decorator } from '../decorators/models/decorator.model';
import { DECORATOR_CUSTOM_PREFIX } from '../decorators/constants';

export function getDecorators(
  target: any,
  propertyName: string | symbol
): Decorator[] {
  const keys: any[] = Reflect.getMetadataKeys(target, propertyName);
  return keys
    .filter((key) => key.toString().startsWith(DECORATOR_CUSTOM_PREFIX))
    .map((key) => ({
      name: key,
      value: Reflect.getMetadata(key, target, propertyName),
    }));
}
