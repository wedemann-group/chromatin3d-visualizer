export const REGEX_PATTERN_IS_RGBA_COLOR =
  /rgba\((\s*\d+\s*,\s*){3}(\d+(\.\d+)?|\.\d+)\s*\)/i;
export const REGEX_PATTERN_IS_RGB_COLOR = /rgb\((\s*\d+\s*,\s*){2}\s*\d+\s*\)/i;
export const REGEX_PATTERN_IS_MODERN_RGB_COLOR =
  /rgb\((\s*\d+\s*){3}(\/(\s*(\d+(\.\d+)?|\.\d+)\s*))?\)/i;
export const REGEX_PATTERN_RGBA_SPLITTER = /((\d+(\.\d+)?)|(\.\d+))/g;
