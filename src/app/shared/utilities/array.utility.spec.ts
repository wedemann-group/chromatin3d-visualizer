/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  convert2DArrayTo1D,
  equalsArray,
  findIndexAtPos,
} from './array.utility';

describe('Array Utility', () => {
  it('should find an index at position > 0', () => {
    const array = ['item1', 'item1', 'item2', 'item1', 'item1'];
    expect(findIndexAtPos(array, (v) => v === 'item1', 2)).toEqual(3);
    expect(findIndexAtPos(array, (v) => v === 'item1', 3)).toEqual(3);
  });

  it('should NOT find an index at position X', () => {
    const array = ['item1', 'item1', 'item2', 'item1', 'item1'];
    expect(findIndexAtPos(array, (v) => v === 'item5', 2)).toEqual(-1);
  });

  it('should convert 2d array to 1d', () => {
    expect(
      convert2DArrayTo1D([
        [5, 3],
        [6, 1, 3],
      ])
    ).toEqual([5, 3, 6, 1, 3]);
  });

  it('should equals two arrays', () => {
    expect(equalsArray([6, 1, 3], [6, 1, 3])).toEqual(true);
  });

  it('should NOT equals two arrays with different lengths', () => {
    expect(equalsArray([6, 1], [6, 1, 3])).toEqual(false);
  });

  it('should NOT equals two arrays', () => {
    expect(equalsArray([6, 1, 4], [6, 1, 3])).toEqual(false);
  });
});
