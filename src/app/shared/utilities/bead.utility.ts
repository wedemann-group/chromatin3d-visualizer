/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Quaternion, Vector3 } from 'three';
import NucleosomeBead from '../models/nucleosome-bead.model';
import Vector from '../models/vector.model';
import Bead from '../models/bead.model';
import Position from '../models/position.model';
import CohesinHeadBead from '../models/cohesin-head-bead.model';
import { CohesinRing } from '../models/cohesin-ring.model';
import { mod } from './math.utility';

export function getBeadPosition(bead: Bead, containerSize = -1): Position {
  const pbcMode = containerSize > 0;
  let beadPos = bead.position;

  if (bead instanceof NucleosomeBead) beadPos = bead.center;

  if (pbcMode)
    return new Position(
      mod(beadPos.x, containerSize),
      mod(beadPos.y, containerSize),
      mod(beadPos.z, containerSize)
    );

  return new Position(beadPos.x, beadPos.y, beadPos.z);
}

export function getRotationFromNucleosome(
  nuc: NucleosomeBead,
  containerSize = -1
): Quaternion {
  const pbcMode = containerSize > 0;

  let transVec: Vector;
  if (pbcMode)
    transVec = new Vector(
      mod(nuc.center.x, containerSize),
      mod(nuc.center.y, containerSize),
      mod(nuc.center.z, containerSize)
    );
  else transVec = new Vector(nuc.center.x, nuc.center.y, nuc.center.z);

  let posVec: Vector;
  if (pbcMode)
    posVec = new Vector(
      mod(nuc.position.x, containerSize),
      mod(nuc.position.y, containerSize),
      mod(nuc.position.z, containerSize)
    );
  else posVec = new Vector(nuc.position.x, nuc.position.y, nuc.position.z);

  const uVec = Vector.normalize(nuc.segmentVector);
  const vVec = Vector.normalize(
    Vector.crossProduct(
      Vector.normalize(nuc.segmentVector),
      Vector.normalize(nuc.fVector)
    )
  );
  let toCenterVec = transVec.sub(
    posVec.add(nuc.segmentVector.clone().mult(0.5))
  );
  toCenterVec.mult(-1);
  toCenterVec = Vector.normalize(toCenterVec);

  const refOrientVec = new Vector3(0.0, 1.0, 0.0);
  const newCenterVec = new Vector3(1.0, 0.0, 0.0);
  vVec.mult(-1);

  const rotUVec = new Quaternion();
  rotUVec.setFromUnitVectors(
    refOrientVec.normalize(),
    new Vector3(uVec.x, uVec.y, uVec.z).normalize()
  );
  const newRefToCenterVec = newCenterVec.applyQuaternion(rotUVec.clone());

  const rotRefToCenter = new Quaternion();
  rotRefToCenter.setFromUnitVectors(
    newRefToCenterVec.normalize(),
    new Vector3(vVec.x, vVec.y, vVec.z)
  );

  const rotCenterVec = new Quaternion();
  rotCenterVec.setFromUnitVectors(
    new Vector3(vVec.x, vVec.y, vVec.z),
    new Vector3(toCenterVec.x, toCenterVec.y, toCenterVec.z).normalize()
  );

  const rotOrientVec = new Quaternion();
  rotOrientVec.setFromUnitVectors(
    new Vector3(uVec.x, uVec.y, uVec.z).normalize(),
    new Vector3(
      nuc.centerOrientationVector.x,
      nuc.centerOrientationVector.y,
      nuc.centerOrientationVector.z
    ).normalize()
  );

  return rotUVec
    .premultiply(rotRefToCenter)
    .premultiply(rotCenterVec)
    .premultiply(rotOrientVec);
}

export function computeCohesinHingePosition(
  heads: CohesinHeadBead[],
  ring: CohesinRing
): Position {
  // cohesinDistance: number
  const headIndex = ring.index * 2;
  const head1 = heads[headIndex];
  const head2 = heads[headIndex + 1];

  const headDistanceTogether = head1.drawPosition
    .clone()
    .sub(head2.drawPosition);
  const headDistanceVector = new Vector(
    headDistanceTogether.x,
    headDistanceTogether.y,
    headDistanceTogether.z
  );
  /*
    if (Math.abs(cohesinDistance - headDistanceVector.length) > 1) {
    console.error('Distance is to large!');
    return new Position(0,0,0);
  }
     */

  headDistanceVector.div(2);

  const hingePos = head2.drawPosition.clone().add(headDistanceVector);
  const hingeToRingCenterDis = ring.centerPos.clone().sub(hingePos);
  const hingeToRingCenter = new Vector(
    hingeToRingCenterDis.x,
    hingeToRingCenterDis.y,
    hingeToRingCenterDis.z
  );
  hingeToRingCenter.mult(2);
  hingePos.add(hingeToRingCenter);

  return hingePos;
}

export function getLinkerDNASegments(
  beads: Bead[],
  linkerDNASplitter: number[] = []
): Bead[][] {
  const linkerDNASegments: Bead[][] = [];
  let segmentIndex = 0;
  let beadIndex = 0;
  for (const bead of beads) {
    if (bead instanceof CohesinHeadBead) {
      ++beadIndex;
      continue;
    }

    if (bead instanceof NucleosomeBead) {
      ++segmentIndex;
    } else if (linkerDNASplitter.find((index) => index === beadIndex)) {
      ++segmentIndex;
    } else {
      if (!linkerDNASegments[segmentIndex])
        linkerDNASegments[segmentIndex] = [];
      linkerDNASegments[segmentIndex].push(bead);
    }

    ++beadIndex;
  }

  return linkerDNASegments;
}
