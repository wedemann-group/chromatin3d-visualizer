/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { firstLetterLower } from './string.utility';

describe('String Utility', () => {
  it('should make first letter to lowercase', () => {
    expect(firstLetterLower('FIRST')).toEqual('fIRST');
  });

  it('lowercase string should equals same lowercase string', () => {
    expect(firstLetterLower('first')).toEqual('first');
  });
});
