/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  getFileExtension,
  getFileNameFromPath,
  getFileNameWithoutExtension,
  getFileSizeString,
  isArchive,
  isConfigFile,
  isDraggableFile,
  isReadableFile,
} from './file.utility';

describe('File Utility', () => {
  it('should detect a xml file as readable file', () => {
    expect(isReadableFile('my-file.name.xml')).toBeTruthy();
  });

  it('should detect a xml file as readable file and ignore case sensitive', () => {
    expect(isReadableFile('my-file.name.xMl')).toBeTruthy();
  });

  it('should detect a random extension as NOT readable file', () => {
    expect(isReadableFile('my-file.name.lui')).toBeFalsy();
  });

  it('should detect a draggable extension', () => {
    expect(isDraggableFile('my-file.name.lui.trj')).toBeTruthy();
  });

  it('should detect a non draggable extension', () => {
    expect(isDraggableFile('my-file.name.lui.random')).toBeFalsy();
  });

  it('should detect an archive', () => {
    expect(isArchive('my-file.name.zip')).toBeTruthy();
  });

  it('should detect a non archive', () => {
    expect(isArchive('my-file.name.fake-zip')).toBeFalsy();
  });

  it('should detect a magic file name as config file', () => {
    expect(isConfigFile('config.json')).toBeTruthy();
  });

  it('should detect a path as config file', () => {
    expect(
      isConfigFile('my/path/to/the/magic/name/file/config.json')
    ).toBeTruthy();
  });

  it('should detect a random file as non config file', () => {
    expect(
      isConfigFile('my/path/to/the/magic/name/file/config2.json')
    ).toBeFalsy();
  });

  it('should get the file name without the extension from file name', () => {
    expect(getFileNameWithoutExtension('my.file.ext')).toEqual('my.file');
  });

  it('should get the file name without the extension from file path', () => {
    expect(
      getFileNameWithoutExtension('my/path/to/my/file/my.file.ext')
    ).toEqual('my.file');
  });

  it('should get the file name if no extension present', () => {
    expect(getFileNameWithoutExtension('my/path/to/my/file/my-file')).toEqual(
      'my-file'
    );
  });

  it('should get file name from path', () => {
    expect(getFileNameFromPath('my/path/my-file.ext')).toEqual('my-file.ext');
  });

  it('should get file name from file name (no path)', () => {
    expect(getFileNameFromPath('my-file.ext')).toEqual('my-file.ext');
  });

  it('should get file name from windows path separator', () => {
    expect(getFileNameFromPath('my\\path\\my-file.ext')).toEqual('my-file.ext');
  });

  it('should get file name from path and use the last path separator', () => {
    expect(getFileNameFromPath('my/path\\my-file.ext')).toEqual('my-file.ext');
  });

  it('should get file extension', () => {
    expect(getFileExtension('my-lui-file.xml')).toEqual('xml');
  });

  it('should get file extension, case sensitive', () => {
    expect(getFileExtension('my-lui-file.XML')).toEqual('XML');
  });

  it('should get the real file extension by multiple dots', () => {
    expect(getFileExtension('my-lui-file.fake.ext.real-ext')).toEqual(
      'real-ext'
    );
  });

  it('should get an empty extension by file name without extension', () => {
    expect(getFileExtension('my-lui-file')).toEqual('');
  });

  it('should get file size in bytes', () => {
    expect(getFileSizeString(1023)).toEqual('1023 Bytes');
  });

  it('should get file size in KB', () => {
    expect(getFileSizeString(1024)).toEqual('1.00 KB');
  });

  it('should get rounded file size in KB', () => {
    expect(getFileSizeString(10000)).toEqual('9.77 KB');
  });

  it('should specify the file size maximum in TB', () => {
    expect(getFileSizeString(Number.MAX_VALUE).endsWith(' TB')).toBeTruthy();
  });
});
