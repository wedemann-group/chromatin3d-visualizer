/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

/**
 * This function is supposed to calculated the modulo of a given number to a given base.
 * @param val This is the value that is being viewed in the realm of the base.
 * @param base This is the base the given value is being viewed in.
 * @return Returns the value transformed into a value mod the base.
 */
export function mod(val: number, base: number): number {
  // second addition and mod calculation supposed to transform negative values (val < 0) into positive ones (val > 0)
  return ((val % base) + base) % base;
}
