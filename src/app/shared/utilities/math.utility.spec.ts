/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { mod } from './math.utility';

describe('Math Utility', () => {
  it('should calculate modulo of 5 to base 2', () => {
    expect(mod(5, 2)).toEqual(1);
  });

  it('should calculate modulo of -5 to base 2', () => {
    expect(mod(-5, 2)).toEqual(1);
  });
});
