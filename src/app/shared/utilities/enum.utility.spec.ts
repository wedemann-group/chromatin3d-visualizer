/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { getValueOfEnumKey } from './enum.utility';
import { ECalendar } from '../models/calendar.enum';

describe('Enum utility', () => {
  it('should return enum value from key', () => {
    expect(getValueOfEnumKey<ECalendar>(ECalendar, 'a day')).toEqual(
      ECalendar.DAY
    );
  });

  it('should NOT return a value from undefined input', () => {
    expect(getValueOfEnumKey<ECalendar>(ECalendar, undefined)).toEqual(
      undefined
    );
    expect(getValueOfEnumKey<ECalendar>(ECalendar, null)).toEqual(undefined);
  });

  it('should NOT find a value from invalid key', () => {
    expect(getValueOfEnumKey<ECalendar>(ECalendar, 'INVALID')).toEqual(
      undefined
    );
  });
});
