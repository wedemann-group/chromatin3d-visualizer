/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Vector3 } from 'three';
import {
  getOrthogonalVectorFromVector,
  getOrthogonalVectorFromVectorPoints,
  getOrthogonalVectorFromVectors,
} from './vector.utility';

describe('Vector Utility', () => {
  it('should get orthogonal vector from vector points', () => {
    const vec1 = new Vector3(1, 0, 0);
    expect(
      getOrthogonalVectorFromVectorPoints(1, 0, 0).angleTo(vec1)
    ).toBeCloseTo(Math.PI / 2, Math.PI / 10);
  });

  it('should get orthogonal vector from vector', () => {
    const vec1 = new Vector3(1, 0, 0);
    expect(getOrthogonalVectorFromVector(vec1).angleTo(vec1)).toBeCloseTo(
      Math.PI / 2,
      Math.PI / 10
    );
  });

  it('should get orthogonal vector from vectors', () => {
    const vec1 = new Vector3(1, 0, 0);
    const vec2 = new Vector3(0, 1, 0);
    expect(
      getOrthogonalVectorFromVectors(vec1, vec2).angleTo(vec1)
    ).toBeCloseTo(Math.PI / 2, Math.PI / 10);
  });

  it('should get orthogonal vector from same vectors', () => {
    const vec1 = new Vector3(1, 0, 0);
    const vec2 = new Vector3(1, 0, 0);
    expect(
      getOrthogonalVectorFromVectors(vec1, vec2).angleTo(vec1)
    ).toBeCloseTo(Math.PI / 2, Math.PI / 10);
  });
});
