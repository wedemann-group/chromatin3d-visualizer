/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Vector from '../models/vector.model';
import Position from '../models/position.model';
import { firstLetterLower } from './string.utility';

/**
 * Converts a string expression in a friendly variable name.
 * @example NumberOfBeads => numberOfBeads
 *          PBCContainerStartDimension => pbcContainerStartDimension
 * @param input
 */
export function getLintFriendlyVariableName(input: string): string {
  if (input.length < 2) return firstLetterLower(input);

  const chars = input.split('');
  const firstLowerCaseChar = chars.findIndex((c) => c === c.toLowerCase());

  // if the first two chars are upper case, then it is an own definition like "PBC" and that is okay
  if (firstLowerCaseChar > 1) {
    return `${chars
      .splice(0, firstLowerCaseChar - 1) // -2 because the char "-1" is already upper case and that is fine
      .join('')
      .toLowerCase()}${chars.join('')}`;
  }
  return firstLetterLower(input);
}

export function parseNumber(number: string): number {
  if (number.includes('.')) return parseFloat(number);

  return parseInt(number);
}

export function parseBoolean(value: string): boolean {
  value = value.toString().toLowerCase();
  const number = parseInt(value);
  if (!isNaN(number)) return number !== 0;

  if (value === 'false') return false;

  return value === 'true';
}

export function parseBooleanNumber(val: number | undefined): boolean {
  if (!val) return false;

  return val !== 0;
}

export function parseVectorOrReturnDefaultVector(
  vec: Vector | null | undefined
): Vector {
  if (vec instanceof Vector) return vec;
  else if (vec)
    return new Vector((vec as any)._x, (vec as any)._y, (vec as any)._z);

  return new Vector(0, 0, 0);
}

export function parsePositionOrReturnDefaultPosition(
  vec: Position | null | undefined
): Position {
  if (vec instanceof Position) return vec;
  else if (vec)
    return new Position((vec as any)._x, (vec as any)._y, (vec as any)._z);

  return new Position(0, 0, 0);
}
