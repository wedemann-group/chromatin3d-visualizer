/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  getLintFriendlyVariableName,
  parseBoolean,
  parseBooleanNumber,
  parsePositionOrReturnDefaultPosition,
  parseVectorOrReturnDefaultVector,
} from './parser.utility';
import Vector from '../models/vector.model';
import Position from '../models/position.model';

describe('Parser Utility', () => {
  it('should get lint friendly variable name', () => {
    expect(getLintFriendlyVariableName('L')).toEqual('l');
    expect(getLintFriendlyVariableName('l')).toEqual('l');
    expect(getLintFriendlyVariableName('')).toEqual('');
  });

  it('should get lint friendly variable name #2', () => {
    expect(getLintFriendlyVariableName('Low')).toEqual('low');
    expect(getLintFriendlyVariableName('ThatIsMyVariable')).toEqual(
      'thatIsMyVariable'
    );
    expect(getLintFriendlyVariableName('PBCContainer')).toEqual('pbcContainer');
  });

  it('should parse a string to a boolean', () => {
    expect(parseBoolean('true')).toBeTruthy();
    expect(parseBoolean('false')).toBeFalsy();
    expect(parseBoolean('random text')).toBeFalsy();
    expect(parseBoolean('0')).toBeFalsy();
    expect(parseBoolean('1')).toBeTruthy();
    expect(parseBoolean('10')).toBeTruthy();
  });

  it('should parse a number to a boolean', () => {
    expect(parseBooleanNumber(undefined)).toBeFalsy();
    expect(parseBooleanNumber(0)).toBeFalsy();
    expect(parseBooleanNumber(1)).toBeTruthy();
    expect(parseBooleanNumber(10)).toBeTruthy();
  });

  it('should parse a vector object to vector class', () => {
    const vec = new Vector(10, 5, 15);
    expect(parseVectorOrReturnDefaultVector(vec) === vec).toBeTruthy();
    expect(
      parseVectorOrReturnDefaultVector(JSON.parse(JSON.stringify(vec))).equals(
        vec
      )
    ).toBeTruthy();
  });

  it('should return a default vector by null/undefinded', () => {
    expect(
      parseVectorOrReturnDefaultVector(null).equals(new Vector(0, 0, 0))
    ).toBeTruthy();
    expect(
      parseVectorOrReturnDefaultVector(undefined).equals(new Vector(0, 0, 0))
    ).toBeTruthy();
  });

  it('should parse a position object to position class', () => {
    const vec = new Position(10, 5, 15);
    expect(parsePositionOrReturnDefaultPosition(vec) === vec).toBeTruthy();
    expect(
      parsePositionOrReturnDefaultPosition(
        JSON.parse(JSON.stringify(vec))
      ).equals(vec)
    ).toBeTruthy();
  });

  it('should return a default position by null/undefinded', () => {
    expect(
      parsePositionOrReturnDefaultPosition(null).equals(new Position(0, 0, 0))
    ).toBeTruthy();
    expect(
      parsePositionOrReturnDefaultPosition(undefined).equals(
        new Position(0, 0, 0)
      )
    ).toBeTruthy();
  });
});
