/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { IChangeableColors } from '../../modules/dockable-workspace/viewer/beads/changeable-colors.interface';
import { ColorBead } from '../../core/services/colors/models/color-bead.model';
import { MultipleBuilderResult } from '../models/multiple-builder-result.model';
import {
  MultipleBuilder,
  MultipleLinkerBuilder,
} from '../models/multiple-builder.model';
import { Texture } from 'three';
import QualityLevel from '../../core/services/session-mananger/models/quality-level.model';
import InstancedObjectBuilder from '../../modules/dockable-workspace/viewer/beads/instanced-object.builder';

export function setConfigSpecificColors(
  builder: IChangeableColors | undefined,
  colorBeads: ColorBead[]
): void {
  if (!builder || colorBeads.length === 0) return;

  builder.setConfigSpecificColorsFromColorBeads(colorBeads);
}

export function getBuilderInstance(
  builder: MultipleBuilderResult,
  texture: Texture,
  qualityLevel: QualityLevel,
  linkerSplitter: number[]
): InstancedObjectBuilder<any, any, any, any> {
  return builder.builder.prototype.constructor.length === 4
    ? new (builder.builder as MultipleBuilder)(
        builder.param,
        texture,
        qualityLevel,
        builder.renderSize
      )
    : new (builder.builder as MultipleLinkerBuilder)(
        builder.param,
        texture,
        qualityLevel,
        builder.renderSize,
        linkerSplitter
      );
}
