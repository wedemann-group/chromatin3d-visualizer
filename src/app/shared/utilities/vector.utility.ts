/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Vector3 } from 'three';
import Vector from '../models/vector.model';

export function getOrthogonalVectorFromVectorPoints(
  x: number,
  y: number,
  z: number
): Vector3 {
  return z < x ? new Vector3(y, -x, 0) : new Vector3(0, -z, y);
}

export function getOrthogonalVectorFromVector(vec: Vector3): Vector3 {
  return getOrthogonalVectorFromVectorPoints(vec.x, vec.y, vec.z);
}

export function getOrthogonalVectorFromVectors(
  vec1: Vector3,
  vec2: Vector3
): Vector3 {
  const vec1t = new Vector(vec1.x, vec1.y, vec1.z);
  const vec2t = new Vector(vec2.x, vec2.y, vec2.z);
  const crossVec = Vector.crossProduct(vec1t, vec2t);
  if (crossVec.length > 0)
    return new Vector3(crossVec.x, crossVec.y, crossVec.z);

  return getOrthogonalVectorFromVectorPoints(vec1.x, vec1.y, vec1.z);
}
