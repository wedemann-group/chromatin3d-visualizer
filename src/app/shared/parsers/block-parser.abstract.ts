/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Parser from './parser.abstract';

export default abstract class BlockParser extends Parser {
  protected readonly validationStack: string[] = [];

  protected addBlockToStack(blockName: string): void {
    this.validationStack.push(blockName);
  }

  protected popBlockFromStackAndCheckValidation(
    blockName: string,
    offset: number
  ): void {
    if (this.validationStack.length === 0)
      throw new Error(`The file content is invalid at ${offset}.`);

    const lastItem = this.validationStack[this.validationStack.length - 1];
    if (lastItem === blockName) {
      this.validationStack.pop();
    } else {
      throw new Error(`The file content is invalid at ${offset}.`);
    }
  }

  protected checkValidationStack(): void {
    if (this.validationStack.length > 0)
      throw new Error(`The file content is invalid, block not closed.`);
  }
}
