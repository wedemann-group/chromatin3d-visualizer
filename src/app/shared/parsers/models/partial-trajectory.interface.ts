/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import IParameters from '../../models/parameters.interface';
import IMetaInfo from '../../models/meta-info.interface';

/**
 * Represents a partial of trajectory that members are not "read only" and optional.
 */
export default interface IPartialTrajectory {
  globalParameters: IParameters;
  metaInfo: IMetaInfo | undefined;
  /**
   * Specifies the number of configurations the trajectory holds.
   */
  configurationLength: number;
  maxBeadLength: number;
}
