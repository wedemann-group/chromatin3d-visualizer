/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Configuration } from '../../models/configuration.model';
import ConfigurationParser from '../configuration-parser.abstract';
import { getFileExtension } from '../../utilities/file.utility';
import { ConfigurationOffset } from '../models/configuration-offset.model';
import { ChunkReader } from '../../readers/chunk-reader';
import Bead from '../../models/bead.model';
import SoftwareNucleosomeBead from '../../models/sw-nucleosome-bead.model';
import Position from '../../models/position.model';
import { Euler, Matrix4, Quaternion, Vector3 } from 'three';
import Vector from '../../models/vector.model';
import DNABead from '../../models/dna-bead.model';

/**
 * Specifies the amount of DNA beads between two nucleosomes.
 */
const DNA_SEGMENTS_POINTS = 4;
/**
 * Specifies the number of skips between the beginning and end of two nucleosomes to be used as positions for the DNA beads.
 */
const DNA_SEGMENTS_TOLERANCE = 1;

export default class AiPredictionConfigurationReaderParser extends ConfigurationParser {
  public static useThisParser(fileName: string): boolean {
    return getFileExtension(fileName).toLowerCase() === 'csv';
  }

  /**
   * Creates DNA beads for the visualization.
   * @param prevNuc The previous nucleosome.
   * @param currentPos The position of the "new" nucleosome.
   * @param currentIndex The index to set the ID of beads.
   * @private
   */
  private static getDNABeads(
    prevNuc: SoftwareNucleosomeBead,
    currentPos: Position,
    currentIndex: number
  ): DNABead[] {
    const beads: DNABead[] = [];

    const dnaVectorPos = currentPos.clone().sub(prevNuc.position);
    const dnaVector = new Vector(
      dnaVectorPos.x,
      dnaVectorPos.y,
      dnaVectorPos.z
    );
    for (
      let j = DNA_SEGMENTS_TOLERANCE;
      j < DNA_SEGMENTS_POINTS - DNA_SEGMENTS_TOLERANCE;
      ++j
    ) {
      const dnaPos = prevNuc.position
        .clone()
        .add(dnaVector.clone().div(DNA_SEGMENTS_POINTS).mult(j));
      beads.push(
        new DNABead({
          id: currentIndex + j,
          position: dnaPos,
          drawPosition: dnaPos,
        })
      );
    }

    return beads;
  }

  public async parse(
    file: File,
    configOffset: ConfigurationOffset
  ): Promise<Configuration> {
    const line = await new ChunkReader(file, {
      seek: configOffset.offset,
      readLength: configOffset.endOffset,
    }).readChunkAsString(configOffset.offset);

    const values = line.split(',').map((v) => parseFloat(v));
    const beads: Bead[] = [];

    beads.push(
      new SoftwareNucleosomeBead({
        id: -1,
        position: new Position(0, 0, 0),
        drawPosition: new Position(0, 0, 0),
        rotation: new Quaternion(),
      })
    );

    /*
     * Manually set the first nucleosome for the config, as this is missing from the files
     * returned by the AI. Position and rotation can be set to any value (this will influence
     * the orientation of the final model).
     */
    let currentPos = beads[0].drawPosition.clone();
    let prevRot = new Euler().setFromQuaternion(
      (beads[0] as SoftwareNucleosomeBead).rotation
    );

    for (let i = 0; i < values.length; i += 7) {
      /*
       * Read values out of the file:
       * x: Relative x coordinates of the nucleosome encoded in the previous nuc's coordinate system.
       * y: Relative y coordinates of the nucleosome encoded in the previous nuc's coordinate system.
       * z: Relative z coordinates of the nucleosome encoded in the previous nuc's coordinate system.
       * rotX: Relative euler rotation (around the x-axis) between the coordinate system of the previous nuc and the current one.
       * rotY: Relative euler rotation (around the y-axis) between the coordinate system of the previous nuc and the current one.
       * rotZ: Relative euler rotation (around the z-axis) between the coordinate system of the previous nuc and the current one.
       */
      const x = values[i];
      const y = values[i + 1];
      const z = values[i + 2];
      const rotX = values[i + 3];
      const rotY = values[i + 4];
      const rotZ = values[i + 5];
      // const dnaLen = values[i + 6];

      // Previous read nucleosome object from the file
      const prevNuc = beads
        .slice()
        .reverse()
        .find((b) => b instanceof SoftwareNucleosomeBead) as
        | SoftwareNucleosomeBead
        | undefined;
      // Euler rotation condensed in a Euler object
      const rot = new Euler(rotX, rotY, rotZ);

      if (!prevNuc) throw new Error('Prev nuc is missing!');

      // Segment vector, which describes the relative position of the current nuc in the local coordinates of the previous one
      const segVec = new Vector3(x, y, z);
      // Transform local coordinates to global coordinates using prevRot = R1cust = custom rotation of previous nuc
      // segVecGlob = R1cust * segVec
      segVec.applyEuler(prevRot);
      // Add relative global position onto previous global position
      // nuc2PosGlob = nuc1PosGlob + segVecGlob
      currentPos.add(new Position(segVec.x, segVec.y, segVec.z));

      /*
       * Use rotation (RL) between local coordinates of the previous nucleosome (R1) and the current one (R2), to determine the
       * current local coordinate rotation (R2curr) using the custom, current rotation of the previous nucleosome (R1cust) via:
       * <br>
       * R2curr = (RL * R1cust^-1)^-1 = (R2^-1 * R1 * R1cust^-1)^-1 = R2cust
       *                                     / \
       *                                      |
       *             (see data-preparer: bead.utility, "getDiffEuler" function)
       */
      prevRot.setFromRotationMatrix(
        new Matrix4()
          .makeRotationFromEuler(rot)
          .multiply(new Matrix4().makeRotationFromEuler(prevRot).invert())
          .invert()
      );

      // create DNA segments for the visualization
      // ("fake" segments only; original segments can't be created because no information is available)
      beads.push(
        ...AiPredictionConfigurationReaderParser.getDNABeads(
          prevNuc,
          currentPos,
          i
        )
      );

      // add nuc to bead list
      beads.push(
        new SoftwareNucleosomeBead({
          id: i,
          position: currentPos.clone(),
          drawPosition: currentPos.clone(),
          rotation: new Quaternion().setFromEuler(prevRot),
        })
      );
    }

    return new Configuration({ beads });
  }
}
