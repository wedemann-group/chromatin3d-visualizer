/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

/**
 * This file contains the constants (mostly "magic strings") to parse a trj file.
 */

/** */
export const BLOCK_BEGIN = 'BEGIN';
export const BLOCK_END = 'END';
export const BLOCK_CONFIGURATION = 'Configuration'.toLowerCase();
export const BLOCK_PARAMETERS = 'Parameters'.toLowerCase();
export const BLOCK_SPECIES = 'Species'.toLowerCase();
export const BLOCK_CONTENT_SIM_STEPS_MC = 'MC';
export const EPS = 1e-8;
export const TWO_PI = Math.PI * 2;

export const BEAD_MARKER_DNA = 'D';
export const BEAD_MARKER_DNA_UNCHARGED = 'U';
export const BEAD_MARKER_NUCLEOSOME = 'N';
export const BEAD_MARKER_COHESIN_HEAD = 'C';
