/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Matrix from '../../../models/matrix.model';
import Vector from '../../../models/vector.model';

export default class Matrix3x3 extends Matrix {
  public constructor() {
    super(3, 3);
  }

  public static getRotationMatrix(n: Vector, alpha: number): Matrix3x3 {
    const sin = Math.sin(alpha);
    const cos = Math.cos(alpha);

    const result = new Matrix3x3();

    result.data = [
      [
        Math.pow(n.x, 2) * (1 - cos) + cos,
        n.x * n.y * (1 - cos) - n.z * sin,
        n.x * n.z * (1 - cos) + n.y * sin,
      ],
      [
        n.y * n.x * (1 - cos) + n.z * sin,
        Math.pow(n.y, 2) * (1 - cos) + cos,
        n.y * n.z * (1 - cos) - n.x * sin,
      ],
      [
        n.z * n.x * (1 - cos) - n.y * sin,
        n.z * n.y * (1 - cos) + n.x * sin,
        Math.pow(n.z, 2) * (1 - cos) + cos,
      ],
    ];

    return result;
  }
}
