/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import ConfigurationReaderParser from './configuration-reader.parser';
import { Configuration } from '../../models/configuration.model';

export default class CachedConfigurationReaderParser extends ConfigurationReaderParser {
  protected endConfiguration(): boolean {
    if (!this.currentConfiguration) throw new Error();

    this.result = new Configuration(this.currentConfiguration);

    return true;
  }
}
