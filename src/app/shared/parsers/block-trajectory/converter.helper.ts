/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import IFakeConfiguration from './models/fake-configuration.interface';
import Vector from '../../models/vector.model';
import IConfiguration from '../../models/configuration.interface';
import IBlockTrajectoryNucleosomeBead from './models/block-trajectory-nucleosome-bead.interface';
import IBlockTrajectoryBead from './models/block-trajectory-bead.interface';
import * as Constants from './constants.static';
import Bead from '../../models/bead.model';
import SoftwareNucleosomeBead from '../../models/sw-nucleosome-bead.model';
import CohesinHeadBead from '../../models/cohesin-head-bead.model';
import DNABead from '../../models/dna-bead.model';
import Matrix3x3 from './models/matrix-3x3.model';

export function convertFakeConfigurationToOriginal(
  configuration: IFakeConfiguration,
  speciesList: string[],
  firstFVector: Vector
): IConfiguration {
  for (let i = 0; i < configuration.beads.length; ++i) {
    const bead = configuration.beads[i];

    // compute fVector
    if (i === 0) {
      bead.fVector = firstFVector;
    } else {
      bead.fVector = computeFVector(
        configuration.beads[i - 1],
        configuration.beads[i],
        configuration.beads[i + 1]
      );
    }

    // check if nucleosome
    if ((bead as IBlockTrajectoryNucleosomeBead).epsilon !== undefined) {
      computeCenterPosition(bead as IBlockTrajectoryNucleosomeBead);
    }
  }

  const beads = createBeadChainFromTrajectoryConfig(speciesList, configuration);

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  delete configuration.beads;
  return { ...configuration, beads } as IConfiguration;
}

function computeFVector(
  prevBead: IBlockTrajectoryBead,
  currentBead: IBlockTrajectoryBead,
  nextBead: IBlockTrajectoryBead | undefined | null
): Vector {
  let twistAngle = currentBead.twistAngle;
  if (!nextBead) {
    currentBead.segmentVector = prevBead.segmentVector;
  }

  twistAngle += prevBead.intrinsicTwist;
  const uIm1 = Vector.normalize(prevBead.segmentVector);
  const uI = Vector.normalize(currentBead.segmentVector);

  let myP = Vector.crossProduct(uIm1, uI);
  if (myP.length < Constants.EPS) {
    const vecF = prevBead.fVector.clone();
    vecF.mult(Math.cos(twistAngle));
    vecF.add(
      Vector.mult(
        Vector.crossProduct(uI, prevBead.fVector),
        Math.sin(twistAngle)
      )
    );
    return Vector.normalize(vecF);
  } else {
    myP = Vector.normalize(myP);
    const prevFNormalized = Vector.normalize(prevBead.fVector);
    let cosAlpha = Vector.dotProduct(prevFNormalized, myP);
    if (Math.abs(1.0 - cosAlpha) < 1e3 * Constants.EPS) cosAlpha = 1.0;
    else if (Math.abs(1.0 + cosAlpha) < 1e3 * Constants.EPS) cosAlpha = -1.0;

    const prevF = Vector.crossProduct(prevFNormalized, myP);
    let alpha = Math.acos(cosAlpha);
    if (Vector.dotProduct(uIm1, prevF) <= 0.0) alpha = Constants.TWO_PI - alpha;

    let gamma = twistAngle - alpha;
    while (gamma > Constants.TWO_PI) gamma -= Constants.TWO_PI;
    while (gamma < -Constants.TWO_PI) gamma += Constants.TWO_PI;

    const vecF = myP.clone();
    vecF.mult(Math.cos(gamma));
    vecF.add(Vector.mult(Vector.crossProduct(uI, myP), Math.sin(gamma)));

    return Vector.normalize(vecF);
  }
}

function createBeadChainFromTrajectoryConfig(
  speciesList: string[],
  configuration: IFakeConfiguration
): Bead[] {
  const result: Bead[] = [];
  for (let i = 0; i < speciesList.length; ++i) {
    const obj = configuration.beads[i];
    obj.id = i;

    if (speciesList[i] === Constants.BEAD_MARKER_NUCLEOSOME) {
      result.push(new SoftwareNucleosomeBead(obj));
    } else if (speciesList[i] === Constants.BEAD_MARKER_COHESIN_HEAD) {
      result.push(new CohesinHeadBead(obj));
    } else if (speciesList[i] === Constants.BEAD_MARKER_DNA) {
      result.push(new DNABead({ ...obj, charged: true }));
    } else if (speciesList[i] === Constants.BEAD_MARKER_DNA_UNCHARGED) {
      result.push(new DNABead({ ...obj, charged: false }));
    } else {
      console.warn('Unknown bead marker ', speciesList[i]);
    }
  }

  return result;
}

function computeCenterPosition(
  nucleosome: IBlockTrajectoryNucleosomeBead
): void {
  // compute v-vector ( u x f )
  const vecV = Vector.crossProduct(
    nucleosome.segmentVector,
    nucleosome.fVector
  );

  let nucCenterOrientation = vecV;
  if (nucleosome.epsilon !== 0 || nucleosome.phi !== 0) {
    const rotateMatrixSeg = Matrix3x3.getRotationMatrix(
      Vector.normalize(nucleosome.segmentVector),
      nucleosome.epsilon
    );
    const rotateMatrixFVec = Matrix3x3.getRotationMatrix(
      Vector.normalize(nucleosome.fVector),
      nucleosome.phi
    );

    rotateMatrixFVec.multiply(rotateMatrixSeg);

    nucCenterOrientation = rotateMatrixFVec.multiplyVector(vecV);
  }

  const vecAi = Vector.normalize(nucCenterOrientation);
  vecAi.mult(nucleosome.nucCenterDistance);

  nucleosome.center = nucleosome.position.clone();
  nucleosome.center.add(nucleosome.segmentVector.clone().div(2));
  nucleosome.center.add(vecAi);

  // compute "centerOrientationVector"
  nucleosome.centerOrientationVector = Vector.normalize(
    nucleosome.segmentVector
  );
  if (
    (nucleosome.delta && nucleosome.delta !== 0) ||
    (nucleosome.zeta && nucleosome.zeta !== 0)
  ) {
    const rotateMatrixV = Matrix3x3.getRotationMatrix(
      Vector.normalize(vecV),
      nucleosome.delta ?? 0
    );
    const rotateMatrixFVector = Matrix3x3.getRotationMatrix(
      Vector.normalize(nucleosome.fVector),
      nucleosome.zeta ?? 0
    );
    rotateMatrixFVector.multiply(rotateMatrixV);
    nucleosome.centerOrientationVector = Vector.normalize(
      rotateMatrixFVector.multiplyVector(nucleosome.segmentVector)
    );
  }
}
