/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

/**
 * This file contains the constants to parse a xml file.
 * The most "magic strings" are tag names.
 */

/** */
export const XML_ELEMENT_CONFIGURATION = 'configuration'.toLowerCase();
export const XML_ELEMENT_PARAMETER = 'parameter'.toLowerCase();

export const PARAMETER_NUMBER_OF_BEADS = 'NumberOfBeads';
export const PARAMETER_RADIUS_DNA = 'RadiusDNA';
export const PARAMETER_RADIUS_NUCLEOSOME = 'NucleosomeRadius';
export const PARAMETER_HEIGHT_NUCLEOSOME = 'NucleosomeHeight';
export const PARAMETER_CHAIN_IS_CLOSED = 'ChainIsClosed';
export const PARAMETER_PBC_START_DIMENSION = 'PBCContainerStartDimension';
export const PARAMETER_PBC_CUBE_SIZE_LJ = 'CubeSizeLJ';
export const PARAMETER_PBC_CUBE_SIZE_DNA_NUC_ITR = 'CubeSizeDNANucIntersection';
export const PARAMETER_PBC_CUBE_SIZE_ESTAT = 'CubeSizeEstat';

export const REGEX_XML_HEADER = /<\?[^<>]+\?>/gi;

export const REGEX_XML_ELEMENT_START = /<[^\/][^<>]*>/i;
export const REGEX_XML_ELEMENT_END = /(<\/[^<>]*|<[^<>]+\/>)/i;
export const REGEX_XML_ELEMENT_NAME = /((?<=<)|(?<=<\/))[^\s\/<>]+/i;

export const REQUIRED_PARAMETER_NAME_VALIDATION_STACK = [
  'trajectory',
  'parameters',
  'parameter',
  'name',
];

export const REQUIRED_PARAMETER_OVERRIDE_NAME_VALIDATION_STACK = [
  'trajectory',
  'configurations',
  'configuration',
  'overwrite_parameters',
  'parameter',
  'name',
];

export const REQUIRED_PARAMETER_VALUE_VALIDATION_STACK = [
  'trajectory',
  'parameters',
  'parameter',
  'value',
];

export const REQUIRED_PARAMETER_OVERRIDE_VALUE_VALIDATION_STACK = [
  'trajectory',
  'configurations',
  'configuration',
  'overwrite_parameters',
  'parameter',
  'value',
];

export const REQUIRED_DATE_VALIDATION_STACK = ['trajectory', 'date'];
export const REQUIRED_SCHEMA_VERSION_VALIDATION_STACK = [
  'trajectory',
  'schema_version',
];
export const REQUIRED_PROGRAM_VALIDATION_STACK = ['trajectory', 'program'];
export const REQUIRED_PROGRAM_VERSION_VALIDATION_STACK = [
  'trajectory',
  'program_version',
];
