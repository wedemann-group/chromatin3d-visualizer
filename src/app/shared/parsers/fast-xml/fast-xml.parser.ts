/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import BlockParser from '../block-parser.abstract';
import { getFileExtension } from '../../utilities/file.utility';
import { CachedTrajectory } from '../models/cached-trajectory.model';
import { XmlBlockReader } from '../../readers/xml-block-reader';
import IParameters from '../../models/parameters.interface';
import IMetaInfo from '../../models/meta-info.interface';
import { ConfigurationOffset } from '../models/configuration-offset.model';
import * as Constants from './constants.static';
import * as ParserConstants from '../constants.static';
import { equalsArray } from '../../utilities/array.utility';
import PartialTrajectory from '../models/partial-trajectory.model';
import MetaInfo from '../../models/meta-info.model';
import Parameters from '../../models/parameters.model';
import {
  parseNumber,
  getLintFriendlyVariableName,
} from '../../utilities/parser.utility';

/**
 * Represents a xml trajectory parser (for ".xml" files), which first reads
 * the most necessary information from the whole file, which will be used at
 * runtime.
 */
export default class FastXmlParser extends BlockParser {
  private readonly _configurationOffsets: ConfigurationOffset[] = [];
  private readonly _globalParameters: Partial<IParameters> = {};
  private readonly _globalMetaInfo: Partial<IMetaInfo> = {};

  private _currentParameterName: string | undefined = undefined;
  private readonly _beadsPerConfiguration: number[] = [];

  public static useThisParser(fileName: string): boolean {
    return getFileExtension(fileName.toLowerCase()) === 'xml';
  }

  protected async parseTrajectoryFile(file: File): Promise<CachedTrajectory> {
    let offset = 0;
    let lastUpdate = 0;
    for await (const blocks of new XmlBlockReader(file, {
      chunkSize: FastXmlParser.CHUNK_SIZE,
    }).blocks()) {
      const matches = XmlBlockReader.getBlocksAndValues(blocks);
      matches.forEach((m) => (m.match.index += offset));

      for (const match of matches.filter(
        (m) => !m.match[0].match(Constants.REGEX_XML_HEADER)
      )) {
        if (!match.isValue) {
          this.readElement(match.match);
        } else {
          this.readValue(match.match);
        }
      }

      offset += blocks.length;

      const currentUpdate = Date.now();
      if (
        Math.abs(currentUpdate - lastUpdate) >
        FastXmlParser.UPDATE_NOTIFICATION_DURATION
      ) {
        this._readBytes.next(offset);
        lastUpdate = currentUpdate;
      }
    }

    return {
      trajectory: new PartialTrajectory({
        metaInfo: new MetaInfo(this._globalMetaInfo),
        globalParameters: new Parameters(this._globalParameters),
        configurationLength: this._configurationOffsets.length,
        maxBeadLength: this.getMaxBeadLength(),
      }),
      file,
      configurations: this._configurationOffsets,
    };
  }

  private readElement(match: RegExpExecArray): void {
    const isElementStart = Constants.REGEX_XML_ELEMENT_START.exec(match[0]);
    const isElementEnd = Constants.REGEX_XML_ELEMENT_END.exec(match[0]);

    if (isElementStart && isElementEnd) return; // means that match equals "<element />"

    if (isElementStart) this.parseBlockBegin(match);
    else if (isElementEnd) this.parseBlockEnd(match);
  }

  private readValue(match: RegExpExecArray): void {
    this.readMetaInfo(match);
    this.readParameter(match);
  }

  private readMetaInfo(match: RegExpExecArray): void {
    if (
      equalsArray<string>(
        this.validationStack,
        Constants.REQUIRED_DATE_VALIDATION_STACK
      )
    ) {
      this._globalMetaInfo.date = Date.parse(match[0]);
    } else if (
      equalsArray<string>(
        this.validationStack,
        Constants.REQUIRED_SCHEMA_VERSION_VALIDATION_STACK
      )
    ) {
      this._globalMetaInfo.schemaVersion = match[0];
    } else if (
      equalsArray<string>(
        this.validationStack,
        Constants.REQUIRED_PROGRAM_VALIDATION_STACK
      )
    ) {
      this._globalMetaInfo.program = match[0];
    } else if (
      equalsArray<string>(
        this.validationStack,
        Constants.REQUIRED_PROGRAM_VERSION_VALIDATION_STACK
      )
    ) {
      this._globalMetaInfo.version = match[0];
    }
  }

  private readParameter(match: RegExpExecArray): void {
    if (
      equalsArray<string>(
        this.validationStack,
        Constants.REQUIRED_PARAMETER_NAME_VALIDATION_STACK
      ) ||
      equalsArray<string>(
        this.validationStack,
        Constants.REQUIRED_PARAMETER_OVERRIDE_NAME_VALIDATION_STACK
      )
    ) {
      this._currentParameterName = match[0];
    } else if (
      equalsArray<string>(
        this.validationStack,
        Constants.REQUIRED_PARAMETER_VALUE_VALIDATION_STACK
      )
    ) {
      if (this._currentParameterName === undefined)
        throw new Error(
          `Invalid parameter value without name on position ${match.index}`
        );

      const attrNameInClass = getLintFriendlyVariableName(
        ParserConstants.ATTRIBUTE_RENAMER[this._currentParameterName] ??
          this._currentParameterName
      );
      this._globalParameters[attrNameInClass] = parseNumber(match[0].trim());
    } else if (
      equalsArray<string>(
        this.validationStack,
        Constants.REQUIRED_PARAMETER_OVERRIDE_VALUE_VALIDATION_STACK
      ) &&
      this._currentParameterName === Constants.PARAMETER_NUMBER_OF_BEADS
    ) {
      while (
        this._configurationOffsets.length > this._beadsPerConfiguration.length
      )
        this._beadsPerConfiguration.push(0);

      this._beadsPerConfiguration[this._beadsPerConfiguration.length - 1] =
        parseNumber(match[0].trim());
    }
  }

  private parseBlockBegin(match: RegExpExecArray): void {
    const eleName = Constants.REGEX_XML_ELEMENT_NAME.exec(match[0]);
    if (!eleName) return;

    this.addBlockToStack(eleName[0]);

    if (eleName[0].toLowerCase() === Constants.XML_ELEMENT_CONFIGURATION) {
      this._configurationOffsets.push({ offset: match.index, endOffset: -1 });
    }
  }

  private parseBlockEnd(match: RegExpExecArray): void {
    const eleName = Constants.REGEX_XML_ELEMENT_NAME.exec(match[0]);
    if (!eleName) return;

    this.popBlockFromStackAndCheckValidation(eleName[0], match.index);

    if (eleName[0].toLowerCase() === Constants.XML_ELEMENT_CONFIGURATION) {
      if (
        this._configurationOffsets.length > 0 &&
        this._configurationOffsets[this._configurationOffsets.length - 1]
          .endOffset < 0
      )
        this._configurationOffsets[
          this._configurationOffsets.length - 1
        ].endOffset = match.index + match[0].length;
      else console.warn('Could not set endOffset of configuration!');
    } else if (eleName[0].toLowerCase() === Constants.XML_ELEMENT_PARAMETER) {
      this._currentParameterName = undefined;
    }
  }

  private getMaxBeadLength(): number {
    let max = this._globalParameters.numberOfBeads ?? 0;
    for (const count of this._beadsPerConfiguration) max = Math.max(max, count);

    return max;
  }
}
