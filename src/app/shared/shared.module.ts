/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseTabComponentDirective } from './directives/base-tab-component.directive';
import { DisableDirective } from './directives/disable.directive';
import { ZipExplorerModule } from './components/zip-explorer/zip-explorer.module';
import { DockableAreaModule } from './components/dockable-area/dockable-area.module';
import { ZipExplorerComponent } from './components/zip-explorer/zip-explorer.component';
import { DockableAreaComponent } from './components/dockable-area/dockable-area.component';
import { NotificationCenterComponent } from './components/notification-center/notification-center.component';
import { NotificationCenterModule } from './components/notification-center/notification-center.module';
import { SliderModule } from './components/slider/slider.module';
import { SliderComponent } from './components/slider/slider.component';
import { ConfirmDialogModule } from './components/confirm-dialog/confirm-dialog.module';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { LoadingDialogModule } from './components/loading-dialog/loading-dialog.module';
import { LoadingDialogComponent } from './components/loading-dialog/loading-dialog.component';
import { PlayerControlsComponent } from './components/player-controls/player-controls.component';
import { PlayerControlsModule } from './components/player-controls/player-controls.module';
import { VarDirective } from './directives/var.directive';

@NgModule({
  declarations: [BaseTabComponentDirective, DisableDirective, VarDirective],
  imports: [
    CommonModule,
    DockableAreaModule,
    NotificationCenterModule,
    PlayerControlsModule,
    ConfirmDialogModule,
    LoadingDialogModule,
    SliderModule,
    ZipExplorerModule,
  ],
  exports: [
    BaseTabComponentDirective,
    DisableDirective,
    VarDirective,
    DockableAreaComponent,
    NotificationCenterComponent,
    PlayerControlsComponent,
    ConfirmDialogComponent,
    LoadingDialogComponent,
    SliderComponent,
    ZipExplorerComponent,
  ],
})
export class SharedModule {}
