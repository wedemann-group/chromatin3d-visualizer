/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

export const DECORATOR_CUSTOM_PREFIX = 'custom:anotations';
export const DECORATOR_CUSTOM_CHANGEABLE_CATEGORY = `${DECORATOR_CUSTOM_PREFIX}:changeable:category`;
export const DECORATOR_CUSTOM_CHANGEABLE_CONDITION = `${DECORATOR_CUSTOM_PREFIX}:changeable:condition`;
export const DECORATOR_CUSTOM_CHANGEABLE_NUMBER = `${DECORATOR_CUSTOM_PREFIX}:changeable:number`;
export const DECORATOR_CUSTOM_CHANGEABLE_OPTION = `${DECORATOR_CUSTOM_PREFIX}:changeable:option`;
export const DECORATOR_CUSTOM_DISPLAY_NAME = `${DECORATOR_CUSTOM_PREFIX}:display:name`;
