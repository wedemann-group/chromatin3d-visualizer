/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import 'reflect-metadata';
import { DECORATOR_CUSTOM_DISPLAY_NAME } from './constants';
import { DecoratorValueString } from './models/decorator-value-string.model';

export function DisplayName(
  displayName: DecoratorValueString
): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void {
  return function (target: any, propertyKey: string) {
    Reflect.defineMetadata(
      DECORATOR_CUSTOM_DISPLAY_NAME,
      displayName,
      target,
      propertyKey
    );
  };
}
