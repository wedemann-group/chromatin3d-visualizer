/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import DoubleHelixBasePairBuilder from '../../modules/dockable-workspace/viewer/beads/double-helix-base-pair.builder';
import { BASE_PAIR_RENDER_SIZE_INIT } from '../../modules/dockable-workspace/viewer/beads/models.variables';
import { ELinkerDNABuilderType } from '../../core/services/session-mananger/models/linker-dna-builder-type.enum';
import DoubleHelixNucleosomeDNA from '../../modules/dockable-workspace/viewer/beads/double-helix-nucleosome-dna.builder';
import DoubleHelixCounterNucleosomeDNABuilder from '../../modules/dockable-workspace/viewer/beads/double-helix-counter-nucleosome-dna.builder';
import NucleosomeDNABuilder from '../../modules/dockable-workspace/viewer/beads/nucleosome-dna.builder';
import HistoneOctamerBuilder from '../../modules/dockable-workspace/viewer/beads/histone-octamer.builder';
import CohesinHeadBuilder from '../../modules/dockable-workspace/viewer/beads/cohesin-head.builder';
import CohesinLinkerBuilder from '../../modules/dockable-workspace/viewer/beads/cohesin-linker.builder';
import { MultipleBuilderResult } from '../models/multiple-builder-result.model';
import SoftwareNucleosomeBead from '../models/sw-nucleosome-bead.model';
import CohesinHeadBead from '../models/cohesin-head-bead.model';
import { Configuration } from '../models/configuration.model';
import { ColorsService } from '../../core/services/colors/colors.service';
import QualityLevel from '../../core/services/session-mananger/models/quality-level.model';
import LinkerDNADoubleHelixBuilder from '../../modules/dockable-workspace/viewer/beads/linker-dna-double-helix.builder';
import LinkerDNALowBuilder from '../../modules/dockable-workspace/viewer/beads/linker-dna-low.builder';
import CohesinRingBuilder from '../../modules/dockable-workspace/viewer/beads/cohesin-ring.builder';
import CohesinHingeBuilder from '../../modules/dockable-workspace/viewer/beads/cohesin-hinge.builder';
import { BuilderColorSubscriberMap } from '../models/builder-color-subscriber-map.model';
import LinkerDNAHighBuilder from '../../modules/dockable-workspace/viewer/beads/linker-dna-high.builder';
import { OneTimeBuilder } from '../models/one-time-builder.model';
import { Texture } from 'three';
import { CohesinService } from '../../core/services/cohesin.service';
import { ViewService } from '../../core/services/view/view.service';
import LinkerDNAApproxBuilder from '../../modules/dockable-workspace/viewer/beads/linker-dna-approx.builder';

export default class BuilderGetter {
  public static getMultipleUseBuilders(
    currentConfig: Configuration | undefined,
    colorService: ColorsService,
    qualityLevel: QualityLevel
  ): MultipleBuilderResult[] {
    const beads = currentConfig?.beads ?? [];
    const nucleosomes = beads.filter(
      (b) => b instanceof SoftwareNucleosomeBead
    ) as SoftwareNucleosomeBead[];
    const heads = beads.filter(
      (b) => b instanceof CohesinHeadBead
    ) as CohesinHeadBead[];

    return [
      {
        builder: DoubleHelixBasePairBuilder,
        colorClass: colorService.basePairColors,
        param: [],
        renderSize: BASE_PAIR_RENDER_SIZE_INIT,
        isActive:
          qualityLevel.linkerDNABuilder === ELinkerDNABuilderType.DOUBLE_HELIX,
      },
      {
        builder: DoubleHelixNucleosomeDNA,
        colorClass: colorService.dnaColors,
        param: nucleosomes,
        renderSize: nucleosomes.length,
        isActive:
          qualityLevel.linkerDNABuilder === ELinkerDNABuilderType.DOUBLE_HELIX,
      },
      {
        builder: DoubleHelixCounterNucleosomeDNABuilder,
        colorClass: colorService.dnaColors,
        param: nucleosomes,
        renderSize: nucleosomes.length,
        isActive:
          qualityLevel.linkerDNABuilder === ELinkerDNABuilderType.DOUBLE_HELIX,
      },
      {
        builder: NucleosomeDNABuilder,
        colorClass: colorService.dnaColors,
        param: nucleosomes,
        renderSize: nucleosomes.length,
        isActive:
          qualityLevel.linkerDNABuilder !== ELinkerDNABuilderType.DOUBLE_HELIX,
      },
      {
        builder: HistoneOctamerBuilder,
        colorClass: colorService.histoneOctamerColors,
        param: nucleosomes,
        renderSize: nucleosomes.length,
        isActive: true,
      },
      {
        builder: CohesinHeadBuilder,
        colorClass: colorService.cohesinHeadColors,
        param: heads,
        renderSize: heads.length,
        isActive: heads.length > 0,
      },
      {
        builder: CohesinLinkerBuilder,
        colorClass: colorService.cohesinLinkerColors,
        param: beads,
        renderSize: beads.length,
        isActive: heads.length > 0,
      },
      {
        builder: LinkerDNAApproxBuilder,
        colorClass: colorService.linkerDNAColors,
        param: beads,
        renderSize: beads.length,
        isActive:
          qualityLevel.linkerDNABuilder ===
          ELinkerDNABuilderType.INSTANCED_MESHED,
      },
    ];
  }

  public static getBuilderColorSubscriberMap(
    colorService: ColorsService
  ): BuilderColorSubscriberMap[] {
    return [
      {
        toSubscribe: colorService.histoneOctamerColors,
        builder: HistoneOctamerBuilder,
      },
      {
        toSubscribe: colorService.basePairColors,
        builder: DoubleHelixBasePairBuilder,
      },
      {
        toSubscribe: colorService.dnaColors,
        builder: NucleosomeDNABuilder,
      },
      {
        toSubscribe: colorService.dnaColors,
        builder: DoubleHelixNucleosomeDNA,
      },
      {
        toSubscribe: colorService.dnaColors,
        builder: DoubleHelixCounterNucleosomeDNABuilder,
      },
      {
        toSubscribe: colorService.linkerDNAColors,
        builder: LinkerDNADoubleHelixBuilder,
      },
      {
        toSubscribe: colorService.linkerDNAColors,
        builder: LinkerDNALowBuilder,
      },
      {
        toSubscribe: colorService.linkerDNAColors,
        builder: LinkerDNAHighBuilder,
      },
      {
        toSubscribe: colorService.linkerDNAColors,
        builder: LinkerDNAApproxBuilder,
      },
      {
        toSubscribe: colorService.cohesinHeadColors,
        builder: CohesinHeadBuilder,
      },
      {
        toSubscribe: colorService.cohesinRingAndHingeColors,
        builder: CohesinRingBuilder,
      },
      {
        toSubscribe: colorService.cohesinRingAndHingeColors,
        builder: CohesinHingeBuilder,
      },
      {
        toSubscribe: colorService.cohesinLinkerColors,
        builder: CohesinLinkerBuilder,
      },
    ];
  }

  public static getOneTimeBuilders(
    currentConfig: Configuration,
    colorService: ColorsService,
    viewService: ViewService,
    cohesinService: CohesinService,
    qualityLevel: QualityLevel,
    texture: Texture
  ): OneTimeBuilder[] {
    const result: OneTimeBuilder[] = [];

    if (currentConfig.cohesinRings.length > 0) {
      const builder = new CohesinRingBuilder(
        currentConfig.cohesinRings,
        currentConfig.parameters?.ringThickness ?? 1,
        currentConfig.parameters?.ringRadius ?? 1,
        texture,
        qualityLevel
      );
      builder.setUpFirstStyle(colorService.cohesinRingAndHingeColors.color);
      result.push(builder);

      const builderHinge = new CohesinHingeBuilder(
        currentConfig.cohesinRings,
        cohesinService.getCohDistance(),
        cohesinService.getCohesinThickness(),
        texture,
        qualityLevel
      );
      builderHinge.setUpFirstStyle(
        colorService.cohesinRingAndHingeColors.color
      );
      result.push(builderHinge);
    }

    if (qualityLevel.linkerDNABuilder === ELinkerDNABuilderType.DOUBLE_HELIX)
      result.push(
        new LinkerDNADoubleHelixBuilder(
          currentConfig?.beads ?? [],
          qualityLevel,
          colorService.linkerDNAColors.color,
          viewService.linkerDNASplitter,
          texture
        ),
        new LinkerDNADoubleHelixBuilder(
          currentConfig?.beads ?? [],
          qualityLevel,
          colorService.linkerDNAColors.color,
          viewService.linkerDNASplitter,
          texture,
          Math.PI
        )
      );
    else if (qualityLevel.linkerDNABuilder === ELinkerDNABuilderType.NORMAL)
      result.push(
        new LinkerDNAHighBuilder(
          currentConfig?.beads ?? [],
          qualityLevel,
          colorService.linkerDNAColors.color,
          viewService.linkerDNASplitter,
          texture
        )
      );
    else if (qualityLevel.linkerDNABuilder === ELinkerDNABuilderType.LOW)
      result.push(
        new LinkerDNALowBuilder(
          currentConfig?.beads ?? [],
          qualityLevel,
          colorService.linkerDNAColors.color,
          viewService.linkerDNASplitter
        )
      );

    return result;
  }
}
