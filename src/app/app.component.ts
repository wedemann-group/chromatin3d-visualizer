/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import DateDiff from 'date-diff';
import { ECalendar } from './shared/models/calendar.enum';
import { PopUpService } from './core/services/pop-up.service';
import {
  GITLAB_DOMAIN_COMMITS,
  NOTIFICATION_MAXIMUM_MONTHS,
  NOTIFICATION_UPDATE_DESCRIPTION,
  NOTIFICATION_UPDATE_TITLE,
} from './constants';
import { SettingsService } from './core/services/settings/settings.service';
import { DownloadService } from './core/services/download.service';
import { SubscribeEvents } from './core/classes/subscribe-events';
import { EDownloadParams } from './core/services/models/download-params.enum';

declare const BUILD_DATE_JS: string;
declare const COMMIT: string;

@Component({
  selector: 'trj-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent
  extends SubscribeEvents
  implements OnInit, AfterViewInit, OnDestroy
{
  constructor(
    private readonly _popUpService: PopUpService,
    private readonly _downloadService: DownloadService,
    private readonly _route: ActivatedRoute
  ) {
    super();
  }

  private static getLastUpdateDay(visited: Date, updated: Date): string | null {
    return AppComponent.getDifference(new DateDiff(updated, visited));
  }

  private static getDifference(diff: DateDiff): string | null {
    if (diff.months() >= NOTIFICATION_MAXIMUM_MONTHS) return null;

    if (diff.months() >= 1) {
      return AppComponent.getCalendarValue(
        diff.months(),
        ECalendar.MONTH,
        ECalendar.MONTH_MULTIPLE
      );
    } else if (diff.days() >= 1) {
      return AppComponent.getCalendarValue(
        diff.days(),
        ECalendar.DAY,
        ECalendar.DAY_MULTIPLE
      );
    } else if (diff.hours() >= 1) {
      return AppComponent.getCalendarValue(
        diff.hours(),
        ECalendar.HOUR,
        ECalendar.HOUR_MULTIPLE
      );
    } else if (diff.minutes() >= 1) {
      return AppComponent.getCalendarValue(
        diff.minutes(),
        ECalendar.MINUTE,
        ECalendar.MINUTE_MULTIPLE
      );
    }

    return AppComponent.getCalendarValue(
      diff.seconds(),
      ECalendar.SECOND,
      ECalendar.SECOND_MULTIPLE
    );
  }

  private static getCalendarValue(
    val: number,
    single: string,
    multiple: string
  ): string {
    val = Math.floor(val);
    if (val === 1) return single;

    return multiple.format(val.toString());
  }

  private static getBuildDate(): number {
    try {
      return Date.parse(BUILD_DATE_JS);
    } catch (e) {
      console.warn("'Last Visit' failed to parse");
    }

    return 0;
  }

  ngOnInit(): void {
    this.subscribes.push(
      this._route.queryParams.subscribe((params) => {
        if (params[EDownloadParams.PROVIDER])
          this._downloadService.download(params);
      })
    );
  }

  ngAfterViewInit(): void {
    this.setUpdateNotification();
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  private setUpdateNotification(): void {
    const currentDate = new Date();
    const lastVisit = SettingsService.getLastVisitDateFromLocalStorage();

    if (!lastVisit.date) {
      SettingsService.writeLastVisitDateToLocalStorage(currentDate);
      return;
    }

    // "lastVisit" is set in the future to hide the update notification forever
    if (lastVisit.isInFuture) return;

    const parsedBuildDate = AppComponent.getBuildDate();
    // save the current date as "lastVisit"
    SettingsService.writeLastVisitDateToLocalStorage(currentDate);

    if (parsedBuildDate <= lastVisit.date) return;

    const agoString = AppComponent.getLastUpdateDay(
      new Date(parsedBuildDate),
      currentDate
    );
    if (!agoString) return;

    this._popUpService.addNotification({
      title: NOTIFICATION_UPDATE_TITLE,
      description: NOTIFICATION_UPDATE_DESCRIPTION.format(
        `${agoString} ago`,
        `${GITLAB_DOMAIN_COMMITS}/${COMMIT}`
      ),
    });
  }
}
