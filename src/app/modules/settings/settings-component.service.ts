/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import EditableQualityLevelParameters from '../../core/services/session-mananger/models/editable-quality-level-parameters.model';
import { Decorator } from '../../shared/decorators/models/decorator.model';
import { DecoratorValueCondition } from '../../shared/decorators/models/decorator-value-condition.model';
import { SessionManagerService } from '../../core/services/session-mananger/session-manager.service';
import { CUSTOM_QUALITY_PROFILE_NAME } from '../../core/services/session-mananger/qualities/custom-quality.model';
import { Settings } from '../../core/services/settings/models/settings.model';
import { SettingsService } from '../../core/services/settings/settings.service';

@Injectable({
  providedIn: 'any',
})
export class SettingsComponentService {
  public readonly customizableQualityLevel: EditableQualityLevelParameters;
  public readonly settings: Settings;

  public constructor(
    private readonly _sessionManagerService: SessionManagerService,
    private readonly _settingsService: SettingsService
  ) {
    this.settings = this._settingsService.getSettings();
    this.customizableQualityLevel = new EditableQualityLevelParameters(
      this._sessionManagerService.qualityProfiles.find(
        (p) => p.name === CUSTOM_QUALITY_PROFILE_NAME
      )!.level
    );
  }

  public areConditionsMet(
    conditions: Decorator<DecoratorValueCondition>[]
  ): boolean {
    for (const condition of conditions) {
      if (!condition.value(this.customizableQualityLevel)) return false;
    }

    return true;
  }
}
