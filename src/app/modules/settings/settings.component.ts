/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { SettingsPerformanceComponent } from './settings-performance/settings-performance.component';
import QualityLevel from '../../core/services/session-mananger/models/quality-level.model';
import { QualityLevelParameters } from '../../core/services/session-mananger/models/quality-level-parameters.model';
import { SettingsComponentService } from './settings-component.service';
import { SettingsService } from '../../core/services/settings/settings.service';
import { LOCAL_STORAGE_SETTINGS_ANIMATION } from '../../shared/local-storage-keys.constants';

@Component({
  selector: 'trj-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  providers: [SettingsComponentService],
})
export class SettingsComponent {
  @ViewChild('performance')
  public performanceTabPage!: SettingsPerformanceComponent;

  constructor(
    private readonly _dialogRef: MatDialogRef<SettingsComponent>,
    private readonly _settingsService: SettingsService,
    private readonly _settingsComponentService: SettingsComponentService
  ) {}

  saveAndClose(): void {
    this._settingsComponentService.settings.performance.customQualityLevel =
      new QualityLevel(
        this._settingsComponentService
          .customizableQualityLevel as QualityLevelParameters
      );
    this._settingsComponentService.settings.performance.selectedQualityLevel =
      this.performanceTabPage.qualityProfile.name;
    this._settingsService.apply(this._settingsComponentService.settings);
    // must be stored separately, because it must be read BEFORE a service is parsed
    localStorage.setItem(
      LOCAL_STORAGE_SETTINGS_ANIMATION,
      this._settingsComponentService.settings.appearance.animationEnabled
        ? 'true'
        : 'false'
    );

    this._dialogRef.close();
  }
}
