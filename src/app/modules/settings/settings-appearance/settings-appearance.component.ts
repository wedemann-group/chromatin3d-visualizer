/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component } from '@angular/core';
import { SettingsComponentService } from '../settings-component.service';
import { ETheme } from '../../../shared/models/theme.enum';

function getDisplayNameFromTheme(theme: ETheme): string {
  if (theme === ETheme.LIGHT) return 'Light';
  if (theme === ETheme.DARK) return 'Dark';
  if (theme === ETheme.SYSTEM) return 'System';

  return '';
}

@Component({
  selector: 'trj-settings-appearance',
  templateUrl: './settings-appearance.component.html',
  styleUrls: [
    '../settings-base.component.scss',
    './settings-appearance.component.scss',
  ],
})
export class SettingsAppearanceComponent {
  public isSmall = false;
  protected readonly THEMES = Object.values(ETheme).map((n) => ({
    value: n,
    displayName: getDisplayNameFromTheme(n),
  }));

  constructor(
    public readonly settingsComponentService: SettingsComponentService
  ) {}
}
