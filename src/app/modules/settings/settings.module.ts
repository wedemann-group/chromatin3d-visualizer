/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { SettingsNotificationComponent } from './settings-notification/settings-notification.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { SettingsBehaviourComponent } from './settings-behaviour/settings-behaviour.component';
import { SettingsViewComponent } from './settings-view/settings-view.component';
import { SettingsPerformanceComponent } from './settings-performance/settings-performance.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { SettingsPreviewComponent } from './settings-performance/settings-preview/settings-preview.component';
import { AngularResizeEventModule } from 'angular-resize-event';
import { CategoryComponent } from './settings-performance/category/category.component';
import { CategoryItemComponent } from './settings-performance/category/category-item/category-item.component';
import { CategoryItemOptionComponent } from './settings-performance/category/category-item/category-item-option/category-item-option.component';
import { CategoryItemNumberComponent } from './settings-performance/category/category-item/category-item-number/category-item-number.component';
import { SharedModule } from '../../shared/shared.module';
import { AngularSplitModule } from 'angular-split';
import { MatExpansionModule } from '@angular/material/expansion';
import { SettingsComponentService } from './settings-component.service';
import { SettingsExportComponent } from './settings-export/settings-export.component';
import { SettingsAppearanceComponent } from './settings-appearance/settings-appearance.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SettingsComponent,
    SettingsNotificationComponent,
    SettingsBehaviourComponent,
    SettingsViewComponent,
    SettingsPerformanceComponent,
    SettingsPreviewComponent,
    SettingsAppearanceComponent,
    SettingsExportComponent,
    CategoryComponent,
    CategoryItemComponent,
    CategoryItemOptionComponent,
    CategoryItemNumberComponent,
  ],
  providers: [SettingsComponentService],
  imports: [
    AngularResizeEventModule,
    CommonModule,
    MatTabsModule,
    MatDialogModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    SharedModule,
    AngularSplitModule,
    MatExpansionModule,
    FormsModule,
  ],
  exports: [SettingsComponent],
})
export class SettingsModule {}
