/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsExportComponent } from './settings-export.component';
import { MATERIAL_IMPORTS } from '../../../core/services/__mocks__/imports';

describe('SettingsExportComponent', () => {
  let component: SettingsExportComponent;
  let fixture: ComponentFixture<SettingsExportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SettingsExportComponent],
      imports: [...MATERIAL_IMPORTS],
    }).compileComponents();

    fixture = TestBed.createComponent(SettingsExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
