/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Decorator } from '../../../../../../shared/decorators/models/decorator.model';
import { DecoratorValueNumberRange } from '../../../../../../shared/decorators/models/decorator-value-number-range.model';
import { DecoratorValueCondition } from '../../../../../../shared/decorators/models/decorator-value-condition.model';
import { SettingsComponentService } from '../../../../settings-component.service';

@Component({
  selector: 'trj-category-item-number[decorator][conditions][selectedValue]',
  templateUrl: './category-item-number.component.html',
  styleUrls: ['./category-item-number.component.scss'],
})
export class CategoryItemNumberComponent {
  @Input()
  public decorator!: Decorator<DecoratorValueNumberRange>;

  @Input()
  public conditions!: Decorator<DecoratorValueCondition>[];

  @Input()
  public selectedValue!: number;

  @Output()
  public selectedValueChange = new EventEmitter<number>();

  @Input()
  public propName?: string;

  @Input()
  public displayName?: string;

  constructor(public readonly performanceService: SettingsComponentService) {}
}
