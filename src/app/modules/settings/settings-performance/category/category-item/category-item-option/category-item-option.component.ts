/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { Decorator } from '../../../../../../shared/decorators/models/decorator.model';
import { DecoratorValueOption } from '../../../../../../shared/decorators/models/decorator-value-option.model';
import { DecoratorValueCondition } from '../../../../../../shared/decorators/models/decorator-value-condition.model';
import { SettingsComponentService } from '../../../../settings-component.service';

@Component({
  selector: 'trj-category-item-option[decorator][conditions][selectedValue]',
  templateUrl: './category-item-option.component.html',
  styleUrls: ['./category-item-option.component.scss'],
})
export class CategoryItemOptionComponent implements OnChanges {
  @Input()
  public decorator!: Decorator<DecoratorValueOption>;

  @Input()
  public conditions!: Decorator<DecoratorValueCondition>[];

  @Input()
  public selectedValue!: any;

  @Output()
  public selectedValueChange = new EventEmitter<any>();

  @Input()
  public displayName?: string;

  public readonly keyValues: { name: string; value: any }[] = [];

  constructor(public readonly performanceService: SettingsComponentService) {}

  ngOnChanges(): void {
    this.keyValues.splice(0);
    for (const name in this.decorator.value) {
      this.keyValues.push({ name, value: this.decorator.value[name] });
    }
  }
}
