/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularSplitModule } from 'angular-split';
import {
  APPLICATION_IMPORTS,
  MATERIAL_IMPORTS,
} from '../../../core/services/__mocks__/imports';
import { SettingsPerformanceComponent } from './settings-performance.component';
import { CategoryComponent } from './category/category.component';
import { WorkspaceService } from '../../../core/services/workspace.service';
import { WorkspaceService as MockWorkspaceService } from '../../../core/services/__mocks__/workspace.service';
import { AssetsService as MockAssetsService } from '../../../core/services/__mocks__/assets.service';
import { CategoryItemComponent } from './category/category-item/category-item.component';
import { CategoryItemNumberComponent } from './category/category-item/category-item-number/category-item-number.component';
import { CategoryItemOptionComponent } from './category/category-item/category-item-option/category-item-option.component';
import { SettingsPreviewComponent } from './settings-preview/settings-preview.component';
import { AssetsService } from '../../../core/services/assets/assets.service';

describe('SettingsPerformanceComponent', () => {
  let component: SettingsPerformanceComponent;
  let fixture: ComponentFixture<SettingsPerformanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        SettingsPerformanceComponent,
        CategoryComponent,
        CategoryItemComponent,
        CategoryItemNumberComponent,
        CategoryItemOptionComponent,
        SettingsPreviewComponent,
      ],
      imports: [
        ...MATERIAL_IMPORTS,
        ...APPLICATION_IMPORTS,
        AngularSplitModule,
      ],
      providers: [
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
        {
          provide: AssetsService,
          useClass: MockAssetsService,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
