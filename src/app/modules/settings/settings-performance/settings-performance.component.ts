/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, ViewChild } from '@angular/core';
import { SessionManagerService } from '../../../core/services/session-mananger/session-manager.service';
import { ResizedEvent } from 'angular-resize-event';
import { SettingsPreviewComponent } from './settings-preview/settings-preview.component';
import QualityLevel from '../../../core/services/session-mananger/models/quality-level.model';
import { getDecorators } from '../../../shared/utilities/decorator.util';
import { DECORATOR_CUSTOM_CHANGEABLE_CATEGORY } from '../../../shared/decorators/constants';
import { Category } from './models/category.model';
import { QualityLevelParameters } from '../../../core/services/session-mananger/models/quality-level-parameters.model';
import { QualityProfile } from '../../../core/services/session-mananger/models/quality-profile.model';
import { CUSTOM_QUALITY_PROFILE_NAME } from '../../../core/services/session-mananger/qualities/custom-quality.model';
import { SettingsComponentService } from '../settings-component.service';

const MINIMUM_PREVIEW_HEIGHT = 200;
const MAXIMUM_PREVIEW_HEIGHT = 400;

@Component({
  selector: 'trj-settings-performance',
  templateUrl: './settings-performance.component.html',
  styleUrls: ['./settings-performance.component.scss'],
})
export class SettingsPerformanceComponent {
  public readonly CUSTOM_QUALITY_PROFILE_NAME = CUSTOM_QUALITY_PROFILE_NAME;

  public readonly categories: Category[] = [];

  public autoRotate = true;

  public wireframe = false;

  @ViewChild('preview')
  public readonly preview!: SettingsPreviewComponent;

  public qualityProfile: QualityProfile;

  constructor(
    public readonly sessionManager: SessionManagerService,
    public readonly performanceService: SettingsComponentService
  ) {
    this.qualityProfile = sessionManager.qualityProfile;

    this.setCategories();
  }

  changeQuality(name: string): void {
    const qualityProfile = this.sessionManager.qualityProfiles.find(
      (q) => q.name === name
    );
    if (!qualityProfile) {
      console.warn("Quality profile doesn't exist!");
      return;
    }

    if (name === CUSTOM_QUALITY_PROFILE_NAME) {
      qualityProfile.level = new QualityLevel(
        this.performanceService
          .customizableQualityLevel as QualityLevelParameters
      );
    }

    this.qualityProfile = qualityProfile;
  }

  changeRenderSize(event: ResizedEvent): void {
    this.preview.resize(
      event.newRect.width,
      Math.max(
        MINIMUM_PREVIEW_HEIGHT,
        Math.min(MAXIMUM_PREVIEW_HEIGHT, event.newRect.height)
      )
    );
  }

  setCustomizableQualityLevel(): void {
    this.qualityProfile.level = new QualityLevel(
      this.performanceService.customizableQualityLevel as QualityLevelParameters
    );
  }

  private setCategories(): void {
    for (const prop of Object.getOwnPropertyNames(
      this.performanceService.customizableQualityLevel
    )) {
      let propName = prop;
      if (prop.match(/_[A-Za-z]+/)) propName = prop.substring(1);

      const decorators = getDecorators(
        this.performanceService.customizableQualityLevel,
        propName
      );
      if (decorators.length === 0) continue;

      let category = 'Misc';
      const categoryDecorator = decorators.find(
        (d) => d.name === DECORATOR_CUSTOM_CHANGEABLE_CATEGORY
      );
      const decoratorsWithoutCategory = decorators.filter(
        (d) => d.name !== DECORATOR_CUSTOM_CHANGEABLE_CATEGORY
      );
      if (categoryDecorator) category = categoryDecorator.value as string;

      const item = this.categories.find((c) => c.name === category);
      const newCategoryItem = {
        property: propName,
        decorators: decoratorsWithoutCategory,
      };
      if (item) item.items.push(newCategoryItem);
      else
        this.categories.push({
          name: category,
          items: [newCategoryItem],
        });
    }

    this.setCategoryMiscAsLastItem();
  }

  private setCategoryMiscAsLastItem(): void {
    const miscIndex = this.categories.findIndex((c) => c.name === 'Misc');
    if (miscIndex < 0 || miscIndex === this.categories.length - 1) return;

    const miscItem = this.categories[miscIndex];
    this.categories.splice(miscIndex, 1);
    this.categories.push(miscItem);
  }
}
