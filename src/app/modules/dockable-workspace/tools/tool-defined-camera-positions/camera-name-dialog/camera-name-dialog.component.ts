/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DialogData } from '../models/dialog-data.model';

@Component({
  selector: 'trj-camera-name-dialog',
  templateUrl: './camera-name-dialog.component.html',
  styleUrls: ['./camera-name-dialog.component.scss'],
})
export class CameraNameDialogComponent {
  constructor(
    public readonly dialogRef: MatDialogRef<CameraNameDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  submit(name: string | null): void {
    if (!name || this.isAlreadyInUse(name)) return;

    this.dialogRef.close(this.data);
  }

  isAlreadyInUse(name: string | null): boolean {
    if (!name) return false;

    return this.data.takenNames.includes(name);
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
