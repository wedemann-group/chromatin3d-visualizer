/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolDefinedCameraPositionsComponent } from './tool-defined-camera-positions.component';
import { CameraNameDialogComponent } from './camera-name-dialog/camera-name-dialog.component';
import { MATERIAL_IMPORTS } from '../../../../core/services/__mocks__/imports';
import { BaseTabComponentDirective } from '../../../../shared/directives/base-tab-component.directive';
import { BaseTabComponentDirective as MockBaseTabComponentDirective } from '../../../../core/services/__mocks__/base-tab-component.directive';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { WorkspaceService as MockWorkspaceService } from '../../../../core/services/__mocks__/workspace.service';

describe('ToolDefinedCameraPositionsComponent', () => {
  let component: ToolDefinedCameraPositionsComponent;
  let fixture: ComponentFixture<ToolDefinedCameraPositionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        CameraNameDialogComponent,
        ToolDefinedCameraPositionsComponent,
      ],
      imports: [...MATERIAL_IMPORTS],
      providers: [
        {
          provide:
            BaseTabComponentDirective.GOLDEN_LAYOUT_CONTAINER_INJECTION_TOKEN,
          useClass: MockBaseTabComponentDirective,
        },
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolDefinedCameraPositionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
