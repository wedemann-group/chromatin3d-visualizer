/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Input } from '@angular/core';
import {
  DEFAULT_COLOR_DISTANCE_LINE,
  DEFAULT_COLOR_DISTANCE_TEXT,
} from 'src/app/shared/color.constants';
import { DistanceMeasurementService } from '../../../../../core/services/distance-measurement.service';
import { SubscribeEvents } from '../../../../../core/classes/subscribe-events';

const LINE_WIDTH_MAXIMUM = 5;
const LINE_WIDTH_MINIMUM = 0.05;

const TEXT_SIZE_MAXIMUM = 50;
const TEXT_SIZE_MINIMUM = 1;

@Component({
  selector: 'trj-visualization-options[isSmall]',
  templateUrl: './visualization-options.component.html',
  styleUrls: ['./visualization-options.component.scss'],
})
export class VisualizationOptionsComponent extends SubscribeEvents {
  public readonly DEFAULT_COLOR_DISTANCE_LINE = DEFAULT_COLOR_DISTANCE_LINE;
  public readonly DEFAULT_COLOR_DISTANCE_TEXT = DEFAULT_COLOR_DISTANCE_TEXT;
  public readonly LINE_WIDTH_MINIMUM = LINE_WIDTH_MINIMUM;
  public readonly LINE_WIDTH_MAXIMUM = LINE_WIDTH_MAXIMUM;
  public readonly TEXT_SIZE_MINIMUM = TEXT_SIZE_MINIMUM;
  public readonly TEXT_SIZE_MAXIMUM = TEXT_SIZE_MAXIMUM;

  public lineColor = DEFAULT_COLOR_DISTANCE_LINE;
  public textColor = DEFAULT_COLOR_DISTANCE_TEXT;

  @Input()
  public isSmall = false;

  private _ignoreColorChange = false;

  constructor(
    public readonly distanceMeasurementService: DistanceMeasurementService
  ) {
    super();

    this.subscribes.push(
      this.distanceMeasurementService.lineColor$.subscribe((col) => {
        if (this._ignoreColorChange) {
          this._ignoreColorChange = false;
          return;
        }
        this.lineColor = col;
      }),
      this.distanceMeasurementService.textColor$.subscribe((col) => {
        if (this._ignoreColorChange) {
          this._ignoreColorChange = false;
          return;
        }
        this.textColor = col;
      })
    );
  }

  changeLineColor(newColor: string): void {
    this._ignoreColorChange = true;
    this.lineColor = newColor;
    this.distanceMeasurementService.lineColor = newColor;
  }

  changeLineWidth(lineWidth: number | null): void {
    if (lineWidth === null) return;

    this.distanceMeasurementService.lineWidth = lineWidth;
  }

  changeTextColor(newColor: string): void {
    this._ignoreColorChange = true;
    this.textColor = newColor;
    this.distanceMeasurementService.textColor = newColor;
  }

  changeTextSize(textSize: number | null): void {
    if (textSize === null) return;

    this.distanceMeasurementService.textSize = textSize;
  }
}
