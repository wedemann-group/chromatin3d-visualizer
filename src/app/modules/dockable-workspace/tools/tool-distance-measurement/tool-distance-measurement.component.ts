/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject, Input, OnDestroy } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ComponentContainer } from 'golden-layout';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { RaycastService } from '../../../../core/services/raycast.service';
import { UNIT, USER_FLOATING_DECIMAL_PLACES } from '../../../../constants';
import { DistanceMeasurementService } from '../../../../core/services/distance-measurement.service';
import { BaseTabComponentDirective } from '../../../../shared/directives/base-tab-component.directive';

@Component({
  selector: 'trj-tool-distance-measurement[isSmall]',
  templateUrl: './tool-distance-measurement.component.html',
  styleUrls: ['./tool-distance-measurement.component.scss'],
})
export class ToolDistanceMeasurementComponent
  extends BaseTabComponentDirective
  implements OnDestroy
{
  @Input()
  public isSmall = false;

  public readonly UNIT = UNIT;

  public readonly distance = new BehaviorSubject<number | null>(null);

  constructor(
    public readonly workspaceService: WorkspaceService,
    public readonly raycastService: RaycastService,
    public readonly distanceMeasureService: DistanceMeasurementService,
    @Inject(BaseTabComponentDirective.GOLDEN_LAYOUT_CONTAINER_INJECTION_TOKEN)
    protected readonly container: ComponentContainer
  ) {
    super();

    this.subscribeTabResizeForBreakpoints(container);

    this.subscribes.push(
      this.distanceMeasureService.secondPoint$.subscribe((point2) => {
        const point1 = this.distanceMeasureService.firstPoint;
        if (!point2 || !point1) {
          this.distance.next(null);
          return;
        }

        this.distance.next(point1.point.distanceTo(point2.point));
      })
    );
  }

  toggleToolDistanceMeasure(): void {
    this.distanceMeasureService.isDistanceMeasurement =
      !this.distanceMeasureService.isDistanceMeasurement;
  }

  getUserFriendlyFloat(number: number): string {
    return number.toFixed(USER_FLOATING_DECIMAL_PLACES);
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }
}
