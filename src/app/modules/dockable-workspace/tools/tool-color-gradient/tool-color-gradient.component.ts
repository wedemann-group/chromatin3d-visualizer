/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BaseTabComponentDirective } from '../../../../shared/directives/base-tab-component.directive';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { RGBAColor } from '../../../../shared/models/rgba-color.model';
import {
  aOptionalRGBToHexCSSFriendly,
  colorToArgb,
} from '../../../../shared/utilities/color.utility';
import { ColorsService } from '../../../../core/services/colors/colors.service';
import NucleosomeBead from '../../../../shared/models/nucleosome-bead.model';
import ChangeableColorWithConfigColor from '../../../../core/services/colors/changeable-color-with-config-color';
import DNABead from '../../../../shared/models/dna-bead.model';
import Bead from '../../../../shared/models/bead.model';
import { ViewService } from '../../../../core/services/view/view.service';
import { CohesinRing } from '../../../../shared/models/cohesin-ring.model';
import CohesinHeadBead from '../../../../shared/models/cohesin-head-bead.model';
import { ColorBead } from '../../../../core/services/colors/models/color-bead.model';
import { Configuration } from '../../../../shared/models/configuration.model';
import { ComponentContainer } from 'golden-layout';

type BeadOption = {
  marker: string;
  text: string;
};

type BeadColoring = {
  changer: ChangeableColorWithConfigColor;
  index: number;
};

type ColorChangerAndToColorBeads = { [key: string]: ColorBead[] };

const BEAD_LINKER_DNA_MARKER = 'LD';
const BEAD_NUCLEOSOME_DNA_MARKER = 'D';
const BEAD_NUCLEOSOMES_MARKER = 'N';
const BEAD_COHESIN_HEAD_MARKER = 'CH';
const BEAD_COHESIN_LINKER_MARKER = 'CL';
const BEAD_COHESIN_RINGS_HINGS_MARKER = 'CRH';

@Component({
  selector: 'trj-tool-color-gradient',
  templateUrl: './tool-color-gradient.component.html',
  styleUrls: ['./tool-color-gradient.component.scss'],
})
export class ToolColorGradientComponent extends BaseTabComponentDirective {
  public startColor: string = '#ff0000';
  public endColor: string = '#00ff00';

  public maxBeadIndex: number = -1;

  public startIndex: number = 0;
  public endIndex: number = 0;

  public selectedStartBead: string = '';
  public selectedEndBead: string = '';
  public affectedBeads: number = 0;

  public readonly beadTypeSelection = new FormControl<BeadOption[]>([]);
  public readonly beadTypes: BeadOption[] = [
    {
      marker: BEAD_LINKER_DNA_MARKER,
      text: 'Linker DNA',
    },
    {
      marker: BEAD_NUCLEOSOME_DNA_MARKER,
      text: 'Nucleosome DNA',
    },
    {
      marker: BEAD_NUCLEOSOMES_MARKER,
      text: 'Histone Octamer',
    },
    {
      marker: BEAD_COHESIN_HEAD_MARKER,
      text: 'Cohesin Heads',
    },
    {
      marker: BEAD_COHESIN_LINKER_MARKER,
      text: 'Cohesin Linkers',
    },
    {
      marker: BEAD_COHESIN_RINGS_HINGS_MARKER,
      text: 'Cohesin Rings & Hinges',
    },
  ];

  private _configBeads: Bead[] = [];
  private _configNucBeads: NucleosomeBead[] = [];
  private _configLinkerDNABeads: DNABead[] = [];
  private _configCohesinRings: CohesinRing[] = [];
  private _configCohesinHeads: CohesinHeadBead[] = [];

  constructor(
    public readonly workspaceService: WorkspaceService,
    @Inject(BaseTabComponentDirective.GOLDEN_LAYOUT_CONTAINER_INJECTION_TOKEN)
    protected readonly container: ComponentContainer,
    private readonly _colorService: ColorsService,
    private readonly _viewService: ViewService
  ) {
    super();

    this.subscribeTabResizeForBreakpoints(container);

    this.setConfigSpecificValues(this.workspaceService.config?.config);

    this.subscribes.push(
      this.workspaceService.config$.subscribe((config) =>
        this.setConfigSpecificValues(config?.config)
      )
    );

    this.subscribes.push(
      this._viewService.revisibility$.subscribe(() =>
        this.setConfigSpecificValues(this.workspaceService.config?.config)
      )
    );

    this.subscribes.push(
      this.beadTypeSelection.valueChanges.subscribe(
        () => (this.affectedBeads = this.getAffectedCounter())
      )
    );
  }

  private static getColorSteps(
    startColor: RGBAColor,
    endColor: RGBAColor,
    steps: number
  ): RGBAColor {
    if (steps === 0) return startColor;

    return {
      r: (endColor.r - startColor.r) / steps,
      g: (endColor.g - startColor.g) / steps,
      b: (endColor.b - startColor.b) / steps,
      a: (endColor.a - startColor.a) / steps,
    };
  }

  private static getColor(
    startColor: RGBAColor,
    steps: RGBAColor,
    stepIndex: number
  ): RGBAColor {
    return {
      r: Math.trunc(startColor.r + steps.r * stepIndex),
      g: Math.trunc(startColor.g + steps.g * stepIndex),
      b: Math.trunc(startColor.b + steps.b * stepIndex),
      a: startColor.a + steps.a * stepIndex,
    };
  }

  changeBeadStartIndex(val: number): void {
    this.selectedStartBead = this.getSelectionHint(val);
    this.affectedBeads = this.getAffectedCounter();
  }

  changeBeadEndIndex(val: number): void {
    this.selectedEndBead = this.getSelectionHint(val);
    this.affectedBeads = this.getAffectedCounter();
  }

  setMinimum(): void {
    this.startIndex = 0;
    this.changeBeadStartIndex(this.startIndex);
  }

  setMaximum(): void {
    this.endIndex = this.maxBeadIndex;
    this.changeBeadEndIndex(this.endIndex);
  }

  onSubmit(): void {
    if (this.endIndex > this.maxBeadIndex) {
      console.warn('Invalid endIndex!');
      return;
    }
    const distance = this.endIndex - this.startIndex;
    if (distance < 0) {
      console.warn('Invalid range coloring');
      return;
    }

    this.colorBeads(distance);
  }

  private setConfigSpecificValues(config: Configuration | undefined): void {
    this.setBeads(config);
    this.setMaxBeadIndex(config);

    this.changeBeadEndIndex(this.endIndex);
    this.changeBeadStartIndex(this.startIndex);
  }

  private setMaxBeadIndex(config: Configuration | undefined): void {
    if (!config || this._configBeads.length === 0) {
      this.maxBeadIndex = -1;
      return;
    }

    this.maxBeadIndex = this._configBeads.length - 1;
  }

  private setBeads(config: Configuration | undefined): void {
    this._configBeads = config?.beads ?? [];

    this._configNucBeads = <NucleosomeBead[]>(
      this._configBeads.filter((b) => b instanceof NucleosomeBead)
    );
    this._configCohesinHeads = <CohesinHeadBead[]>(
      this._configBeads.filter((b) => b instanceof CohesinHeadBead)
    );

    this._configLinkerDNABeads = <DNABead[]>(
      this._configBeads.filter((b) => b instanceof DNABead)
    );
    this._configCohesinRings =
      this.workspaceService.config?.config?.cohesinRings ?? [];
  }

  private getToColoringBeadsFromNucleosome(
    nucleosome: NucleosomeBead
  ): BeadColoring[] {
    const result: BeadColoring[] = [];
    const nucIndex = this._configNucBeads.findIndex((b) => b === nucleosome);
    if (this.isMarkerSelected(BEAD_NUCLEOSOMES_MARKER))
      result.push({
        changer: this._colorService.histoneOctamerColors,
        index: nucIndex,
      });

    if (this.isMarkerSelected(BEAD_NUCLEOSOME_DNA_MARKER))
      result.push({
        changer: this._colorService.dnaColors,
        index: nucIndex,
      });

    return result;
  }

  private getToColoringBeadsFromCohesinHead(
    head: CohesinHeadBead
  ): BeadColoring[] {
    const result: BeadColoring[] = [];
    if (this.isMarkerSelected(BEAD_COHESIN_HEAD_MARKER))
      result.push({
        changer: this._colorService.cohesinHeadColors,
        index: this._configCohesinHeads.findIndex((b) => b === head),
      });

    if (this.isMarkerSelected(BEAD_COHESIN_LINKER_MARKER))
      result.push({
        changer: this._colorService.cohesinLinkerColors,
        index: this._configCohesinHeads.findIndex((b) => b === head),
      });

    return result;
  }

  private getToColoringBeadsFromDNABead(bead: DNABead): BeadColoring[] {
    const result: BeadColoring[] = [];

    if (this.isMarkerSelected(BEAD_LINKER_DNA_MARKER))
      result.push({
        changer: this._colorService.linkerDNAColors,
        index: this._configLinkerDNABeads.findIndex((b) => b === bead),
      });

    return result;
  }

  private getSelectionHint(beadIndex: number): string {
    if (beadIndex < 0 || beadIndex > this.maxBeadIndex) {
      return 'Invalid index!';
    }

    const selectedBead = this.workspaceService.config?.config?.beads[beadIndex];

    if (!selectedBead) {
      return 'Invalid index!';
    }

    return `ID: ${selectedBead.id} (${selectedBead.marker})`;
  }

  private isMarkerSelected(marker: string): boolean {
    return (
      Array.isArray(this.beadTypeSelection.value) &&
      !!(<BeadOption[]>this.beadTypeSelection.value).find(
        (s) => s.marker === marker
      )
    );
  }

  /**
   * Colors the beads according to the user's settings.
   * @param distance
   * @private
   */
  private colorBeads(distance: number): void {
    const changersAndColors = this.getColorChangersAndTheirColorBeads(distance);
    const allChangers =
      this._colorService.getAllChangeableColorsWithConfigColor();

    for (const changer of Object.keys(changersAndColors)) {
      const realChanger = allChangers.find((c) => c.sessionID === changer);
      if (!realChanger) {
        console.warn('Changer not found!');
        continue;
      }

      realChanger.setConfigSpecificColorsFromColorBeads(
        changersAndColors[changer]
      );
    }
  }

  /**
   * Returns a list of color changers and the beads to be colored.
   * @param distance
   * @private
   */
  private getColorChangersAndTheirColorBeads(
    distance: number
  ): ColorChangerAndToColorBeads {
    const result: ColorChangerAndToColorBeads = {};
    const startColor = colorToArgb(this.startColor);
    const stepColor = ToolColorGradientComponent.getColorSteps(
      startColor,
      colorToArgb(this.endColor),
      distance
    );
    for (
      let i = this.startIndex, stepIndex = 0;
      i <= this.endIndex;
      ++i, ++stepIndex
    ) {
      const bead = this._configBeads[i];
      const currentColor = ToolColorGradientComponent.getColor(
        startColor,
        stepColor,
        stepIndex
      );
      const currentColorCssFriendly =
        aOptionalRGBToHexCSSFriendly(currentColor);

      for (const changeableColor of this.getToColoringBeads(bead, i)) {
        if (changeableColor.index < 0) continue;

        const changerID = changeableColor.changer.sessionID;
        if (!result[changerID]) result[changerID] = [];

        result[changerID].push({
          color: currentColorCssFriendly,
          index: changeableColor.index,
        });
      }
    }

    return result;
  }

  /**
   * Returns which beads should actually be colored based on a bead and an index, depending on the bead's dependency and the user's settings.
   * @param bead
   * @param index
   * @private
   */
  private getToColoringBeads(bead: Bead, index: number): BeadColoring[] {
    const changeableColors: BeadColoring[] = [];

    if (bead instanceof NucleosomeBead)
      changeableColors.push(...this.getToColoringBeadsFromNucleosome(bead));
    else if (bead instanceof CohesinHeadBead)
      changeableColors.push(...this.getToColoringBeadsFromCohesinHead(bead));
    else if (bead instanceof DNABead)
      changeableColors.push(...this.getToColoringBeadsFromDNABead(bead));

    if (
      index < this._configCohesinRings.length &&
      this.isMarkerSelected(BEAD_COHESIN_RINGS_HINGS_MARKER)
    ) {
      changeableColors.push({
        changer: this._colorService.cohesinRingAndHingeColors,
        index: index,
      });
    }

    return changeableColors;
  }

  private getAffectedCounter(): number {
    let affected = 0;

    const selectionBeads = [...this._configBeads].splice(
      this.startIndex,
      this.endIndex - this.startIndex + 1
    );
    if (this.isMarkerSelected(BEAD_NUCLEOSOMES_MARKER))
      affected += selectionBeads.filter(
        (b) => b instanceof NucleosomeBead
      ).length;

    if (this.isMarkerSelected(BEAD_NUCLEOSOME_DNA_MARKER))
      affected += selectionBeads.filter(
        (b) => b instanceof NucleosomeBead
      ).length;

    if (this.isMarkerSelected(BEAD_COHESIN_HEAD_MARKER))
      affected += selectionBeads.filter(
        (b) => b instanceof CohesinHeadBead
      ).length;

    if (this.isMarkerSelected(BEAD_COHESIN_LINKER_MARKER))
      affected += selectionBeads.filter(
        (b) => b instanceof CohesinHeadBead
      ).length;

    if (this.isMarkerSelected(BEAD_LINKER_DNA_MARKER))
      affected += selectionBeads.filter((b) => b instanceof DNABead).length;

    if (
      this.isMarkerSelected(BEAD_COHESIN_RINGS_HINGS_MARKER) &&
      this.startIndex < this._configCohesinRings.length
    )
      affected +=
        Math.min(this._configCohesinRings.length, this.endIndex) -
        this.startIndex;

    return affected;
  }
}
