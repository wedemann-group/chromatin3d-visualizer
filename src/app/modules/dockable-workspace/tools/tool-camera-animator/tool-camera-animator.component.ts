/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { generateUUID } from 'three/src/math/MathUtils';
import { AnimationService } from '../../../../core/services/animation/animation.service';
import { BaseTabComponentDirective } from '../../../../shared/directives/base-tab-component.directive';
import { NameDialogComponent } from './name-dialog/name-dialog.component';
import { DialogData } from './name-dialog/models/dialog-data.model';
import { Animation } from '../../../../core/services/animation/models/animation.model';
import { Keyframe } from '../../../../core/services/animation/models/keyframe.model';
import { ViewService } from '../../../../core/services/view/view.service';
import { ConfirmDialogComponent } from '../../../../shared/components/confirm-dialog/confirm-dialog.component';
import { ConfirmDialogData } from '../../../../shared/components/confirm-dialog/models/confirm-dialog-data.model';

const DIALOG_MAXIMUM_WIDTH = '550px';
const DIALOG_WIDTH = '50vw';

@Component({
  selector: 'trj-tool-camera-animator',
  templateUrl: './tool-camera-animator.component.html',
  styleUrls: ['./tool-camera-animator.component.scss'],
})
export class ToolCameraAnimatorComponent
  extends BaseTabComponentDirective
  implements OnDestroy
{
  public readonly selectedAnimation = new FormControl<Animation | null>(null);

  constructor(
    private readonly _dialog: MatDialog,
    private readonly _viewService: ViewService,
    public readonly animationService: AnimationService
  ) {
    super();

    this.subscribes.push(
      this.animationService.animations$.subscribe((animations) => {
        if (animations.length <= 0) this.selectedAnimation.disable();
        else this.selectedAnimation.enable();
      })
    );
  }

  openCameraCreator(): void {
    const dialogRef = this._dialog.open(NameDialogComponent, {
      disableClose: true,
      width: DIALOG_WIDTH,
      maxWidth: DIALOG_MAXIMUM_WIDTH,
      data: { name: '' } as DialogData,
    });

    dialogRef
      .afterClosed()
      .pipe(filter<DialogData>((r) => r && !!r.name))
      .subscribe((result: DialogData) => {
        const animation = {
          name: result.name,
          id: generateUUID(),
          keyframes: new BehaviorSubject<Keyframe[]>([]),
        } as Animation;
        this.animationService.animations = [
          ...this.animationService.animations,
          animation,
        ];
        this.animationService.selectedAnimation = animation;
        this.selectedAnimation.setValue(animation);
      });
  }

  animationChange(animation: Animation | null): void {
    this.animationService.selectedAnimation = animation;
  }

  removeCurrentAnimation(): void {
    const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      disableClose: true,
      width: DIALOG_WIDTH,
      maxWidth: DIALOG_MAXIMUM_WIDTH,
      data: {
        title: `Remove Animation '${this.animationService.selectedAnimation?.name}'`,
        text: `Do you really want to delete the whole animation?`,
        buttonNegative: 'Cancel',
        buttonPositive: 'Yes',
        buttonPositiveColor: 'warn',
      } as ConfirmDialogData,
    });

    dialogRef
      .afterClosed()
      .pipe(filter((d) => d))
      .subscribe(() => {
        const selectedAnimation = this.animationService.selectedAnimation;
        if (!selectedAnimation) return;

        this.animationService.removeKeyframesFromAnimationAndRemoveGeneratedDefinedCameras(
          selectedAnimation
        );
        this.animationService.animations =
          this.animationService.animations.filter(
            (a) => a !== selectedAnimation
          );
        this.animationService.selectedAnimation = null;
      });
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }
}
