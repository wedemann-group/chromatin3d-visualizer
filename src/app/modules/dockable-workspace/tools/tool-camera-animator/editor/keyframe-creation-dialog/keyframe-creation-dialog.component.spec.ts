/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { KeyframeCreationDialogComponent } from './keyframe-creation-dialog.component';
import {
  APPLICATION_IMPORTS,
  MATERIAL_IMPORTS,
  WORKSPACE_IMPORTS,
} from '../../../../../../core/services/__mocks__/imports';
import { AnimationService } from '../../../../../../core/services/animation/animation.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DialogData } from '../../name-dialog/models/dialog-data.model';
import Spy = jasmine.Spy;
import createSpy = jasmine.createSpy;
import { WorkspaceService } from '../../../../../../core/services/workspace.service';
import { WorkspaceService as MockWorkspaceService } from '../../../../../../core/services/__mocks__/workspace.service';

describe('KeyframeCreationDialogComponent', () => {
  let component: KeyframeCreationDialogComponent;
  let fixture: ComponentFixture<KeyframeCreationDialogComponent>;
  let dialogRef: { close: Spy<() => void> };

  beforeEach(async () => {
    dialogRef = { close: createSpy('close') };
    await TestBed.configureTestingModule({
      declarations: [KeyframeCreationDialogComponent],
      imports: [
        ...MATERIAL_IMPORTS,
        ...WORKSPACE_IMPORTS,
        ...APPLICATION_IMPORTS,
      ],
      providers: [
        AnimationService,
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: { name: 'test' } as DialogData,
        },
        {
          provide: MatDialogRef,
          useValue: dialogRef,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(KeyframeCreationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
