/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ViewService } from '../../../../../../core/services/view/view.service';
import { DialogData } from './models/dialog-data.model';
import { DefinedCamera } from '../../../tool-defined-camera-positions/models/defined-camera.model';
import { BehaviorSubject } from 'rxjs';
import { SubscribeEvents } from '../../../../../../core/classes/subscribe-events';

@Component({
  selector: 'trj-keyframe-creation-dialog',
  templateUrl: './keyframe-creation-dialog.component.html',
  styleUrls: ['./keyframe-creation-dialog.component.scss'],
})
export class KeyframeCreationDialogComponent
  extends SubscribeEvents
  implements OnDestroy
{
  public readonly cameras = new BehaviorSubject<DefinedCamera[]>([]);

  constructor(
    public readonly dialogRef: MatDialogRef<KeyframeCreationDialogComponent>,
    public readonly viewService: ViewService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    super();

    this.subscribes.push(
      this.viewService.definedCameras$.subscribe((cameras) => {
        this.cameras.next(cameras.filter((c) => !c.wasGenerated));
      })
    );
  }

  cameraChange(id: string): void {
    this.data.cameraID = id;
  }

  submit(name: string | null): void {
    if (!name) return;

    this.dialogRef.close(this.data);
  }

  cancel(): void {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }
}
