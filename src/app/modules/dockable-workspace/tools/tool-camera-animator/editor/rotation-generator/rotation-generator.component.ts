/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component } from '@angular/core';
import { Camera, Quaternion, Vector3 } from 'three';
import { MatDialogRef } from '@angular/material/dialog';
import { ViewService } from '../../../../../../core/services/view/view.service';
import { DialogData } from './models/dialog-data.model';
import { generateUUID } from 'three/src/math/MathUtils';
import { SerializableCamera } from '../../../../viewer/models/serializable-camera.model';
import { DefinedCamera } from '../../../tool-defined-camera-positions/models/defined-camera.model';
import { ECameraSetter } from '../../../../viewer/models/camera-setter.enum';
import { EControlType } from '../../../../viewer/models/control-type.enum';

const ROTATION_MAXIMUM = 2 * Math.PI; // in radians
const ROTATION_STEPS = 100; // in radians
const ROTATION_STEP = ROTATION_MAXIMUM / ROTATION_STEPS; // in radians

@Component({
  selector: 'trj-rotation-generator',
  templateUrl: './rotation-generator.component.html',
  styleUrls: ['./rotation-generator.component.scss'],
})
export class RotationGeneratorComponent {
  public readonly rotationVec = new Vector3();
  public duration = 10;

  constructor(
    public readonly dialogRef: MatDialogRef<RotationGeneratorComponent>,
    private readonly _viewService: ViewService
  ) {}

  submit(): void {
    if (this.rotationVec.length() < 0.001) {
      console.warn('Vector must be length of greater 0');
      return;
    }

    this._viewService.cameraGetter$$.next((camera) => {
      const realCamera = new Camera();
      realCamera.position.set(
        camera.position.x,
        camera.position.y,
        camera.position.z
      );
      realCamera.rotation.setFromQuaternion(
        new Quaternion().setFromEuler(camera.rotation)
      );
      const fiber =
        this._viewService.getContainerSizeOfPBCOrFiberWithCenterPoint();
      const centerPoint = new Vector3(
        fiber.centerX,
        fiber.centerY,
        fiber.centerZ
      );

      const points: DefinedCamera[] = [];
      const rotationVec = this.rotationVec.clone().normalize();
      const diffToCenterVec = camera.position.clone().sub(centerPoint);

      for (let i = 0; i < ROTATION_MAXIMUM; i += ROTATION_STEP) {
        const directionVec = diffToCenterVec
          .clone()
          .normalize()
          .applyAxisAngle(rotationVec, i)
          .setLength(diffToCenterVec.length());
        const newCameraPos = centerPoint.clone().add(directionVec);
        realCamera.position.set(newCameraPos.x, newCameraPos.y, newCameraPos.z);
        const newUpVec = camera.upVector
          .clone()
          .normalize()
          .applyAxisAngle(rotationVec, i)
          .setLength(camera.upVector.length());
        realCamera.up.set(newUpVec.x, newUpVec.y, newUpVec.z);
        realCamera.updateMatrix();

        points.push({
          name: `Generated Rotation Point ${i}`,
          id: generateUUID(),
          camera: {
            position: realCamera.position.clone(),
            rotation: realCamera.rotation.clone(),
            setter: ECameraSetter.toCenter,
            controlType: EControlType.fixedCenter,
            targetPosition: centerPoint.clone(),
            upVector: realCamera.up.clone(),
          } as SerializableCamera,
          wasGenerated: true,
        } as DefinedCamera);
      }

      this.dialogRef.close({
        points,
        duration: this.duration,
      } as DialogData);
    });
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
