/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EditorComponent } from './editor.component';
import {
  APPLICATION_IMPORTS,
  MATERIAL_IMPORTS,
  WORKSPACE_IMPORTS,
} from '../../../../../core/services/__mocks__/imports';
import { AnimationService } from '../../../../../core/services/animation/animation.service';
import { WorkspaceService } from '../../../../../core/services/workspace.service';
import { WorkspaceService as MockWorkspaceService } from '../../../../../core/services/__mocks__/workspace.service';
import { MatDialog } from '@angular/material/dialog';

describe('EditorComponent', () => {
  let component: EditorComponent;
  let fixture: ComponentFixture<EditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EditorComponent],
      imports: [
        ...MATERIAL_IMPORTS,
        ...WORKSPACE_IMPORTS,
        ...APPLICATION_IMPORTS,
      ],
      providers: [
        AnimationService,
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
        { provide: MatDialog, useValue: {} },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
