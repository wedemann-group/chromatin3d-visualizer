/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, OnDestroy, ViewChild } from '@angular/core';
import { CdkDragMove, Point } from '@angular/cdk/drag-drop';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { filter } from 'rxjs/operators';
import { AnimationService } from '../../../../../core/services/animation/animation.service';
import { ViewService } from '../../../../../core/services/view/view.service';
import { Keyframe } from '../../../../../core/services/animation/models/keyframe.model';
import { KeyframeCreationDialogComponent } from './keyframe-creation-dialog/keyframe-creation-dialog.component';
import { DialogData } from './keyframe-creation-dialog/models/dialog-data.model';
import { DialogData as GeneratedDialogData } from './rotation-generator/models/dialog-data.model';
import { RotationGeneratorComponent } from './rotation-generator/rotation-generator.component';
import { ConfirmDialogComponent } from '../../../../../shared/components/confirm-dialog/confirm-dialog.component';
import { ConfirmDialogData } from '../../../../../shared/components/confirm-dialog/models/confirm-dialog-data.model';
import { SubscribeEvents } from '../../../../../core/classes/subscribe-events';

const KEYFRAMES_PADDING = 500;
const KEYFRAMES_MINIMUM_WIDTH = 400;
const DIALOG_MAXIMUM_WIDTH = '550px';
const DIALOG_WIDTH = '50vw';

type KeyframeWithDragPoint = {
  keyframe: Keyframe;
  point: Point;
};

@Component({
  selector: 'trj-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
})
export class EditorComponent extends SubscribeEvents implements OnDestroy {
  public maximalWidth = KEYFRAMES_MINIMUM_WIDTH;
  public keyframes: KeyframeWithDragPoint[] = [];
  public menuPosition = { x: '0', y: '0' };

  @ViewChild(MatMenuTrigger, { static: true })
  public matMenuTrigger: MatMenuTrigger | undefined;

  constructor(
    public readonly animationService: AnimationService,
    private readonly _viewService: ViewService,
    private readonly _dialog: MatDialog
  ) {
    super();

    this.subscribes.push(
      this.animationService.selectedAnimation$.subscribe((ani) => {
        this.keyframes = !ani
          ? []
          : ani.keyframes.value.map(
              EditorComponent.convertKeyframeToKeyframeWithPoint.bind(this)
            );

        this.maximalWidth =
          (this.keyframes[this.keyframes.length - 1]?.point.x ?? 0) +
          KEYFRAMES_PADDING;
      }),
      this.animationService.invalidKeyframeRemoved$
        .pipe(filter((a) => a === this.animationService.selectedAnimation))
        .subscribe((ani) => {
          this.keyframes = !ani
            ? []
            : ani.keyframes.value.map(
                EditorComponent.convertKeyframeToKeyframeWithPoint.bind(this)
              );
        })
    );
  }

  private static convertKeyframeToKeyframeWithPoint(
    keyframe: Keyframe
  ): KeyframeWithDragPoint {
    return {
      keyframe,
      point: { x: keyframe.time, y: 0 },
    } as KeyframeWithDragPoint;
  }

  move(eventArg: CdkDragMove, keyframe: KeyframeWithDragPoint): void {
    let { x } = eventArg.source.getFreeDragPosition();
    x += eventArg.source.element.nativeElement.offsetLeft;
    console.log(x);

    this.maximalWidth = Math.max(
      KEYFRAMES_MINIMUM_WIDTH,
      x + KEYFRAMES_PADDING
    );

    keyframe.keyframe.time = x;
  }

  moveEnded(): void {
    const sortedKeyframes = this.keyframes.map((k) => k.keyframe);
    sortedKeyframes.sort((k1, k2) => k1.time - k2.time);
    this.animationService.selectedAnimation!.keyframes.next(sortedKeyframes);

    this.animationService.saveAnimations();
  }

  getCameraName(cameraID: string): string {
    return (
      this._viewService.definedCameras.find((c) => c.id === cameraID)?.name ??
      '-'
    );
  }

  addKeyframe(): void {
    const dialogRef = this._dialog.open(KeyframeCreationDialogComponent, {
      disableClose: true,
      width: DIALOG_WIDTH,
      maxWidth: DIALOG_MAXIMUM_WIDTH,
      data: { cameraID: null } as DialogData,
    });

    dialogRef
      .afterClosed()
      .pipe(filter<DialogData>((r) => r && !!r.cameraID))
      .subscribe((result: DialogData) => {
        this.addKeyframes([
          {
            cameraID: result.cameraID,
            time: 100,
          } as Keyframe,
        ]);
      });
  }

  openRotationGenerator(): void {
    const dialogRef = this._dialog.open(RotationGeneratorComponent, {
      disableClose: true,
      width: DIALOG_WIDTH,
      maxWidth: DIALOG_MAXIMUM_WIDTH,
      data: { cameraID: null } as DialogData,
    });

    dialogRef.afterClosed().subscribe((result: GeneratedDialogData) => {
      this._viewService.setDefinedCameras([
        ...this._viewService.definedCameras,
        ...result.points,
      ]);
      this.addKeyframes(
        result.points.map(
          (k, i) =>
            ({
              cameraID: k.id,
              time: i * result.duration,
            } as Keyframe)
        )
      );
    });
  }

  openContextMenu(event: MouseEvent, keyframe: Keyframe): void {
    if (!this.matMenuTrigger) return;

    event.preventDefault();

    this.menuPosition.x = `${event.clientX}px`;
    this.menuPosition.y = `${event.clientY}px`;

    this.matMenuTrigger.menuData = { item: keyframe };
    this.matMenuTrigger.openMenu();
  }

  removeKeyframe(keyframe: Keyframe): void {
    this.keyframes = this.keyframes.filter((k) => k.keyframe !== keyframe);
    if (!this.animationService.selectedAnimation) return;

    this.animationService.selectedAnimation.keyframes.next(
      this.animationService.selectedAnimation.keyframes.value.filter(
        (k) => k !== keyframe
      )
    );

    if (
      this._viewService.definedCameras.find((c) => c.id === keyframe.cameraID)
        ?.wasGenerated
    ) {
      this._viewService.setDefinedCameras(
        this._viewService.definedCameras.filter(
          (c) => c.id !== keyframe.cameraID
        )
      );
    }

    this.animationService.saveAnimations();
  }

  removeAllKeyframes(): void {
    const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      disableClose: true,
      width: DIALOG_WIDTH,
      maxWidth: DIALOG_MAXIMUM_WIDTH,
      data: {
        title: `Remove All Keyframes for '${this.animationService.selectedAnimation?.name}'`,
        text: `Do you really want to remove all keyframes from the animation?`,
        buttonNegative: 'Cancel',
        buttonPositive: 'Yes',
        buttonPositiveColor: 'warn',
      } as ConfirmDialogData,
    });

    dialogRef
      .afterClosed()
      .pipe(filter((d) => d))
      .subscribe(() => {
        if (!this.animationService.selectedAnimation) return;

        this.animationService.removeKeyframesFromAnimationAndRemoveGeneratedDefinedCameras(
          this.animationService.selectedAnimation
        );
        this.keyframes.splice(0);
      });
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  private addKeyframes(keyframes: Keyframe[]): void {
    const sortedKeyframes: Keyframe[] = [
      ...this.animationService.selectedAnimation!.keyframes.value,
      ...keyframes,
    ];
    sortedKeyframes.sort((k1, k2) => k1.time - k2.time);
    this.animationService.selectedAnimation!.keyframes.next(sortedKeyframes);
    this.keyframes.push(
      ...keyframes.map((k) =>
        EditorComponent.convertKeyframeToKeyframeWithPoint(k)
      )
    );

    this.animationService.saveAnimations();
  }
}
