/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ViewService } from '../../../../../core/services/view/view.service';
import { AnimatableCameraPoint } from '../models/animatable-camera-point.model';
import { AnimationService } from '../../../../../core/services/animation/animation.service';
import { Animation } from '../../../../../core/services/animation/models/animation.model';

const ANIMATION_FPS = 33;

const ANIMATION_INTERVAL = 1000 / ANIMATION_FPS; // 1000ms = 1s (calculate "interval" value)

@Component({
  selector: 'trj-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
})
export class PlayerComponent implements OnInit, OnDestroy {
  public _animatorTimer = -1;

  public _animation: Animation | null = null;
  public _maxTime = -1;

  private _keyframesChangeEvent: Subscription | undefined;

  constructor(
    public readonly animationService: AnimationService,
    private readonly _viewService: ViewService
  ) {
    this.animationService.selectedAnimation$.subscribe((ani) => {
      this._keyframesChangeEvent?.unsubscribe();
      this._keyframesChangeEvent = undefined;

      this._animation = ani;
      this.calculateMaxTime(ani);

      if (ani) {
        this._keyframesChangeEvent = ani.keyframes.subscribe(() => {
          this.calculateMaxTime(ani);
        });
      }

      this.stopAnimation();
    });
  }

  ngOnInit(): void {
    window.addEventListener('keydown', (event) => {
      if (!this.animationService.selectedAnimation) return;

      if (event.key === 'F8') {
        this.first();
      } else if (event.key === 'F9') {
        this.pauseStart(this._animatorTimer > 0);
      }
    });
  }

  pauseStart(isPause: boolean): void {
    if (isPause) this.stopAnimation();
    else this.startAnimation();
  }

  previous(): void {
    this.animationService.time$$.next(this.animationService.time$$.value - 1);
    this.animate(false);
  }

  next(): void {
    this.animationService.time$$.next(this.animationService.time$$.value + 1);
    this.animate(false);
  }

  first(): void {
    this.animationService.time$$.next(0);
    this.animate(false);
  }

  last(): void {
    this.animationService.time$$.next(this._maxTime);
    this.animate(false);
  }

  ngOnDestroy(): void {
    this.stopAnimation();
  }

  private calculateMaxTime(ani: Animation | null): void {
    if (ani && ani.keyframes.value.length > 0) {
      this._maxTime = ani.keyframes.value[ani.keyframes.value.length - 1].time;
    } else {
      this._maxTime = -1;
    }
  }

  private startAnimation(): void {
    this.stopAnimation();
    if (!this._animation || this._animation.keyframes.value.length <= 1) return;

    if (this.animationService.time$$.value > this._maxTime) {
      this.animationService.time$$.next(0);
    }

    this._animatorTimer = setInterval(
      this.animate.bind(this),
      ANIMATION_INTERVAL
    );
  }

  private stopAnimation(): void {
    if (this._animatorTimer < 0) return;

    clearInterval(this._animatorTimer);
    this._animatorTimer = -1;
  }

  private animate(increaseCounter = true): void {
    if (!this._animation) return;

    if (this.animationService.time$$.value > this._maxTime) {
      this.stopAnimation();
      return;
    }

    const cameras = this._viewService.definedCameras;
    const cameraIndex = Math.max(
      0,
      this._animation.keyframes.value.findIndex(
        (k) => k.time >= this.animationService.time$$.value
      )
    );
    const fromKeypoint =
      this._animation.keyframes.value[Math.max(0, cameraIndex - 1)];
    const toKeypoint = this._animation.keyframes.value[cameraIndex];

    const fromCamera = cameras.find((c) => c.id === fromKeypoint.cameraID);
    const toCamera = cameras.find((c) => c.id === toKeypoint.cameraID);

    if (!fromCamera || !toCamera) {
      console.warn('Camera(s) does not exist!');
      return;
    }

    const aniFromPoint = new AnimatableCameraPoint(
      fromCamera.camera.position,
      fromCamera.camera.upVector,
      fromCamera.camera.rotation,
      fromCamera.camera.targetPosition
    );
    const aniToPoint = new AnimatableCameraPoint(
      toCamera.camera.position,
      toCamera.camera.upVector,
      toCamera.camera.rotation,
      toCamera.camera.targetPosition
    );
    const diff = aniToPoint
      .clone()
      .sub(aniFromPoint)
      .divideScalar(toKeypoint.time - fromKeypoint.time)
      .multiplyScalar(this.animationService.time$$.value - fromKeypoint.time);

    aniFromPoint.add(diff);

    this._viewService.cameraSetter$$.next({
      position: aniFromPoint.position,
      upVector: aniFromPoint.upVector,
      rotation: aniFromPoint.rotation,
      targetPosition: aniFromPoint.targetPosition,
      controlType: fromCamera.camera.controlType,
      setter: fromCamera.camera.setter,
    });

    if (increaseCounter)
      this.animationService.time$$.next(this.animationService.time$$.value + 1);
  }
}
