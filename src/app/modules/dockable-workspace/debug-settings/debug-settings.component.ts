/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject } from '@angular/core';
import { BaseTabComponentDirective } from '../../../shared/directives/base-tab-component.directive';
import { DebugService } from '../../../core/services/debug.service';
import { ComponentContainer } from 'golden-layout';

@Component({
  selector: 'trj-debug-settings',
  templateUrl: './debug-settings.component.html',
  styleUrls: ['./debug-settings.component.scss'],
})
export class DebugSettingsComponent extends BaseTabComponentDirective {
  constructor(
    public readonly debugService: DebugService,
    @Inject(BaseTabComponentDirective.GOLDEN_LAYOUT_CONTAINER_INJECTION_TOKEN)
    protected readonly container: ComponentContainer
  ) {
    super();

    this.subscribeTabResizeForBreakpoints(container);
  }
}
