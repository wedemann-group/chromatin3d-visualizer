/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { ColorPickerModule } from 'ngx-color-picker';
import { BeadsExplorerComponent } from './beads-explorer.component';
import { BeadsExplorerCohesinComponent } from './beads-explorer-cohesin/beads-explorer-cohesin.component';
import { BeadsExplorerNucleosomeDNAComponent } from './beads-explorer-nucleosome-dna/beads-explorer-nucleosome-dna.component';
import { BeadsExplorerHistoneOctamerComponent } from './beads-explorer-histone-octamer/beads-explorer-histone-octamer.component';
import { BeadsExplorerLinkerDnaComponent } from './beads-explorer-linker-dna/beads-explorer-linker-dna.component';

@NgModule({
  declarations: [
    BeadsExplorerComponent,
    BeadsExplorerCohesinComponent,
    BeadsExplorerNucleosomeDNAComponent,
    BeadsExplorerHistoneOctamerComponent,
    BeadsExplorerLinkerDnaComponent,
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatCheckboxModule,
    ColorPickerModule,
    MatPaginatorModule,
  ],
  exports: [
    BeadsExplorerCohesinComponent,
    BeadsExplorerNucleosomeDNAComponent,
    BeadsExplorerHistoneOctamerComponent,
    BeadsExplorerLinkerDnaComponent,
  ],
})
export class BeadsExplorerModule {}
