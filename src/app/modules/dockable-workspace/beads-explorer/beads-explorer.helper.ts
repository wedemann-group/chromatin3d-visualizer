/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Bead from '../../../shared/models/bead.model';
import { BeadEntry } from './models/bead-entry';
import ChangeableColorWithConfigColor from '../../../core/services/colors/changeable-color-with-config-color';
import { BeadGroupEntry } from './models/bead-group-entry';
import { BEAD_USER_NAME_LINKER_DNA } from '../../../constants';

export function convertBeadsToBeadEntriesLinkerDNAVersion(
  beads: Bead[][],
  color: ChangeableColorWithConfigColor
): BeadGroupEntry[] {
  const result: BeadGroupEntry[] = [];

  const colorBeads = color.configSpecificColors;
  let beadIndex = 0;
  for (const currentBeads of beads) {
    if (!currentBeads) continue;

    const colorBead = colorBeads.find((cb) => cb.index === beadIndex);
    result.push({
      id: currentBeads.map((b) => b.id.toString()).join(', '),
      indices: new Array<number>(currentBeads.length)
        .fill(beadIndex)
        .map((val, index) => val + index),
      displayName: `${BEAD_USER_NAME_LINKER_DNA} (${currentBeads.length} segments)`,
      displayIndex: currentBeads.map((b) => b.id.toString()).join(', '),
      color: colorBead ? colorBead.color : color.color,
    });

    beadIndex += currentBeads.length;
  }

  return result;
}

export function convertBeadsToBeadEntries(
  beads: Bead[],
  color: ChangeableColorWithConfigColor,
  getName: (bead: Bead) => string,
  getIndex?: (bead: Bead) => string
): BeadEntry[] {
  const result: BeadEntry[] = [];

  const colorBeads = color.configSpecificColors;
  for (let i = 0; i < beads.length; ++i) {
    const colorBead = colorBeads.find((cb) => cb.index === i);
    result.push({
      id: beads[i].id.toString(),
      index: i,
      displayName: getName(beads[i]),
      displayIndex: getIndex ? getIndex(beads[i]) : i.toString(),
      color: colorBead ? colorBead.color : color.color,
    } as BeadEntry);
  }

  return result;
}
