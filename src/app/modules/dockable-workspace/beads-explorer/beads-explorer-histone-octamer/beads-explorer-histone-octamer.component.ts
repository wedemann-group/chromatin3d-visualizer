/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component } from '@angular/core';
import { BeadsExplorerComponent } from '../beads-explorer.component';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { ColorsService } from '../../../../core/services/colors/colors.service';
import NucleosomeBead from '../../../../shared/models/nucleosome-bead.model';
import { convertBeadsToBeadEntries } from '../beads-explorer.helper';
import { Configuration } from '../../../../shared/models/configuration.model';
import { BEAD_USER_NAME_HISTONE_OCTAMER } from '../../../../constants';

@Component({
  selector: 'trj-beads-explorer-histone',
  templateUrl: '../beads-explorer.component.html',
  styleUrls: [
    '../beads-explorer.component.scss',
    './beads-explorer-histone-octamer.component.scss',
  ],
})
export class BeadsExplorerHistoneOctamerComponent extends BeadsExplorerComponent {
  constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _colorsService: ColorsService
  ) {
    super(_workspaceService, _colorsService.histoneOctamerColors);

    super.setBeadsFromTrajectoryFunction =
      this.setBeadsFromTrajectory.bind(this);
  }

  private setBeadsFromTrajectory(config: Configuration | undefined): void {
    this.entries = convertBeadsToBeadEntries(
      (config?.beads ?? []).filter(
        (b) => b instanceof NucleosomeBead
      ) as NucleosomeBead[],
      this._colorsService.histoneOctamerColors,
      (bead) => `${BEAD_USER_NAME_HISTONE_OCTAMER} ${bead.id}`
    );
  }
}
