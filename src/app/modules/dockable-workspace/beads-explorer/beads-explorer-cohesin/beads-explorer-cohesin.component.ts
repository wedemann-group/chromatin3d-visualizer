/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { BeadEntry } from '../models/bead-entry';
import { DEFAULT_COLOR_HISTONE_OCTAMER } from '../../../../shared/color.constants';
import { BeadsExplorerBase } from '../beads-explorer-base';
import { ColorsService } from '../../../../core/services/colors/colors.service';
import { convertBeadsToBeadEntries } from '../beads-explorer.helper';
import CohesinHeadBead from '../../../../shared/models/cohesin-head-bead.model';
import { Configuration } from '../../../../shared/models/configuration.model';
import {
  BEAD_USER_NAME_COHESIN_HEAD,
  BEAD_USER_NAME_COHESIN_LINKER,
  BEAD_USER_NAME_COHESIN_RING,
} from '../../../../constants';

const COHESIN_LINKER_ID_START = 10000; // magic number to decide between cohesin head, cohesin ring/hinge and linker

@Component({
  selector: 'trj-beads-explorer-cohesin',
  templateUrl: '../beads-explorer.component.html',
  styleUrls: [
    '../beads-explorer.component.scss',
    './beads-explorer-cohesin.component.scss',
  ],
})
export class BeadsExplorerCohesinComponent
  extends BeadsExplorerBase<BeadEntry>
  implements AfterViewInit, OnDestroy
{
  public readonly displayedColumns: string[] = [
    'select',
    'displayIndex',
    'displayName',
    'color',
  ];
  public readonly dataSource = new MatTableDataSource<BeadEntry>([]);
  public readonly selection = new SelectionModel<BeadEntry>(true, []);

  public selectionColor = DEFAULT_COLOR_HISTONE_OCTAMER;

  @ViewChild(MatPaginator)
  public paginator: MatPaginator | undefined;

  constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _colorsService: ColorsService
  ) {
    super();

    this.defaultColor = ['#00ff00'];

    this.subscribes.push(
      _workspaceService.config$.subscribe((config) => {
        if (!config || !_workspaceService.trajectory) {
          this.dataSource.data = [];
          return;
        }

        this.setBeadsFromTrajectory(config?.config);
      })
    );

    this.subscribes.push(
      this._colorsService.cohesinRingAndHingeColors.color$.subscribe(() =>
        this.setDefaultColors()
      )
    );

    this.subscribes.push(
      this._colorsService.cohesinHeadColors.color$.subscribe(() =>
        this.setDefaultColors()
      )
    );

    this.subscribes.push(
      this._colorsService.cohesinLinkerColors.color$.subscribe(() =>
        this.setDefaultColors()
      )
    );
  }

  ngAfterViewInit(): void {
    this.afterInit();
    this.setDefaultColors();

    if (this._workspaceService.trajectory && !!this._workspaceService.config)
      this.setBeadsFromTrajectory(this._workspaceService.config.config);
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  changeSelectionColor(col: string, bead: BeadEntry): void {
    const beads = [...this.selection.selected];
    if (beads.length === 0) beads.push(bead);

    beads.forEach((s) => (s.color = col));
    this.setConfigSpecificColors(
      col,
      beads.map((e) => e.index)
    );
  }

  setSelectionColor(bead: BeadEntry): void {
    this.selectionColor = bead.color;
  }

  protected setConfigSpecificColors(
    color: string,
    beadIndices: number[]
  ): void {
    const ringIndices = beadIndices
      .filter((i) => i < 0)
      .map((index) => -(index + 1));
    const headIndices = beadIndices
      .filter((i) => i > 0)
      .map((index) => index - 1);
    const linkerIndices = beadIndices
      .filter((i) => i > 0)
      .map((index) => index - COHESIN_LINKER_ID_START);

    if (ringIndices.length > 0)
      this._colorsService.cohesinRingAndHingeColors.setConfigSpecificColors(
        color,
        ringIndices
      );

    if (headIndices.length > 0)
      this._colorsService.cohesinHeadColors.setConfigSpecificColors(
        color,
        headIndices
      );

    if (linkerIndices.length > 0)
      this._colorsService.cohesinLinkerColors.setConfigSpecificColors(
        color,
        linkerIndices
      );
  }

  private setDefaultColors(): void {
    this.defaultColor = [
      this._colorsService.cohesinHeadColors.color,
      this._colorsService.cohesinLinkerColors.color,
      this._colorsService.cohesinRingAndHingeColors.color,
    ];
  }

  private setBeadsFromTrajectory(config: Configuration | undefined): void {
    const heads = (config?.beads ?? []).filter(
      (b) => b instanceof CohesinHeadBead
    ) as CohesinHeadBead[];
    const entries = convertBeadsToBeadEntries(
      heads,
      this._colorsService.cohesinHeadColors,
      (bead) => `${BEAD_USER_NAME_COHESIN_HEAD} ${bead.id}`
    );
    entries.forEach((entry) => (entry.index = entry.index + 1));

    const rings: BeadEntry[] = [];
    const colorBeads =
      this._colorsService.cohesinRingAndHingeColors.configSpecificColors;
    const defaultColor = this._colorsService.cohesinRingAndHingeColors.color;
    let ringIndex = 0;
    for (const ring of config?.cohesinRings ?? []) {
      --ringIndex;

      const colorBead = colorBeads.find((cb) => cb.index === -(ringIndex + 1));

      rings.push({
        index: ringIndex,
        id: ring.index.toString(),
        displayName: `${BEAD_USER_NAME_COHESIN_RING} ${ring.index}`,
        displayIndex: (-(ringIndex + 1)).toString(),
        color: colorBead ? colorBead.color : defaultColor,
      });
    }

    const colorBeadsLinker =
      this._colorsService.cohesinLinkerColors.configSpecificColors;
    const defaultColorLinker = this._colorsService.cohesinLinkerColors.color;
    const linkers = heads.map((head, index) => {
      const colorBead = colorBeadsLinker.find((cb) => cb.index === index);

      return {
        index: index + COHESIN_LINKER_ID_START,
        id: head.id.toString(),
        displayName: `${BEAD_USER_NAME_COHESIN_LINKER} ${head.id}`,
        displayIndex: index.toString(),
        color: colorBead ? colorBead.color : defaultColorLinker,
      } as BeadEntry;
    });

    this.entries = [...entries, ...rings, ...linkers];
  }
}
