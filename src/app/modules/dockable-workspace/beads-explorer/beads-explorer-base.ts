/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { BaseTabComponentDirective } from '../../../shared/directives/base-tab-component.directive';

export abstract class BeadsExplorerBase<
  TEntry
> extends BaseTabComponentDirective {
  public abstract readonly dataSource: MatTableDataSource<TEntry>;
  public abstract readonly selection: SelectionModel<TEntry>;

  public abstract paginator: MatPaginator | undefined;

  public defaultColor: string[] = [''];

  private _oldPaginatorLength = 0;
  private _oldPaginatorPageSize = 0;
  private _isViewInit = false;

  public set entries(beads: TEntry[]) {
    if (!this._isViewInit) return;

    this.selection.clear();
    this._oldPaginatorLength = beads.length;
    this.dataSource.data = beads;
  }

  public afterInit(): void {
    if (!this.paginator) throw new Error('Paginator is nothing, invalid!');

    this.dataSource.paginator = this.paginator;

    this._isViewInit = true;

    this.subscribes.push(
      this.paginator.page.subscribe((page) => {
        if (
          page.previousPageIndex === page.pageIndex &&
          page.length === this._oldPaginatorLength &&
          page.pageSize >= this._oldPaginatorPageSize
        ) {
          this._oldPaginatorLength = page.length;
          this._oldPaginatorPageSize = page.pageSize;
          return;
        }

        this._oldPaginatorLength = page.length;
        this._oldPaginatorPageSize = page.pageSize;
        this.selection.clear();
      })
    );
  }

  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  public isAllSelected(): boolean {
    const items = this.getFilteredItemsOnCurrentPage();
    const selectedItems = items.filter((i) => this.selection.isSelected(i));
    return items.length === selectedItems.length;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  public masterToggle(): void {
    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      const items = this.getFilteredItemsOnCurrentPage();
      items.forEach((i) => this.selection.select(i));
    }
  }

  /** The label for the checkbox on the passed row */
  public checkboxLabel(row?: TEntry): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row`;
  }

  private getFilteredItemsOnCurrentPage(): TEntry[] {
    if (!this.paginator) return this.dataSource.data;

    return [...this.dataSource.filteredData].splice(
      this.paginator.pageIndex * this.paginator.pageSize,
      this.paginator.pageSize
    );
  }
}
