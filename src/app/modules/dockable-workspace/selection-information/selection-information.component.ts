/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject } from '@angular/core';
import { ComponentContainer } from 'golden-layout';
import { BaseTabComponentDirective } from '../../../shared/directives/base-tab-component.directive';
import { WorkspaceService } from '../../../core/services/workspace.service';
import { RaycastService } from '../../../core/services/raycast.service';

@Component({
  selector: 'trj-selections',
  templateUrl: './selection-information.component.html',
  styleUrls: ['./selection-information.component.scss'],
})
export class SelectionInformationComponent extends BaseTabComponentDirective {
  constructor(
    public readonly workspaceService: WorkspaceService,
    public readonly raycastService: RaycastService,
    @Inject(BaseTabComponentDirective.GOLDEN_LAYOUT_CONTAINER_INJECTION_TOKEN)
    protected readonly container: ComponentContainer
  ) {
    super();

    this.subscribeTabResizeForBreakpoints(container);
  }

  clearSelections(): void {
    this.raycastService.clearSelections();
  }
}
