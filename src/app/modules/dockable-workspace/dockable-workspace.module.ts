/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AngularResizeEventModule } from 'angular-resize-event';
import { ViewerModule } from './viewer/viewer.module';
import { DockableWorkspaceComponent } from './dockable-workspace.component';
import { SharedModule } from '../../shared/shared.module';
import { ColorSerializerDeserializerService } from '../../core/services/serializations/color-serializer-deserializer.service';
import { SettingsService } from '../../core/services/settings/settings.service';
import { DistanceMeasurementSerializerDeserializerService } from '../../core/services/serializations/distance-measurement-serializer-deserializer.service';
import { TrajectoryListerService } from '../../core/services/trajectory-lister.service';
import { BeadsExplorerModule } from './beads-explorer/beads-explorer.module';
import { CameraSettingsModule } from './camera-settings/camera-settings.module';
import { CohesinCtcfModule } from './cohesin-ctcf/cohesin-ctcf.module';
import { ConfigurationPlayerModule } from './configuration-player/configuration-player.module';
import { DebugSettingsModule } from './debug-settings/debug-settings.module';
import { FiberInformationModule } from './fiber-information/fiber-information.module';
import { PbcModule } from './pbc/pbc.module';
import { SelectionInformationModule } from './selection-information/selection-information.module';
import { ViewSettingsModule } from './view-settings/view-settings.module';
import { ToolCameraAnimatorModule } from './tools/tool-camera-animator/tool-camera-animator.module';
import { ToolColorGradientModule } from './tools/tool-color-gradient/tool-color-gradient.module';
import { ToolDefinedCameraPositionsModule } from './tools/tool-defined-camera-positions/tool-defined-camera-positions.module';
import { ToolDistanceMeasurementModule } from './tools/tool-distance-measurement/tool-distance-measurement.module';
import { PbcSettingsSerializerDeserializerService } from '../../core/services/serializations/pbc-settings-serializer-deserializer.service';

@NgModule({
  declarations: [DockableWorkspaceComponent],
  providers: [],
  imports: [
    CommonModule,
    AngularResizeEventModule,
    MatSnackBarModule,
    ViewerModule,
    SharedModule,
    BeadsExplorerModule,
    CameraSettingsModule,
    CohesinCtcfModule,
    ConfigurationPlayerModule,
    DebugSettingsModule,
    FiberInformationModule,
    PbcModule,
    SelectionInformationModule,
    ViewSettingsModule,
    ToolCameraAnimatorModule,
    ToolColorGradientModule,
    ToolDefinedCameraPositionsModule,
    ToolDistanceMeasurementModule,
  ],
  exports: [DockableWorkspaceComponent],
})
export class DockableWorkspaceModule {
  public constructor(
    /* Injection of handlers so that they are used in the angular application (and thus callable) */
    private readonly _: ColorSerializerDeserializerService,
    private readonly __: SettingsService,
    private readonly ___: DistanceMeasurementSerializerDeserializerService,
    private readonly ____: TrajectoryListerService,
    private readonly _____: PbcSettingsSerializerDeserializerService /* ------ end ------ */
  ) {}
}
