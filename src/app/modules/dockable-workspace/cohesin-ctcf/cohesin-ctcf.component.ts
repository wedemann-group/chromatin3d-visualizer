/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject, OnDestroy } from '@angular/core';
import { BaseTabComponentDirective } from '../../../shared/directives/base-tab-component.directive';
import { CohesinService } from '../../../core/services/cohesin.service';
import { ComponentContainer } from 'golden-layout';

@Component({
  selector: 'trj-cohesin-ctcf',
  templateUrl: './cohesin-ctcf.component.html',
  styleUrls: ['./cohesin-ctcf.component.scss'],
})
export class CohesinCtcfComponent
  extends BaseTabComponentDirective
  implements OnDestroy
{
  public hasCohesin = false;

  public get cohDistance(): string {
    const size = this._cohesinService.getCohDistance();
    return size > 0 ? size.toString() : '-';
  }

  public get headRadius(): string {
    const size = this._cohesinService.getCohesinHeadRadius();
    return size > 0 ? size.toString() : '-';
  }

  public get ringRadius(): string {
    const size = this._cohesinService.getCohesinRadius();
    return size > 0 ? size.toString() : '-';
  }

  public get ringThickness(): string {
    const size = this._cohesinService.getCohesinThickness();
    return size > 0 ? size.toString() : '-';
  }

  constructor(
    @Inject(BaseTabComponentDirective.GOLDEN_LAYOUT_CONTAINER_INJECTION_TOKEN)
    protected readonly container: ComponentContainer,
    private readonly _cohesinService: CohesinService
  ) {
    super();

    this.subscribeTabResizeForBreakpoints(container);

    this._cohesinService.hasCohesin$.subscribe((has) => {
      this.hasCohesin = has;
    });
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }
}
