/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject, OnDestroy } from '@angular/core';
import { WorkspaceService } from '../../../core/services/workspace.service';
import { BaseTabComponentDirective } from '../../../shared/directives/base-tab-component.directive';
import { ComponentContainer } from 'golden-layout';

@Component({
  selector: 'trj-configuration-player',
  templateUrl: './configuration-player.component.html',
  styleUrls: ['./configuration-player.component.scss'],
})
export class ConfigurationPlayerComponent
  extends BaseTabComponentDirective
  implements OnDestroy
{
  public maxConfig = -1;
  public currentConfig = -1;
  public interval = 250;
  public loop = false;
  public readonly MINIMAL_INTERVAL = 100;
  public readonly MAXIMAL_INTERVAL = 10000;

  public timerID = -1;
  public mcStep = -1;

  private _ignoreConfigChange = true;

  constructor(
    @Inject(BaseTabComponentDirective.GOLDEN_LAYOUT_CONTAINER_INJECTION_TOKEN)
    protected readonly container: ComponentContainer,
    private readonly _workspaceService: WorkspaceService
  ) {
    super();

    this.subscribeTabResizeForBreakpoints(container);

    this.subscribes.push(
      this._workspaceService.trajectory$.subscribe((trj) => {
        if (!trj) {
          this.maxConfig = -1;
          this.currentConfig = -1;
          this.mcStep = -1;
        } else {
          this.mcStep = -1;
          this.maxConfig = trj.configurationLength - 1;
          this.currentConfig = 0;

          if (this._workspaceService.config?.config)
            this.mcStep = this._workspaceService.config.config.mcSteps;
        }
      })
    );

    this.subscribes.push(
      this._workspaceService.config$.subscribe((conf) => {
        if (!conf || !this._workspaceService.trajectory) {
          this.mcStep = -1;
        } else {
          this.mcStep = conf.config.mcSteps;
        }

        if (this._ignoreConfigChange) {
          this._ignoreConfigChange = false;
          return;
        }

        this.currentConfig = !!conf ? conf.index : -1;
      })
    );
  }

  changeCurrentConfig(config: number | null): void {
    this.currentConfig = config ?? 0;
    this.changeWorkspaceConfig(this.currentConfig);
  }

  setFirstConfiguration(): void {
    if (this.currentConfig <= 0) return;

    this.changeCurrentConfig(0);
  }

  lastConfiguration(): void {
    if (this.currentConfig >= this.maxConfig) return;

    this.changeCurrentConfig(this.maxConfig);
  }

  previousConfiguration(): void {
    if (this.currentConfig <= 0) {
      if (!this.loop) return;

      this.currentConfig = this.maxConfig;
    } else {
      --this.currentConfig;
    }

    this.changeWorkspaceConfig(this.currentConfig);
  }

  nextConfiguration(): void {
    if (this.currentConfig >= this.maxConfig) {
      if (!this.loop) return;

      this.currentConfig = 0;
    } else {
      ++this.currentConfig;
    }

    this.changeWorkspaceConfig(this.currentConfig);
  }

  changeInterval(newInterval: number): void {
    this.interval = newInterval;

    // a timer runs already, "change" the interval
    if (this.timerID >= 0) {
      this.abortTimer();
      this.setAndStartTimer();
    }
  }

  playAndPause(): void {
    if (this.timerID >= 0) this.abortTimer();
    else this.setAndStartTimer();
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  private changeWorkspaceConfig(configIndex: number): void {
    if (
      this._workspaceService.isLoading ||
      this._workspaceService.config?.index === configIndex
    )
      return;

    this._ignoreConfigChange = false;
    this._workspaceService.setConfig(configIndex, this.timerID < 0);
  }

  private abortTimer(): void {
    const intervalVal = this.timerID;
    clearInterval(intervalVal);
    this.timerID = -1;
  }

  private setAndStartTimer(): void {
    this.timerID = window.setInterval(
      this.nextConfiguration.bind(this),
      this.interval
    );
  }
}
