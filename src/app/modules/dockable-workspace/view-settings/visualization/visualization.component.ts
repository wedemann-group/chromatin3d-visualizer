/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Input } from '@angular/core';
import { ViewService } from '../../../../core/services/view/view.service';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { EMaterialType } from '../../../../shared/models/material-type.enum';

@Component({
  selector: 'trj-view-settings-visualization[isSmall]',
  templateUrl: './visualization.component.html',
  styleUrls: ['./visualization.component.scss'],
})
export class VisualizationComponent {
  @Input()
  public isSmall = false;

  constructor(
    public readonly viewService: ViewService,
    public readonly workspaceService: WorkspaceService
  ) {}

  isMaterialWithTexture(): boolean {
    return this.viewService.materialType === EMaterialType.WITH_TEXTURE;
  }

  changeMaterial(withTexture: boolean): void {
    this.viewService.materialType = withTexture
      ? EMaterialType.WITH_TEXTURE
      : EMaterialType.NORMAL;
  }
}
