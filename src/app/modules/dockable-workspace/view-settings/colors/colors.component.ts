/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Input } from '@angular/core';
import { ColorsService } from '../../../../core/services/colors/colors.service';
import {
  DEFAULT_BACK_COLOR,
  DEFAULT_COLOR_COHESIN_HEAD,
  DEFAULT_COLOR_COHESIN_LINKER,
  DEFAULT_COLOR_COHESIN_RING,
  DEFAULT_COLOR_HISTONE_OCTAMER,
  DEFAULT_COLOR_LINKER_DNA,
  DEFAULT_COLOR_NUCLEOSOME_DNA,
  DEFAULT_COLOR_SELECTION,
} from '../../../../shared/color.constants';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { CohesinService } from '../../../../core/services/cohesin.service';
import { SessionManagerService } from '../../../../core/services/session-mananger/session-manager.service';
import { ELinkerDNABuilderType } from '../../../../core/services/session-mananger/models/linker-dna-builder-type.enum';

@Component({
  selector: 'trj-view-settings-colors[isSmall]',
  templateUrl: './colors.component.html',
  styleUrls: ['./colors.component.scss'],
})
export class ColorsComponent {
  public readonly ELinkerDNABuilderType = ELinkerDNABuilderType;

  public readonly DEFAULT_BACK_COLOR = DEFAULT_BACK_COLOR;
  public readonly DEFAULT_COLOR_HISTONE_OCTAMER = DEFAULT_COLOR_HISTONE_OCTAMER;
  public readonly DEFAULT_COLOR_NUCLEOSOME_DNA = DEFAULT_COLOR_NUCLEOSOME_DNA;
  public readonly DEFAULT_COLOR_LINKER_DNA = DEFAULT_COLOR_LINKER_DNA;
  public readonly DEFAULT_COLOR_BASE_PAIR = DEFAULT_COLOR_LINKER_DNA;
  public readonly DEFAULT_COLOR_COHESIN_HEAD = DEFAULT_COLOR_COHESIN_HEAD;
  public readonly DEFAULT_COLOR_COHESIN_RING = DEFAULT_COLOR_COHESIN_RING;
  public readonly DEFAULT_COLOR_COHESIN_LINKER = DEFAULT_COLOR_COHESIN_LINKER;
  public readonly DEFAULT_COLOR_SELECTION = DEFAULT_COLOR_SELECTION;

  @Input()
  public isSmall = false;

  constructor(
    public readonly colorService: ColorsService,
    public readonly cohesinService: CohesinService,
    public readonly workspaceService: WorkspaceService,
    public readonly sessionManagerService: SessionManagerService
  ) {}

  changeBackColor(color: string): void {
    this.colorService.backColor.color = color;
  }

  changeDefaultHistoneOctamerColor(color: string): void {
    this.colorService.histoneOctamerColors.color = color;
  }

  changeDefaultNucleosomeDNAColor(color: string): void {
    this.colorService.dnaColors.color = color;
  }

  changeDefaultCohesinHeadColor(color: string): void {
    this.colorService.cohesinHeadColors.color = color;
  }

  changeDefaultCohesinRingNHingeColor(color: string): void {
    this.colorService.cohesinRingAndHingeColors.color = color;
  }

  changeDefaultCohesinLinkerColor(color: string): void {
    this.colorService.cohesinLinkerColors.color = color;
  }

  changeDefaultDNAColor(color: string): void {
    this.colorService.linkerDNAColors.color = color;
  }

  changeDefaultBasePairColor(color: string): void {
    this.colorService.basePairColors.color = color;
  }

  changeDefaultSelectionColor(color: string): void {
    this.colorService.selectionColor.color = color;
  }
}
