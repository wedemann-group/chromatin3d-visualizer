/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject, OnDestroy } from '@angular/core';
import { PbcService } from '../../../core/services/pbc.service';
import { DEFAULT_COLOR_PBC_CONTAINER } from '../../../shared/color.constants';
import { BaseTabComponentDirective } from '../../../shared/directives/base-tab-component.directive';
import { WorkspaceService } from '../../../core/services/workspace.service';
import { ComponentContainer } from 'golden-layout';

@Component({
  selector: 'trj-pbc',
  templateUrl: './pbc.component.html',
  styleUrls: ['./pbc.component.scss'],
})
export class PbcComponent
  extends BaseTabComponentDirective
  implements OnDestroy
{
  public readonly DEFAULT_COLOR_PBC_BOX = DEFAULT_COLOR_PBC_CONTAINER;

  public hasPBCMode = false;

  public get containerSize(): string {
    const size = this.pbcService.getContainerSize();
    return size > 0 ? size.toString() : '-';
  }

  public get cubeSizeLJ(): string {
    const size = this.pbcService.getCubeSizeLJ();
    return size > 0 ? size.toString() : '-';
  }

  public get cubeSizeDNANucItr(): string {
    const size = this.pbcService.getCubeSizeDNANucItr();
    return size > 0 ? size.toString() : '-';
  }

  public get cubeSizeEstat(): string {
    const size = this.pbcService.getCubeSizeEstat();
    return size > 0 ? size.toString() : '-';
  }

  constructor(
    public pbcService: PbcService,
    @Inject(BaseTabComponentDirective.GOLDEN_LAYOUT_CONTAINER_INJECTION_TOKEN)
    protected readonly container: ComponentContainer,
    private readonly _workspaceService: WorkspaceService
  ) {
    super();

    this.subscribeTabResizeForBreakpoints(container);

    this.subscribes.push(
      this._workspaceService.config$.subscribe((config) => {
        this.hasPBCMode =
          this.pbcService.getContainerSizeFromConfig(config?.config) > 0;
      })
    );
  }

  changePBCContainerColor(newColor: string): void {
    this.pbcService.pbcContainerColor = newColor;
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }
}
