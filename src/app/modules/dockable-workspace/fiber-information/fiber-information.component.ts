/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject, OnDestroy } from '@angular/core';
import { WorkspaceService } from '../../../core/services/workspace.service';
import { BaseTabComponentDirective } from '../../../shared/directives/base-tab-component.directive';
import { ComponentContainer } from 'golden-layout';

const NO_FILE_LOADED = '-';
const FILE_NAME_COULD_NOT_BE_READ = 'File name could not be read!';

@Component({
  selector: 'trj-fiber-information',
  templateUrl: './fiber-information.component.html',
  styleUrls: ['./fiber-information.component.scss'],
})
export class FiberInformationComponent
  extends BaseTabComponentDirective
  implements OnDestroy
{
  public currentFileName = NO_FILE_LOADED;

  constructor(
    @Inject(BaseTabComponentDirective.GOLDEN_LAYOUT_CONTAINER_INJECTION_TOKEN)
    protected readonly container: ComponentContainer,
    public readonly workspaceService: WorkspaceService
  ) {
    super();

    this.subscribeTabResizeForBreakpoints(container);

    // Subscribe listeners
    this.subscribes.push(
      this.workspaceService.trajectory$.subscribe(() => {
        if (!this.workspaceService.currentFile) {
          this.currentFileName = NO_FILE_LOADED;
          return;
        }

        if (!this.workspaceService.currentFile.name) {
          this.currentFileName = FILE_NAME_COULD_NOT_BE_READ;
          return;
        }

        this.currentFileName = this.workspaceService.currentFile.name;
      })
    );
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  getTemperature(): number {
    if (!this.workspaceService.trajectory) return -1;
    const config = this.workspaceService.config;
    const fallbackResult =
      this.workspaceService.trajectory.globalParameters.temperature ?? -1;

    if (config?.config?.parameters)
      return config.config.parameters?.temperature ?? fallbackResult;

    return fallbackResult;
  }
}
