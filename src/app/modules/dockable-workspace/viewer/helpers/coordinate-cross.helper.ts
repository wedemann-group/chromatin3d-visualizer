/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable, NgZone, OnDestroy } from '@angular/core';
import { DirectionalLight, PerspectiveCamera } from 'three';
import { ViewService } from '../../../../core/services/view/view.service';
import { SceneModule } from '../scene.module';
import { ColorsService } from '../../../../core/services/colors/colors.service';
import AbstractThreeJsSceneHelper from './abstract-three-js-scene.helper';
import { AssetsService } from '../../../../core/services/assets/assets.service';
import CoordinateCrossManager from '../objects/threejs/managers/coordinate-cross/coordinate-cross.manager';
import { THREE_JS_DEFAULT_FONT_NAME_BOLD } from '../../../../core/services/assets/constants';

const COORDINATE_CROSS_BOX_WIDTH = 150; // defined in css
const COORDINATE_CROSS_BOX_HEIGHT = 150; // defined in css

@Injectable({
  providedIn: SceneModule,
})
export class CoordinateCrossHelper
  extends AbstractThreeJsSceneHelper
  implements OnDestroy
{
  public constructor(
    private readonly _ngZone: NgZone,
    private readonly _colorsService: ColorsService,
    private readonly _viewService: ViewService,
    private readonly _assetsService: AssetsService
  ) {
    super(_ngZone);
  }

  public ngOnDestroy(): void {
    this.cancelAnimation();

    this.unsubscribeListeners();

    this.clearManagers();
    this.clearScene();
  }

  public createScene(canvas: HTMLCanvasElement): void {
    this.scene = super.createDefaultScene(
      canvas,
      COORDINATE_CROSS_BOX_WIDTH,
      COORDINATE_CROSS_BOX_HEIGHT
    );
    const camera = new PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.1,
      100000000
    );
    this.camera = camera;
    camera.position.z = 5;

    camera.aspect = COORDINATE_CROSS_BOX_WIDTH / COORDINATE_CROSS_BOX_HEIGHT;
    camera.updateProjectionMatrix();

    this.managers.push(
      new CoordinateCrossManager(
        this._assetsService.getFont(THREE_JS_DEFAULT_FONT_NAME_BOLD),
        this.camera,
        this._viewService,
        this._colorsService
      )
    );
    this.scene.add(this.camera);
    this.scene.add(...this.managers.map((m) => m.getGroup()));

    const directionalLight = new DirectionalLight(0xffffff, 0.9);
    directionalLight.castShadow = true;
    this.scene.add(directionalLight);

    super.createAmbientLight();
  }

  protected render(): void {
    this.frameId = requestAnimationFrame(() => {
      this.render();
    });

    if (!this.renderer || !this.scene || !this.camera) return;

    this.renderer.render(this.scene, this.camera);
  }

  protected subscribeListeners(): void {
    // ...
  }
}
