/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable, NgZone, OnDestroy } from '@angular/core';
import { Color, DirectionalLight, OrthographicCamera, Vector2 } from 'three';
import { Line2 } from 'three/examples/jsm/lines/Line2';
import { LineGeometry } from 'three/examples/jsm/lines/LineGeometry';
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial';
import AbstractThreeJsSceneHelper from './abstract-three-js-scene.helper';
import { SceneModule } from '../scene.module';
import { ViewService } from '../../../../core/services/view/view.service';
import { ColorsService } from '../../../../core/services/colors/colors.service';
import {
  colorToArgb,
  rgbToHex,
} from '../../../../shared/utilities/color.utility';
import { DistanceMeasurementService } from '../../../../core/services/distance-measurement.service';

const CAMERA_POSITION_Z = 500;
const CAMERA_ORTHOGRAPHIC_FAR = 1000;

const LINE_WIDTH = 5;

@Injectable({
  providedIn: SceneModule,
})
export class ToolDistanceMeasurement2DHelper
  extends AbstractThreeJsSceneHelper
  implements OnDestroy
{
  private _lineGeometry: LineGeometry | undefined;
  private _lineMaterial: LineMaterial | undefined;
  private _line: Line2 | undefined;

  public constructor(
    private readonly _ngZone: NgZone,
    private readonly _colorsService: ColorsService,
    private readonly _viewService: ViewService,
    private readonly _distanceMeasurement: DistanceMeasurementService
  ) {
    super(_ngZone);
  }

  public subscribeListeners(): void {
    this.subscribes.push(
      this._distanceMeasurement.lineColor$.subscribe((col) => {
        if (!this._line) return;

        const colorComponents = colorToArgb(col);
        this._line.material.color = new Color(rgbToHex(colorComponents)); // remove alpha because threejs doesn't support alpha
        this._line.material.opacity = colorComponents.a;
        this._line.material.visible = colorComponents.a > 0;
        this._line.material.needsUpdate = true;
      })
    );
  }

  public ngOnDestroy(): void {
    this.cancelAnimation();

    this.unsubscribeListeners();

    this.clearScene();
  }

  public createScene(canvas: HTMLCanvasElement): void {
    this.scene = super.createDefaultScene(
      canvas,
      window.innerWidth,
      window.innerHeight
    );
    this.createCameraFromSizeAndAdd(window.innerWidth, window.innerHeight);

    const directionalLight = new DirectionalLight(0xffffff, 0.9);
    directionalLight.castShadow = true;
    this.scene.add(directionalLight);

    super.createAmbientLight();

    this._lineGeometry = new LineGeometry();
    this._lineGeometry.setPositions([0, 0, 0, 0, 0, 0]);

    const colorComponents = colorToArgb(this._distanceMeasurement.lineColor);
    this._lineMaterial = new LineMaterial({
      color: new Color(rgbToHex(colorComponents)).getHex(), // remove alpha because threejs doesn't support alpha
      opacity: colorComponents.a,
      visible: colorComponents.a > 0,
      transparent: true,
      vertexColors: false,
      linewidth: LINE_WIDTH,
      worldUnits: true,
    });

    this._line = new Line2(this._lineGeometry, this._lineMaterial);
    this._line.computeLineDistances();

    this.scene.add(this._line);
  }

  public resize(width: number, height: number): void {
    if (this.camera) this.scene?.remove(this.camera);

    this.createCameraFromSizeAndAdd(width, height);

    if (!this.renderer) return;
    this.renderer.setSize(width, height);
  }

  public setPoint(startPoint: Vector2, endPoint: Vector2): void {
    if (!this._line || !this._lineGeometry) return;

    this._lineGeometry.setPositions([
      startPoint.x,
      -startPoint.y,
      0,
      endPoint.x,
      -endPoint.y,
      0,
    ]);
    this._line.computeLineDistances();
  }

  protected render(): void {
    this.frameId = requestAnimationFrame(() => {
      this.render();
    });

    if (!this.renderer || !this.scene || !this.camera) return;

    this.renderer.render(this.scene, this.camera);
  }

  private createCameraFromSizeAndAdd(width: number, height: number): void {
    const camera = new OrthographicCamera(
      width / -2,
      width / 2,
      height / 2,
      height / -2,
      1,
      CAMERA_ORTHOGRAPHIC_FAR
    );
    this.camera = camera;
    camera.position.z = CAMERA_POSITION_Z;
    camera.position.x = width / 2;
    camera.position.y = -height / 2;

    camera.updateProjectionMatrix();

    this.scene?.add(this.camera);
  }
}
