/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Camera, PerspectiveCamera, Scene, Vector3 } from 'three';
import { Injectable } from '@angular/core';
import { ViewService } from '../../../../core/services/view/view.service';
import { EControlType } from '../models/control-type.enum';
import { SerializableCamera } from '../models/serializable-camera.model';
import { DeserializedCamera } from '../models/deserialized-camera.model';
import { LOCAL_STORAGE_CAMERA } from '../../../../shared/local-storage-keys.constants';
import {
  deserializeCamera,
  deserializeCameraFromString,
  serializeCamera,
} from '../camera/camera.utility';
import { SessionManagerService } from '../../../../core/services/session-mananger/session-manager.service';
import { MenuStripService } from '../../../../core/services/menu-strip.service';
import { ECameraSetter } from '../models/camera-setter.enum';
import { SceneModule } from '../scene.module';
import CameraControl from '../camera/controls/camera-control';
import { CAMERA_POV, START_ORBIT_CONTROLS_TARGET } from '../camera/constants';
import FixedCenterControl from '../camera/controls/fixed-center-control';
import FirstPersonPerspectiveControl from '../camera/controls/first-person-perspective-control';
import { TrackballControls } from 'three/examples/jsm/controls/TrackballControls';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { ContainerDimensionExtended } from '../../../../core/services/view/models/container-dimension-extended.model';

const INTERVAL_CAMERA_SAVER = 60 * 10; // in fps (default 60fps)
const CAMERA_TOLERANCE = 0.001;

@Injectable({
  providedIn: SceneModule,
})
export class SceneCameraHelper {
  private _scene: Scene | undefined;
  private _dom: HTMLCanvasElement | undefined;
  private _camera: PerspectiveCamera | undefined;

  private _frameCounter = 0;
  private _oldPosition = new Vector3(0, 0, 0);
  private _controlType: EControlType = EControlType.firstPersonPerspective;
  private _setter: ECameraSetter = ECameraSetter.none;
  private _ignoreControlTypeChanged = false;
  private _lastTarget = START_ORBIT_CONTROLS_TARGET.clone();
  private _lastCenterOfMass: ContainerDimensionExtended | undefined;
  private _controlsClass:
    | CameraControl<TrackballControls | OrbitControls>
    | undefined;

  public constructor(
    private readonly _viewService: ViewService,
    private readonly _sessionManagerService: SessionManagerService,
    private readonly _menuService: MenuStripService
  ) {
    this._viewService.rerender$.subscribe(() => {
      this._controlsClass?.setCameraAutomatic(
        this._setter,
        this._lastCenterOfMass
      );
      this._lastCenterOfMass =
        this._viewService.getContainerSizeOfPBCOrFiberWithCenterPoint();
    });

    this._viewService.setCameraToCenterOfMass$.subscribe(() => {
      this._controlsClass?.setCameraAutomatic(
        ECameraSetter.toCenter,
        this._lastCenterOfMass
      );
    });

    this._sessionManagerService.getCurrentSession.push(() => ({
      camera: serializeCamera(this.getSerializableCamera()),
    }));

    this._sessionManagerService.loadSession$.subscribe((session) => {
      if (!session.camera) return;

      this.loadCamera(deserializeCamera(session.camera));
    });

    this._menuService.resetCamera$.subscribe(() => {
      this.resetCamera();
    });

    this._viewService.cameraGetter$.subscribe((callback) =>
      callback(deserializeCamera(serializeCamera(this.getSerializableCamera())))
    );

    this._viewService.cameraSetter$.subscribe((camera) => {
      this.loadCamera(camera);
    });

    this._viewService.controls$.subscribe((type) => {
      if (this._ignoreControlTypeChanged) {
        this._ignoreControlTypeChanged = false;
        return;
      }
      this.setLastTargetPosition();
      this.createControls(type);
      this._controlsClass?.resetTarget();
    });

    this._viewService.cameraSetType$.subscribe(
      (setter) => (this._setter = setter)
    );
  }

  public init(scene: Scene, dom: HTMLCanvasElement): void {
    this.disposeControls();

    this._scene = scene;
    this._dom = dom;
  }

  public render(): void {
    if (this._controlsClass instanceof FixedCenterControl)
      this._controlsClass.controls.update();

    this.saveCamera();
  }

  public createCamera(): Camera {
    this._camera = new PerspectiveCamera(
      CAMERA_POV,
      window.innerWidth / window.innerHeight,
      0.1,
      100000000
    );
    this.createControls(EControlType.firstPersonPerspective);
    this.resetCamera();

    try {
      const serializedCamera = localStorage.getItem(LOCAL_STORAGE_CAMERA);
      if (!serializedCamera) return this._camera;

      this.loadCamera(deserializeCameraFromString(serializedCamera));
    } catch (e) {}

    return this._camera;
  }

  public setAspect(width: number, height: number): void {
    if (!this._camera) return;

    this._camera.aspect = width / height;
    this._camera.updateProjectionMatrix();
  }

  public resetCamera(): void {
    this._camera?.rotation.set(0, 0, 0);
    this._camera?.up.set(0, 1, 0);
    // this._controls?.update();

    this._controlsClass?.reset();
  }

  public dispose(): void {
    this.disposeControls();
  }

  public disposeControls(): void {
    this._controlsClass?.dispose();
  }

  private setLastTargetPosition(): void {
    if (!this._controlsClass) return;

    this._lastTarget = this._controlsClass.controls.target.clone();
  }

  private createControls(controlType: EControlType): void {
    if (!this._camera || !this._dom) return;

    if (this._controlsClass) this._controlsClass.dispose();

    this._controlType = controlType;
    if (controlType === EControlType.firstPersonPerspective) {
      this._controlsClass = new FirstPersonPerspectiveControl(
        this._camera,
        this._dom,
        this._viewService,
        this._lastTarget
      );
    } else if (controlType === EControlType.fixedCenter) {
      this._controlsClass = new FixedCenterControl(
        this._camera,
        this._dom,
        this._viewService
      );
    }
  }

  private saveCamera(): void {
    if (!this._camera) return;

    // check and save camera
    if (++this._frameCounter > INTERVAL_CAMERA_SAVER) {
      if (
        Math.abs(this._oldPosition.x - this._camera.position.x) >
          CAMERA_TOLERANCE ||
        Math.abs(this._oldPosition.y - this._camera.position.y) >
          CAMERA_TOLERANCE ||
        Math.abs(this._oldPosition.z - this._camera.position.y) >
          CAMERA_TOLERANCE
      ) {
        this._oldPosition = this._camera.position.clone();
        localStorage.setItem(
          LOCAL_STORAGE_CAMERA,
          JSON.stringify(serializeCamera(this.getSerializableCamera()))
        );
      }
      this._frameCounter = 0;
    }
  }

  private getSerializableCamera(): SerializableCamera {
    if (!this._camera) return {} as SerializableCamera;

    return {
      position: this._camera.position,
      upVector: this._camera.up,
      rotation: this._camera.rotation,
      targetPosition:
        this._controlsClass?.controls.target ?? START_ORBIT_CONTROLS_TARGET,
      controlType: this._controlType,
      setter: this._setter,
    } as SerializableCamera;
  }

  private loadCamera(parsedCamera: DeserializedCamera): void {
    if (!this._camera) return;

    this._camera.setRotationFromEuler(parsedCamera.rotation);
    this._camera.position.copy(parsedCamera.position);
    this._camera.up.copy(parsedCamera.upVector);
    this._camera.updateMatrix();
    this.createControls(parsedCamera.controlType);
    if (parsedCamera.controlType !== EControlType.fixedCenter)
      this._controlsClass?.restoreTarget(parsedCamera.targetPosition);

    this.setControlTypeToService(parsedCamera.controlType);
    this._viewService.setCameraSetterType(parsedCamera.setter);
  }

  private setControlTypeToService(type: EControlType): void {
    this._ignoreControlTypeChanged = true;
    this._viewService.setControlType(type);
  }
}
