/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { PbcService } from '../../../../core/services/pbc.service';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { SessionManagerService } from '../../../../core/services/session-mananger/session-manager.service';
import QualityLevelModel from '../../../../core/services/session-mananger/models/quality-level.model';
import {
  AmbientLight,
  BoxGeometry,
  BufferGeometry,
  Camera,
  Color,
  Group,
  Intersection,
  Line,
  Matrix4,
  Mesh,
  MeshStandardMaterial,
  Object3D,
  PerspectiveCamera,
  Raycaster,
  RingGeometry,
  Scene,
  sRGBEncoding,
  Vector3,
  WebGLRenderer,
} from 'three';
import { ColorsService } from '../../../../core/services/colors/colors.service';
import InstancedObjectBuilder from '../beads/instanced-object.builder';
import CohesinHeadBuilder from '../beads/cohesin-head.builder';
import { AssetsService } from '../../../../core/services/assets/assets.service';
import { CohesinService } from '../../../../core/services/cohesin.service';
import { Configuration } from '../../../../shared/models/configuration.model';
import { ViewService } from '../../../../core/services/view/view.service';
import { XRControllerModelFactory } from 'three/examples/jsm/webxr/XRControllerModelFactory';
import { ContainerDimensionExtended } from '../../../../core/services/view/models/container-dimension-extended.model';
import { Injectable, OnDestroy } from '@angular/core';
import { SceneModule } from '../scene.module';
import LinkerDNAApproxBuilder from '../beads/linker-dna-approx.builder';
import { ColorBead } from '../../../../core/services/colors/models/color-bead.model';
import { MultipleBuilderResult } from '../../../../shared/models/multiple-builder-result.model';
import { XR_QUALITY } from '../../../../core/services/session-mananger/qualities/xr-quality.model';
import BuilderGetter from '../../../../shared/builders/builder-getter';
import {
  getBuilderInstance,
  setConfigSpecificColors,
} from '../../../../shared/utilities/bead-builder.utility';
import AbstractThreeJsSceneHelper from './abstract-three-js-scene.helper';
import PbcManager from '../objects/threejs/managers/pbc/pbc.manager';
import {
  THREE_JS_TEXTURE_NAME_BEAD,
  THREE_JS_TEXTURE_NAME_BTN_XR_CONF_CHANGE,
} from '../../../../core/services/assets/constants';
import { isConfigChangedListener } from '../objects/threejs/utility';
import { IConfigChangedListener } from '../objects/threejs/managers/config-changed-listener.interface';

const XR_REFERENCE_SPACE = 'local'; // local-floor is not supported on HoloLens 2, local means static model in the room (so the viewer might move around the model)
const XR_DEFAULT_MODEL_LENGTH = 0.6; // Should scale models to a diameter of 0.6 meters in XR
const XR_DEFAULT_MODEL_OFFSET = 20; // Should place the models slightly in front of the user
const XR_RENDERER_PRECISION = 'lowp'; // Sets the renderer to employ low precision for shaders during rendering
const XR_CONTROLLER_SELECTION_LINE_LENGTH = 5; // Sets the length of the line emanating from the controllers to 5 (meters)
const XR_POWER_PREFERENCE = 'high-performance'; // Tells the user's hardware to employ the more powerful rendering device if there are more than one.
const XR_DISPLAY_LOAD_COLOR = '#4fd24f'; // Color of the load display in XR mode
// Dimensions for the loading display geometry
const XR_DISPLAY_LOAD_GEOM_DIM = {
  innerRadius: 3,
  outerRadius: 4,
  thetaSegments: 30,
  phiSegments: 1,
  thetaStart: 0,
  thetaLength: 5,
};
const XR_DISPLAY_CONF_CHANGE_BTN_DIM = XR_DEFAULT_MODEL_LENGTH / 4; // Dimensions for the config change button
const XR_DISPLAY_OFFSET_Z = 5; // Offset for display controls in XR on the z axis
const XR_DISPLAY_LOAD_SCALE_FACTOR = 0.05; // Scale factor for the loading display
/**
 * Determines, how quickly the loading display is rotating when changing configs (higher numbers mean faster rotations).
 */
const ROTATION_FACTOR_LOAD_DISPLAY = 0.005;

type Controller = {
  group: Group;
  line: Line;
};

type PrevCoordinates = {
  prevPosition: Vector3;
  prevCenter: ContainerDimensionExtended;
  prevScale: number;
};
@Injectable({
  providedIn: SceneModule,
})
export default class XRSceneHelper
  extends AbstractThreeJsSceneHelper
  implements OnDestroy
{
  private readonly _xrQualityLevel: QualityLevelModel;
  private readonly _controllers: Controller[] = [];

  private _geometryGroup = new Group();
  private _scaleGroup = new Group();
  private _selectionRayCaster: Raycaster | undefined;
  private _nextConfigButton: Mesh | undefined;
  private _configLoadDisplay: Mesh | undefined;
  private _prevCoord: PrevCoordinates | undefined;

  public constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _colorService: ColorsService,
    private readonly _pbcService: PbcService,
    private readonly _sessionService: SessionManagerService,
    private readonly _assetService: AssetsService,
    private readonly _cohesinService: CohesinService,
    private readonly _viewService: ViewService
  ) {
    super();
    this._xrQualityLevel = XR_QUALITY;
  }

  /**
   * Calculates the factor to which the model needs to be scaled in order to
   * be as well visible as possible,
   * @param fibreCube An object which contains measurements of the model used to determine the scale factor.
   * @returns Vector3 A vector which contains the scale factors as numbers for every coordinate.
   * @private
   */
  private static calculateScaleFactor(
    fibreCube: ContainerDimensionExtended
  ): Vector3 {
    const lengthX = fibreCube.width;
    const scaleFactor = XR_DEFAULT_MODEL_LENGTH / lengthX;

    return new Vector3(scaleFactor, scaleFactor, scaleFactor);
  }

  /**
   * Creates the XR-scene with all its components and prepares the models
   * within the scene.
   */
  public buildXRScene(): void {
    if (!this._workspaceService.config) throw new Error();

    this.managers.push(
      new PbcManager(this._pbcService, this._workspaceService)
    );

    this.setUpConfigChangeButton();
    this.setUpConfigChangeLoadDisplay();

    this.scene = this.createDefaultScene();
    this.createOrRepositionModel(this._workspaceService.config.config);
    this.prepareSceneForXR();
  }

  /**
   * Creates the ThreeJS Renderer instances and sets it up for WebXR.
   */
  public createXRRenderer(): void {
    this.renderer = new WebGLRenderer({
      antialias: false,
      precision: XR_RENDERER_PRECISION,
      powerPreference: XR_POWER_PREFERENCE,
    });
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.outputEncoding = sRGBEncoding;
    this.renderer.xr.enabled = true;
  }

  /**
   * Starts rendering the scene in WebXR-mode by setting some last remaining
   * properties to the renderer and starting the render loop.
   * @param session
   */
  public async startXRRendering(session: XRSession): Promise<void> {
    if (!this.renderer || !this.scene || !this.camera) return;

    this.camera.matrixAutoUpdate = false;
    this.renderer.autoClear = false;
    // Try to get the coordinates of the users view for the camera
    this.camera = this.renderer.xr.getCamera();

    this.renderer.xr.setReferenceSpaceType(XR_REFERENCE_SPACE);
    await this.renderer.xr.setSession(session as any);

    this.setUpControllers(this.scene);

    this.renderer.setAnimationLoop(() => {
      if (!this.renderer || !this.scene || !this.camera) return;

      for (const controller of this._controllers) {
        this.intersectObjects(controller);
      }

      if (this._workspaceService.isLoading && this._configLoadDisplay) {
        this._configLoadDisplay.rotation.z =
          Date.now() * ROTATION_FACTOR_LOAD_DISPLAY; // Rotate the load display in a certain interval
      }

      this.renderer.render(this.scene, this.camera);
    });
  }

  /**
   * Ends the XR-rendering and cleans up all elements that were part of
   * that process to free up unused resources.
   */
  public endXRRendering(): void {
    if (!this.renderer) return;

    this.camera = undefined;
    this.scene?.clear();
    this.managers.splice(0);
    this.scene = undefined;
    this._geometryGroup.clear();
    this._scaleGroup.clear();
    this._configLoadDisplay = undefined;
    this._nextConfigButton = undefined;

    this.renderer.setAnimationLoop(null);
    this.cleanUpControllers();
  }

  public clearScene(): void {
    this.endXRRendering();
  }

  public cancelAnimation(): void {
    this.endXRRendering();
  }

  public ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  protected subscribeListeners(): void {
    this.subscribes.push(
      this._workspaceService.config$.subscribe(() => {
        if (!this.scene) return;

        this.updateScene();
      })
    );
  }

  /**
   * @override
   * Deals with the builders set up by the parent class and uses them
   * to construct the contents of the scene.
   * @param builders The builders used to create the separate parts of the model.
   * @protected
   */
  protected handleMultipleUseBuilders(builders: MultipleBuilderResult[]): void {
    for (const builder of builders.filter((b) => b.isActive)) {
      const modelBuilder = getBuilderInstance(
        builder,
        this._assetService.getTexture(THREE_JS_TEXTURE_NAME_BEAD),
        this._xrQualityLevel,
        this._viewService.linkerDNASplitter
      );
      if (modelBuilder instanceof CohesinHeadBuilder)
        modelBuilder.setHeadSize(this._cohesinService.getCohesinHeadRadius());

      modelBuilder.setUpFirstStyle(builder.colorClass.color);
      setConfigSpecificColors(
        modelBuilder,
        builder.colorClass.configSpecificColors
      );

      modelBuilder.setSpecificVisibility(this._viewService.invisibleIDs, false);
      this._geometryGroup.add(modelBuilder.mesh);
    }
  }

  /**
   * @override
   * Deals with remaining builders set up by the parent class that didn't fit
   * with the builders of the previous method.
   * @param currentConfig The configuration used to build elements of the scene.
   * @protected
   */
  protected handleOneTimeUseBuilders(currentConfig: Configuration): void {
    if (!currentConfig) return;

    for (const builder of BuilderGetter.getOneTimeBuilders(
      currentConfig,
      this._colorService,
      this._viewService,
      this._cohesinService,
      this._xrQualityLevel,
      this._assetService.getTexture(THREE_JS_TEXTURE_NAME_BEAD)
    ).filter(
      (b) => b instanceof InstancedObjectBuilder
    ) as InstancedObjectBuilder<any, any>[]) {
      let color = '';
      let configSpecificColors: ColorBead[] = [];
      if (builder instanceof LinkerDNAApproxBuilder) {
        color = this._colorService.linkerDNAColors.color;
        configSpecificColors =
          this._colorService.linkerDNAColors.configSpecificColors;
      } else {
        color = this._colorService.cohesinRingAndHingeColors.color;
        configSpecificColors =
          this._colorService.cohesinRingAndHingeColors.configSpecificColors;
      }
      builder.setUpFirstStyle(color);
      setConfigSpecificColors(builder, configSpecificColors);
      this._geometryGroup.add(builder.mesh);
      builder.setSpecificVisibility(this._viewService.invisibleIDs, false);
    }
  }

  protected render(): void {
    throw new Error('Method not implemented.');
  }

  /**
   * Creates the standard elements of the scene that remain unchanged
   * not matter what model is being displayed later on.
   * @protected
   */
  protected createDefaultScene(): Scene {
    this.unsubscribeListeners();
    this.subscribeListeners();

    const scene = new Scene();
    this.camera = new PerspectiveCamera();

    const ambientLight = new AmbientLight();
    scene.add(ambientLight, this._scaleGroup);

    return scene;
  }

  /**
   * First creation / setup of the config change button.
   * @private
   */
  private setUpConfigChangeButton(): void {
    if (
      this._nextConfigButton ||
      !this._workspaceService.trajectory ||
      this._workspaceService.trajectory.configurationLength < 2
    )
      return;

    const geometry = new BoxGeometry(
      XR_DISPLAY_CONF_CHANGE_BTN_DIM,
      XR_DISPLAY_CONF_CHANGE_BTN_DIM,
      0
    );
    const material = new MeshStandardMaterial({
      map: this._assetService.getTexture(
        THREE_JS_TEXTURE_NAME_BTN_XR_CONF_CHANGE
      ),
    });

    this._nextConfigButton = new Mesh(geometry, material);
  }

  /**
   * First creation / setup of the config change load display.
   * @private
   */
  private setUpConfigChangeLoadDisplay(): void {
    if (this._configLoadDisplay || !this._nextConfigButton) return;

    const geometry = new RingGeometry(
      XR_DISPLAY_LOAD_GEOM_DIM.innerRadius,
      XR_DISPLAY_LOAD_GEOM_DIM.outerRadius,
      XR_DISPLAY_LOAD_GEOM_DIM.thetaSegments,
      XR_DISPLAY_LOAD_GEOM_DIM.phiSegments,
      XR_DISPLAY_LOAD_GEOM_DIM.thetaStart,
      XR_DISPLAY_LOAD_GEOM_DIM.thetaLength
    );
    const material = new MeshStandardMaterial({
      color: new Color(XR_DISPLAY_LOAD_COLOR),
    });

    this._configLoadDisplay = new Mesh(geometry, material);
  }

  /**
   * Prepares the scene for the VR / AR experience, by moving the models closer to
   * the view point of the viewer and making sure, that the XR-Scene is disconnected
   * from the original viewer scene so that changes for XR do not affect the
   * actual viewer as well.
   *
   * @private
   */
  private prepareSceneForXR(): void {
    const fibreCenterCube =
      this._viewService.getContainerSizeOfPBCOrFiberWithCenterPoint();
    const camera: Camera = this.camera as Camera;
    const scaleVector: Vector3 =
      XRSceneHelper.calculateScaleFactor(fibreCenterCube);

    // Add config btn to the scene, if it is required
    if (
      this._nextConfigButton &&
      this._workspaceService.config!.index + 1 <
        this._workspaceService.trajectory!.configurationLength
    ) {
      this.adaptConfBtnToModel(fibreCenterCube, scaleVector);
      this._geometryGroup.add(this._nextConfigButton as Object3D);
    }

    if (!this._prevCoord) {
      // Move the group and its contents to the camera
      this._geometryGroup.position.sub(
        new Vector3(
          fibreCenterCube.centerX - camera.position.x,
          fibreCenterCube.centerY - camera.position.y,
          fibreCenterCube.centerZ - camera.position.z + XR_DEFAULT_MODEL_OFFSET
        )
      );
    } else {
      this.moveNewFibreToPreviousCoordinates(fibreCenterCube, scaleVector);
    }

    this._geometryGroup.updateMatrixWorld(true);
    this._geometryGroup.add(...this.managers.map((m) => m.getGroup()));

    this._scaleGroup.add(this._geometryGroup);
    // Since scaling the geometry group would have a big impact on the position of the model within it, instead a specific group is assigned for that purpose
    this._scaleGroup.scale.set(scaleVector.x, scaleVector.y, scaleVector.z);
  }

  /**
   * Builds the model out of the provided config for display.
   *
   * @param config The config off of which to build the model.
   * @private
   */
  private createOrRepositionModel(config: Configuration): void {
    if (!this.scene || !this._workspaceService.trajectory) return;

    const builders = BuilderGetter.getMultipleUseBuilders(
      config,
      this._colorService,
      this._xrQualityLevel
    );

    this.handleMultipleUseBuilders(builders);
    this.handleOneTimeUseBuilders(config);
  }

  /**
   * Code adapted from https://github.com/mrdoob/three.js/blob/master/examples/webxr_vr_dragging.html <br>
   * Set up the controllers for the XR session (these constructed controllers
   * only work in a VR-session and not an AR-session!) by creating the controllers,
   * providing event listeners etc.
   * @param xrScene The scene containing the objects to be displayed in the XR session.
   * @private
   */
  private setUpControllers(xrScene: Scene): void {
    if (!this.renderer) return;

    // Create a line as visual aid for the user to see what the controllers are aiming at
    const geometry = new BufferGeometry().setFromPoints([
      new Vector3(0, 0, 0),
      new Vector3(0, 0, -1),
    ]);

    const line = new Line(geometry);
    line.scale.z = XR_CONTROLLER_SELECTION_LINE_LENGTH;

    for (const controller of [
      this.renderer.xr.getController(0),
      this.renderer.xr.getController(1),
    ]) {
      controller.addEventListener('selectstart', this.onSelectStart.bind(this));
      controller.addEventListener('selectend', this.onSelectEnd.bind(this));
      const controllerLine = line.clone();
      controller.add(controllerLine);
      this._controllers.push({ group: controller, line: controllerLine });
    }

    const controllerModelFactory = new XRControllerModelFactory();
    const controllerGrips: Group[] = [];

    for (let i = 0; i < this._controllers.length; ++i) {
      const controllerGrip = this.renderer.xr.getControllerGrip(i);
      controllerGrips.push(controllerGrip);
      controllerGrip.add(
        controllerModelFactory.createControllerModel(controllerGrip)
      );
    }

    this._selectionRayCaster = new Raycaster();

    xrScene.add(
      ...this._controllers.map((controller) => controller.group),
      ...controllerGrips
    );
  }

  /**
   * Code adapted from https://github.com/mrdoob/three.js/blob/master/examples/webxr_vr_dragging.html <br>
   * React to the user grabbing an object via the controller.
   * @param event The event describing the grabbed object
   * @private
   */
  private onSelectStart(event: any): void {
    const controller = event.target;
    const intersections = this.getIntersections(controller);

    if (intersections.length < 1) return;

    if (
      intersections.find(
        (intersection) => intersection.object === this._nextConfigButton
      )
    ) {
      this.triggerConfigChange();
      return;
    }

    controller.attach(this._geometryGroup);
    controller.userData.selected = this._geometryGroup;
  }

  /**
   * Code adapted from https://github.com/mrdoob/three.js/blob/master/examples/webxr_vr_dragging.html <br>
   * React to the user letting go of an object.
   * @param event The event describing the formerly grabbed object and controller.
   * @private
   */
  private onSelectEnd(event: any): void {
    const controller = event.target;

    if (!controller.userData.selected) return;

    this._scaleGroup.attach(this._geometryGroup);

    controller.userData.selected = undefined;
  }

  /**
   * Code adapted from https://github.com/mrdoob/three.js/blob/master/examples/webxr_vr_dragging.html <br>
   * Determines the objects that have been intersected by the ray emanating
   * from the controller.
   * @param controller The controller for which to determine the intersections.
   * @private
   */
  private getIntersections(controller: Group): Intersection[] {
    if (!this._selectionRayCaster) return [];

    const tempMatrix: Matrix4 = new Matrix4();
    tempMatrix.identity().extractRotation(controller.matrixWorld);

    this._selectionRayCaster.ray.origin.setFromMatrixPosition(
      controller.matrixWorld
    );
    this._selectionRayCaster.ray.direction
      .set(0, 0, -1)
      .applyMatrix4(tempMatrix);

    return this._selectionRayCaster.intersectObjects(
      this._geometryGroup.children,
      false
    );
  }

  /**
   * Code adapted from https://github.com/mrdoob/three.js/blob/master/examples/webxr_vr_dragging.html <br>
   * This handles the visual representation of the ray emanating from the controller
   * "hitting" one of the objects belonging to the model, so that the user knows
   * that the object is grabbable.
   * @param controller The controller for which to handle the visible ray intersections.
   * @private
   */
  private intersectObjects(controller: Controller): void {
    if (controller.group.userData.selected) return;

    const intersections: Intersection[] = this.getIntersections(
      controller.group
    );

    if (intersections.length < 1) {
      controller.line.scale.z = XR_CONTROLLER_SELECTION_LINE_LENGTH;
      return;
    }

    const intersection: Intersection = intersections[0];
    controller.line.scale.z = intersection.distance;
  }

  /**
   * Cleans up every resource relating to the controllers in XR-mode.
   * @private
   */
  private cleanUpControllers(): void {
    this._controllers.splice(0);
  }

  /**
   * Remove the contents of the groups of the controllers, so that no item
   * remains "selected" by the controllers.
   * @private
   */
  private clearControllerGroups(): void {
    for (const controller of this._controllers) {
      // Remove every object except for the help lines!!!!
      controller.group.children.splice(0);
      controller.group.add(controller.line);
    }
  }

  /**
   * Triggers a change of the current config.
   * @private
   */
  private triggerConfigChange(): void {
    const nextConfigIndex = (this._workspaceService.config?.index ?? 0) + 1;

    if (
      this._workspaceService.isLoading ||
      !this._workspaceService.trajectory ||
      this._workspaceService.trajectory.configurationLength <= nextConfigIndex
    )
      return;

    // Reattach the model back to the scene itself (to prevent the models position
    // not being updated due to being stuck on the controller of a user at the moment of config change).
    if (
      !this._scaleGroup.children.find((child) => child === this._geometryGroup)
    ) {
      this._scaleGroup.attach(this._geometryGroup);
    }

    // Keep the information of the previous config
    this._prevCoord = {
      prevPosition: this._geometryGroup.position.clone(),
      prevCenter:
        this._viewService.getContainerSizeOfPBCOrFiberWithCenterPoint(),
      prevScale: this._scaleGroup.scale.x,
    };

    if (this._nextConfigButton) {
      this._geometryGroup.remove(this._nextConfigButton);
    }

    this.adaptConfLoadDisplayToModel();

    if (this._configLoadDisplay)
      this._geometryGroup.add(this._configLoadDisplay);

    this._workspaceService.setConfig(nextConfigIndex, false);
  }

  /**
   * Updates the xr scene by removing outdated displayed content
   * and introducing the current, new content.
   * @private
   */
  private updateScene(): void {
    if (!this._workspaceService.config) return;

    (<IConfigChangedListener[]>(
      (<unknown>this.managers.filter((m) => isConfigChangedListener(m)))
    )).forEach((m) => m.configChanged(this._workspaceService.config));

    this._scaleGroup.clear();
    this._geometryGroup.clear();
    this.clearControllerGroups();

    this.createOrRepositionModel(this._workspaceService.config.config);
    this.prepareSceneForXR();
    this._prevCoord = undefined;
  }

  /**
   * Adapt the button to the model. This means, that
   * the button is positioned, scaled etc. in accordance with the
   * current model and its properties.
   *
   * @param fibreContainerDim The container dimensions of the model.
   * @param scaleVector The determined final scale of the model.
   * @private
   */
  private adaptConfBtnToModel(
    fibreContainerDim: ContainerDimensionExtended,
    scaleVector: Vector3
  ): void {
    const position = new Vector3(
      fibreContainerDim.xStart - 1, // position the button a little left of the model
      fibreContainerDim.centerY,
      fibreContainerDim.zEnd + XR_DISPLAY_OFFSET_Z
    );

    this._nextConfigButton?.position.set(position.x, position.y, position.z);
    // Reverse the scaling so that the element remains its regular size
    this._nextConfigButton?.scale.set(
      1 / scaleVector.x,
      1 / scaleVector.y,
      1 / scaleVector.z
    );
    this._nextConfigButton?.updateMatrixWorld();
  }

  /**
   * Adapt the loading display for config changes to the model.
   * This means, that the display is positioned, scaled etc. in accordance with
   * the current model and its properties.
   * @private
   */
  private adaptConfLoadDisplayToModel(): void {
    if (!this._configLoadDisplay) return;

    const scaleFactor = this._scaleGroup.scale;
    // Adjust the scaling of the config load button to the scale set for the current model.
    this._configLoadDisplay.scale.set(
      XR_DISPLAY_LOAD_SCALE_FACTOR / scaleFactor.x,
      XR_DISPLAY_LOAD_SCALE_FACTOR / scaleFactor.y,
      XR_DISPLAY_LOAD_SCALE_FACTOR / scaleFactor.z
    );

    const fibreDim =
      this._viewService.getContainerSizeOfPBCOrFiberWithCenterPoint();

    this._configLoadDisplay?.position.set(
      fibreDim.centerX,
      fibreDim.centerY,
      fibreDim.zEnd + XR_DISPLAY_OFFSET_Z
    );

    this._nextConfigButton?.updateMatrixWorld();
  }

  /**
   * Moves the current fibre into the same position as the previous, if there
   * is one. This takes scaling into account and ensures, that the new fibre,
   * which might be more scaled down or less, still gets placed at the same
   * spot despite the scale-shift.
   *
   * @param fibreCenterCube The dimensions of the current fibre.
   * @param scaleVector The scale of the current fibre.
   * @private
   */
  private moveNewFibreToPreviousCoordinates(
    fibreCenterCube: ContainerDimensionExtended,
    scaleVector: Vector3
  ): void {
    if (!this._prevCoord) return;

    // Offset between the old dimensions of the fibre and the new ones
    // (Difference from previous to current fibre is calculated)
    const fibreCenterOffset = new Vector3(
      this._prevCoord.prevCenter.centerX - fibreCenterCube.centerX,
      this._prevCoord.prevCenter.centerY - fibreCenterCube.centerY,
      this._prevCoord.prevCenter.centerZ - fibreCenterCube.centerZ
    );
    const prevPosition = this._prevCoord.prevPosition.clone();
    // Offset the previous position by the calculated dimension offset, so that
    // the new fibre dimensions will be positioned in the same area
    prevPosition.add(fibreCenterOffset);

    // Factor in scaling so that the calculated coordinate offset is translated correctly
    // prevScale / currScale = diffScale between the two
    const counterScaling = this._prevCoord.prevScale / scaleVector.x;
    // Counter-Scale the previous coordinates so they remain at the same place
    prevPosition.multiplyScalar(counterScaling);

    this._geometryGroup.position.set(
      prevPosition.x,
      prevPosition.y,
      prevPosition.z
    );
  }
}
