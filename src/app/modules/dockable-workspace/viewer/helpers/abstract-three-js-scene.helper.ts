/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgZone } from '@angular/core';
import { AmbientLight, Camera, Light, Scene, WebGLRenderer } from 'three';
import { SubscribeEvents } from '../../../../core/classes/subscribe-events';
import SimpleManager from '../objects/threejs/managers/simple-manager';
import { AMBIENT_LIGHT_GLOSSY } from './constants';

export default abstract class AbstractThreeJsSceneHelper extends SubscribeEvents {
  protected canvas: HTMLCanvasElement | undefined;
  protected renderer: WebGLRenderer | undefined;
  protected scene: Scene | undefined;
  protected camera: Camera | undefined;
  protected frameId: number | null = null;

  protected readonly lights: Light[] = [];
  protected readonly managers: SimpleManager<any>[] = [];

  private readonly _ngZoneService?: NgZone;

  protected constructor(_ngZone?: NgZone) {
    super();

    this._ngZoneService = _ngZone;
  }

  public cancelAnimation(): void {
    if (this.frameId) cancelAnimationFrame(this.frameId);
  }

  public clearScene(): void {
    this.lights.splice(0);

    if (this.renderer) {
      this.renderer.dispose();
      this.renderer = undefined;
    }

    if (this.scene) {
      this.scene.clear();
      this.scene = undefined; // to prevent double drawing after close and reopen the tab (the service still exists as instance)
    }
  }

  public animate(): void {
    if (!this._ngZoneService) {
      console.warn('ngZone not passed!');
      return;
    }

    // We have to run this outside angular zones,
    // because it could trigger heavy changeDetection cycles.

    this._ngZoneService.runOutsideAngular(() => {
      if (document.readyState !== 'loading') {
        this.render();
      } else {
        window.addEventListener('DOMContentLoaded', () => {
          this.render();
        });
      }
    });
  }

  protected createDefaultScene(
    canvas: HTMLCanvasElement,
    width: number,
    height: number
  ): Scene {
    this.unsubscribeListeners();
    this.subscribeListeners();

    this.canvas = canvas;

    this.renderer = new WebGLRenderer({
      canvas: this.canvas,
      alpha: true, // transparent background
      antialias: true, // smooth edges
      preserveDrawingBuffer: true,
    });
    this.renderer.setSize(width, height);

    return new Scene();
  }

  protected createAmbientLight(intensity = AMBIENT_LIGHT_GLOSSY): void {
    if (!this.scene) return;

    const light = new AmbientLight(0xffffff, intensity);
    this.lights.push(light);
    this.scene.add(light);
  }

  protected addManager(manager: SimpleManager<any>): void {
    this.managers.push(manager);
    this.scene?.add(manager.getGroup());
  }

  protected removeManagers(managers: SimpleManager<any>[]): void {
    this.removeManagers2(...managers);
  }

  protected removeManagers2(...managers: SimpleManager<any>[]): void {
    this.managers.splice(
      0,
      this.managers.length,
      ...this.managers.filter((m) => !managers.includes(m))
    );

    managers.forEach((m) => this.scene?.remove(m.getGroup()));
  }

  protected clearManagers(): void {
    this.managers.forEach((m) => m.destroy());
    this.managers.splice(0);
  }

  protected abstract subscribeListeners(): void;
  protected abstract render(): void;
}
