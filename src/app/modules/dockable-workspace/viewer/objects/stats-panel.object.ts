/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable, OnDestroy } from '@angular/core';
import Stats from 'three/examples/jsm/libs/stats.module';
import { DebugService } from '../../../../core/services/debug.service';
import { SceneModule } from '../scene.module';
import { SubscribeEvents } from '../../../../core/classes/subscribe-events';

@Injectable({
  providedIn: SceneModule,
})
export class StatsPanel extends SubscribeEvents implements OnDestroy {
  public stats: Stats | undefined;

  public constructor(private readonly _debugService: DebugService) {
    super();
    this.subscribes.push(
      this._debugService.showStats$$.subscribe((isVis) => {
        if (isVis && !this.stats) this.createStats();
        else if (!isVis && this.stats) this.destroy();
      })
    );
  }

  public createStats(): void {
    this.stats = Stats();
    this.stats.showPanel(0);
    const doc = document.getElementById('stats');
    doc?.appendChild(this.stats.dom);
  }

  public destroy(): void {
    if (!this.stats) return;

    this.stats.domElement.parentElement?.removeChild(this.stats.domElement);
    this.stats.end();
    this.stats = undefined;

    /*
    const statsDoc = document.getElementById('stats');
    if (!statsDoc) return;

    for (let i = 0; i < statsDoc.childNodes.length; ++i) {
      statsDoc?.removeChild(statsDoc.childNodes[0]);
    }
     */
  }

  public ngOnDestroy(): void {
    this.unsubscribeListeners();
    this.destroy();
  }
}
