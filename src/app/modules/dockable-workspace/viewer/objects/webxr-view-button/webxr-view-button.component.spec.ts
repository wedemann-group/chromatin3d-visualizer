/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SCENE_DECLARATIONS } from '../../../../../core/services/__mocks__/imports';
import { WebXRViewButtonComponent } from './webxr-view-button.component';
import { WorkspaceService as MockWorkspaceService } from '../../../../../core/services/__mocks__/workspace.service';
import { WorkspaceService } from '../../../../../core/services/workspace.service';
import XRSceneHelper from '../../helpers/xr-scene.helper';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader';

describe('WebxrViewButtonComponent', () => {
  let component: WebXRViewButtonComponent;
  let fixture: ComponentFixture<WebXRViewButtonComponent>;

  beforeEach(async () => {
    spyOn(FontLoader.prototype, 'load');
    await TestBed.configureTestingModule({
      declarations: [WebXRViewButtonComponent],
      providers: [
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
        SCENE_DECLARATIONS,
        XRSceneHelper,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebXRViewButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
