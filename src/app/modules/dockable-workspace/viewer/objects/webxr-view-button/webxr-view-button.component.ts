/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { SceneHelper } from '../../helpers/scene.helper';
import { WorkspaceService } from '../../../../../core/services/workspace.service';
import XRSceneHelper from '../../helpers/xr-scene.helper';

const XR_NOT_SUFFICIENTLY_SUPPORTED =
  'Device does not support WebXR or Virtual Reality!';

@Component({
  selector: 'trj-webxr-view-button',
  templateUrl: './webxr-view-button.component.html',
  styleUrls: ['./webxr-view-button.component.scss'],
})
export class WebXRViewButtonComponent implements OnInit {
  private _currentSession: XRSession | undefined;

  private readonly _xr: XRSystem | undefined;
  @ViewChild('toggleXRButton')
  private _xrButton!: MatButton;
  private _xrSupported = false;

  public get xrSupported(): boolean {
    return this._xrSupported;
  }

  public get sessionActive(): boolean {
    return !!this._currentSession;
  }

  constructor(
    private readonly _sceneHelper: SceneHelper,
    private readonly _xrSceneHelper: XRSceneHelper,
    public readonly workspaceService: WorkspaceService
  ) {
    this._xr = navigator.xr;

    // Since it is possible that a headset connected to the pc, will be plugged in or unplugged, this status could change
    this._xr?.addEventListener(
      'devicechange',
      this.checkAndSetUpWebXR.bind(this)
    );
  }

  ngOnInit(): void {
    this.checkAndSetUpWebXR();
  }

  /**
   * Sets up the WebXR experience by checking whether the device of the user
   * supports a session of this type and by setting up the required tools to
   * enter into and display a WebXR session.
   */
  async checkAndSetUpWebXR(): Promise<void> {
    if (!this._xr || !(await this._xr.isSessionSupported('immersive-vr'))) {
      console.warn(XR_NOT_SUFFICIENTLY_SUPPORTED);
      this._xrSupported = false;
      return;
    }

    this._xrSceneHelper.createXRRenderer();
    this._xrSupported = true;
  }

  /**
   * Switch on the immersive XR session
   */
  async toggleImmersiveXR(): Promise<void> {
    if (this.sessionActive) {
      await this.triggerXRSessionEnd();
    } else {
      await this.startXRSession();
    }
  }

  private async triggerXRSessionEnd(): Promise<void> {
    await this._currentSession?.end();
  }

  private async startXRSession(): Promise<void> {
    if (!this._xr) return;
    this._currentSession = await this._xr.requestSession('immersive-vr', {
      optionalFeatures: ['hand-tracking', 'dom-overlay'],
      domOverlay: {
        root: this._xrButton._elementRef.nativeElement,
      },
    });

    await this.startSession();

    // React to the VR-Session ending and restore original settings
    this._currentSession.onend = this.endXRSession.bind(this);
  }

  /**
   * Starts the XR session by processing the scene of the viewer and
   * providing that changed scene to the required XR-components.
   * @private
   */
  private async startSession(): Promise<void> {
    if (!this._currentSession) return;

    this._sceneHelper.pauseScene();
    this._xrSceneHelper.buildXRScene();
    await this._xrSceneHelper.startXRRendering(this._currentSession);
  }

  /**
   * End the XR-Session and clean up all components created for the XR
   * session.
   * @private
   */
  private endXRSession(): void {
    this._currentSession = undefined;
    this._xrSceneHelper.endXRRendering();
    // Return to Non-VR-Rendering
    this._sceneHelper.resumeScene();
  }
}
