/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { DoubleSide, Mesh, MeshStandardMaterial, SphereGeometry } from 'three';
import ThreeJsObject from './three-js.object';
import { MeshStandardMaterialParameters } from 'three/src/materials/MeshStandardMaterial';

export default class SphereObject extends ThreeJsObject<
  SphereGeometry,
  MeshStandardMaterial
> {
  public readonly geometry: SphereGeometry;
  public readonly material: MeshStandardMaterial;
  public readonly mesh: Mesh;

  private readonly _widthSegments: number;
  private readonly _heightSegments: number;

  public constructor(
    widthSegments = 5,
    heightSegments = 5,
    parameters?: MeshStandardMaterialParameters
  ) {
    super();

    this._widthSegments = widthSegments;
    this._heightSegments = heightSegments;

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial(parameters);
    this.mesh = this.generateMesh();
  }

  protected generateGeometry(): SphereGeometry {
    return new SphereGeometry(1, this._widthSegments, this._heightSegments);
  }

  protected generateMaterial(
    parameters?: MeshStandardMaterialParameters
  ): MeshStandardMaterial {
    return new MeshStandardMaterial({
      transparent: true,
      color: 0x900000,
      side: DoubleSide,
      depthWrite: true,
      depthTest: true,
      wireframe: true,
      ...parameters,
    });
  }

  protected generateMesh(): Mesh {
    return new Mesh(this.geometry, this.material);
  }
}
