/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ICameraSynchronizer } from './managers/camera-synchronizer.interface';
import { IConfigChangedListener } from './managers/config-changed-listener.interface';
import { IQualityLevelListener } from './quality-level-listener.interface';
import { IServiceListener } from './service-listener.interface';

export function isCameraSynchronizer(com: any): com is ICameraSynchronizer {
  return (com as ICameraSynchronizer).cameraUpdated !== undefined;
}

export function isConfigChangedListener(
  com: any
): com is IConfigChangedListener {
  return (com as IConfigChangedListener).configChanged !== undefined;
}

export function isQualityLevelListener(com: any): com is IQualityLevelListener {
  return (com as IQualityLevelListener).qualityLevelChanged !== undefined;
}
export function isServiceListener(com: any): com is IServiceListener {
  return (com as IServiceListener).subscribeListeners !== undefined;
}
