/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { SubscribeEvents } from '../../../../../../core/classes/subscribe-events';
import { Group } from 'three';

export default abstract class SimpleManager<TObject> extends SubscribeEvents {
  protected readonly group: Group;
  protected readonly objects: TObject[] = [];

  protected constructor() {
    super();

    this.group = new Group();
  }

  public getGroup(): Group {
    return this.group;
  }

  public destroy(): void {
    this.unsubscribeListeners();
    this.group.clear();
  }

  protected abstract initGroupFromObjects(): void;
}
