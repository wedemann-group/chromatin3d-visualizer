/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { AmbientLight, DirectionalLight } from 'three';
import SimpleManager from '../simple-manager';
import {
  AMBIENT_LIGHT_GLOSSY,
  AMBIENT_LIGHT_WITHOUT_GLOSSY,
  DIRECTIONAL_LIGHT_GLOSSY,
  DIRECTIONAL_LIGHT_WITHOUT_GLOSSY,
} from '../../../../helpers/constants';
import { ViewService } from '../../../../../../../core/services/view/view.service';
import { IServiceListener } from '../../service-listener.interface';

const DIRECTIONAL_LIGHT_TARGET_NAME = 'Directional Light Target';

export default class FiberLightsManager
  extends SimpleManager<DirectionalLight | AmbientLight>
  implements IServiceListener
{
  public constructor(private readonly _viewService: ViewService) {
    super();

    this.createSceneLights();
    this.subscribeListeners();
    this.initGroupFromObjects();
  }

  public subscribeListeners(): void {
    this.subscribes.push(
      this._viewService.rerender$.subscribe(() => {
        // while close/open an open it is possible that the threejs scene doesnt exit in the moment, so ignore the event. the scene will create in "createScene"
        // this condition is not strictly necessary here, since this is checked at the latest when is drawing. However, this routine resolves other routines. Therefore it makes sense to abort as soon as possible.
        this.repositionLights();
      }),
      this._viewService.glossyMaterial$$.subscribe((isGlossyMaterial) =>
        this.changeLightIntensities(isGlossyMaterial)
      )
    );
  }

  public unsubscribeListeners(): void {
    super.unsubscribeListeners();
  }

  protected initGroupFromObjects(): void {
    this.group.add(...this.objects);
  }

  private createSceneLights(): void {
    // soft white light
    const directionLight1 = new DirectionalLight(
      0xffffff,
      this._viewService.glossyMaterial$$.value
        ? DIRECTIONAL_LIGHT_GLOSSY
        : DIRECTIONAL_LIGHT_WITHOUT_GLOSSY
    );
    directionLight1.castShadow = true;
    const directionLight2 = new DirectionalLight(
      0xffffff,
      this._viewService.glossyMaterial$$.value
        ? DIRECTIONAL_LIGHT_GLOSSY
        : DIRECTIONAL_LIGHT_WITHOUT_GLOSSY
    );
    directionLight2.castShadow = true;
    this.objects.push(directionLight1, directionLight2);

    for (const light of <DirectionalLight[]>(
      this.objects.filter((o) => o instanceof DirectionalLight)
    )) {
      light.target.name = DIRECTIONAL_LIGHT_TARGET_NAME;
      this.group.add(light);
      this.group.add(light.target);
    }

    this.createAmbientLight();

    this.repositionLights();
  }

  private createAmbientLight(): void {
    const light = new AmbientLight(
      0xffffff,
      this._viewService.glossyMaterial$$.value
        ? AMBIENT_LIGHT_GLOSSY
        : AMBIENT_LIGHT_WITHOUT_GLOSSY
    );
    this.objects.push(light);
    this.group.add(light);
  }

  private repositionLights(): void {
    const directionalLights = <DirectionalLight[]>(
      this.objects.filter((l) => l instanceof DirectionalLight)
    );
    if (directionalLights.length < 2) return;

    const info =
      this._viewService.getContainerSizeOfPBCOrFiberWithCenterPoint();

    directionalLights[0].position.set(info.xStart, info.yStart, info.zStart);
    directionalLights[0].target.position.set(
      info.centerX,
      info.centerY,
      info.centerZ
    );
    directionalLights[1].position.set(info.xEnd, info.yEnd, info.zEnd);
    directionalLights[1].target.position.set(
      info.centerX,
      info.centerY,
      info.centerZ
    );
  }

  private changeLightIntensities(isGlossyMaterial: boolean): void {
    for (const light of this.objects) {
      if (light instanceof DirectionalLight)
        light.intensity = isGlossyMaterial
          ? DIRECTIONAL_LIGHT_GLOSSY
          : DIRECTIONAL_LIGHT_WITHOUT_GLOSSY;
      else
        light.intensity = isGlossyMaterial
          ? AMBIENT_LIGHT_GLOSSY
          : AMBIENT_LIGHT_WITHOUT_GLOSSY;
    }
  }
}
