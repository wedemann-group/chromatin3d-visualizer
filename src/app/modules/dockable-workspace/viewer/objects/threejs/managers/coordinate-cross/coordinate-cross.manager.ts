/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  Camera,
  CylinderGeometry,
  MeshPhongMaterial,
  MeshStandardMaterial,
  SphereGeometry,
  TubeGeometry,
  Vector3,
} from 'three';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import { Font } from 'three/examples/jsm/loaders/FontLoader';
import CoordinateArrowObject from './coordinate-arrow.object';
import { ViewService } from '../../../../../../../core/services/view/view.service';
import { ColorsService } from '../../../../../../../core/services/colors/colors.service';
import {
  AXIS_COLOR_X,
  AXIS_COLOR_Y,
  AXIS_COLOR_Z,
  AXIS_LABEL_POSITION,
  AXIS_X,
  AXIS_Y,
  AXIS_Z,
  SCALE_SPHERE,
} from './constants';
import CoordinateArrowSpireObject from './coordinate-arrow-spire.object';
import Manager from '../manager';
import TextObject from '../../text.object';
import SphereObject from '../../sphere.object';
import { IServiceListener } from '../../service-listener.interface';

const DISTANCE_TO_CAMERA_FACTOR = 0.2;

export default class CoordinateCrossManager
  extends Manager<
    | CoordinateArrowObject
    | CoordinateArrowSpireObject
    | TextObject
    | SphereObject,
    TubeGeometry | CylinderGeometry | TextGeometry | SphereGeometry,
    MeshStandardMaterial | MeshPhongMaterial
  >
  implements IServiceListener
{
  public constructor(
    font: Font,
    private readonly _camera: Camera,
    private readonly _viewService: ViewService,
    private readonly _colorsService: ColorsService
  ) {
    super();

    this.objects.push(
      // arrow and arrow spire x
      new CoordinateArrowObject(AXIS_X, AXIS_COLOR_X),
      new CoordinateArrowSpireObject(
        AXIS_X,
        AXIS_COLOR_X,
        new Vector3(90, 90, 0)
      ),
      // arrow and arrow spire y
      new CoordinateArrowObject(AXIS_Y, AXIS_COLOR_Y),
      new CoordinateArrowSpireObject(
        AXIS_Y,
        AXIS_COLOR_Y,
        new Vector3(90, 90, 90)
      ),
      // arrow and arrow spire z
      new CoordinateArrowObject(AXIS_Z, AXIS_COLOR_Z),
      new CoordinateArrowSpireObject(
        AXIS_Z,
        AXIS_COLOR_Z,
        new Vector3(90, 0, 90)
      ),
      new TextObject(font, new Vector3(AXIS_LABEL_POSITION, 0, 0), 'X'),
      new TextObject(font, new Vector3(0, AXIS_LABEL_POSITION, 0), 'Y'),
      new TextObject(font, new Vector3(0, 0, AXIS_LABEL_POSITION), 'Z'),
      new SphereObject(10, 10, {
        transparent: true,
        color: 0x939393,
        depthWrite: true,
        depthTest: true,
        wireframe: false,
      })
    );

    (<TextObject[]>this.objects.filter((o) => o instanceof TextObject)).forEach(
      (o) => {
        o.geometry.center();
      }
    );

    (<SphereObject[]>(
      this.objects.filter((o) => o instanceof SphereObject)
    )).forEach((o) => {
      o.setScaleFromXYZ(SCALE_SPHERE, SCALE_SPHERE, SCALE_SPHERE);
    });

    this.subscribeListeners();

    this.group.scale.set(0.45, 0.45, 0.45);

    this.initGroupFromObjects();
  }

  public subscribeListeners(): void {
    this.subscribes.push(
      this._viewService.cameraRotation$$.subscribe((euler) => {
        this.group.rotation.set(euler.x, euler.y, euler.z, euler.order);

        (<TextObject[]>(
          this.objects.filter((o) => o instanceof TextObject)
        )).forEach((o) => {
          o.setRotationToCameraPosition(this._camera.position);

          const vecToCamera = o.startPosition
            .clone()
            .add(
              this._camera.position
                .clone()
                .sub(o.startPosition)
                .normalize()
                .multiplyScalar(DISTANCE_TO_CAMERA_FACTOR)
            );
          o.mesh.position.copy(vecToCamera);
        });
      })
    );

    this.subscribes.push(
      this._colorsService.backColor.color$.subscribe((col) => {
        (<TextObject[]>(
          this.objects.filter((o) => o instanceof TextObject)
        )).forEach((t) => t.setReadableForeColorToBackColor(col));
      })
    );
  }

  public unsubscribeListeners(): void {
    super.unsubscribeListeners();
  }
}
