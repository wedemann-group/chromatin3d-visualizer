/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { BufferGeometry, Group, Material, Mesh, ShaderMaterial } from 'three';
import { Geometry } from 'three/examples/jsm/deprecated/Geometry';
import { Object3D } from 'three/src/core/Object3D';
import ThreeJsObject from '../three-js.object';
import SimpleManager from './simple-manager';

export default abstract class Manager<
  TObject extends ThreeJsObject<TGeometry, TMaterial, TMesh>,
  TGeometry extends Geometry | BufferGeometry = BufferGeometry,
  TMaterial extends Material = ShaderMaterial,
  TMesh extends Mesh | Object3D = Mesh
> extends SimpleManager<TObject> {
  protected readonly group: Group;
  protected readonly objects: TObject[] = [];

  protected constructor() {
    super();

    this.group = new Group();
  }

  protected initGroupFromObjects(): void {
    for (const obj of this.objects) this.group.add(obj.mesh);
  }
}
