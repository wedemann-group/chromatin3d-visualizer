/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ColorRepresentation, DoubleSide, Vector3 } from 'three';
import TubeObject from '../../tube.object';
import SimpleLineCurve from '../../../simple-line.curve';
import { RADIUS_SEGMENTS, THICKNESS, TUBULAR_SEGMENTS } from './constants';

export default class CoordinateArrowObject extends TubeObject {
  public constructor(orientation: Vector3, color: ColorRepresentation) {
    super(
      {
        path: new SimpleLineCurve(orientation),
        tubularSegments: TUBULAR_SEGMENTS,
        radius: THICKNESS,
        radiusSegments: RADIUS_SEGMENTS,
        closed: false,
      },
      {
        transparent: true,
        color: color,
        side: DoubleSide,
        depthWrite: true,
        depthTest: true,
        wireframe: false,
      }
    );
  }
}
