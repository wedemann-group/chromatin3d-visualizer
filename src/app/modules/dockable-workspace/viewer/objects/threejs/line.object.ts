/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Color, Vector3 } from 'three';
import { Line2 } from 'three/examples/jsm/lines/Line2';
import { LineGeometry } from 'three/examples/jsm/lines/LineGeometry';
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial';
import ThreeJsObject from './three-js.object';
import {
  colorToArgb,
  rgbToHex,
} from '../../../../../shared/utilities/color.utility';
import { convert2DArrayTo1D } from '../../../../../shared/utilities/array.utility';

export default class LineObject extends ThreeJsObject<
  LineGeometry,
  LineMaterial,
  Line2
> {
  public readonly geometry: LineGeometry;
  public readonly material: LineMaterial;
  public readonly mesh: Line2;

  private _linePoints: Vector3[];
  private _lineColor: string;
  private _lineWidth: number;

  public constructor(
    linePoints: Vector3[],
    lineColor: string,
    lineWidth: number
  ) {
    super();

    this._linePoints = linePoints;
    this._lineColor = lineColor;
    this._lineWidth = lineWidth;

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial();
    this.mesh = this.generateMesh();
  }

  public changeLineWidth(newWidth: number): void {
    this._lineWidth = newWidth;
    this.material.linewidth = newWidth;
    this.material.needsUpdate = true;
  }

  protected generateGeometry(): LineGeometry {
    const geometry = new LineGeometry();
    geometry.setPositions(
      convert2DArrayTo1D(this._linePoints.map((p) => [p.x, p.y, p.z]))
    );

    return geometry;
  }

  protected generateMaterial(): LineMaterial {
    const colorComponents = colorToArgb(this._lineColor);
    return new LineMaterial({
      color: new Color(rgbToHex(colorComponents)).getHex(), // remove alpha because threejs doesn't support alpha
      opacity: colorComponents.a,
      visible: colorComponents.a > 0,
      transparent: true,
      vertexColors: false,
      linewidth: this._lineWidth,
      worldUnits: true,
    });
  }

  protected generateMesh(): Line2 {
    const line = new Line2(this.geometry, this.material);
    line.computeLineDistances();

    return line;
  }
}
