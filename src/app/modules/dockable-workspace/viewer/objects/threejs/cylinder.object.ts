/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { CylinderGeometry, Mesh, MeshStandardMaterial } from 'three';
import { MeshStandardMaterialParameters } from 'three/src/materials/MeshStandardMaterial';
import ThreeJsObject from './three-js.object';

type CylinderParameters = {
  radiusTop: number;
  radiusBottom: number;
  height: number;
  radialSegments: number;
  heightSegments: number;
  openEnded: boolean;
  thetaStart?: number;
  thetaLength?: number;
};

export default class CylinderObject extends ThreeJsObject<
  CylinderGeometry,
  MeshStandardMaterial
> {
  public readonly geometry: CylinderGeometry;
  public readonly material: MeshStandardMaterial;
  public readonly mesh: Mesh;

  private readonly _radiusTop: number;
  private readonly _radiusBottom: number;
  private readonly _height: number;
  private readonly _radialSegments: number;
  private readonly _heightSegments: number;
  private readonly _openEnded: boolean;
  private readonly _thetaStart?: number;
  private readonly _thetaLength?: number;

  public constructor(
    parameters: CylinderParameters,
    materialParams?: MeshStandardMaterialParameters
  ) {
    super();

    this._radiusTop = parameters.radiusTop;
    this._radiusBottom = parameters.radiusBottom;
    this._height = parameters.height;
    this._radialSegments = parameters.radialSegments;
    this._heightSegments = parameters.heightSegments;
    this._openEnded = parameters.openEnded;
    this._thetaStart = parameters.thetaStart;
    this._thetaLength = parameters.thetaLength;

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial(materialParams);
    this.mesh = this.generateMesh();
  }

  protected generateGeometry(): CylinderGeometry {
    return new CylinderGeometry(
      this._radiusTop,
      this._radiusBottom,
      this._height,
      this._radialSegments,
      this._heightSegments,
      this._openEnded,
      this._thetaStart,
      this._thetaLength
    );
  }

  protected generateMaterial(
    parameters?: MeshStandardMaterialParameters
  ): MeshStandardMaterial {
    return new MeshStandardMaterial({
      color: 0x900000,
      ...parameters,
    });
  }

  protected generateMesh(): Mesh {
    return new Mesh(this.geometry, this.material);
  }
}
