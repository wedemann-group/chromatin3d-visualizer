/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Box3, MeshPhongMaterial, Quaternion, Vector3 } from 'three';
import { Mesh } from 'three/src/objects/Mesh';
import { Font } from 'three/examples/jsm/loaders/FontLoader';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import { colorToArgb } from '../../../../../shared/utilities/color.utility';
import ThreeJsObject from './three-js.object';

/**
 * @source https://stackoverflow.com/a/3943023
 */
const FORE_COLOR_DETECTION_MULTIPLIER_RED = 0.299;

/**
 * @source https://stackoverflow.com/a/3943023
 */
const FORE_COLOR_DETECTION_MULTIPLIER_GREEN = 0.587;

/**
 * @source https://stackoverflow.com/a/3943023
 */
const FORE_COLOR_DETECTION_MULTIPLIER_BLUE = 0.114;

/**
 * @source https://stackoverflow.com/a/3943023
 */
const FORE_COLOR_DETECTION_SUM = 186;

export default class TextObject extends ThreeJsObject<
  TextGeometry,
  MeshPhongMaterial
> {
  public readonly geometry: TextGeometry;
  public readonly material: MeshPhongMaterial;
  public readonly mesh: Mesh;

  private readonly _font: Font;
  private readonly _position: Vector3;
  private readonly _text: string;

  public get startPosition(): Vector3 {
    return this._position;
  }

  public constructor(font: Font, position: Vector3, text: string) {
    super();

    this._font = font;
    this._position = position;
    this._text = text;

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial();
    this.mesh = this.generateMesh();
  }

  /**
   *
   * @param backColor
   * @private
   * @source https://stackoverflow.com/a/3943023
   */
  private static textColor(backColor: string): string {
    const rgb = colorToArgb(backColor);
    if (
      rgb.r * FORE_COLOR_DETECTION_MULTIPLIER_RED +
        rgb.g * FORE_COLOR_DETECTION_MULTIPLIER_GREEN +
        rgb.b * FORE_COLOR_DETECTION_MULTIPLIER_BLUE >
      FORE_COLOR_DETECTION_SUM
    )
      return '#000000';

    return '#ffffff';
  }

  public setReadableForeColorToBackColor(backColor: string): void {
    this.material.color.set(TextObject.textColor(backColor));
  }

  public setRotationToCameraPosition(cameraPos: Vector3): void {
    this.mesh.lookAt(cameraPos.x, cameraPos.y, cameraPos.z);
  }

  public setRotationFromCameraQuaternion(cameraQuat: Quaternion): void {
    this.mesh.quaternion.copy(cameraQuat);
  }

  public getBoundingBox(): Box3 | null {
    this.geometry.computeBoundingBox();
    return this.geometry.boundingBox;
  }

  protected generateGeometry(): TextGeometry {
    return new TextGeometry(this._text, {
      font: this._font,
      size: 1,
      height: 1,
      curveSegments: 12,
    });
  }

  protected generateMaterial(): MeshPhongMaterial {
    return new MeshPhongMaterial({
      transparent: true,
      color: 0x000000,
      depthWrite: true,
      depthTest: true,
      wireframe: false,
    });
  }

  protected generateMesh(): Mesh {
    const mesh = new Mesh(this.geometry, this.material);
    mesh.position.set(this._position.x, this._position.y, this._position.z);
    return mesh;
  }
}
