/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { InstancedMesh, ShaderMaterial, TubeGeometry, Vector3 } from 'three';
import DoubleHelixNucleosomeDNABuilder from './double-helix-nucleosome-dna.builder';
import { DNA_THICKNESS } from './models.variables';
import { convertBufferAttrToVector3List } from '../../../../shared/utilities/three.js.utility';

const MESH_NAME = 'Double Helix Counter Nucleosome DNA-Mesh';

export default class DoubleHelixCounterNucleosomeDNABuilder extends DoubleHelixNucleosomeDNABuilder {
  public getDoubleStrandPoints(): Vector3[] {
    const result: Vector3[] = [];

    for (const bead of this.beads) {
      const geometry =
        this.qualityLevel.doubleHelixNucleosomeDNACounterGeometry.clone();
      geometry.applyMatrix4(
        DoubleHelixNucleosomeDNABuilder.setRotation(
          bead,
          this.tmpScratchObject3D
        ).matrix
      );
      const vertices = convertBufferAttrToVector3List(
        geometry.getAttribute('position')
      );
      result.push(...vertices);
    }

    return result;
  }

  public get isCounterPart(): boolean {
    return false;
  }

  protected generateGeometry(): TubeGeometry {
    const geometry = new TubeGeometry(
      this.qualityLevel.doubleHelixCounterNucleosomeDNA,
      this.qualityLevel.nucleosomeDNATubularSegments,
      DNA_THICKNESS,
      this.qualityLevel.nucleosomeDNARadiusSegments,
      false
    );

    this.addAttributesToGeometry(geometry);
    return geometry;
  }

  protected generateMesh(): InstancedMesh<TubeGeometry, ShaderMaterial> {
    const mesh = super.generateMesh();
    mesh.name = MESH_NAME;
    return mesh;
  }
}
