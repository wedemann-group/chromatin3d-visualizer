/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ColorBead } from '../../../../core/services/colors/models/color-bead.model';

export interface IChangeableColors {
  changeDefaultColor(color: string, indicesOfOwnColor: number[]): void;
  setConfigSpecificColors(indices: number[], color: string): void;
  setConfigSpecificColorsFromColorBeads(colorBeads: ColorBead[]): void;
}
