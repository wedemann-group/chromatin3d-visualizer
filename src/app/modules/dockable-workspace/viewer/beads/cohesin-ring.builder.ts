/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  InstancedMesh,
  Object3D,
  ShaderMaterial,
  Texture,
  TorusGeometry,
  Vector3,
} from 'three';
import InstancedObjectBuilder from './instanced-object.builder';
import { DEFAULT_COLOR_COHESIN_RING } from '../../../../shared/color.constants';
import { CohesinRing } from '../../../../shared/models/cohesin-ring.model';
import { convertDegreeToRadians } from '../../../../shared/utilities/three.js.utility';
import { EMaterialType } from '../../../../shared/models/material-type.enum';
import { ICohesinRingData } from '../../../../core/services/session-mananger/models/cohesin-ring-data.interface';
import { COHESIN_RING_ARC } from './models.variables';
import {
  BEAD_USER_NAME_COHESIN_PREFIX,
  BEAD_USER_NAME_COHESIN_RING,
} from '../../../../constants';

const MAGIC_ROTATION_NUMBER = 5; // must be odd!!
const ROTATION_PROVISION_OFFSET = 10; // can be also 1 because it builds a vector but it must be positive!
const MESH_NAME = 'Cohesin Ring-Mesh';

export default class CohesinRingBuilder extends InstancedObjectBuilder<
  CohesinRing,
  ICohesinRingData,
  TorusGeometry,
  ShaderMaterial
> {
  public readonly geometry: TorusGeometry;
  public readonly material: ShaderMaterial;
  public readonly mesh: InstancedMesh<TorusGeometry, ShaderMaterial>;

  private readonly _thickness: number;
  private readonly _radius: number;

  protected readonly defaultColor = DEFAULT_COLOR_COHESIN_RING;

  public constructor(
    rings: CohesinRing[],
    thickness: number,
    radius: number,
    texture: Texture,
    qualityLevel: ICohesinRingData
  ) {
    super(rings, texture, qualityLevel);

    this._thickness = thickness;
    this._radius = radius;

    this.geometry = this.generateGeometry();
    this.material = this.generateShaderMaterial();
    this.mesh = this.generateMesh();

    this.changeableMaterials.push(
      { type: EMaterialType.NORMAL, material: this.material },
      {
        type: EMaterialType.WITH_TEXTURE,
        material: this.generateShaderMaterialWithTexture(),
      }
    );
  }

  private static getRotationToHinge(
    ring: CohesinRing,
    ringUpVector: Vector3
  ): number {
    const torusOpenerPos = new Object3D();
    torusOpenerPos.position.set(
      ring.centerPos.x,
      ring.centerPos.y,
      ring.centerPos.z
    );
    torusOpenerPos.lookAt(ringUpVector);
    torusOpenerPos.translateY(ROTATION_PROVISION_OFFSET);
    torusOpenerPos.updateMatrix();

    const currentDirection = new Vector3(
      torusOpenerPos.position.x - ring.centerPos.x,
      torusOpenerPos.position.y - ring.centerPos.y,
      torusOpenerPos.position.z - ring.centerPos.z
    ).normalize();
    const shouldDirection = new Vector3(
      ring.hingePosition.x - ring.centerPos.x,
      ring.hingePosition.y - ring.centerPos.y,
      ring.hingePosition.z - ring.centerPos.z
    ).normalize();

    let angleDirection = currentDirection.angleTo(shouldDirection);

    // find out if rotation negative or positive
    torusOpenerPos.translateY(-ROTATION_PROVISION_OFFSET);
    torusOpenerPos.rotateZ(convertDegreeToRadians(MAGIC_ROTATION_NUMBER));
    torusOpenerPos.translateY(ROTATION_PROVISION_OFFSET);
    torusOpenerPos.updateMatrix();
    const currentDirectionRotated = new Vector3(
      torusOpenerPos.position.x - ring.centerPos.x,
      torusOpenerPos.position.y - ring.centerPos.y,
      torusOpenerPos.position.z - ring.centerPos.z
    ).normalize();
    const angleDirection2 = currentDirectionRotated.angleTo(shouldDirection);
    if (angleDirection < angleDirection2) angleDirection *= -1;

    let arcWidth = (2 * Math.PI - COHESIN_RING_ARC) / 2;

    angleDirection += arcWidth;
    return angleDirection;
  }

  public getSelectionTexts(): string[] {
    return this.getSelectionInfoWithDynamicTextGetter(
      (i) =>
        `${BEAD_USER_NAME_COHESIN_PREFIX} ${BEAD_USER_NAME_COHESIN_RING} ${i}`
    );
  }

  // eslint-disable-next-line
  public setSpecificVisibility(ids: number[], isVis: boolean): void {
    console.log('TO DO!');
  }

  protected generateGeometry(): TorusGeometry {
    const geometry = new TorusGeometry(
      this._radius,
      this._thickness,
      this.qualityLevel.cohesinRingRadialSegments,
      this.qualityLevel.cohesinRingTubularSegments,
      COHESIN_RING_ARC
    );

    this.addAttributesToGeometry(geometry);
    return geometry;
  }

  protected generateMesh(): InstancedMesh<TorusGeometry, ShaderMaterial> {
    const mesh = super.generateMesh();
    mesh.name = MESH_NAME;
    return mesh;
  }

  protected setSingleInstance(index: number): void {
    const ring = this.beads[index];
    this.tmpScratchObject3D.clear();
    this.tmpScratchObject3D.position.set(
      ring.centerPos.x,
      ring.centerPos.y,
      ring.centerPos.z
    );

    const ringUpVector = new Vector3(
      ring.centerPos.x + ring.orientationVector.x,
      ring.centerPos.y + ring.orientationVector.y,
      ring.centerPos.z + ring.orientationVector.z
    );
    this.tmpScratchObject3D.lookAt(ringUpVector);
    // this.tmpScratchObject3D.rotateX(convertDegreeToRadians(90));
    this.tmpScratchObject3D.rotateZ(convertDegreeToRadians(90));

    this.tmpScratchObject3D.rotateZ(
      CohesinRingBuilder.getRotationToHinge(ring, ringUpVector)
    );

    this.tmpScratchObject3D.updateMatrix();
    this.mesh.setMatrixAt(index, this.tmpScratchObject3D.matrix);
  }
}
