/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { BufferAttribute, BufferGeometry, Vector3 } from 'three';
import QualityLevel from '../../../../core/services/session-mananger/models/quality-level.model';
import {
  HISTONE_OCTAMER_RADIUS,
  NUCLEOSOME_DNA_ROUNDS,
  NUCLEOSOME_DNA_THICKNESS,
} from './models.variables';
import { convertVectorArrayToFloat32Array } from '../../../../shared/utilities/three.js.utility';

/**
 * Specifies a magic number to close the gap between histone and nucleosome dna.
 * The linker DNA (DNA strand) calls {@link getNucleosomeDNAGeometry} to get the anchor points.
 * Due to the tube algorithm (three.js) it arises a small gap between histone and DNA (see ticket #52).
 * To fix this gap, the invisible nucleosome dna anchor point starts a few degrees before the real (visible) nucleosome dna.
 */
const NUCLEOSOME_DNA_CLOSER_START = -0.05;

/**
 * Specifies a magic number to close the gap between histone and nucleosome dna.
 * The linker DNA (DNA strand) calls {@link getNucleosomeDNAGeometry} to get the anchor points.
 * Due to the tube algorithm (three.js) it arises a small gap between histone and DNA (see ticket #52).
 * To fix this gap, the invisible nucleosome dna anchor point ends a few degrees after the real (visible) nucleosome dna.
 */
const NUCLEOSOME_DNA_CLOSER_END = 0.02;

/**
 *
 * @param qualityLevel
 * @param expanded Specifies whether the nucleosome dna should be starts and ends some degrees before the "real" nucleosome/histone. This value fixes the gap bug (ticket #52) and should NOT use to display a nucleosome.
 */
export const getNucleosomeDNAGeometry = (
  { nucleosomeDNATubularSegments }: QualityLevel,
  expanded = false
): BufferGeometry => {
  const startDegree = 0 + (expanded ? NUCLEOSOME_DNA_CLOSER_START : 0);
  const endDegree = 2 * Math.PI + (expanded ? NUCLEOSOME_DNA_CLOSER_END : 0);
  const degreePerPoint =
    (endDegree - startDegree) / nucleosomeDNATubularSegments;
  let currentDegree = startDegree;

  const radius = HISTONE_OCTAMER_RADIUS + NUCLEOSOME_DNA_THICKNESS;

  const amountOfPoints = nucleosomeDNATubularSegments * NUCLEOSOME_DNA_ROUNDS;
  const vertices: Vector3[] = [];
  for (let i = 0; i < amountOfPoints; ++i) {
    vertices.push(
      new Vector3(
        radius * Math.cos(currentDegree),
        ((NUCLEOSOME_DNA_THICKNESS * NUCLEOSOME_DNA_ROUNDS) / endDegree) *
          currentDegree,
        radius * Math.sin(currentDegree)
      )
    );
    currentDegree += degreePerPoint;
  }

  const geometry = new BufferGeometry();
  geometry.setAttribute(
    'position',
    new BufferAttribute(
      new Float32Array(convertVectorArrayToFloat32Array(vertices)),
      3
    )
  );
  return geometry;
};

export function getNucleosomeDNAPoints(
  { nucleosomeDNATubularSegments }: QualityLevel,
  height: number,
  expanded = false,
  rounds = NUCLEOSOME_DNA_ROUNDS
): Vector3[] {
  const startDegree = 0 + (expanded ? NUCLEOSOME_DNA_CLOSER_START : 0);
  const endDegree = 2 * Math.PI + (expanded ? NUCLEOSOME_DNA_CLOSER_END : 0);
  const degreePerPoint =
    (endDegree - startDegree) / nucleosomeDNATubularSegments;
  let currentDegree = startDegree;

  const radius = 3.5;
  const amountOfPoints = nucleosomeDNATubularSegments * rounds;
  const stepHeight = height / amountOfPoints;

  const result: Vector3[] = [];
  for (let i = 0; i < amountOfPoints; ++i) {
    result.push(
      new Vector3(
        radius * Math.cos(currentDegree),
        stepHeight * i,
        radius * Math.sin(currentDegree)
      )
    );
    currentDegree += degreePerPoint;
  }

  return result;
}
