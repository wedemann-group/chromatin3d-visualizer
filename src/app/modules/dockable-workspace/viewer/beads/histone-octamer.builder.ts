/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  CylinderGeometry,
  InstancedMesh,
  ShaderMaterial,
  Texture,
} from 'three';
import InstancedBeadBuilder from './instanced-bead.builder';
import SoftwareNucleosomeBead from '../../../../shared/models/sw-nucleosome-bead.model';
import { DEFAULT_COLOR_HISTONE_OCTAMER } from '../../../../shared/color.constants';
import { EMaterialType } from '../../../../shared/models/material-type.enum';
import { IHistoneOctamerData } from '../../../../core/services/session-mananger/models/histone-octamer-data.interface';
import {
  HISTONE_OCTAMER_RADIUS,
  HISTONE_OCTAMER_SIZE,
} from './models.variables';
import { BEAD_USER_NAME_HISTONE_OCTAMER } from '../../../../constants';

const MESH_NAME = 'Histone-Octamer-Mesh';

export default class HistoneOctamerBuilder extends InstancedBeadBuilder<
  SoftwareNucleosomeBead,
  IHistoneOctamerData,
  CylinderGeometry,
  ShaderMaterial
> {
  public geometry: CylinderGeometry;
  public material: ShaderMaterial;
  public mesh: InstancedMesh<CylinderGeometry, ShaderMaterial>;

  protected defaultColor = DEFAULT_COLOR_HISTONE_OCTAMER;

  public constructor(
    nucleosomes: SoftwareNucleosomeBead[],
    texture: Texture,
    qualityLevel: IHistoneOctamerData,
    renderSize = -1
  ) {
    super(nucleosomes, texture, qualityLevel, renderSize);

    this.geometry = this.generateGeometry();
    this.material = this.generateShaderMaterial();
    this.mesh = this.generateMesh();

    this.changeableMaterials.push(
      { type: EMaterialType.NORMAL, material: this.material },
      {
        type: EMaterialType.WITH_TEXTURE,
        material: this.generateShaderMaterialWithTexture(),
      }
    );
  }

  public getSelectionTexts(): string[] {
    return this.getSelectionInfosFromBeads(
      this.beads,
      BEAD_USER_NAME_HISTONE_OCTAMER
    );
  }

  protected generateGeometry(): CylinderGeometry {
    const geometry = new CylinderGeometry(
      HISTONE_OCTAMER_RADIUS,
      HISTONE_OCTAMER_RADIUS,
      HISTONE_OCTAMER_SIZE,
      this.qualityLevel.histoneOctamerRadialSegments
    );

    this.addAttributesToGeometry(geometry);
    return geometry;
  }

  protected generateMesh(): InstancedMesh<CylinderGeometry, ShaderMaterial> {
    const mesh = super.generateMesh();
    mesh.renderOrder = 1;
    mesh.name = MESH_NAME;
    return mesh;
  }

  protected setSingleInstance(index: number): void {
    const nucleosome = this.beads[index];
    this.tmpScratchObject3D.clear();
    this.tmpScratchObject3D.position.set(
      nucleosome.drawPosition.x,
      nucleosome.drawPosition.y,
      nucleosome.drawPosition.z
    );
    this.tmpScratchObject3D.setRotationFromQuaternion(nucleosome.rotation);
    this.tmpScratchObject3D.updateMatrix();
    this.mesh.setMatrixAt(index, this.tmpScratchObject3D.matrix);
  }
}
