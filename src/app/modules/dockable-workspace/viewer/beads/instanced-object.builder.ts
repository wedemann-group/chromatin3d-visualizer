/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  BufferGeometry,
  Color,
  DoubleSide,
  InstancedBufferAttribute,
  InstancedMesh,
  Material,
  Matrix3,
  MeshPhongMaterial,
  Object3D,
  ShaderLib,
  ShaderMaterial,
  Texture,
  UniformsUtils,
} from 'three';
import {
  aOptionalRGBToHex,
  colorToArgb,
  rgbToHex,
} from '../../../../shared/utilities/color.utility';
import { IChangeableColors } from './changeable-colors.interface';
import { IChangeableVisibility } from './changeable-visibility.interface';
import {
  MATERIAL_ATTRIBUTE_NAME_COLOR,
  MATERIAL_ATTRIBUTE_NAME_SELECTED,
  MATERIAL_ATTRIBUTE_NAME_VIS,
  MATERIAL_COLOR_LENGTH,
  MATERIAL_REFLECTIVITY,
  MATERIAL_SHININESS,
  MATERIAL_SPECULAR,
} from './models.variables';
import { VERTEX_PHONG_SHADER } from '../shaders/invisible-selection-color.shader.vertex';
import { FRAGMENT_PHONG_SHADER } from '../shaders/invisible-selection-color.shader.fragment';
import { BEAD_TEXTURE_REPEAT } from '../../../../core/services/assets/constants';
import { EMaterialType } from '../../../../shared/models/material-type.enum';
import { IChangeableMaterial } from './changeable-material.interface';
import { ColorBead } from '../../../../core/services/colors/models/color-bead.model';
import { PossibleQualitySegments } from '../../../../shared/models/possible-quality-segments.model';
import { DEFAULT_COLOR_SELECTION } from '../../../../shared/color.constants';
import { ISelectableInstancedMesh } from './selectable-instanced-mesh.interface';
import Bead from '../../../../shared/models/bead.model';

export default abstract class InstancedObjectBuilder<
  TBead,
  TQuality extends PossibleQualitySegments,
  TGeometry extends BufferGeometry = BufferGeometry,
  TMaterial extends Material = MeshPhongMaterial
> implements
    IChangeableColors,
    IChangeableVisibility,
    IChangeableMaterial,
    ISelectableInstancedMesh
{
  public readonly renderSize: number;

  public abstract readonly geometry: TGeometry;
  public abstract readonly material: TMaterial;
  public abstract readonly mesh: InstancedMesh<TGeometry, TMaterial>;

  protected bufferAttrColors: InstancedBufferAttribute | undefined;
  protected bufferAttrInvisible: InstancedBufferAttribute | undefined;
  protected bufferAttrSelected: InstancedBufferAttribute | undefined;
  protected readonly tmpScratchObject3D = new Object3D();
  protected abstract readonly defaultColor: string;
  protected beads: TBead[];

  protected readonly qualityLevel: TQuality;
  protected readonly changeableMaterials: {
    type: EMaterialType;
    material: TMaterial;
  }[] = [];

  private readonly _texture: Texture;

  protected constructor(
    beads: TBead[],
    texture: Texture,
    qualityLevel: TQuality,
    renderSize = -1
  ) {
    this.beads = beads;
    this._texture = texture;
    this.qualityLevel = qualityLevel;
    if (renderSize > 0) this.renderSize = renderSize;
    else this.renderSize = beads.length;
  }

  public changeDefaultColor(
    color: string,
    indicesOfOwnColor: number[] = []
  ): void {
    const allIndices = new Array(this.renderSize)
      .fill(0)
      .map((_, index) => index);
    const changeColorsOf = allIndices.filter(
      (indexVal) => !indicesOfOwnColor.includes(indexVal)
    );
    this.setConfigSpecificColors(changeColorsOf, color);
  }

  public setConfigSpecificColors(indices: number[], color: string): void {
    if (!this.bufferAttrColors) return;

    const castedColor = colorToArgb(color);
    color = rgbToHex(castedColor); // remove alpha because threejs doesn't support alpha
    const newColor = new Color(color);

    for (const index of indices) {
      // set color (rgb)
      newColor.toArray(
        this.bufferAttrColors.array,
        index * MATERIAL_COLOR_LENGTH
      );
      // set alpha
      this.bufferAttrColors.setW(index, castedColor.a);
    }

    this.bufferAttrColors.needsUpdate = true;
  }

  public setConfigSpecificColorsFromColorBeads(colorBeads: ColorBead[]): void {
    colorBeads.forEach((c) => this.setConfigSpecificColors([c.index], c.color));
  }

  public reposition(beads: TBead[]): void {
    this.beads = beads;
    this.mesh.count = beads.length;

    beads.forEach((_, index) => this.setSingleInstance(index));

    this.mesh.instanceMatrix.needsUpdate = true;
  }

  public setUpFirstStyle(defaultColor: string): void {
    if (!defaultColor) defaultColor = this.defaultColor;
    if (!this.bufferAttrColors) throw new Error('Invalid buffer colors!');
    if (!this.bufferAttrInvisible) throw new Error('Invalid buffer colors!');

    for (let i = 0; i < this.beads.length; ++i) {
      this.setSingleInstance(i);

      this.setConfigSpecificColors([i], defaultColor);
    }

    this.mesh.instanceMatrix.needsUpdate = true;
    this.bufferAttrColors.needsUpdate = true;
  }

  public changeMaterialTo(to: EMaterialType): void {
    const newMaterial = this.changeableMaterials.find((m) => m.type === to);
    if (newMaterial) this.mesh.material = newMaterial.material;
  }

  public setSelectionColor(color: string): void {
    const col = new Color(
      aOptionalRGBToHex({ ...colorToArgb(color), a: undefined })
    );

    for (const material of this.changeableMaterials) {
      if (!(material.material instanceof ShaderMaterial)) continue;

      material.material.uniforms.selectionColor.value = col;
    }
  }

  public toggleSelection(instanceID: number): void {
    const selectionAttr = this.geometry.getAttribute(
      MATERIAL_ATTRIBUTE_NAME_SELECTED
    );
    const isAlreadySelected = selectionAttr.getX(instanceID) === 1;
    selectionAttr.setX(instanceID, isAlreadySelected ? 0 : 1);
    selectionAttr.needsUpdate = true;
  }

  public clearSelection(): void {
    const selectionAttr = this.geometry.getAttribute(
      MATERIAL_ATTRIBUTE_NAME_SELECTED
    );
    for (let i = 0; i < selectionAttr.count; ++i) {
      selectionAttr.setX(i, 0);
    }

    selectionAttr.needsUpdate = true;
  }

  public destroy(): void {
    this.geometry.dispose();
  }

  protected generateShaderMaterial(): ShaderMaterial {
    const customUniforms = UniformsUtils.merge([
      ShaderLib.phong.uniforms,
      {
        // unlike the predefined shaders, you can't just take the attribute name and define the value behind it
        // for reference, see the setting of the default values in the source code:
        // https://github.com/mrdoob/three.js/blob/r68/src/renderers/shaders/ShaderLib.js#L595
        shininess: { type: 'f', value: MATERIAL_SHININESS },
        specular: { type: 'c', value: new Color(MATERIAL_SPECULAR) },
        reflectivity: { type: 'f', value: MATERIAL_REFLECTIVITY },
        selectionColor: {
          type: 'c',
          value: new Color(DEFAULT_COLOR_SELECTION),
        },
      },
    ]);
    return new ShaderMaterial({
      uniforms: customUniforms,
      vertexShader: VERTEX_PHONG_SHADER,
      fragmentShader: FRAGMENT_PHONG_SHADER,
      lights: true,
      vertexColors: true,
      transparent: true,
      side: DoubleSide,
    });
  }

  protected generateShaderMaterialWithTexture(): ShaderMaterial {
    const customUniforms = UniformsUtils.merge([
      ShaderLib.phong.uniforms,
      {
        // unlike the predefined shaders, you can't just take the attribute name and define the value behind it
        // for reference, see the setting of the default values in the source code:
        // https://github.com/mrdoob/three.js/blob/r68/src/renderers/shaders/ShaderLib.js#L595
        shininess: { type: 'f', value: MATERIAL_SHININESS },
        specular: { type: 'c', value: new Color(MATERIAL_SPECULAR) },
        reflectivity: { type: 'f', value: MATERIAL_REFLECTIVITY },
        selectionColor: {
          type: 'c',
          value: new Color(DEFAULT_COLOR_SELECTION),
        },
        map: { value: this._texture },
        uvTransform: {
          value: new Matrix3().set(
            BEAD_TEXTURE_REPEAT,
            0,
            0,
            0,
            BEAD_TEXTURE_REPEAT,
            0,
            0,
            0,
            BEAD_TEXTURE_REPEAT
          ),
        },
      },
    ]);
    return new ShaderMaterial({
      uniforms: customUniforms,
      defines: { USE_UV: '', USE_MAP: '' },
      vertexShader: VERTEX_PHONG_SHADER,
      fragmentShader: FRAGMENT_PHONG_SHADER,
      lights: true,
      vertexColors: true,
      transparent: true,
      side: DoubleSide,
    });
  }

  protected generateMesh(): InstancedMesh<TGeometry, TMaterial> {
    return new InstancedMesh(this.geometry, this.material, this.renderSize);
  }

  protected addAttributesToGeometry(geometry: BufferGeometry): void {
    this.bufferAttrColors = new InstancedBufferAttribute(
      new Float32Array(this.renderSize * MATERIAL_COLOR_LENGTH),
      MATERIAL_COLOR_LENGTH
    );
    this.bufferAttrInvisible = new InstancedBufferAttribute(
      new Int32Array(this.renderSize),
      1
    );
    this.bufferAttrSelected = new InstancedBufferAttribute(
      new Int32Array(this.renderSize),
      1
    );
    geometry.setAttribute(MATERIAL_ATTRIBUTE_NAME_COLOR, this.bufferAttrColors);
    geometry.setAttribute(
      MATERIAL_ATTRIBUTE_NAME_VIS,
      this.bufferAttrInvisible
    );
    geometry.setAttribute(
      MATERIAL_ATTRIBUTE_NAME_SELECTED,
      this.bufferAttrSelected
    );
  }

  protected getSelectionInfosFromBeads(
    beads: Bead[],
    beadIndexPrefix: string
  ): string[] {
    return this.getSelectionInfoWithDynamicTextGetter(
      (i) => `${beadIndexPrefix} Index: ${i} (Bead ID: ${beads[i].id})`
    );
  }

  protected getSelectionInfoWithDynamicTextGetter(
    getText: (index: number) => string
  ): string[] {
    const result: string[] = [];
    const attr = this.geometry.getAttribute(MATERIAL_ATTRIBUTE_NAME_SELECTED);
    for (let i = 0; i < attr.count; ++i) {
      const selectionVal = attr.getX(i);
      if (selectionVal === 0.0) continue;

      result.push(getText(i));
    }

    return result;
  }

  public abstract getSelectionTexts(): string[];
  public abstract setSpecificVisibility(ids: number[], isVis: boolean): void;

  protected abstract setSingleInstance(index: number): void;
}
