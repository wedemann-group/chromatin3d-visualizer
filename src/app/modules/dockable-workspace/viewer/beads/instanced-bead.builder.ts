/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { BufferGeometry, Material, MeshPhongMaterial, Texture } from 'three';
import IBead from '../../../../shared/models/bead.interface';
import InstancedModelBuilder from './instanced-model.builder';
import { PossibleQualitySegments } from '../../../../shared/models/possible-quality-segments.model';

export default abstract class InstancedBeadBuilder<
  TBead extends IBead | { id: number },
  TQuality extends PossibleQualitySegments,
  TGeometry extends BufferGeometry = BufferGeometry,
  TMaterial extends Material = MeshPhongMaterial
> extends InstancedModelBuilder<TBead, TQuality, TGeometry, TMaterial> {
  protected constructor(
    beads: TBead[],
    texture: Texture,
    qualityLevel: TQuality,
    renderSize = -1
  ) {
    super(beads, texture, qualityLevel, renderSize);
  }

  protected setSpecificVisibilityFromList(ids: number[], isVis: boolean): void {
    if (!this.bufferAttrInvisible) return;

    const sortedBeads = this.beads
      .map((b, index) => ({ index, id: b.id }))
      .sort((i1, i2) => i1.id - i2.id);

    let visibleIDIndex = 0;
    let sortedBeadIndex = 0;

    while (
      visibleIDIndex < ids.length &&
      sortedBeadIndex < sortedBeads.length
    ) {
      const visID = ids[visibleIDIndex];
      const sortedID = sortedBeads[sortedBeadIndex];
      if (visID === sortedID.id) {
        this.bufferAttrInvisible.setX(sortedID.index, isVis ? 0 : 1);
        ++visibleIDIndex;
        ++sortedBeadIndex;
      } else if (visID < sortedID.id) {
        ++visibleIDIndex;
      } else {
        ++sortedBeadIndex;
        this.bufferAttrInvisible.setX(sortedID.index, isVis ? 1 : 0);
      }
    }
  }
}
