/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  convertDegreeToRadians,
  DEGREE_MAX_VALUE,
  DEGREE_ONE_QUARTER,
} from '../../../../shared/utilities/three.js.utility';

export const BASE_PAIR_RENDER_SIZE_INIT = 100000;

export const DOUBLE_HELIX_LENGTH_OF_ROUND = 12;
/** Specifies how many points are to be used per turn. This parameter is not adjustable, because smaller numbers make the model very inaccurate. */
export const DOUBLE_HELIX_POINTS_PER_ROUND = 8;
/** Specifies how large the radius of the DNA strand is. The larger the number, the larger the strand is "pulled outward". */
export const DOUBLE_HELIX_RADIUS = 0.75;
/** Specifies the amount of rounds of the strand of DNA. */
export const NUCLEOSOME_DNA_ROUNDS = 1.675; // actually 1.675 but the old trj viewer looks like more than 1.675, so expand this value...
/** Specifies how long it takes to make a complete turn. The smaller the number, the more turns there are in a strand. */
export const COHESIN_RING_ARC = 6.185;

// --------------------------------------------------------------------- //
// - These variables do NOT have an influence on the amount of polygons - //
// --------------------------------------------------------------------- //
export const BASE_PAIR_THICKNESS = 0.1;
export const COHESIN_HINGE_RADIUS = 1.0;
export const COHESIN_HINGE_TUBE = 0.4;
export const DOUBLE_HELIX_DEGREES_PER_STEP =
  (2 * Math.PI) / DOUBLE_HELIX_POINTS_PER_ROUND;
/** Specifies the height of the strand of DNA that turns around a histone octamer. The variable specifies the stowage. */
export const NUCLEOSOME_DNA_HEIGHT = 3.5;

export const NUCLEOSOME_DNA_OFFSET = 1.5;
/** Specifies the radius of the strand of DNA. */
export const NUCLEOSOME_DNA_RADIUS = 3.5;
export const NUCLEOSOME_DNA_ROTATION_Z = convertDegreeToRadians(
  DEGREE_MAX_VALUE * NUCLEOSOME_DNA_ROUNDS -
    DEGREE_MAX_VALUE * Math.floor(NUCLEOSOME_DNA_ROUNDS) +
    DEGREE_ONE_QUARTER
); // + 90 (DEGREE_ONE_QUARTER) because three.js rotate -90 in the "wrong" direction;

/** ! Specifies the thickness of the strand of DNA. */
export const NUCLEOSOME_DNA_THICKNESS = 1;

export const NUCLEOSOME_DNA_THICKNESS_REDUCER = 0.25;

/** ! Specifies the thickness of the strand of DNA. */
export const DNA_THICKNESS = 0.25;

export const MATERIAL_SHININESS = 11;
export const MATERIAL_REFLECTIVITY = 1;
export const MATERIAL_SPECULAR = 0xababab;

/**
 * Defines the length of an entry in a whole array for a color.
 */
export const MATERIAL_COLOR_LENGTH = 4; // len 4 because "a" (alpha) is included since Aug 2021 (!!)

export const MATERIAL_ATTRIBUTE_NAME_COLOR = 'attrColor';
export const MATERIAL_ATTRIBUTE_NAME_VIS = 'attrVis';
export const MATERIAL_ATTRIBUTE_NAME_SELECTED = 'attrSel';

/** Specifies the radius of an approximated histone group. */
export const HISTONE_OCTAMER_RADIUS =
  NUCLEOSOME_DNA_RADIUS - NUCLEOSOME_DNA_THICKNESS;

/** Specifies the height of an approximated histone group. */
export const HISTONE_OCTAMER_SIZE = 5;
