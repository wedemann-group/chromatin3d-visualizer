/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import LinkerDNACurveBuilder from './linker-dna-curve.builder';
import { Vector3 } from 'three';

export default class LinkerDNACurveApproxBuilder extends LinkerDNACurveBuilder {
  protected getFirstPointsOfNuc(vertices: Vector3[]): Vector3[] {
    return [vertices[0]];
  }

  protected getLastPointsOfNuc(vertices: Vector3[]): Vector3[] {
    return [vertices[vertices.length - 1]];
  }
}
