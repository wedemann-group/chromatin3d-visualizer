/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { BufferGeometry, Color, Material, Mesh } from 'three';
import {
  colorToArgb,
  rgbToHex,
} from '../../../../shared/utilities/color.utility';
import { IChangeableColors } from './changeable-colors.interface';
import { IChangeableVisibility } from './changeable-visibility.interface';
import { RGBAColor } from '../../../../shared/models/rgba-color.model';
import { ColorBead } from '../../../../core/services/colors/models/color-bead.model';
import { PossibleQualitySegments } from '../../../../shared/models/possible-quality-segments.model';
import { ISelectableGroupMesh } from './selectable-group-mesh.interface';

export default abstract class MultipleMeshBeadBuilder<
  TBead,
  TQuality extends PossibleQualitySegments,
  TGeometry extends BufferGeometry,
  TMaterial extends Material & { color?: Color }
> implements IChangeableColors, IChangeableVisibility, ISelectableGroupMesh
{
  protected readonly beads: TBead[];
  protected qualityLevel: TQuality;

  public readonly geometries: TGeometry[] = [];
  public readonly materials: TMaterial[] = [];
  public readonly meshes: Mesh[] = [];

  protected constructor(beads: TBead[], qualityLevel: TQuality) {
    this.beads = beads;
    this.qualityLevel = qualityLevel;
  }

  public changeDefaultColor(
    color: string,
    indicesOfOwnColor: number[] = []
  ): void {
    const colorComponents = colorToArgb(color);
    const colorWithoutAlpha = rgbToHex(colorComponents); // remove alpha

    for (let i = 0; i < this.materials.length; ++i) {
      // change only color of beads which doesn't have an own color
      this.changeVisibilityOpacityAndColorFromIndex(
        i,
        colorComponents,
        colorWithoutAlpha,
        !indicesOfOwnColor.includes(i)
      );
    }
  }

  public setConfigSpecificColors(indices: number[], color: string): void {
    const colorComponents = colorToArgb(color);
    const colorWithoutAlpha = rgbToHex(colorComponents); // remove alpha

    for (const index of indices) {
      if (index >= this.materials.length) continue;

      this.changeVisibilityOpacityAndColorFromIndex(
        index,
        colorComponents,
        colorWithoutAlpha,
        true
      );
    }
  }

  public setConfigSpecificColorsFromColorBeads(colorBeads: ColorBead[]): void {
    colorBeads.forEach((c) => this.setConfigSpecificColors([c.index], c.color));
  }

  public destroy(): void {
    for (const geo of this.geometries) {
      geo.dispose();
    }
  }

  protected changeVisibilityOpacityAndColorFromIndex(
    index: number,
    colorComponents: RGBAColor,
    colorWithoutAlpha: string,
    changeColor: boolean = false
  ): void {
    const material = this.materials[index];
    if (changeColor && material.color) material.color.set(colorWithoutAlpha);
    material.opacity = colorComponents.a;
    material.visible = this.isVisible(index, material);
    material.needsUpdate = true;
  }

  protected isVisible(index: number, material: TMaterial): boolean {
    return material.opacity > 0;
  }

  public abstract setSpecificVisibility(ids: number[], isVis: boolean): void;
  public abstract toggleSelection(mesh: Mesh): void;
  public abstract getSelectionTexts(): string[];
  public abstract clearSelection(): void;
  public abstract setSelectionColor(col: string): void;
}
