/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  NUCLEOSOME_DNA_OFFSET,
  NUCLEOSOME_DNA_ROTATION_Z,
  NUCLEOSOME_DNA_THICKNESS,
} from './models.variables';
import {
  InstancedMesh,
  Object3D,
  ShaderMaterial,
  Texture,
  TubeGeometry,
} from 'three';
import InstancedBeadBuilder from './instanced-bead.builder';
import SoftwareNucleosomeBead from '../../../../shared/models/sw-nucleosome-bead.model';
import { DEFAULT_COLOR_NUCLEOSOME_DNA } from '../../../../shared/color.constants';
import { EMaterialType } from '../../../../shared/models/material-type.enum';
import { INucleosomeDNAData } from '../../../../core/services/session-mananger/models/nucleosome-dna-data.interface';
import { BEAD_USER_NAME_NUCLEOSOME_DNA } from '../../../../constants';

const MESH_NAME = 'Nucleosome DNA-Mesh';

export default class NucleosomeDNABuilder extends InstancedBeadBuilder<
  SoftwareNucleosomeBead,
  INucleosomeDNAData,
  TubeGeometry,
  ShaderMaterial
> {
  public readonly geometry: TubeGeometry;
  public readonly material: ShaderMaterial;
  public readonly mesh: InstancedMesh<TubeGeometry, ShaderMaterial>;

  protected defaultColor = DEFAULT_COLOR_NUCLEOSOME_DNA;

  public constructor(
    nucleosomes: SoftwareNucleosomeBead[],
    texture: Texture,
    qualityLevel: INucleosomeDNAData,
    renderSize = -1
  ) {
    super(nucleosomes, texture, qualityLevel, renderSize);

    this.geometry = this.generateGeometry();
    this.material = this.generateShaderMaterial();
    this.mesh = this.generateMesh();

    this.changeableMaterials.push(
      { type: EMaterialType.NORMAL, material: this.material },
      {
        type: EMaterialType.WITH_TEXTURE,
        material: this.generateShaderMaterialWithTexture(),
      }
    );
  }

  public static setRotation(
    nuc: SoftwareNucleosomeBead,
    tmpScratchObject3D: Object3D
  ): Object3D {
    tmpScratchObject3D.clear();
    tmpScratchObject3D.position.set(
      nuc.drawPosition.x,
      nuc.drawPosition.y,
      nuc.drawPosition.z
    );
    tmpScratchObject3D.setRotationFromQuaternion(nuc.rotation);
    tmpScratchObject3D.rotateY(NUCLEOSOME_DNA_ROTATION_Z);
    tmpScratchObject3D.translateY(-NUCLEOSOME_DNA_OFFSET);
    tmpScratchObject3D.updateMatrix();

    return tmpScratchObject3D;
  }

  public getSelectionTexts(): string[] {
    return this.getSelectionInfosFromBeads(
      this.beads,
      BEAD_USER_NAME_NUCLEOSOME_DNA
    );
  }

  protected generateGeometry(): TubeGeometry {
    const geometry = new TubeGeometry(
      this.qualityLevel.normalNucleosomeDNACurve,
      this.qualityLevel.nucleosomeDNATubularSegments,
      NUCLEOSOME_DNA_THICKNESS,
      this.qualityLevel.nucleosomeDNARadiusSegments,
      false
    );

    this.addAttributesToGeometry(geometry);
    return geometry;
  }

  protected generateMesh(): InstancedMesh<TubeGeometry, ShaderMaterial> {
    const mesh = super.generateMesh();
    mesh.renderOrder = 2;
    mesh.name = MESH_NAME;
    return mesh;
  }

  protected setSingleInstance(index: number): void {
    const nucleosome = this.beads[index];
    this.mesh.setMatrixAt(
      index,
      NucleosomeDNABuilder.setRotation(nucleosome, this.tmpScratchObject3D)
        .matrix
    );
  }
}
