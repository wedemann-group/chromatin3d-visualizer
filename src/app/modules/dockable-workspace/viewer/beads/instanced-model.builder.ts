/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import InstancedObjectBuilder from './instanced-object.builder';
import { BufferGeometry, Material, MeshPhongMaterial, Texture } from 'three';
import { PossibleQualitySegments } from '../../../../shared/models/possible-quality-segments.model';

export default abstract class InstancedModelBuilder<
  TBead,
  TQuality extends PossibleQualitySegments,
  TGeometry extends BufferGeometry = BufferGeometry,
  TMaterial extends Material = MeshPhongMaterial
> extends InstancedObjectBuilder<TBead, TQuality, TGeometry, TMaterial> {
  protected constructor(
    beads: TBead[],
    texture: Texture,
    qualityLevel: TQuality,
    renderSize = -1
  ) {
    super(beads, texture, qualityLevel, renderSize);
  }

  public reposition(beads: any[]): void {
    super.reposition(beads);
  }

  public setSpecificVisibility(ids: number[], isVis: boolean): void {
    if (!this.bufferAttrInvisible) return;

    if (ids.length === 0) this.setSpecificVisibilityFromEmptyList(isVis);
    else this.setSpecificVisibilityFromList(ids, isVis);

    this.bufferAttrInvisible.needsUpdate = true;
  }

  protected setSpecificVisibilityFromEmptyList(isVis: boolean): void {
    if (!this.bufferAttrInvisible) return;

    for (let index = 0; index < this.beads.length; ++index)
      this.bufferAttrInvisible.setX(index, isVis ? 1 : 0);
  }

  protected abstract setSpecificVisibilityFromList(
    ids: number[],
    isVis: boolean
  ): void;
}
