/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { DNA_THICKNESS } from './models.variables';
import { InstancedMesh, ShaderMaterial, TubeGeometry, Vector3 } from 'three';
import { IDoubleStrand } from './double-strand.interface';
import NucleosomeDNABuilder from './nucleosome-dna.builder';
import { convertBufferAttrToVector3List } from '../../../../shared/utilities/three.js.utility';
import { BEAD_USER_NAME_NUCLEOSOME_DNA } from '../../../../constants';

const MESH_NAME = 'Double Helix Nucleosome DNA-Mesh';

export default class DoubleHelixNucleosomeDNABuilder
  extends NucleosomeDNABuilder
  implements IDoubleStrand
{
  public getDoubleStrandPoints(): Vector3[] {
    const result: Vector3[] = [];

    for (const bead of this.beads) {
      const geometry =
        this.qualityLevel.doubleHelixNucleosomeDNAGeometry.clone();
      geometry.applyMatrix4(
        DoubleHelixNucleosomeDNABuilder.setRotation(
          bead,
          this.tmpScratchObject3D
        ).matrix
      );
      const vertices = convertBufferAttrToVector3List(
        geometry.getAttribute('position')
      );
      result.push(...vertices);
    }

    return result;
  }

  public getSelectionTexts(): string[] {
    if (this.isCounterPart)
      return this.getSelectionInfosFromBeads(
        this.beads,
        `${BEAD_USER_NAME_NUCLEOSOME_DNA} (counter part)`
      );

    return super.getSelectionTexts();
  }

  public get isCounterPart(): boolean {
    return true;
  }

  protected generateGeometry(): TubeGeometry {
    const geometry = new TubeGeometry(
      this.qualityLevel.doubleHelixNucleosomeDNA,
      this.qualityLevel.nucleosomeDNATubularSegments,
      DNA_THICKNESS,
      this.qualityLevel.nucleosomeDNARadiusSegments,
      false
    );

    this.addAttributesToGeometry(geometry);
    return geometry;
  }

  protected generateMesh(): InstancedMesh<TubeGeometry, ShaderMaterial> {
    const mesh = super.generateMesh();
    mesh.name = MESH_NAME;
    return mesh;
  }
}
