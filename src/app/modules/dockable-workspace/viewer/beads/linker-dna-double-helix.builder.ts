/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  CatmullRomCurve3,
  Mesh,
  ShaderMaterial,
  Texture,
  TubeGeometry,
  UniformsUtils,
  Vector3,
} from 'three';
import { DNA_THICKNESS } from './models.variables';
import Bead from '../../../../shared/models/bead.model';
import { DEFAULT_COLOR_LINKER_DNA } from '../../../../shared/color.constants';
import { EMaterialType } from '../../../../shared/models/material-type.enum';
import { INucleosomeDNAData } from '../../../../core/services/session-mananger/models/nucleosome-dna-data.interface';
import { Curve } from 'three/src/extras/core/Curve';
import DoubleHelixCurve from './double-helix.curve';
import DoubleHelixNucleosomeDNABuilder from './double-helix-nucleosome-dna.builder';
import SoftwareNucleosomeBead from '../../../../shared/models/sw-nucleosome-bead.model';
import { IDoubleStrand } from './double-strand.interface';
import { convertBufferAttrToVector3 } from '../../../../shared/utilities/three.js.utility';
import { BEAD_USER_NAME_LINKER_DNA } from '../../../../constants';
import LinkerDNAMultipleMaterialsBuilder from './linker-dna-multiple-materials.builder';

const MESH_NAME = 'Linker DNA Double Helix-Mesh';

export default class LinkerDNADoubleHelixBuilder
  extends LinkerDNAMultipleMaterialsBuilder<
    Bead,
    INucleosomeDNAData,
    TubeGeometry,
    ShaderMaterial
  >
  implements IDoubleStrand
{
  private readonly _texture: Texture;
  private readonly _startDegree: number;
  private readonly _dnaSpiralPoints: Vector3[] = [];

  public constructor(
    beads: Bead[],
    qualityLevel: INucleosomeDNAData,
    color: string,
    splicer: number[],
    texture: Texture,
    startDegree = 0
  ) {
    super(beads, qualityLevel, splicer);
    this._texture = texture;
    this._startDegree = startDegree;

    if (!color) color = DEFAULT_COLOR_LINKER_DNA;

    this.calculateCurves();
    this.generateMeshes(color);
  }

  public getSelectionTexts(): string[] {
    if (this.isCounterPart)
      return this.getSelectionsWithCustomPrefix(
        `${BEAD_USER_NAME_LINKER_DNA} (counter part)`
      );

    return super.getSelectionTexts();
  }

  public getDoubleStrandPoints(): Vector3[] {
    return this._dnaSpiralPoints;
  }

  public get isCounterPart(): boolean {
    return this._startDegree === 0;
  }

  public changeMaterialTo(to: EMaterialType): void {
    for (let i = 0; i < this.meshes.length; ++i) {
      const newMaterial = this.changeableMaterials[i].find(
        (m) => m.type === to
      );
      if (newMaterial) this.meshes[i].material = newMaterial.material;
    }
  }

  protected generateMaterial(color: string): ShaderMaterial {
    const shaderStuff =
      LinkerDNADoubleHelixBuilder.getMaterialParameters(color);
    return new ShaderMaterial({
      ...shaderStuff.param,
      uniforms: shaderStuff.uniforms,
    });
  }

  protected generateMaterialWithTexture(color: string): ShaderMaterial {
    const shaderStuff =
      LinkerDNADoubleHelixBuilder.getMaterialParameters(color);
    shaderStuff.uniforms = UniformsUtils.merge([
      shaderStuff.uniforms,
      {
        texture: { type: 't', value: this._texture },
      },
    ]);
    return new ShaderMaterial({
      ...shaderStuff.param,
      uniforms: shaderStuff.uniforms,
    });
  }

  protected generateGeometry(curvePoints: Vector3[]): TubeGeometry {
    return new TubeGeometry(
      this.generateCurveFromPoints(curvePoints),
      (curvePoints.length - 1) * this.qualityLevel.linkerDNASmoothing,
      DNA_THICKNESS,
      this.qualityLevel.nucleosomeDNARadiusSegments,
      false
    );
  }

  protected generateCurveFromPoints(curvePoints: Vector3[]): Curve<Vector3> {
    return new CatmullRomCurve3(curvePoints);
  }

  protected generateMeshes(color: string): void {
    let curveIndex = 0;
    const geo =
      this._startDegree !== 0
        ? this.qualityLevel.doubleHelixNucleosomeDNACounterGeometry
        : this.qualityLevel.doubleHelixNucleosomeDNAGeometry;

    const geo2 =
      this._startDegree !== 0
        ? this.qualityLevel.doubleHelixNucleosomeDNAGeometry
        : this.qualityLevel.doubleHelixNucleosomeDNACounterGeometry;

    const curvesNucIds = this._linkerDNACurveBuilder.getCurvesNucIDs();

    for (const curvePoints of this.curves) {
      const doubleHelixCurvePoints = new DoubleHelixCurve(
        curvePoints,
        true
      ).getCurvePoints(this._startDegree);

      if (curvesNucIds[curveIndex].start >= 0) {
        const spiralGeo = geo2.clone();
        spiralGeo.applyMatrix4(
          DoubleHelixNucleosomeDNABuilder.setRotation(
            this.beads[
              curvesNucIds[curveIndex].start
            ] as SoftwareNucleosomeBead,
            this.tmpScratchObject3D
          ).matrix
        );

        const vertices = spiralGeo.getAttribute('position');
        doubleHelixCurvePoints[0] = convertBufferAttrToVector3(
          vertices,
          vertices.count - 2
        );
        doubleHelixCurvePoints[1] = convertBufferAttrToVector3(
          vertices,
          vertices.count - 1
        );

        spiralGeo.dispose();
      }

      if (curvesNucIds[curveIndex].end >= 0) {
        const spiralGeo = geo.clone();
        spiralGeo.applyMatrix4(
          DoubleHelixNucleosomeDNABuilder.setRotation(
            this.beads[curvesNucIds[curveIndex].end] as SoftwareNucleosomeBead,
            this.tmpScratchObject3D
          ).matrix
        );

        const vertices = spiralGeo.getAttribute('position');
        doubleHelixCurvePoints[doubleHelixCurvePoints.length - 1] =
          convertBufferAttrToVector3(vertices, 1);
        doubleHelixCurvePoints[doubleHelixCurvePoints.length - 2] =
          convertBufferAttrToVector3(vertices, 0);
        spiralGeo.dispose();
      }

      this._dnaSpiralPoints.push(...doubleHelixCurvePoints);
      ++curveIndex;

      const geometry = this.generateGeometry(doubleHelixCurvePoints);
      const material = this.generateMaterial(color);
      const mesh = this.generateMesh(geometry, material);

      this.geometries.push(geometry);
      this.materials.push(material);
      this.meshes.push(mesh);

      this.changeableMaterials.push([
        { type: EMaterialType.NORMAL, material },
        {
          type: EMaterialType.WITH_TEXTURE,
          material: this.generateMaterialWithTexture(color),
        },
      ]);
    }
  }

  protected generateMesh(
    geometry: TubeGeometry,
    material: ShaderMaterial
  ): Mesh {
    const mesh = super.generateMesh(geometry, material);
    mesh.renderOrder = 0;
    mesh.name = MESH_NAME;
    return mesh;
  }
}
