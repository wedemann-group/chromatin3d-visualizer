/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Color, Mesh, Vector3 } from 'three';
import Bead from '../../../../shared/models/bead.model';
import {
  DEFAULT_COLOR_LINKER_DNA,
  DEFAULT_COLOR_SELECTION,
} from '../../../../shared/color.constants';
import {
  aOptionalRGBToHex,
  colorToArgb,
  rgbToHex,
} from '../../../../shared/utilities/color.utility';
import LinkerDNABuilder from './linker-dna.builder';
import { INucleosomeDNAData } from '../../../../core/services/session-mananger/models/nucleosome-dna-data.interface';
import {
  MATERIAL_ATTRIBUTE_NAME_SELECTED,
  NUCLEOSOME_DNA_THICKNESS,
  NUCLEOSOME_DNA_THICKNESS_REDUCER,
} from './models.variables';
import { LineGeometry } from 'three/examples/jsm/lines/LineGeometry';
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial';
import { Line2 } from 'three/examples/jsm/lines/Line2';
import { EPS_IS_ZERO } from '../../../../constants';

const MESH_NAME = 'Linker DNA Low-Mesh';

export default class LinkerDNALowBuilder extends LinkerDNABuilder<
  Bead,
  INucleosomeDNAData,
  LineGeometry,
  LineMaterial
> {
  public constructor(
    beads: Bead[],
    qualityLevel: INucleosomeDNAData,
    color: string,
    splicer: number[]
  ) {
    super(beads, qualityLevel, splicer);
    if (!color) color = DEFAULT_COLOR_LINKER_DNA;

    this.calculateCurves();
    this.generateMeshes(color);
  }

  public toggleSelection(meshSelection: Mesh): void {
    const selectedIndex = this.meshes.findIndex((m) => m === meshSelection);
    if (selectedIndex < 0) return;

    const isAlreadySelected =
      Math.abs(
        this.materials[selectedIndex].uniforms[MATERIAL_ATTRIBUTE_NAME_SELECTED]
          .value - 1.0
      ) <= EPS_IS_ZERO;

    this.materials[selectedIndex].uniforms[
      MATERIAL_ATTRIBUTE_NAME_SELECTED
    ].value = isAlreadySelected ? 0.0 : 1.0;
  }

  public clearSelection(): void {
    this.materials.forEach(
      (m) => (m.uniforms[MATERIAL_ATTRIBUTE_NAME_SELECTED].value = 0.0)
    );
  }

  public setSelectionColor(col: string): void {
    const color = new Color(
      aOptionalRGBToHex({ ...colorToArgb(col), a: undefined })
    );
    this.materials.forEach((m) => (m.uniforms.selectionColor.value = color));
  }

  protected generateMaterial(color: string): LineMaterial {
    const colorComponents = colorToArgb(color);
    const matLine = new LineMaterial({
      color: new Color(rgbToHex(colorComponents)).getHex(), // remove alpha because threejs doesn't support alpha
      opacity: colorComponents.a,
      visible: colorComponents.a > 0,
      transparent: true,
      vertexColors: false,
      linewidth:
        NUCLEOSOME_DNA_THICKNESS * 2 - NUCLEOSOME_DNA_THICKNESS_REDUCER,
    });
    matLine.worldUnits = true;
    matLine.vertexShader = matLine.vertexShader.replace(
      /(void main\(\) \{.*?\n)/i,
      `uniform float ${MATERIAL_ATTRIBUTE_NAME_SELECTED}; // ADDED
uniform vec3 selectionColor; // ADDED
varying float isSelected; // ADDED
varying vec3 selColor; // ADDED
$1
isSelected = ${MATERIAL_ATTRIBUTE_NAME_SELECTED};
selColor = selectionColor;
`
    );
    matLine.fragmentShader = matLine.fragmentShader.replace(
      /(void main\(\) \{.*?\n)/i,
      `varying float isSelected; // ADDED\n varying vec3 selColor; // ADDED\n$1`
    );
    matLine.fragmentShader = matLine.fragmentShader.replace(
      /gl_FragColor = ([^;]+?);/i,
      `gl_FragColor = abs(isSelected - 1.0) <= 0.001 ? vec4(selColor, alpha) : $1; // ADDED`
    );

    matLine.uniforms.selectionColor = {
      value: new Color(DEFAULT_COLOR_SELECTION),
    };
    matLine.uniforms[MATERIAL_ATTRIBUTE_NAME_SELECTED] = {
      value: 0.0,
    };
    return matLine;
  }

  protected generateGeometry(curvePoints: Vector3[]): LineGeometry {
    const positions: number[] = [];
    for (const pos of curvePoints) positions.push(pos.x, pos.y, pos.z);

    const line = new LineGeometry();
    line.setPositions(positions);

    return line;
  }

  protected generateMesh(
    geometry: LineGeometry,
    material: LineMaterial
  ): Line2 {
    const mesh = new Line2(geometry, material);
    mesh.computeLineDistances();
    mesh.name = MESH_NAME;
    return mesh;
  }
}
