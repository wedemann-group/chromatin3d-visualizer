/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  CylinderGeometry,
  InstancedMesh,
  ShaderMaterial,
  Texture,
} from 'three';
import InstancedBeadBuilder from './instanced-bead.builder';
import { DEFAULT_COLOR_COHESIN_HEAD } from '../../../../shared/color.constants';
import CohesinHeadBead from '../../../../shared/models/cohesin-head-bead.model';
import Bead from '../../../../shared/models/bead.model';
import DNABead from '../../../../shared/models/dna-bead.model';
import Vector from '../../../../shared/models/vector.model';
import { convertDegreeToRadians } from '../../../../shared/utilities/three.js.utility';
import { EMaterialType } from '../../../../shared/models/material-type.enum';
import { INucleosomeDNAData } from '../../../../core/services/session-mananger/models/nucleosome-dna-data.interface';
import { NUCLEOSOME_DNA_THICKNESS } from './models.variables';
import {
  BEAD_USER_NAME_COHESIN_LINKER,
  BEAD_USER_NAME_COHESIN_PREFIX,
} from '../../../../constants';

const MESH_NAME = 'Cohesin Linker-Mesh';

export default class CohesinLinkerBuilder extends InstancedBeadBuilder<
  CohesinHeadBead,
  INucleosomeDNAData,
  CylinderGeometry,
  ShaderMaterial
> {
  public readonly geometry: CylinderGeometry;
  public readonly material: ShaderMaterial;
  public readonly mesh: InstancedMesh<CylinderGeometry, ShaderMaterial>;

  protected readonly defaultColor = DEFAULT_COLOR_COHESIN_HEAD;

  private _unfilteredBeads: Bead[] = [];
  private _realBeadIndexMap: {
    realIndex: number;
    relativeIndex: number;
  }[] = [];

  public constructor(
    beads: Bead[],
    texture: Texture,
    qualityLevel: INucleosomeDNAData,
    renderSize = -1
  ) {
    super(
      beads.filter((b) => b instanceof CohesinHeadBead),
      texture,
      qualityLevel,
      renderSize
    );

    this.setBeadsAndIndices(beads);

    this.geometry = this.generateGeometry();
    this.material = this.generateShaderMaterial();
    this.mesh = this.generateMesh();

    this.changeableMaterials.push(
      { type: EMaterialType.NORMAL, material: this.material },
      {
        type: EMaterialType.WITH_TEXTURE,
        material: this.generateShaderMaterialWithTexture(),
      }
    );

    this.mesh.count = this.beads.length;
  }

  public reposition(beads: Bead[]): void {
    this.setBeadsAndIndices(beads);

    super.reposition(beads.filter((b) => b instanceof CohesinHeadBead));
  }

  public getSelectionTexts(): string[] {
    return this.getSelectionInfosFromBeads(
      this.beads,
      `${BEAD_USER_NAME_COHESIN_PREFIX} ${BEAD_USER_NAME_COHESIN_LINKER}`
    );
  }

  protected generateGeometry(): CylinderGeometry {
    const geometry = new CylinderGeometry(
      NUCLEOSOME_DNA_THICKNESS / 3,
      NUCLEOSOME_DNA_THICKNESS / 3,
      1,
      8,
      1
    );

    this.addAttributesToGeometry(geometry);
    return geometry;
  }

  protected generateMesh(): InstancedMesh<CylinderGeometry, ShaderMaterial> {
    const mesh = super.generateMesh();
    mesh.name = MESH_NAME;
    return mesh;
  }

  protected setSingleInstance(index: number): void {
    const head = this.beads[index];
    const linkerDNABead = this.getPreviousDNABead(index);
    if (!linkerDNABead) {
      console.error('Linker DNA not found!');
      return;
    }

    const segmentBetweenLinkerDNA = linkerDNABead.next.drawPosition
      .clone()
      .sub(linkerDNABead.previous.drawPosition);
    const segmentVec = new Vector(
      segmentBetweenLinkerDNA.x,
      segmentBetweenLinkerDNA.y,
      segmentBetweenLinkerDNA.z
    );
    segmentVec.div(2);

    const targetPos = linkerDNABead.previous.drawPosition
      .clone()
      .add(segmentVec);
    const startPos = head.drawPosition.clone();

    const diffBetweenStartAndTargetPos = targetPos.clone().sub(startPos);
    const diffVec = new Vector(
      diffBetweenStartAndTargetPos.x,
      diffBetweenStartAndTargetPos.y,
      diffBetweenStartAndTargetPos.z
    ).div(2);
    const diffMiddle = startPos.clone().add(diffVec);

    this.tmpScratchObject3D.clear();
    this.tmpScratchObject3D.position.set(
      diffMiddle.x,
      diffMiddle.y,
      diffMiddle.z
    );

    this.tmpScratchObject3D.scale.set(1, diffVec.length * 2, 1);
    this.tmpScratchObject3D.lookAt(targetPos.x, targetPos.y, targetPos.z);
    this.tmpScratchObject3D.rotateX(convertDegreeToRadians(90));
    this.tmpScratchObject3D.updateMatrix();
    this.mesh.setMatrixAt(index, this.tmpScratchObject3D.matrix);
  }

  private getPreviousDNABead(
    index: number
  ): { previous: DNABead; next: DNABead } | undefined {
    const headIndex = this._realBeadIndexMap.find(
      (realBeadIndex) => realBeadIndex.relativeIndex === index
    );
    if (!headIndex) {
      console.error('Head index is invalid!');
      return undefined;
    }

    // get the previous dna bead of the head
    let linkerDNAIndex = headIndex.realIndex;
    let previousLinkerDNABead: DNABead | undefined;
    while (--linkerDNAIndex >= 0) {
      const bead = this._unfilteredBeads[linkerDNAIndex];
      if (!(bead instanceof DNABead)) continue;

      previousLinkerDNABead = bead;
      break;
    }

    // get the next dna bead of the head
    linkerDNAIndex = headIndex.realIndex;
    let nextLinkerDNABead: DNABead | undefined;
    while (++linkerDNAIndex < this._unfilteredBeads.length) {
      const bead = this._unfilteredBeads[linkerDNAIndex];
      if (!(bead instanceof DNABead)) continue;

      nextLinkerDNABead = bead;
      break;
    }

    if (!previousLinkerDNABead || !nextLinkerDNABead) return undefined;

    return { previous: previousLinkerDNABead, next: nextLinkerDNABead };
  }

  private setBeadsAndIndices(beads: Bead[]): void {
    this._unfilteredBeads = beads;
    this._realBeadIndexMap = [];

    for (let i = 0; i < beads.length; ++i) {
      if (!(beads[i] instanceof CohesinHeadBead)) continue;

      this._realBeadIndexMap.push({
        realIndex: i,
        relativeIndex: this._realBeadIndexMap.length,
      });
    }
  }
}
