/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  InstancedMesh,
  ShaderMaterial,
  Texture,
  TorusGeometry,
  Vector3,
} from 'three';
import InstancedObjectBuilder from './instanced-object.builder';
import { DEFAULT_COLOR_COHESIN_HEAD } from '../../../../shared/color.constants';
import { CohesinRing } from '../../../../shared/models/cohesin-ring.model';
import { EMaterialType } from '../../../../shared/models/material-type.enum';
import { ICohesinHingeData } from '../../../../core/services/session-mananger/models/cohesin-hinge-data.interface';
import { COHESIN_HINGE_RADIUS, COHESIN_HINGE_TUBE } from './models.variables';
import {
  BEAD_USER_NAME_COHESIN_PREFIX,
  BEAD_USER_NAME_COHESIN_RING,
} from '../../../../constants';

const MESH_NAME = 'Cohesin Hinge-Mesh';

export default class CohesinHingeBuilder extends InstancedObjectBuilder<
  CohesinRing,
  ICohesinHingeData,
  TorusGeometry,
  ShaderMaterial
> {
  private _distance = 1;
  private _ringThickness = 1;

  public readonly geometry: TorusGeometry;
  public readonly material: ShaderMaterial;
  public readonly mesh: InstancedMesh<TorusGeometry, ShaderMaterial>;

  protected readonly defaultColor = DEFAULT_COLOR_COHESIN_HEAD;

  public constructor(
    rings: CohesinRing[],
    cohesinDistance: number,
    ringThickness: number,
    texture: Texture,
    qualityLevel: ICohesinHingeData
  ) {
    super(rings, texture, qualityLevel, rings.length);

    this._distance = cohesinDistance;
    this._ringThickness = ringThickness;

    this.geometry = this.generateGeometry();
    this.material = this.generateShaderMaterial();
    this.mesh = this.generateMesh();

    this.changeableMaterials.push(
      { type: EMaterialType.NORMAL, material: this.material },
      {
        type: EMaterialType.WITH_TEXTURE,
        material: this.generateShaderMaterialWithTexture(),
      }
    );
  }

  public getSelectionTexts(): string[] {
    return this.getSelectionInfoWithDynamicTextGetter(
      (i) =>
        `${BEAD_USER_NAME_COHESIN_PREFIX} Hinge for ${BEAD_USER_NAME_COHESIN_RING} ${i}`
    );
  }

  // eslint-disable-next-line
  public setSpecificVisibility(ids: number[], isVis: boolean): void {
    console.log('TO DO!');
  }

  protected generateGeometry(): TorusGeometry {
    const geometry = new TorusGeometry(
      COHESIN_HINGE_RADIUS,
      COHESIN_HINGE_TUBE,
      this.qualityLevel.cohesinHingeRadialSegments,
      this.qualityLevel.cohesinHingeTubularSegments
    );

    this.addAttributesToGeometry(geometry);
    return geometry;
  }

  protected generateMesh(): InstancedMesh<TorusGeometry, ShaderMaterial> {
    const mesh = super.generateMesh();
    mesh.name = MESH_NAME;
    return mesh;
  }

  protected setSingleInstance(index: number): void {
    const ring = this.beads[index];
    this.tmpScratchObject3D.clear();
    this.tmpScratchObject3D.position.set(
      ring.hingePosition.x,
      ring.hingePosition.y,
      ring.hingePosition.z
    );
    const hingeUpVector = new Vector3(
      ring.hingePosition.x + ring.orientationVector.x,
      ring.hingePosition.y + ring.orientationVector.y,
      ring.hingePosition.z + ring.orientationVector.z
    );
    this.tmpScratchObject3D.scale.set(
      1,
      1,
      this._ringThickness / COHESIN_HINGE_TUBE
    );

    this.tmpScratchObject3D.lookAt(hingeUpVector);
    // this.tmpScratchObject3D.translateZ(-this._ringThickness);

    this.tmpScratchObject3D.updateMatrix();
    this.mesh.setMatrixAt(index, this.tmpScratchObject3D.matrix);
  }
}
