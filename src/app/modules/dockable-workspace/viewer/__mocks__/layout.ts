/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/
import { LayoutConfig } from 'golden-layout';
import { TAB_JSON_NAME_FIBER_INFORMATION } from '../../tab-names.constants';

export const layout = {
  root: {
    type: 'stack',
    content: [
      {
        type: 'component',
        content: [],
        width: 55,
        minWidth: 0,
        height: 50,
        minHeight: 0,
        id: '',
        maximised: false,
        isClosable: true,
        reorderEnabled: true,
        title: 'Info',
        componentType: TAB_JSON_NAME_FIBER_INFORMATION,
        componentState: {},
      },
    ],
    width: 50,
    minWidth: 0,
    height: 50,
    minHeight: 0,
    id: '',
    isClosable: true,
    maximised: false,
    activeItemIndex: 0,
  },
  openPopouts: [],
  settings: {
    constrainDragToContainer: true,
    reorderEnabled: true,
    popoutWholeStack: false,
    blockedPopoutsThrowError: true,
    closePopoutsOnUnload: true,
    responsiveMode: 'none',
    tabOverlapAllowance: 0,
    reorderOnTabMenuClick: true,
    tabControlOffset: 10,
    popInOnClose: false,
  },
  dimensions: {
    borderWidth: 5,
    borderGrabWidth: 5,
    minItemHeight: 150,
    minItemWidth: 200,
    headerHeight: 30,
    dragProxyWidth: 300,
    dragProxyHeight: 200,
  },
  header: {
    show: 'top',
    popout: 'open in new window',
    dock: 'dock',
    close: 'close',
    maximise: 'maximise',
    minimise: 'minimise',
    tabDropdown: 'additional tabs',
  },
  resolved: true,
} as LayoutConfig;
