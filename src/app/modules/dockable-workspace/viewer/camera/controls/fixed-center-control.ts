/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import CameraControl from './camera-control';
import { TrackballControls } from 'three/examples/jsm/controls/TrackballControls';
import { PerspectiveCamera, Vector3 } from 'three';
import { START_POSITION_CAMERA_TRACKBALL_RELATIVE } from '../constants';
import { ECameraSetter } from '../../models/camera-setter.enum';
import { ViewService } from '../../../../../core/services/view/view.service';
import { ContainerDimensionExtended } from '../../../../../core/services/view/models/container-dimension-extended.model';

export default class FixedCenterControl extends CameraControl<TrackballControls> {
  private _controls: TrackballControls;

  public get controls(): TrackballControls {
    return this._controls;
  }

  public constructor(
    camera: PerspectiveCamera,
    canvasDom: HTMLCanvasElement,
    viewService: ViewService
  ) {
    super(camera, canvasDom, viewService);

    this._controls = this.createControls();

    this.resetTarget();
  }

  public setCameraAutomatic(
    setter: ECameraSetter,
    lastContainerSize: ContainerDimensionExtended | undefined
  ): void {
    if (setter === ECameraSetter.none) {
      this.resetTarget();
      return;
    }

    if (setter === ECameraSetter.toCenter) {
      const messOfCenter = this.setCameraToCenter();

      this._controls.target.set(messOfCenter.x, messOfCenter.y, messOfCenter.z);
      this._controls.update();

      this.camera.lookAt(messOfCenter.x, messOfCenter.y, messOfCenter.z);
    } else if (setter === ECameraSetter.keepDiff) {
      if (!lastContainerSize) return;

      this.keepCameraDistanceToCenterOfMass(lastContainerSize);
    }
  }

  public resetTarget(): void {
    const info = this.viewService.getContainerSizeOfPBCOrFiberWithCenterPoint();
    this._controls.target.set(info.centerX, info.centerY, info.centerZ);
    this._controls.update();
  }

  public reset(): void {
    this.resetTarget();
    const info = this.viewService.getContainerSizeOfPBCOrFiberWithCenterPoint();
    const cameraPos = new Vector3(info.centerX, info.centerY, info.centerZ).sub(
      START_POSITION_CAMERA_TRACKBALL_RELATIVE
    );
    this.camera.position.set(cameraPos.x, cameraPos.y, cameraPos.z);
    this.camera.lookAt(info.centerX, info.centerY, info.centerZ);
  }

  private createControls(): TrackballControls {
    this.dispose();
    this._controls = new TrackballControls(this.camera, this.canvasDom);

    this._controls.rotateSpeed = 2.0;
    this._controls.zoomSpeed = 1.0;
    this._controls.panSpeed = 0.8;

    return this._controls;
  }
}
