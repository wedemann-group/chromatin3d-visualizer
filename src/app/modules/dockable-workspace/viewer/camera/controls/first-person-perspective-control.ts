/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import CameraControl from './camera-control';
import { OrbitControlsWithoutZoom } from '../OrbitControlsWithoutZoom';
import { MOUSE, PerspectiveCamera, Vector3 } from 'three';
import {
  START_ORBIT_CONTROLS_TARGET,
  START_POSITION_CAMERA,
} from '../constants';
import { ECameraSetter } from '../../models/camera-setter.enum';
import { ViewService } from '../../../../../core/services/view/view.service';
import { ContainerDimensionExtended } from '../../../../../core/services/view/models/container-dimension-extended.model';

export default class FirstPersonPerspectiveControl extends CameraControl<OrbitControlsWithoutZoom> {
  private _controls: OrbitControlsWithoutZoom;
  private _lastTarget = START_ORBIT_CONTROLS_TARGET.clone();

  public get controls(): OrbitControlsWithoutZoom {
    return this._controls;
  }

  public constructor(
    camera: PerspectiveCamera,
    canvasDom: HTMLCanvasElement,
    viewService: ViewService,
    lastTarget: Vector3 | undefined = undefined
  ) {
    super(camera, canvasDom, viewService);

    this._controls = this.createControls();

    this.resetTarget();

    if (lastTarget) this._lastTarget = lastTarget;
  }

  private static getTargetInOrbitFriendlyDistance(
    cameraPos: Vector3,
    targetDirection: Vector3
  ): Vector3 {
    const defaultOrbitDiff = START_POSITION_CAMERA.clone().sub(
      START_ORBIT_CONTROLS_TARGET
    );
    const oldTargetDiff = cameraPos.clone().sub(targetDirection);
    oldTargetDiff.normalize();
    oldTargetDiff.multiplyScalar(defaultOrbitDiff.length());

    return cameraPos.clone().sub(oldTargetDiff);
  }

  public setCameraAutomatic(
    setter: ECameraSetter,
    lastContainerSize: ContainerDimensionExtended | undefined
  ): void {
    if (setter === ECameraSetter.none) return;

    if (setter === ECameraSetter.toCenter) {
      const messOfCenter = this.setCameraToCenter();
      const newTarget =
        FirstPersonPerspectiveControl.getTargetInOrbitFriendlyDistance(
          this.camera.position,
          messOfCenter
        );
      this.camera.lookAt(messOfCenter.x, messOfCenter.y, messOfCenter.z);
      this._controls = this.createControls();
      this._controls.target.set(newTarget.x, newTarget.y, newTarget.z);
      this._controls.update();
    } else if (setter === ECameraSetter.keepDiff) {
      if (!lastContainerSize) return;

      this.keepCameraDistanceToCenterOfMass(lastContainerSize);
    }
  }

  public restoreTarget(target: Vector3): void {
    this._controls = this.createControls();
    this._controls.target.copy(target);
    this._controls.update();
  }

  public resetTarget(): void {
    const newTarget =
      FirstPersonPerspectiveControl.getTargetInOrbitFriendlyDistance(
        this.camera.position,
        this._lastTarget
      );
    this._controls.target.set(newTarget.x, newTarget.y, newTarget.z);
    this._controls.update();
  }

  public reset(): void {
    this._lastTarget = START_ORBIT_CONTROLS_TARGET;
    // up vector destroys the controls, so re-create controls... (description: the private function "update" makes a copy of the up vector and the public update function uses this copy. it is not possible to change the private cached "up" vector...)
    this.camera.position.set(
      START_POSITION_CAMERA.x,
      START_POSITION_CAMERA.y,
      START_POSITION_CAMERA.z
    );

    this._controls = this.createControls();
    this.resetTarget();
  }

  private createControls(): OrbitControlsWithoutZoom {
    this.dispose();
    this._controls = new OrbitControlsWithoutZoom(
      this.camera,
      this.canvasDom,
      START_ORBIT_CONTROLS_TARGET.clone()
    );
    this._controls.enableZoom = true;
    this._controls.maxAzimuthAngle = 180;
    this._controls.maxPolarAngle = 180;
    this._controls.minAzimuthAngle = -180;
    this._controls.minPolarAngle = -180;
    this._controls.mouseButtons = {
      LEFT: MOUSE.ROTATE,
      MIDDLE: MOUSE.PAN,
      RIGHT: MOUSE.PAN,
    };

    return this._controls;
  }
}
