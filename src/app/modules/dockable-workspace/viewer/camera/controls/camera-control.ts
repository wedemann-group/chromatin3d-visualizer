/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { PerspectiveCamera, Vector3 } from 'three';
import { TrackballControls } from 'three/examples/jsm/controls/TrackballControls';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { ECameraSetter } from '../../models/camera-setter.enum';
import {
  convertDegreeToRadians,
  convertFOVTohFov,
} from '../../../../../shared/utilities/three.js.utility';
import { ViewService } from '../../../../../core/services/view/view.service';
import { ContainerDimension } from '../../../../../core/services/view/models/container-dimension.model';
import { ContainerDimensionExtended } from '../../../../../core/services/view/models/container-dimension-extended.model';

const TRIANGLE_SUM_OF_ANGLES = 180;

export default abstract class CameraControl<
  TControls extends TrackballControls | OrbitControls
> {
  // trackball controls/orbit controls have no base class...
  protected readonly camera: PerspectiveCamera;
  protected readonly canvasDom: HTMLCanvasElement;
  protected readonly viewService: ViewService;

  public abstract get controls(): TControls;

  protected constructor(
    camera: PerspectiveCamera,
    canvasDom: HTMLCanvasElement,
    viewService: ViewService
  ) {
    this.camera = camera;
    this.canvasDom = canvasDom;
    this.viewService = viewService;
  }

  public restoreTarget(target: Vector3): void {
    this.controls.target.copy(target);
    this.controls.update();
  }

  public dispose(): void {
    this.controls?.dispose();
  }

  protected setCameraToCenter(): Vector3 {
    if (!this.camera) return new Vector3(0, 0, 0);

    const hFOV = convertFOVTohFov(this.camera.fov, this.camera.aspect);
    const triangleAngleDeg = (TRIANGLE_SUM_OF_ANGLES - hFOV) / 2;
    const triangleAngle = convertDegreeToRadians(triangleAngleDeg);

    const info = this.viewService.getContainerSizeOfPBCOrFiberWithCenterPoint();

    this.camera.rotation.set(0, 0, 0);
    this.camera.up.set(0, 1, 0);

    if (info.width > info.depth) {
      const hypotenuseOfHalfTriangle = Math.sin(triangleAngle) * info.width; // xWidth = hypotenuse ; hypotenuseOfHalfTriangle = opposite of the big triangle
      const oppositeOfHalfTriangle =
        Math.sin(triangleAngle) * hypotenuseOfHalfTriangle;

      this.camera.position.set(
        info.centerX,
        info.centerY,
        info.centerZ - info.depth / 2 - oppositeOfHalfTriangle
      );
    } else {
      //  const hypotenuse = ; // opposite hypotenuse
      const hypotenuseOfHalfTriangle = Math.sin(triangleAngle) * info.depth; // xWidth = hypotenuse ; hypotenuseOfHalfTriangle = opposite of the big triangle
      const oppositeOfHalfTriangle =
        Math.sin(triangleAngle) * hypotenuseOfHalfTriangle;

      this.camera.position.set(
        info.centerX - info.width / 2 - oppositeOfHalfTriangle,
        info.centerY,
        info.centerZ
      );
    }

    return new Vector3(info.centerX, info.centerY, info.centerZ);
  }

  protected keepCameraDistanceToCenterOfMass(
    lastContainerSize: ContainerDimensionExtended
  ): void {
    const oldMessOfCenter = new Vector3(
      lastContainerSize.centerX,
      lastContainerSize.centerY,
      lastContainerSize.centerZ
    );
    const info = this.viewService.getContainerSizeOfPBCOrFiberWithCenterPoint();
    const newMessOfCenter = new Vector3(
      info.centerX,
      info.centerY,
      info.centerZ
    );
    const messOfCenterDiff = oldMessOfCenter.clone().sub(newMessOfCenter);

    this.camera.position.sub(messOfCenterDiff);
    this.controls.target.sub(messOfCenterDiff);
    this.controls.update();
  }

  public abstract setCameraAutomatic(
    setter: ECameraSetter,
    lastCenterOfMass: ContainerDimension | undefined
  ): void;
  public abstract resetTarget(): void;
  public abstract reset(): void;
}
