/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Vector3 } from 'three';

export const START_ORBIT_CONTROLS_TARGET = new Vector3(2, 2, 2);
export const START_POSITION_CAMERA = new Vector3(0, 0, 5);
export const START_POSITION_CAMERA_TRACKBALL_RELATIVE = new Vector3(5, 5, 5);

export const CAMERA_POV = 75;
