/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { SerializableCamera } from '../models/serializable-camera.model';
import { SerializedCamera } from '../models/serialized-camera.model';
import { DeserializedCamera } from '../models/deserialized-camera.model';
import {
  deserializeVector3,
  serializeVector3,
} from '../../../../shared/utilities/three.js.utility';
import { Euler } from 'three';
import { EControlType } from '../models/control-type.enum';
import { ECameraSetter } from '../models/camera-setter.enum';
import { getValueOfEnumKey } from '../../../../shared/utilities/enum.utility';

export function serializeCamera({
  position,
  rotation,
  upVector,
  targetPosition,
  controlType,
  setter,
}: SerializableCamera): SerializedCamera {
  return {
    position: serializeVector3(position),
    upVector: serializeVector3(upVector),
    rotation: rotation.toArray(),
    targetPosition: serializeVector3(targetPosition),
    controlType,
    setter,
  } as SerializedCamera;
}

export function deserializeCameraFromString(
  serializedCamera: string
): DeserializedCamera {
  const cameraStr = JSON.parse(serializedCamera) as SerializedCamera;
  return deserializeCamera(cameraStr);
}

export function deserializeCamera(
  serializedCamera: SerializedCamera
): DeserializedCamera {
  return {
    position: deserializeVector3(serializedCamera.position),
    upVector: deserializeVector3(serializedCamera.upVector),
    rotation: new Euler().fromArray(serializedCamera.rotation),
    targetPosition: deserializeVector3(serializedCamera.targetPosition),
    controlType:
      getValueOfEnumKey<EControlType>(
        EControlType,
        serializedCamera.controlType
      ) ?? EControlType.firstPersonPerspective,
    setter:
      getValueOfEnumKey<ECameraSetter>(
        ECameraSetter,
        serializedCamera.setter
      ) ?? ECameraSetter.none,
  } as DeserializedCamera;
}
