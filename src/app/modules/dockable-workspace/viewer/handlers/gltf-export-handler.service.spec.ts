/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { TestBed } from '@angular/core/testing';

import { GLTFExportHandlerService } from './gltf-export-handler.service';
import {
  MATERIAL_IMPORTS,
  SCENE_PROVIDERS,
} from '../../../../core/services/__mocks__/imports';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { WorkspaceService as MockWorkspaceService } from '../../../../core/services/__mocks__/workspace.service';
import { SceneModule } from '../scene.module';
import { ProgressStateHandlerService } from '../../../../core/services/progress-state-handler.service';

describe('GltfExportHandlerService', () => {
  let service: GLTFExportHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [...MATERIAL_IMPORTS, SceneModule],
      providers: [
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
        SCENE_PROVIDERS,
        ProgressStateHandlerService,
      ],
    });
    service = TestBed.inject(GLTFExportHandlerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
