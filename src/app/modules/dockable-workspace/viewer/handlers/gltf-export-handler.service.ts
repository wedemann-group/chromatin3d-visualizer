/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable, OnDestroy } from '@angular/core';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { SubscribeEvents } from '../../../../core/classes/subscribe-events';
import { GLTFExportService } from '../../../../core/services/gltf-export.service';
import { SceneHelper } from '../helpers/scene.helper';
import { MenuStripService } from '../../../../core/services/menu-strip.service';
import { SceneModule } from '../scene.module';
import { ProgressStateHandlerService } from '../../../../core/services/progress-state-handler.service';
import { SettingsService } from '../../../../core/services/settings/settings.service';

const EXPORT_NO_SCENE_ERROR_MESSAGE = 'Found no scene to export!';
const DEFAULT_FILE_NAME = 'model';

@Injectable({
  providedIn: SceneModule,
})
export class GLTFExportHandlerService
  extends SubscribeEvents
  implements OnDestroy
{
  public constructor(
    public readonly workspaceService: WorkspaceService,
    private readonly _sceneHelper: SceneHelper,
    private readonly _exportModelService: GLTFExportService,
    private readonly _menuStripService: MenuStripService,
    private readonly _progStateService: ProgressStateHandlerService,
    private readonly _settingsService: SettingsService
  ) {
    super();
    this._menuStripService.startGLTFExport$.subscribe(
      this.exportScene.bind(this)
    );
  }

  public ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  /**
   * Exports the model displayed in the viewer.
   */
  private async exportScene(): Promise<void> {
    const scene = this._sceneHelper.getScene();

    if (!scene) {
      this._progStateService.setErrorMessage(EXPORT_NO_SCENE_ERROR_MESSAGE);
      return;
    }

    this._exportModelService.exportScene(
      scene,
      `${this.workspaceService.currentFile?.name ?? DEFAULT_FILE_NAME}`,
      { onlyVisible: !this._settingsService.exportInvisibleObjectsGLTF.value }, // since the setting is to export the items but the gltf option is not to export them => invert
      this._sceneHelper.rendererInfo
    );
  }
}
