# stages: build, upload, pre-deploy, deploy

variables:
  STABLE_GENERIC_NAME: "stable-version"
  STABLE_PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${STABLE_GENERIC_NAME}/${CI_COMMIT_TAG}/"
  STABLE_ARCHIVE_NAME: "chromatin-3d-visualizer_${CI_COMMIT_TAG}"
  STABLE_ASSET_NAME: "Compiled Release ${CI_COMMIT_TAG}"
  RELEASE_DESCRIPTION_FILE: "./release_description.md"

build_release:
  stage: build
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - yarn install
  script:
    - CI=false yarn build --configuration=production --base-href ./ --deploy-url ./
    # rename build directory (because visible to the user)
    - mv dist/trj-viewer-angular ${STABLE_ARCHIVE_NAME}
    - zip -r ${STABLE_ARCHIVE_NAME}.zip ${STABLE_ARCHIVE_NAME}
    # or without a "root" directory inside the zip
    # - cd ${STABLE_ARCHIVE_NAME}/
    # - zip -r ../${STABLE_ARCHIVE_NAME}.zip $(ls .)/ # reminder: you must be in the directory. `ls` outside doesn't work (zip will keep directory name)
  artifacts:
    paths:
      - ${STABLE_ARCHIVE_NAME}.zip
    expire_in: 1 day

# upload the created zip archive to the "generic packages" on gitlab
upload_release:
  stage: upload
  image: curlimages/curl:7.74.0
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ${STABLE_ARCHIVE_NAME}.zip ${STABLE_PACKAGE_REGISTRY_URL}
  dependencies:
    - build_release

prepare_release:
  stage: pre-deploy
  image: registry.gitlab.com/luigi600/node-alpine-jq-zip-curl:1.0.0
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - URL=$(jq -rn --arg x "changelogs/${CI_COMMIT_TAG}" '$x|@uri')
    - |
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/wikis/${URL}"  \
      | \
      jq -r '.content' > ${RELEASE_DESCRIPTION_FILE}
  artifacts:
    paths:
      - ${RELEASE_DESCRIPTION_FILE}
    expire_in: 1 day

# create a "release" on gitlab with assets of "upload" job
release:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/release-cli:v0.11.0
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "create release"
  release:
    name: "Release ${CI_COMMIT_TAG}"
    description: "${RELEASE_DESCRIPTION_FILE}"
    tag_name: "$CI_COMMIT_TAG"
    ref: "$CI_COMMIT_TAG"
    assets:
      links:
        - name: "${STABLE_ASSET_NAME}"
          url: "${STABLE_PACKAGE_REGISTRY_URL}${STABLE_ARCHIVE_NAME}.zip"
  needs:
    - job: prepare_release
      artifacts: true

docker-version-tag:
  stage: deploy
  image: ${IMAGE_DOCKER}
  rules:
    - if: $CI_COMMIT_TAG
  services:
    - docker:dind
  before_script:
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
  script:
    # build image
    - docker build --pull -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}" .
    # push
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
