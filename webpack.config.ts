import { Configuration, DefinePlugin, SourceMapDevToolPlugin } from 'webpack';
/*
import {
  CustomWebpackBrowserSchema,
  TargetOptions,
} from '@angular-builders/custom-webpack';
 */

const buildDate = new Date();
const buildDateStr = (function () {
  const dashNumbers = [
    buildDate.getFullYear(),
    buildDate.getMonth() + 1, // starts by 0...
    buildDate.getDate(),
  ].map((n) => n.toString().padStart(2, '0'));
  const doublePointNumbers = [buildDate.getHours(), buildDate.getMinutes()].map(
    (n) => n.toString().padStart(2, '0')
  );

  return `${dashNumbers.join('')}${doublePointNumbers.join('')}`;
})();

const jsonPackage: { version: string; license: string } =
  // @ts-ignore
  require('./package.json');

const commitSHA =
  // @ts-ignore
  process.env.CI_COMMIT_SHORT_SHA ?? process.env.COMMIT_SHA ?? '00000000';

export default (
  config: Configuration
  // options: CustomWebpackBrowserSchema,
  // targetOptions: TargetOptions
) => {
  if (!config.module) config.module = { rules: [] };
  if (!config.module.rules) config.module.rules = [];

  if (!config.plugins) config.plugins = [];

  // source: https://github.com/NOWHEREIS/angular-pug/blob/master/webpack.config.js
  // if you start tests, you have to add config for plugin Source Map Dev Tool
  const mainFile =
    // @ts-ignore
    config.entry && config.entry['main'] ? config.entry['main'] : '';
  if (mainFile.includes('test')) {
    const indexForSourceMapDevToolPlugin = config.plugins.findIndex(
      (plugin) => {
        return plugin instanceof SourceMapDevToolPlugin;
      }
    );

    // config to add source map on tests
    const configPluginSourceMapDevToolPlugin = {
      filename: '[file].map',
      include: [/js$/, /css$/],
      sourceRoot: 'webpack:///',
      moduleFilenameTemplate: '[resource-path]',
    };

    if (indexForSourceMapDevToolPlugin > -1) {
      config.plugins.splice(indexForSourceMapDevToolPlugin);
    } else {
      config.plugins.push(
        new SourceMapDevToolPlugin(configPluginSourceMapDevToolPlugin)
      );
    }
  }

  config.module.rules.push({
    test: /\.js$/,
    // @ts-ignore
    loader: require.resolve('@open-wc/webpack-import-meta-loader'),
  });

  config.plugins.push(
    new DefinePlugin({
      PACKAGE_VERSION: JSON.stringify(jsonPackage.version),
      BUILD_DATE: JSON.stringify(buildDateStr),
      BUILD_DATE_JS: JSON.stringify(buildDate.toISOString()),
      LICENSE: JSON.stringify(jsonPackage.license),
      COMMIT: JSON.stringify(commitSHA),
    })
  );

  return config;
};
